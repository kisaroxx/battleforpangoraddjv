echo =================================================================
echo On pull git
echo =================================================================


echo %PROJECT_DIR%
echo %CI_JOB_TOKEN%
echo %CI_REPOSITORY_URL%
echo %CI_COMMIT_REF_NAME%

echo Fetching changes...
cd %PROJECT_DIR%
echo %cd%
call git fetch %CI_REPOSITORY_URL% +refs/heads/*:refs/remotes/origin/* 

call git lfs install  --skip-smudge --local
call git lfs fetch origin %CI_COMMIT_REF_NAME%
call git lfs checkout

echo Extracting informations from local repo...
for /f "Tokens=*" %%I in ('git rev-parse origin/%CI_COMMIT_REF_NAME%') do set ORIGIN_REV=%%I
for /f "Tokens=*" %%I in ('git rev-parse %CI_COMMIT_REF_NAME%') do set LOCAL_REV=%%I
for /f "Tokens=*" %%I in ('git rev-parse --abbrev-ref HEAD') do set CURRENT_BRANCH=%%I

if NOT %CURRENT_BRANCH% == %CI_COMMIT_REF_NAME% (
    echo Invalid branch
    SET ERRORLEVEL=1
    goto End
)

if %ORIGIN_REV% == %LOCAL_REV% (
    echo No changes found
    goto End
)

echo Listing changes...
:: On affiche les commits différents
call git --no-pager log --oneline --no-color %ORIGIN_REV%...%LOCAL_REV%

echo Rebasing local repository to match remote...
:: On récupère les changements distants
call git rebase --autostash origin/%CI_COMMIT_REF_NAME%

:End
::popd