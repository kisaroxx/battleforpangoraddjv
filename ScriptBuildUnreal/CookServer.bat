echo =================================================================
echo On cook le projet (Server) ...
echo =================================================================

call %SCRIPT_BUILD_UE% BuildCookRun -project=%PROJECT_FILE%^
 -noP4 -platform=Win64 -build -clientconfig=Development -serverconfig=Development^
 -cook -stage -pak -server -serverplatform=Win64 -noclient^
 -archive -archivedirectory=%DEST_DIR_BUILD%\Server

if %ERRORLEVEL% NEQ 0 (
    goto End
)

:End