
#include "Util.h"
#include "Engine/World.h"
#include "ConstructorHelpers.h"
#include "TimerManager.h"
#include "Logger.h"
#include <random>

float Util::AngleBetweenVectors(const FVector & vector1, const FVector & vector2) {
    FVector v1 = vector1, v2 = vector2;
    v1.Normalize();
    v2.Normalize();

    float dot = FVector::DotProduct(v1, v2);
    float c = acosf(dot);
    FVector normal = FVector::CrossProduct(v1, v2); normal.Normalize();
    // ATTENTION CETTE METHODE PEUT NE PAS MARCHER DANS CERTAINS CAS QUI NE SONT PAS ASSEZ HORIZONTAUX !!!
    return (normal.Z >= 0) ? c * 180.0f / PI : (2 * PI - c) * 180.0f / PI;
}

float Util::AngleBetweenVectorsInXYPlane(const FVector & vector1, const FVector & vector2) {
    FVector v1 = vector1, v2 = vector2;
    v1 = ProjectInXYPlane(v1);
    v2 = ProjectInXYPlane(v2);
    return AngleBetweenVectors(v1, v2);
}

float Util::AngleBetweenVectors2D(const FVector2D & v1, const FVector2D & v2)
{
	 FVector vector1 =  FVector(v1.X, v1.Y, 0);
	 FVector vector2 =  FVector(v2.X, v2.Y, 0);

	 float angle = AngleBetweenVectorsInXYPlane(vector1,vector2);
	return angle;
}

FVector Util::ProjectInXYPlane(const FVector& v1) {
    return FVector::PointPlaneProject(v1, FVector::ZeroVector, FVector::UpVector);
}

class RandomGenerateur {
    std::random_device rd;
    std::mt19937 prng;

    RandomGenerateur()
        : prng{ rd() }
    {}

public:
    RandomGenerateur(const RandomGenerateur&) = delete;
    RandomGenerateur& operator=(const RandomGenerateur&) = delete;

    static RandomGenerateur& get() {
        static RandomGenerateur singleton;
        return singleton;
    }

    int random(int minInclu, int maxInclu) {
        std::uniform_int_distribution<int> d{ minInclu, maxInclu };
        return d(prng);
    }
    float random(float min, float max) {
        std::uniform_real_distribution<float> d{ min, max };
        return d(prng);
    }
};

int Util::Random(int minInclu, int maxInclu) {
    return RandomGenerateur::get().random(minInclu, maxInclu);
}
float Util::Random(float min, float max) {
    return RandomGenerateur::get().random(min, max);
}

std::pair<int, int> Util::GetSecAndMilli(float dureeEnSecondes) {
    int s = dureeEnSecondes;
    int m = (dureeEnSecondes - s) * 1000.0f;
    return std::make_pair(s, m);
}

//void Util::SpawnDebugActor(UWorld* world, FVector place, float disparitionTime) {
//    //UClass * c = LoadClass<AActor>(nullptr, TEXT("/Game/Geometry/Meshes/DebugObject.DebugObject");
//    ConstructorHelpers::FClassFinder<APawn> debugObject(TEXT("/Game/Geometry/Meshes/DebugObject"));
//    if (debugObject.Succeeded()) {
//        TSubclassOf<AActor> c = debugObject.Class;
//        AActor* actor = world->SpawnActor<AActor>(c, FTransform(place));
//        if (actor == nullptr) {
//            DEBUG_SCREEN(-1, 5.f, FColor::Black, TEXT("L'acteur est nul !"));
//        }
//
//        if (disparitionTime != -1) {
//            FTimerHandle handle;
//            FTimerDelegate del;
//            del.BindUFunction(actor, FName("Destroy"));
//            world->GetTimerManager().SetTimer(handle, del, disparitionTime, false);
//        }
//    }
//}

FVector Util::PolaireToCartesian(float angle, float rayon) {
    FVector v;
    v.X = cos(ToRad(angle)) * rayon;
    v.Y = sin(ToRad(angle)) * rayon;
    v.Z = 0.0f;
    return v;
}

float Util::ToRad(float angleDegree) {
    return angleDegree / 180.0f * PI;
}
float Util::ToDegree(float angleRadian) {
    return angleRadian * 180.0f / PI;
}

