// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CommandReturn.h"
#include "Command.generated.h"

class ASquadMemberController;

/**
*
*/
UCLASS(abstract)
class BATTLE_FOR_PANDORA_API UCommand : public UObject
{
    GENERATED_BODY()

public:
    UCommand();
    ~UCommand();

    UFUNCTION(BlueprintCallable, Category = "Command")
        virtual UCommandReturn* execute();

    UFUNCTION(BlueprintCallable, Category = "Command")
        void setCharacter(ASquadMemberController* chara);

protected:
    UPROPERTY(EditAnywhere, Category = "Command")
        ASquadMemberController* character;
};
