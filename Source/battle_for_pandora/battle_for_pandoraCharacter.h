// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

//#include "Components/TimelineComponent.h"
#include "battle_for_pandoraCharacter.generated.h"

/// Les enums sont comment�s car elles sont mainteannt dans BFPCharacter !
//// Les diff�rents modes de combats possibles.
//// Note : Le mouvement est ind�pendant de l'�tat de combat du personnage !
//UENUM(BlueprintType)
//enum class CombatState : uint8
//{
//    IDLE,       // Immobile, ou en train de se d�placer. 
//    ATTACK,     // En train d'attaquer
//    HIT,        // En train de recevoir un coup
//    DODGING,    // Roulade
//    BLOCKING    // En train de parer
//};
//
//// Les diff�rentes parties d'une attaque possible.
//UENUM(BlueprintType)
//enum class AttackState : uint8
//{
//    ARMING,     // La phase d'attente avant le d�clanchement d'une attaque
//    ACTIVATING, // La phase o� l'attaque fait des d�gats
//    DISARMING   // La phase d'attente apr�s la fin de l'activation de l'attaque
//};

UCLASS(config=Game)

class Abattle_for_pandoraCharacter : public ACharacter
{
	GENERATED_BODY()
//
//	/** Camera boom positioning the camera behind the character */
//	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
//	class USpringArmComponent* CameraBoom;
//
//	/** Follow camera */
//	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
//	class UCameraComponent* FollowCamera;
//
//   /** La vitesse maximale de course dans le 3Pchar*/
//   UPROPERTY(Category = "Character Movement: Walking", EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "0", UIMin = "0", AllowPrivateAccess = "true"))
//      float RunSpeed = 1200;
//
//   /** La vitesse maximale de marche dans le 3Pchar*/
//   UPROPERTY(Category = "Character Movement: Walking", EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "0", UIMin = "0", AllowPrivateAccess = "true"))
//      float WalkSpeed = 600;
//
//
//public:
//	Abattle_for_pandoraCharacter();
//
//	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
//	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
//	float BaseTurnRate;
//
//	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
//	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
//	float BaseLookUpRate;
//
//protected:
//
//	/** Resets HMD orientation in VR. */
//	void OnResetVR();
//
//   void OnStartAttack();
//   void OnEndAttack();
//
//	/** Called for forwards/backward input */
//	void MoveForward(float Value);
//
//	/** Called for side to side input */
//	void MoveRight(float Value);
//
//   /**
//*Fait courrir le joueur
//*/
//   UFUNCTION(BlueprintCallable, Category = Character)
//      virtual void Run();
//   /**
//   *Fait retourner le joueur en mode marche
//   */
//   UFUNCTION(BlueprintCallable, Category = Character)
//      virtual void StopRunning();
//
//	/** 
//	 * Called via input to turn at a given rate. 
//	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
//	 */
//	void TurnAtRate(float Rate);
//
//	/**
//	 * Called via input to turn look up/down at a given rate. 
//	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
//	 */
//	void LookUpAtRate(float Rate);
//
//	/** Handler for when a touch input begins. */
//	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);
//
//	/** Handler for when a touch input stops. */
//	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);
//
//
//    /******************************************************************
//    COMBAT SYSTEM
//    ******************************************************************/
//protected:
//    ///* L'�tat de combat actuel du personage. */
    //UPROPERTY(VisibleAnywhere, Category = Combat)
    //CombatState combatState = CombatState::IDLE; // On commence immobile.

    ///* L'�tat de l'attaque actuelle du personnage.
    // * Ceci n'a de sens que si le personnage est en train d'attaquer ! */
    //UPROPERTY(VisibleAnywhere, Category = Combat)
    //AttackState attackState;

    ///* L'attaque component contenant l'attaque de base. */
    //UPROPERTY(EditAnywhere, Category = Combat)
    //TSubclassOf<class UAttaqueComponent> attaqueSimpleComponent;

public:

    ///* Declanche la phase d'armement d'une attaque. */
    //UFUNCTION()
    //void SimpleAttackStart();

    ///* Declanche la phase d'activation d'une attaque. */
    //UFUNCTION()
    //void SimpleAttackActivate();

    ///* Declanche la phase de d�sarmement d'une attaque. */
    //UFUNCTION()
    //void SimpleAttackEnd();

    ///* Permet de changer l'�tat de combat du joueur pour qu'il passe en IDLE. */
    //UFUNCTION()
    //void SetCombatStateIdle();

   //void OnStartHeavyAttackTest();
   //void OnEndHeavyAttackTest();

	//virtual void BeginPlay();
	//virtual void Tick(float DeltaTime) override;

	
	


public:
	///** Returns CameraBoom subobject **/
	//FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	///** Returns FollowCamera subobject **/
	//FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

 //  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Fight)
 //  bool Attacking;

 //  UFUNCTION(BluePrintCallable)
 //  bool IsAttacking() { return Attacking; }

 /*UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Fight)
 bool HeavyAttacking;

 UFUNCTION(BluePrintCallable)
 bool IsHeavyAttacking() { return HeavyAttacking; }*/
};



  

