// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BFPCharacter.h"
#include "BFPCharacterPlayable.h"
#include "Spell.h"
#include "Engine/TriggerCapsule.h"
#include "Engine/StaticMeshActor.h"
#include "NeelfBoostSpell.generated.h"

UCLASS()
class BATTLE_FOR_PANDORA_API ANeelfBoostSpell : public ASpell
{
    GENERATED_BODY()

    /* Le multiplicateur de degats. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float degatMultiplicateur = 2.0f;

    /* La duree du boost. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float dureeBoost = 10.0f;

    /* La portee du sort pour booster les allies egalement. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float porteeBoost = 1500.0f;

    /* L'aura a ajouter � tous les joueurs ayant ete touchees par le boost. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    TSubclassOf<ASpell> auraClass;

    TArray<ABFPCharacterPlayable*> herosSaved;
    TArray<ASpell*> aurasSaved;

protected:
   ANeelfBoostSpell(const FObjectInitializer & ObjectInitializer);
   // Called when the game starts or when spawned
   virtual void BeginPlay() override;

   virtual void Tick(float DeltaTime) override;

   UFUNCTION()
   void DestroyAll();
};