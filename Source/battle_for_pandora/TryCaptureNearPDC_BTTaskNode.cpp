// Fill out your copyright notice in the Description page of Project Settings.

#include "TryCaptureNearPDC_BTTaskNode.h"
#include "SquadSbireController.h"
#include "Logger.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Maneuver.h"
#include "ManeuverDefensive.h"
#include "BFPCharacterPlayable.h"
#include "SquadDescriptor.h"
#include "MoveCommand.h"
#include "BFP_GameState.h"
#include "DrawDebugHelpers.h"

UTryCaptureNearPDC::UTryCaptureNearPDC(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    NodeName = "TryCaptureNearPDC";
    bShouldFailOnAddTargetPoint = true;
}

EBTNodeResult::Type UTryCaptureNearPDC::ExecuteTaskImpl(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, AManeuver* maneuvre) {
    maneuvre->nbMemberCompleted = 0;
    maneuvre->lieutenantCompleted = false;

    // On get tous les PDC à proximite :
    TArray<ACheckpointTriggerSphere*> checkpoints = maneuvre->GetWorld()->GetGameState<ABFP_GameState>()->GetAllCPs();
    FTransform posLieutenant = maneuvre->GetDescriptor()->GetFirstLieutenant()->GetPawn()->GetTransform();
    checkpoints = maneuvre->GetWorld()->GetGameState<ABFP_GameState>()->GetAllInDistanceFrom(checkpoints, posLieutenant, distanceMaxToCapturePDC);

    if (checkpoints.Num() > 0) {
        // Si on est deja dessus, on ne fait rien !
        if (FVector::Dist(posLieutenant.GetLocation(), checkpoints[0]->GetActorLocation()) >= distanceMinToCapturePDC) {
            MY_LOG_IA(TEXT("%s MODE = TryCaptureNearPDC"), *maneuvre->GetDescriptor()->GetName());
            DRAW_DEBUG_LINE(GetWorld(), posLieutenant.GetLocation(), checkpoints[0]->GetActorLocation(), FColor::Red, true, -1.0F, (uint8)'\000', 10.0F);
            maneuvre->MoveTo(checkpoints[0]->GetTransform(), false);
            return EBTNodeResult::InProgress;
        } else {
            return EBTNodeResult::Failed;
        }
    }
    else {
        return EBTNodeResult::Failed;
    }
}

FString UTryCaptureNearPDC::GetStaticDescription() const
{
    return TEXT("Si un PDC est assez proche, va sur celui-ci pour le capturer !");
}

void UTryCaptureNearPDC::OnMessage(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, FName Message, int32 RequestID, bool bSuccess) {
    Super::OnMessage(OwnerComp, NodeMemory, Message, RequestID, bSuccess);

    // On distingue les messages
    AManeuver* maneuvre = Cast<AManeuver>(OwnerComp.GetOwner());
    AManeuverDefensive* maneuvreDefensive = Cast<AManeuverDefensive>(OwnerComp.GetOwner());
    ASquadMemberController* member = maneuvre->GetLastMessage()->GetSquadMemberController();
    switch (maneuvre->GetLastMessage()->GetType()) {
    case MessageType::COMPLETED: {
        if (IsValid(Cast<ASquadLieutenantController>(member))) {
            maneuvre->lieutenantCompleted = true;
            // Pour tous les sbires, on leur dit qu'ils ne sont pas arriv�s !
            for (ASquadSbireController* sbire : maneuvre->GetDescriptor()->GetSbires()) {
                UMoveCommand* moveCommand = Cast<UMoveCommand>(sbire->GetCommand());
                if(IsValid(moveCommand))
                    moveCommand->SetNotArrived();
            }
        }
        else { // Sbire
            if (maneuvre->lieutenantCompleted) {
                maneuvre->nbMemberCompleted++;
                // Si on a re�u assez de messages alors on quitte la tache ! :3
                if (maneuvre->nbMemberCompleted >= Cast<AManeuver>(OwnerComp.GetOwner())->GetDescriptor()->GetNbMembers() * 0.75f) {
                    // On supprime le premier target point car on vient d'y arriver !
                    maneuvre->RemoveFirstTargetPoint();
                    FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
                }
            }
        }
        break;
    }
    case MessageType::IN_AGRESSION_RANGE:
    case MessageType::IN_RUSH_RANGE:
        FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
        break;
    default:
        break;
    }
}
