// Fill out your copyright notice in the Description page of Project Settings.

#include "DavrosTrouNoirSpell.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "BFP_GameState.h"
#include "BFPCharacterPlayable.h"
#include "Poussee.h"

ADavrosTrouNoirSpell::ADavrosTrouNoirSpell(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    PrimaryActorTick.bCanEverTick = true;
}

void ADavrosTrouNoirSpell::BeginPlay() {
   Super::BeginPlay();
   debutTrouNoir = UGameplayStatics::GetRealTimeSeconds(GetWorld());
   lastTimeRafraichissement = debutTrouNoir;
   DestroyIn(dureeTrouNoir);
}

void ADavrosTrouNoirSpell::Tick(float DeltaTime) {
   Super::Tick(DeltaTime);
   // Mettre � jour la taille
   SetSize();

   // Appliquer le rafraichissement si il le faut
   TryRafraichir();
}

float ADavrosTrouNoirSpell::GetPorteeActuelle() const {
    float now = UGameplayStatics::GetRealTimeSeconds(GetWorld());
    float avancement = (now - debutTrouNoir) / dureeTrouNoir;
    float portee = porteeInitiale * (tailleInitiale + (tailleFinale - tailleInitiale) * avancement);
    return portee;
}

void ADavrosTrouNoirSpell::SetSize() {
   float now = UGameplayStatics::GetRealTimeSeconds(GetWorld());
   float avancement = (now - debutTrouNoir) / dureeTrouNoir;
   float taille = tailleInitiale + (tailleFinale - tailleInitiale) * avancement;
   SetActorScale3D(FVector::OneVector * taille);
}

void ADavrosTrouNoirSpell::TryRafraichir() {
    float now = UGameplayStatics::GetRealTimeSeconds(GetWorld());
    if (now - lastTimeRafraichissement >= frequenceRafraichissement) {
        lastTimeRafraichissement = UGameplayStatics::GetRealTimeSeconds(GetWorld());
        Rafraichir();
    }
}

void ADavrosTrouNoirSpell::Rafraichir() {
    if (!GetWorld()->IsServer()) {
        return;
    }
    // Pour tous les heros qui sont dans la range d'action
    ABFP_GameState* gs = GetWorld()->GetGameState<ABFP_GameState>();
    if (IsValid(gs)) {
        TArray<ABFPCharacterPlayable*> heros = gs->GetAllJoueursAllies();
        heros = gs->GetAllInDistanceFrom(heros, GetTransform(), GetPorteeActuelle());
        for (int i = 0; i < heros.Num(); i++) {
            // On lui prepare sa poussee
            FPoussee poussee(forceAttraction, frequenceRafraichissement);
            poussee.SetDirection(GetActorLocation() - heros[i]->GetActorLocation());
            heros[i]->AddPoussee(poussee);

            // On lui inflige des degats ! :)
            heros[i]->TakeDamage(degats, 0.0f, {}, false, false); // On ne peut ni esquiver ni parer cette attaque mouhahahahaha ! :)
        }
    }
}

