// Fill out your copyright notice in the Description page of Project Settings.

#include "ManeuverTargetPoint.h"

// Sets default values
AManeuverTargetPoint::AManeuverTargetPoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
   bReplicates = true;
   bAlwaysRelevant = true;
   SetActorEnableCollision(false);
}

// Called when the game starts or when spawned
void AManeuverTargetPoint::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AManeuverTargetPoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

