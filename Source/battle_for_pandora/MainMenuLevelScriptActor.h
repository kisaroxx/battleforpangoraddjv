// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "MainMenuLevelScriptActor.generated.h"

/**
 * 
 */
UCLASS()
class BATTLE_FOR_PANDORA_API AMainMenuLevelScriptActor : public ALevelScriptActor
{
	GENERATED_BODY()
	
private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameParams", meta = (AllowPrivateAccess = "true"))
		uint8 nbJoueur;

public:
	virtual void BeginPlay() override;
};
