// Fill out your copyright notice in the Description page of Project Settings.

#include "ExecuteBTTaskNode.h"
#include "SquadMemberController.h"
#include "SquadSbireController.h"
#include "SquadLieutenantController.h"
#include "BFPCharacterLieutenant.h"
#include "SquadDescriptor.h"
#include "GameManager.h"
#include "BFPHUD.h"
#include "MoveCommand.h"
#include "CommandReturn.h"

/** Constructeur de la classe */
UExecuteBTTaskNode::UExecuteBTTaskNode(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer)
{
    // Nous d�finissons le nom que protera le noeud dans le BT
    NodeName = "Execute";
    bNotifyTick = true;
}
/* Fonction d'ex�cution de la t�che, cette t�che devra retourner Succeeded, Failed ou InProgress */
EBTNodeResult::Type UExecuteBTTaskNode::ExecuteTask(UBehaviorTreeComponent& OwnerComp,
    uint8* NodeMemory)
{
    //MY_LOG_IA(TEXT("UExecuteBTTaskNode ExecuteTask"));

    ASquadMemberController* SquadMemberController = Cast<ASquadMemberController>(OwnerComp.GetOwner());
    UCommandReturn* com = NewObject<UCommandReturn>(this, UCommandReturn::StaticClass());
    com = SquadMemberController->execute();

    if (com == nullptr)
        return EBTNodeResult::Failed;
    if (com->HasEndCommand())
        return EBTNodeResult::Succeeded;

    // Ce n'est pas a cet endroit qu'il faut supprimer les targets points ^^'
    // Mais je comprends que vous l'ayez mis ici car à l'époque il n'y avait pas forcément de "bon endroit" :)
    // Je vais placer un code équivalent dans le MoveToNextPoint_BTTaskNode, car c'est son rôle :)
    // Au fait, pour suppirmer un point d'un array, il y a plus simple ^^'
    //EPathFollowingRequestResult::Type MoveToResult = com->getReturn();
    //if (MoveToResult == EPathFollowingRequestResult::AlreadyAtGoal)
    //{
    //    // DEBUG_SCREEN(-1, 5.f, FColor::Green, this->GetName().Append(" Objectif atteint "));
    //    // Enl�ve le point atteint de la liste des points
    //    SquadMemberController = Cast<ASquadLieutenantController>(OwnerComp.GetOwner());
    //    if (SquadMemberController != nullptr) {
    //        ABFPCharacterLieutenant* lieutenant = Cast<ABFPCharacterLieutenant>(SquadMemberController->GetPawn());
    //        ASquadDescriptor* squad = lieutenant->GetSquad();
    //        TArray<FTransform>& targetPoints = squad->getManeuver()->GetTargetPoint();
    //        ABFPHUD* HUD = Cast<ABFPHUD>(UGameplayStatics::GetPlayerController(this, 0)->GetHUD());
    //        //HUD->DeleteNextPositionIndicator(lieutenant, targetPoints[0]); 
    //        if (targetPoints.Num() > 1) {
    //            TArray<FTransform> nouvTargetPoint;
    //            // Bravo UE4 de commenter le == dans les FTransform ...
    //            for (int i = 1; i < targetPoints.Num(); ++i) {
    //                nouvTargetPoint.Add(targetPoints[i]);
    //            }

    //            targetPoints = nouvTargetPoint;

    //        }

    //    }

    //    return EBTNodeResult::Succeeded;
    //}
    return EBTNodeResult::InProgress;
}

int UExecuteBTTaskNode::nbAppelExecute = 0;
void UExecuteBTTaskNode::TickTask(class UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory,
    float DeltaSeconds)
{
    ASquadMemberController* SquadMemberController = Cast<ASquadMemberController>(OwnerComp.GetOwner());
    UCommandReturn* com = NewObject<UCommandReturn>(this, UCommandReturn::StaticClass());
    com = SquadMemberController->execute();
    UExecuteBTTaskNode::nbAppelExecute++;

    if (com == nullptr) {
        FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
        return;
    }
    if (com->HasEndCommand()) {
        FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
        return;
    }

    EPathFollowingRequestResult::Type MoveToResult = com->getReturn();
    if (MoveToResult == EPathFollowingRequestResult::AlreadyAtGoal)
    {
        FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
    }
}

/** Permet de d�finir une description pour la t�che. C'est ce texte qui
appara�tra dans le noeud que nous ajouterons au Behavior Tree */
FString UExecuteBTTaskNode::GetStaticDescription() const
{
    return TEXT("Execute la commande du SquadMember");
}