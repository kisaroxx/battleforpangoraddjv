// Fill out your copyright notice in the Description page of Project Settings.

#include "OrienterToNextPoint_BTTaskNode.h"
#include "SquadSbireController.h"
#include "Logger.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Maneuver.h"
#include "ManeuverDefensive.h"
#include "BFPCharacterPlayable.h"
#include "SquadDescriptor.h"

UOrienterToNextPoint::UOrienterToNextPoint(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    NodeName = "OrienterToNextPoint";
    bUseCompletedMessages = true;
}

FString UOrienterToNextPoint::GetStaticDescription() const
{
    return TEXT("Permet de s'orienter dans la direction du prochain point a atteindre.");
}

EBTNodeResult::Type UOrienterToNextPoint::ExecuteTaskImpl(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, AManeuver* maneuvre) {
    if (maneuvre->GetDescriptor()->GetLieutenants().Num() <= 0) { // Si on a pas de lieutenant, on fail tout de suite !
        return EBTNodeResult::Failed;
    }
   if (maneuvre->GetTargetPoints().Num() > 0) {
        MY_LOG_IA(TEXT("%s MODE = OrienterToNextPoint with %s"), *maneuvre->GetDescriptor()->GetName(), *maneuvre->GetName());
        //FDebug::DumpStackTraceToLog();
        maneuvre->OrienterTo(maneuvre->GetTargetPoints()[0]->GetActorTransform());
        return EBTNodeResult::InProgress;
    }
    else {
        return EBTNodeResult::Failed;
    }
}

void UOrienterToNextPoint::OnMessage(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, FName Message, int32 RequestID, bool bSuccess) {
    Super::OnMessage(OwnerComp, NodeMemory, Message, RequestID, bSuccess);

    // On distingue les messages
    AManeuver* maneuvre = Cast<AManeuver>(OwnerComp.GetOwner());
    AManeuverDefensive* maneuvreDefensive = Cast<AManeuverDefensive>(OwnerComp.GetOwner());
    if (maneuvre->GetLastMessage()->GetType() == MessageType::COMPLETED) {
        MY_LOG_IA(TEXT("%s COMPLETION = %d / %d"), *maneuvre->GetDescriptor()->GetName(), maneuvre->nbCompleted, maneuvre->GetDescriptor()->GetNbMembers());
    }
    switch (maneuvre->GetLastMessage()->GetType()) {
    case MessageType::IN_AGRESSION_RANGE:
        if (!continueEvenIfInAgressionRange) {
            if (IsValid(maneuvreDefensive)) {
                FVector posTargetPoint = maneuvreDefensive->GetLastTargetPoint();
                FVector posLieutenant = maneuvreDefensive->GetDescriptor()->GetFirstLieutenant()->GetPawn()->GetActorLocation();
                float d = FVector::DistSquared(posTargetPoint, posLieutenant);
                float distanceToPoint = maneuvreDefensive->GetDistanceMaxFromLastTargetPoint();

                if (d <= distanceToPoint * distanceToPoint) {
                    FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
                    MY_LOG_IA(TEXT("%s FAILED because of IN_AGRESSION_RANGE"), *maneuvre->GetDescriptor()->GetName());
                }
            }
        }
        break;
    case MessageType::IN_RUSH_RANGE:
        FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
        MY_LOG_IA(TEXT("%s FAILED because of IN_RUSH_RANGE"), *maneuvre->GetDescriptor()->GetName());
        break;
    default:
        break;
    }
}
