// Fill out your copyright notice in the Description page of Project Settings.

#include "BFP_PlayerState.h"
#include "UnrealNetwork.h"
#include "Logger.h"



ABFP_PlayerState::ABFP_PlayerState(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
	bReplicates = true;
}

void ABFP_PlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ABFP_PlayerState, S_PlayerInfo);	
}

void ABFP_PlayerState::BeginPlay()
{
	Super::BeginPlay();
}

void ABFP_PlayerState::CopyProperties(APlayerState* PlayerState) {
	Super::CopyProperties(PlayerState);
	if (PlayerState) {
		ABFP_PlayerState* TestPlayerState = Cast<ABFP_PlayerState>(PlayerState);
		if (TestPlayerState) {
			TestPlayerState->S_PlayerInfo = S_PlayerInfo;
		}
	}
}

void ABFP_PlayerState::OverrideWith(APlayerState* PlayerState) {
	Super::OverrideWith(PlayerState);
	if (PlayerState) {
		ABFP_PlayerState* TestPlayerState = Cast<ABFP_PlayerState>(PlayerState);
		if (TestPlayerState) {
			S_PlayerInfo = TestPlayerState->S_PlayerInfo;
		}
	}
}
