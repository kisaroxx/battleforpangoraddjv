// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Spell.h"
#include "Engine/TriggerCapsule.h"
#include "Engine/StaticMeshActor.h"
#include "KiaraDashSpell.generated.h"

UCLASS()
class BATTLE_FOR_PANDORA_API AKiaraDashSpell : public ASpell
{
    GENERATED_BODY()

    /* La puissance (donc la vitesse) de la poussee (du dash. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float vitesseDash = 2000.0f;

    /* La duree du dash. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float dureeDash = 0.3f;

protected:
   AKiaraDashSpell(const FObjectInitializer & ObjectInitializer);
   // Called when the game starts or when spawned
   virtual void BeginPlay() override;

public:
   // Called every frame
   virtual void Tick(float DeltaTime) override;
};