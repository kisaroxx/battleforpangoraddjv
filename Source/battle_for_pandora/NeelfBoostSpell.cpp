// Fill out your copyright notice in the Description page of Project Settings.

#include "NeelfBoostSpell.h"
#include "Public/TimerManager.h"
#include "BFP_GameState.h"
#include "NeelfBoostSpell.h"

ANeelfBoostSpell::ANeelfBoostSpell(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    PrimaryActorTick.bCanEverTick = true;
}


void ANeelfBoostSpell::BeginPlay() {
   Super::BeginPlay();
   ABFPCharacter* neelf = Cast<ABFPCharacter>(GetOwner());
   if (IsValid(neelf)) {
       ABFP_GameState* gs = GetWorld()->GetGameState<ABFP_GameState>();
       TArray<ABFPCharacterPlayable*> heros = gs->GetAllJoueursAllies();
       herosSaved = gs->GetAllInDistanceFrom(heros, GetTransform(), porteeBoost);
       
       for (int i = 0; i < herosSaved.Num(); i++) {
           if (GetWorld()->IsServer()) {
               // On boost le personnage
               herosSaved[i]->ModifyDegatMultiplicateur(degatMultiplicateur);
           }

           // On fait spaner l'aura
           FVector position = herosSaved[i]->GetActorLocation() + FVector::UpVector * (-50.0f);
           FRotator rotation = herosSaved[i]->GetActorRotation();
           FActorSpawnParameters spawnParams;
           spawnParams.Owner = herosSaved[i];
           spawnParams.bNoFail = true;
           spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

           ASpell* aura = GetWorld()->SpawnActor<ASpell>(auraClass, position, rotation, spawnParams);
           if (IsValid(aura)) {
               aura->DestroyIn(dureeBoost);
               aurasSaved.Push(aura);
           } else {
               MY_LOG_GAME(TEXT("Un boost de Neelf n'a pas pu �tre spawn !"));
           }
       }

       // On detruit toutes les auras puis nous-m�me ! :)
       FTimerHandle fuzeTimerHandle;
       FTimerDelegate timerDelegate;
       timerDelegate.BindUFunction(this, FName("DestroyAll"));
       GetWorld()->GetTimerManager().SetTimer(
           fuzeTimerHandle,
           timerDelegate,
           dureeBoost,
           false);

   } else {
       DEBUG_SCREEN(-1, 5.f, FColor::Red, TEXT("Impossible de recuperer l'owner dans neelfSpell !!"));
   }
}

void ANeelfBoostSpell::Tick(float DeltaTime) {
    Super::Tick(DeltaTime);

    // On met � jour les positions !
    for (int i = 0; i < herosSaved.Num(); i++) {
        if(IsValid(aurasSaved[i]) && IsValid(herosSaved[i]))
            aurasSaved[i]->SetActorLocation(herosSaved[i]->GetActorLocation());
    }
}

void ANeelfBoostSpell::DestroyAll() {
    if (GetWorld()->IsServer()) {
        for (int i = 0; i < herosSaved.Num(); i++) {
            if(IsValid(herosSaved[i]))
                herosSaved[i]->ModifyDegatMultiplicateur(1.0f / degatMultiplicateur);
        }
    }
    AutoDestroy();
}

