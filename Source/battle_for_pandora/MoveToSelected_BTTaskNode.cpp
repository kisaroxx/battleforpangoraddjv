// Fill out your copyright notice in the Description page of Project Settings.

#include "MoveToSelected_BTTaskNode.h"
#include "SquadSbireController.h"
#include "Logger.h"
#include "BFPCharacterPlayable.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Maneuver.h"
#include "BFPCharacterPlayable.h"
#include "SquadDescriptor.h"

UMoveToSelected::UMoveToSelected(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    NodeName = "MoveToSelected";
    bUseCompletedMessages = true;
    bShouldFailOnAddTargetPoint = true;
}

FString UMoveToSelected::GetStaticDescription() const
{
    return TEXT("Va a l'endroit precedement selectionne.");
}

EBTNodeResult::Type UMoveToSelected::ExecuteTaskImpl(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, AManeuver* maneuvre) {
    if (maneuvre->GetJoueursVus().Num() > 0) {
        MY_LOG_IA(TEXT("%s MODE = MoveToSelected"), *maneuvre->GetDescriptor()->GetName());
        maneuvre->MoveToActor(maneuvre->GetJoueurSelected());
        return EBTNodeResult::InProgress;
    }
    else {
        return EBTNodeResult::Failed;
    }
}

void UMoveToSelected::OnMessage(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, FName Message, int32 RequestID, bool bSuccess) {
    Super::OnMessage(OwnerComp, NodeMemory, Message, RequestID, bSuccess);

    // On distingue les messages
    AManeuver* maneuvre = Cast<AManeuver>(OwnerComp.GetOwner());
    switch (maneuvre->GetLastMessage()->GetType()) {
    case MessageType::IN_AGRESSION_RANGE: case MessageType::IN_RUSH_RANGE:
        FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
        break;
    case MessageType::OUT_OF_VISION_RANGE:
        // Si le joueur est trop loin, alors on sort !
        FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
        break;
    default:
        break;
    }
}
