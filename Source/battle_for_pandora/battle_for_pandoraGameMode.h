// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "battle_for_pandoraGameMode.generated.h"



UCLASS(minimalapi)
class Abattle_for_pandoraGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Abattle_for_pandoraGameMode();

};



