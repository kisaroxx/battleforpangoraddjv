// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core.h" //Pour la replication on utilise Core.h
#include "GameFramework/Character.h"
#include "Components/ActorComponent.h"
#include "Net/UnrealNetwork.h"//Pour la replication
#include "TimerManager.h"
#include "HealthComponent.generated.h"


class UInputComponent;
class UTimelineComponent;
class ABFPCharacter;

DECLARE_EVENT(UHealthComponent, FChangeHealthEvent)
DECLARE_EVENT(UHealthComponent, FChangeHealingStateEvent)
DECLARE_EVENT(UHealthComponent, FChangeDamageStateEvent)
DECLARE_EVENT(UHealthComponent, FEmptyHealthEvent)

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BATTLE_FOR_PANDORA_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent(const FObjectInitializer & ObjectInitializer);

	UFUNCTION(BlueprintPure, Category = "Health")
	float GetHealth();

	UFUNCTION(BlueprintPure, Category = "Health")
	float GetFullHealth();

	UFUNCTION(BlueprintPure, Category = "Health")
	float GetHealthPercentage();

	UFUNCTION(BlueprintPure, Category = "Health")
	FText GetHealthIntText();

	UFUNCTION()
	void SetHealthValue();

	UFUNCTION()
	void SetHealthState();

private:

	UFUNCTION(BlueprintCallable, Category = "Health")
	void UpdateHealth(float HealthChange, bool bAsPercentage = false);

	UFUNCTION(BlueprintCallable, Category = "Health")
	void UpdateHealthPercentage();

private:
	UFUNCTION()
	virtual float TakeDamage(float DamageAmount, bool bAsPercentage = false);

	UFUNCTION()
    void SetRedFlash(bool newVal);
public:

	void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;

	//--------------------HEAL----------------------
	UFUNCTION()
    void StartHealing(FTimerHandle& timer, float amount, bool bAsPercentage = false, float delay = 0.5f, bool repeat = false);
	UFUNCTION()
	void StopHealing(FTimerHandle& timerToStop);
	UFUNCTION()
	bool IsHealed() { return bIsHealed; }
	UFUNCTION()
	void Heal(float HealthChange, bool bAsPercentage = false);

	//--------------------DAMAGE----------------------
	UFUNCTION()
    void StartDamaging(FTimerHandle& timer, float amount, bool bAsPercentage = false, float delay = 0.5f, bool repeat = false);
	UFUNCTION()
	void StopDamaging(FTimerHandle& timerToStop);
	UFUNCTION()
	bool IsDamaged() { return bIsDamaged; }
	UFUNCTION()
	void Damage(float HealthChange, bool bAsPercentage = false);

private:
	//--------------------HEAL----------------------
	UFUNCTION()
	void HealCallback(FTimerHandle timer);

	//--------------------DAMAGE----------------------
	UFUNCTION()
	void DamageCallback(FTimerHandle timer);

private : 

    UPROPERTY(EditAnywhere, Category = "Health")
    float FullHealth = 50.0f;

	UPROPERTY(ReplicatedUsing = OnRep_Health, Transient, EditAnywhere,  Category = "Health")
	float Health;

	UPROPERTY(Transient, VisibleAnywhere,  Category = "Health")
	float HealthPercentage;

	UPROPERTY(VisibleAnywhere,  Category = "Health")
	float PreviousHealth;

	UPROPERTY(VisibleAnywhere,  Category = "Health")
	float HealthValue;

	//--------------------HEAL----------------------

	UPROPERTY(Transient, EditAnywhere, Category = "Health")
	float amountHeal;

	UPROPERTY(Transient, EditAnywhere, Category = "Health")
	bool bAsPercentageHealing;

	bool bRepeatHealing;

	UPROPERTY(ReplicatedUsing = OnRep_IsHealed, Transient, EditAnywhere, Category = "Health")
	bool bIsHealed = false;

	FTimerHandle HealingTimerHandle;

	//--------------------DAMAGE----------------------

	UPROPERTY(Transient, EditAnywhere, Category = "Health")
	float amountDamage;

	UPROPERTY(Transient, EditAnywhere, Category = "Health")
	bool bAsPercentageDamage;

	bool bRepeatDamage;

	UPROPERTY(ReplicatedUsing = OnRep_IsDamaged, Transient, EditAnywhere, Category = "Health")
	bool bIsDamaged = false;

	//---------------------------------------------------------
	
	UPROPERTY(EditDefaultsOnly,  Category = "Health")
	UCurveFloat *HealthCurve;

	UPROPERTY(EditAnywhere)
	ABFPCharacter* MyCharacter;

	float CurveFloatValue;
	float TimelineValue;

	UTimelineComponent* MyTimeline;
	struct FTimerHandle MemberTimerHandle;
	struct FTimerHandle MagicTimerHandle;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	virtual void InitializeComponent() override;
	
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void SetTimer(FTimerHandle& InOutHandle, typename FTimerDelegate::TUObjectMethodDelegate<UHealthComponent>::FMethodPtr InTimerMethod, float InRate, bool InbLoop = false, float InFirstDelay = -1.f);
	
	FChangeHealthEvent ChangeHealthEvent;
	FChangeHealingStateEvent ChangeHealingStateEvent;
	FChangeDamageStateEvent ChangeDamageStateEvent;
	FEmptyHealthEvent EmptyHealthEvent;

	UFUNCTION()
	void OnRep_Health();
	UFUNCTION()
	void OnRep_IsHealed();
	UFUNCTION()
	void OnRep_IsDamaged();
};
