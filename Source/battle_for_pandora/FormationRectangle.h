// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Formation.h"
#include "FormationRectangle.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class BATTLE_FOR_PANDORA_API UFormationRectangle : public UFormation
{
    GENERATED_BODY()

    /* Indique le nombre de sbires � mettre par lignes. */
    UPROPERTY(EditDefaultsOnly, Category = "Parametres")
    int nbFrontLane = 10;

    /* Indique l'espacement entre les sbires. */
    UPROPERTY(EditDefaultsOnly, Category = "Parametres")
    float espacement = 150.0f;


protected:
    virtual void ComputeAllPositions(int nbSbires, int nbLieutenants) override;

private:
    int GetNbLignes(int nbSbires);
};
