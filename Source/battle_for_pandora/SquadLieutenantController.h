// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SquadMemberController.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Character.h"
#include "BFPCharacter.h"
#include "Net/UnrealNetwork.h"
#include "SquadLieutenantController.generated.h"

/**
 *
 */
UCLASS()
class BATTLE_FOR_PANDORA_API ASquadLieutenantController : public ASquadMemberController
{
	GENERATED_BODY()

	TSubclassOf<UBehaviorTree> lieutenantBTClass;

	UPROPERTY(Replicated, VisibleAnywhere, Category = "BT")
	UBehaviorTree*  lieutenantBT;

public:
	//DECLARE_DYNAMIC_MULTICAST_DELEGATE(FRunBtDelegate);

	ASquadLieutenantController(const FObjectInitializer & ObjectInitializer);

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void PostInitializeComponents() override;
    virtual void BeginPlay() override;
    virtual void Tick(float DeltaTime) override;

	//UPROPERTY(BlueprintAssignable)
	//FRunBtDelegate runBtFlag;

	//UFUNCTION(BlueprintImplementableEvent, Reliable, Server, WithValidation, Category = "BaseCharacter")
	//UFUNCTION(BlueprintImplementableEvent, Category = "BaseCharacter")
	//UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BaseCharacter")
    void RunBT() override;
};
