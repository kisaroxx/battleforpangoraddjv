// Fill out your copyright notice in the Description page of Project Settings.

#include "SelectionnerPlusProche_BTTaskNode.h"
#include "SquadSbireController.h"
#include "Logger.h"
#include "BFP_GameState.h"
#include "SquadLieutenantController.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Maneuver.h"
#include "BFPCharacterPlayable.h"
#include "SquadDescriptor.h"

USelectionnerPlusProche::USelectionnerPlusProche(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    NodeName = "SelectionnerPlusProche";
}

EBTNodeResult::Type USelectionnerPlusProche::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8*
    NodeMemory)
{
    Super::ExecuteTask(OwnerComp, NodeMemory);

    // On recupere la manoeuvre !
    AManeuver* maneuvre = Cast<AManeuver>(OwnerComp.GetOwner());

    FTransform lieutenantPosition = maneuvre->GetDescriptor()->GetLieutenants()[0]->GetPawn()->GetTransform();
    TArray<ABFPCharacterPlayable*> plusProches = GetWorld()->GetGameState<ABFP_GameState>()->GetAllNearestFrom(maneuvre->GetJoueursVus(), lieutenantPosition, 1);

    if(plusProches.Num() <= 0)
        return EBTNodeResult::Failed;

    maneuvre->SetJoueurSelected(plusProches[0]);

    return EBTNodeResult::Succeeded;
}

FString USelectionnerPlusProche::GetStaticDescription() const
{
    return TEXT("Permet de selectionner l'adversaire le plus proche.");
}