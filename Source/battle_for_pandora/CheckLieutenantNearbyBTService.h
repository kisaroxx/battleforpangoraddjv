// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "CheckLieutenantNearbyBTService.generated.h"

/**
 * 
 */
UCLASS()
class BATTLE_FOR_PANDORA_API UCheckLieutenantNearbyBTService : public UBTService
{
   GENERATED_BODY()

public:
      UCheckLieutenantNearbyBTService();

      /** Sera appel� � chaque �update� du service */
      virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
