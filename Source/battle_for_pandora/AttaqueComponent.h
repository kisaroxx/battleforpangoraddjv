// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AttaqueComponent.generated.h"


UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BATTLE_FOR_PANDORA_API UAttaqueComponent : public UActorComponent
{
	GENERATED_BODY()

    /* L'attaque � d�clancher, utilise pour determiner la taille de la boite de collision. */
    UPROPERTY(EditAnywhere, Category = Attaque)
    TSubclassOf<class AAttaque> attaqueClass;
    /* Comment d�caler l'arme par rapport � son joint. */
    UPROPERTY(EditAnywhere, Category = Attaque)
    FVector weaponOffset{};
    /* Comment orienter l'arme par rapport � son joint. */
    UPROPERTY(EditAnywhere, Category = Attaque)
    FRotator weaponOffsetRotation{};

    /* Temps que prends la phase d'armement (en s). */
    UPROPERTY(EditAnywhere, Category = Attaque)
    float dureeArmement = 0.5f;
    /* Temps que prends la phase d'activation (en s). */
    UPROPERTY(EditAnywhere, Category = Attaque)
    float dureeActivation = 0.5f;
    /* Temps que prends la phase de desarmement (en s). */
    UPROPERTY(EditAnywhere, Category = Attaque)
    float dureeDesarmement = 0.5f;

    /* Temps que prends la phase d'armement (en s) pour l'animation. */
    UPROPERTY(EditAnywhere, Category = "Attaque|Animation")
        float dureeArmementAnim = 0.5f;
    /* Temps que prends la phase d'activation (en s) pour l'animation. */
    UPROPERTY(EditAnywhere, Category = "Attaque|Animation")
        float dureeActivationAnim = 0.5f;
    /* Temps que prends la phase de desarmement (en s) pour l'animation. */
    UPROPERTY(EditAnywhere, Category = "Attaque|Animation")
        float dureeDesarmementAnim = 0.5f;

    /* Les degats de base de l'attaque. */
    UPROPERTY(EditAnywhere, Category = Attaque)
    int baseDamages = 10;
    /* Le temps que l'attaque met a recover de l'attaque. */
    UPROPERTY(EditAnywhere, Category = Attaque)
    float dureeStun = 1.0f;

    /* Le temps que l'attaque met a recover de l'attaque. */
    UPROPERTY(EditAnywhere, Category = "Attaque|Poussee")
    float dureePoussee = 0.2f;
    /* Le temps que l'attaque met a recover de l'attaque. */
    UPROPERTY(EditAnywhere, Category = "Attaque|Poussee")
    float puissancePoussee = 100.0f;

    /* O� attacher l'attaque. */
    UPROPERTY(EditAnywhere, Category = Attaque)
    FName attachedBone = "hand_r";

    TArray<class AAttaque*> attaquesEnCours{};


public:	
	// Sets default values for this component's properties
	UAttaqueComponent();

    /* Pour declancher l'attaque associee a ce component. */
    UFUNCTION()
    void ActivateAttack(class ABFPCharacter* owner, class UWorld* world);

    /* Permet d'annuler toutes les attaques en cours. */
    UFUNCTION()
    void CancelAll();

    /* Permet de supprimer une attaque de la liste. */
    void UntrackAttack(class AAttaque* attaque);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

    UFUNCTION()
    float GetDureeArmement();
    UFUNCTION()
    float GetDureeActivation();
    UFUNCTION()
    float GetDureeDesarmement();
    UFUNCTION()
        float GetDureeArmementAnim();
    UFUNCTION()
        float GetDureeActivationAnim();
    UFUNCTION()
        float GetDureeDesarmementAnim();
};
