// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "CanAttackBTTaskNode.generated.h"

/**
 * 
 */
UCLASS()
class BATTLE_FOR_PANDORA_API UCanAttackBTTaskNode : public UBTTaskNode
{
   GENERATED_BODY()

   UCanAttackBTTaskNode();

   /* Sera appelee au demarrage de la tache et devra retourner Succeeded, Failed ou InProgress */
   virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

   /** Retourne une chaine de description pour la tache. Ce texte apparaitre dans le BT */
   virtual FString GetStaticDescription() const override;

};
