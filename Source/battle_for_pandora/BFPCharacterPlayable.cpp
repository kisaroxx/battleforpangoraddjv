// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "BFPCharacterPlayable.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/WidgetComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Math/UnrealMathUtility.h"
#include "BFPHUD.h"
#include "Camera/PlayerCameraManager.h"
#include "Components/SceneComponent.h"
#include "Engine/World.h"
#include "EngineUtils.h"
#include "BFPCharacterNonPlayable.h"
#include "BFPCharacter.h"
#include "Logger.h"
#include <string>
#include <Landscape/Classes/Landscape.h>
#include <BFPCharacterSbire.h>
#include "Components/StaticMeshComponent.h"
#include "CampFire.h"
#include "BFPHUD.h"
#include "BFPCharacterLieutenant.h"
#include "BFP_GameState.h"
#include "SquadDescriptor.h"
#include "BFP_PlayerController.h"
#include "HealthComponent.h"
#include "Util.h"
#include "DrawDebugHelpers.h"

//////////////////////////////////////////////////////////////////////////
// Abattle_for_pandoraCharacter

ABFPCharacterPlayable::ABFPCharacterPlayable(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer)
{
   // Set size for collision capsule
   GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

   //APlayerController *p = GetWorld()->GetFirstPlayerController();
   //APlayerController *p = GEngine->GetGamePlayer(GetWorld(), 0)->PlayerController;

   // set our turn rates for input
   BaseTurnRate = 45.f;
   BaseLookUpRate = 45.f;

   // Don't rotate when the controller rotates. Let that just affect the camera.
   bUseControllerRotationPitch = false;
   bUseControllerRotationYaw = false;
   bUseControllerRotationRoll = false;

   // Configure character movement
   GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
   GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
   GetCharacterMovement()->JumpZVelocity = 600.f;
   GetCharacterMovement()->AirControl = 0.2f;

   // Create a camera boom (pulls in towards the player if there is a collision)
   CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
   CameraBoom->SetupAttachment(RootComponent);
   CameraBoom->TargetArmLength = 500.0f; // The camera follows at this distance behind the character	
   CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

                            // Create a follow camera
   FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
   FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
   FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

                              // Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
                                 // are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
   bAlwaysRelevant = true;
   isInInteractiveMap = false;
}

void ABFPCharacterPlayable::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
   Super::GetLifetimeReplicatedProps(OutLifetimeProps);
   DOREPLIFETIME(ABFPCharacterPlayable, NbMort);
   DOREPLIFETIME(ABFPCharacterPlayable, bGhosted);
   DOREPLIFETIME(ABFPCharacterPlayable, bVraiMort);
   DOREPLIFETIME(ABFPCharacterPlayable, Name);
}

void ABFPCharacterPlayable::Die()
{
   if (getNbMort() < getNbMortMax()) {
      bGhosted = true;
      AddNbMort();
      DiedFlag.Broadcast(getCharacterAffiliation());
      SetDead(false);
      setOldAffiliation(getOldAffiliation());
      setCharacterAffiliation(Affiliation::NEUTRAL);
      Cast<ABFP_GameState>(this->GetWorld()->GetGameState())->UnRegister(this);
      DEBUG_SCREEN(-1, 5.f, FColor::Yellow, TEXT("Ghosted fin"));
      FTimerHandle fuzeTimerHandle;
      FTimerDelegate timerDelegate;
      timerDelegate.BindUFunction(this, FName("SetUnGhosted"));
      GetWorld()->GetTimerManager().SetTimer(
         fuzeTimerHandle,
         timerDelegate,
         15.f,
         false);
   }
   else {
      SetDead(true);
      DiedFunction();
      DiedFlag.Broadcast(this->CharacterAffiliation);
   }
}

void ABFPCharacterPlayable::DiedFunction()
{
   MY_LOG_NETWORK(TEXT("ABFPCharacterPlayable::Die begin "));
   MY_LOG_NETWORK(TEXT("ABFPCharacterPlayable::Die die now "));
   this->DisableInput(Cast<APlayerController>(GetController()));
   //Super::Die();
   DEBUG_SCREEN(-1, 5.f, FColor::Yellow, this->GetName().Append(" DIIIEDD !"));
   MY_LOG_NETWORK(TEXT("ABFPCharacter::Die begin "));

   combatState = CombatState::DEAD;
   poussees.Empty();
   /*ABFPHUD* hud = Cast<ABFPHUD>(UGameplayStatics::GetPlayerController(this, 0)->GetHUD());
   MY_LOG_NETWORK(TEXT("ABFPCharacter::Die State hud : %u"), IsValid(hud));
   hud->RemoveWidgetFromHUD(this);*/
   MY_LOG_NETWORK(TEXT("ABFPCharacter::Die unregiter from squad "));
   UnRegisterToSquad();
   MY_LOG_NETWORK(TEXT("ABFPCharacter::Die unregiter from Manager "));
   UnRegisterToGameManager();

   // Désactiver les collisions !
   SetActorEnableCollision(false);
}

void ABFPCharacterPlayable::SetUnGhosted()
{
   DEBUG_SCREEN(-1, 5.f, FColor::Yellow, "ABFPCharacterPlayable SetUnGhosted");
   MY_LOG_NETWORK(TEXT("ABFPCharacterPlayable SetUnGhosted"));
   bGhosted = false;
   if (GetWorld()->IsClient()) {
      DEBUG_SCREEN(-1, 5.f, FColor::Yellow, "ABFPCharacterPlayable SetUnGhosted Client");
   }
   if (GetWorld()->IsServer()) {
      DEBUG_SCREEN(-1, 5.f, FColor::Yellow, "ABFPCharacterPlayable SetUnGhosted Serveur");
      Cast<ABFP_GameState>(this->GetWorld()->GetGameState())->ReRegister(this);

   }
   //On fait ca parce que on n'a pas le temps
   setCharacterAffiliation(Affiliation::GOOD);
   if (getCharacterAffiliation() == Affiliation::NEUTRAL) {
      DEBUG_SCREEN(-1, 5.f, FColor::Yellow, TEXT("UnGhosted Probleme :("));
   }
   float midLife = HealthSystem->GetFullHealth() / 2.f;
   float currentLife = HealthSystem->GetHealth();
   HealthSystem->Heal(midLife - currentLife);

   UnGhost.Broadcast();
}

bool ABFPCharacterPlayable::IsGhost() {
   return bGhosted;
}

void ABFPCharacterPlayable::SetGhosted()
{
   DEBUG_SCREEN(-1, 5.f, FColor::Yellow, "ABFPCharacterPlayable SetGhosted");
   DiedFlag.Broadcast(this->CharacterAffiliation);
}

void ABFPCharacterPlayable::SetDead(bool vraiMort)
{
   bVraiMort = vraiMort;
}

//////////////////////////////////////////////////////////////////////////
// Input

// Called when the game starts
void ABFPCharacterPlayable::BeginPlay()
{
   Super::BeginPlay();

   if (GetWorld()->IsServer()) {
      ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
      if (IsValid(gameState)) {

         gameState->Register(this);
      }
      else {
         MY_LOG_NETWORK(TEXT("ABFPCharacterPlayable BeginPlay game state non valide"));
      }
   }

}

void ABFPCharacterPlayable::PostInitProperties() {
   Super::PostInitProperties();
   HealthSystem->ChangeDamageStateEvent.AddUFunction(this, "PlayFlash");

}


// Called every frame
void ABFPCharacterPlayable::Tick(float DeltaTime) {
   Super::Tick(DeltaTime);
   TWeakObjectPtr<ABFP_PlayerController> lPlayerController = Cast<ABFP_PlayerController>(GetController());
   if (lPlayerController.IsValid() && IsValid(lPlayerController->SoundActorInstance))
      lPlayerController->SoundActorInstance->SetActorRelativeLocation(this->GetActorLocation());
   if (TargetLocked)
   {
      if (!(NearestTarget->IsDead())) {
         LockCamera(NearestTarget);
         if (!IsRunningDedicatedServer()) {
            RotateWidget(NearestTarget->GetCrossHairLock());
         }
      }
      else {
         TargetLocked = false;
      }
   }
}



void ABFPCharacterPlayable::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
   // Set up gameplay key bindings
   check(PlayerInputComponent);
   PlayerInputComponent->BindAction("Run", IE_Pressed, this, &ABFPCharacterPlayable::Run);
   PlayerInputComponent->BindAction("Run", IE_Released, this, &ABFPCharacterPlayable::StopRunning);

   PlayerInputComponent->BindAxis("MoveForward", this, &ABFPCharacterPlayable::MoveForward);
   PlayerInputComponent->BindAxis("MoveRight", this, &ABFPCharacterPlayable::MoveRight);

   // We have 2 versions of the rotation bindings to handle different kinds of devices differently
   // "turn" handles devices that provide an absolute delta, such as a mouse.
   // "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick

   PlayerInputComponent->BindAxis("Turn", this, &ABFPCharacterPlayable::TurnAxisY);


   //PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
   PlayerInputComponent->BindAxis("TurnRate", this, &ABFPCharacterPlayable::TurnAtRate);
   PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
   PlayerInputComponent->BindAxis("LookUpRate", this, &ABFPCharacterPlayable::LookUpAtRate);

   // handle touch devices
   PlayerInputComponent->BindTouch(IE_Pressed, this, &ABFPCharacterPlayable::TouchStarted);
   PlayerInputComponent->BindTouch(IE_Released, this, &ABFPCharacterPlayable::TouchStopped);

   // Le systeme de combat
   PlayerInputComponent->BindAction("Attack", IE_Pressed, this, &ABFPCharacter::OnClickLeft);
   PlayerInputComponent->BindAction("HeavyAttack", IE_Pressed, this, &ABFPCharacter::HeavyAttackStart);
   // PlayerInputComponent->BindAction("Block", IE_Pressed, this, &ABFPCharacter::BlockingStart);
   PlayerInputComponent->BindAction("Block", IE_Pressed, this, &ABFPCharacterPlayable::OnClickRight);
   PlayerInputComponent->BindAction("Block", IE_Released, this, &ABFPCharacter::BlockingStop);
   PlayerInputComponent->BindAction("Dodge", IE_Pressed, this, &ABFPCharacter::DodgingStart);

   //Le systeme de lock
   PlayerInputComponent->BindAction("LockTarget", IE_Pressed, this, &ABFPCharacterPlayable::LockEnemy);


   //Apparition de la map
   PlayerInputComponent->BindAction("InteractiveMapApparition", IE_Pressed, this, &ABFPCharacterPlayable::ApparitionInteractiveMap);
   PlayerInputComponent->BindAction("InteractiveMapApparition", IE_Released, this, &ABFPCharacterPlayable::DisparitionInteractiveMap);

   //Le systeme de menu pour sortir de la partie
   PlayerInputComponent->BindAction("ExitMenu", IE_Pressed, this, &ABFPCharacterPlayable::ExitGameMenu);

   //PlayerInputComponent->BindAction("Spell1", IE_Pressed, this, &ABFPCharacterPlayable::Spell1);
   PlayerInputComponent->BindAction("Spell1", IE_Pressed, this, &ABFPCharacterPlayable::TryCastSpellWithIndice<0>);
   PlayerInputComponent->BindAction("Spell2", IE_Pressed, this, &ABFPCharacterPlayable::TryCastSpellWithIndice<1>);
   PlayerInputComponent->BindAction("Spell3", IE_Pressed, this, &ABFPCharacterPlayable::TryCastSpellWithIndice<2>);
   PlayerInputComponent->BindAction("Spell4", IE_Pressed, this, &ABFPCharacterPlayable::TryCastSpellWithIndice<3>);
   PlayerInputComponent->BindAction("Spell5", IE_Pressed, this, &ABFPCharacterPlayable::TryCastSpellWithIndice<4>);
   PlayerInputComponent->BindAction("Spell6", IE_Pressed, this, &ABFPCharacterPlayable::TryCastSpellWithIndice<5>);

   // Cheatcode
   PlayerInputComponent->BindAction("UnGhost", IE_Pressed, this, &ABFPCharacterPlayable::OnPressedP);
}


void ABFPCharacterPlayable::Spell1() {
   ABFP_PlayerController* controller = GetController<ABFP_PlayerController>();
   controller->Spell1RPC(GetActorTransform(), Name);
}

void ABFPCharacterPlayable::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
   Jump();
}

void ABFPCharacterPlayable::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
   StopJumping();
}

void ABFPCharacterPlayable::OnClickLeft() {
   //on s'assure qu'on est bien dans le jeu et non dans le Lobby
   if (IsValid(GetController<ABFP_PlayerController>())) {
      if (!IsDead() && !bGhosted) {
         if (isInInteractiveMap) {
            if (MoveButtonClicked) {
               MoveActionFromInteractiveMap();
               MoveButtonClicked = false;
               MoveClickIsEnable = false;
            }
            else {
               ABFP_PlayerController* controller = GetController<ABFP_PlayerController>();
               ABFPHUD* HUD = Cast<ABFPHUD>(controller->GetHUD());
               HUD->HideStrategyWheel();
               HUD->HideStrategyButtons();
               HUD->HidePositionIndicators();
               MoveClickIsEnable = false;
               HUD->LieutenantChoisi = false;
            }

         }
         else {
            SimpleAttack();
         }
      }
   }
}

void ABFPCharacterPlayable::OnPressedP_Implementation() {
   GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
   SetUnGhosted();
}
bool ABFPCharacterPlayable::OnPressedP_Validate() {
   return true;
}

void ABFPCharacterPlayable::OnClickRight() {
   if (isInInteractiveMap) {
      if (MoveClickIsEnable) {
         MoveActionFromInteractiveMap();
         MoveButtonClicked = false;
         MoveClickIsEnable = false;
      }
   }
   else {
      BlockingStart();
   }
}


void ABFPCharacterPlayable::TurnAtRate(float Rate)
{
   // calculate delta for this frame from the rate information
   AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ABFPCharacterPlayable::LookUpAtRate(float Rate)
{
   // calculate delta for this frame from the rate information
   AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}
void ABFPCharacterPlayable::TurnAxisY(float Rate) {
   if (!TargetLocked) {
      AddControllerYawInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
   }
}


void ABFPCharacterPlayable::MoveActionFromInteractiveMap()
{
   APlayerController* MyController = GetWorld()->GetFirstPlayerController();
   float locationX, locationY;
   MyController->GetMousePosition(locationX, locationY);

   // Taille du monde en jeu
   float worldSizeX = 28300 * 2.0f;// landscape->GetActorScale().X;
   float worldSizeY = 28300 * 2.0f;// landscape->GetActorScale().Y;

   // Taille du screen
   int screenSizeX, screenSizeY;
   MyController->GetViewportSize(screenSizeX, screenSizeY);

   float gameLocationX = locationX * (worldSizeX / screenSizeX) - worldSizeX / 2;
   float gameLocationY = locationY * (worldSizeY / screenSizeY) - worldSizeY / 2;
   FVector position{ gameLocationX , gameLocationY, 0.0f };

   FVector pointImpact = position;

   FHitResult res;
   FVector start = FVector{ gameLocationX , gameLocationY, -5000.0f };
   FVector end = FVector{ gameLocationX , gameLocationY, +5000.0f };
   ECollisionChannel collisionChannel = ECollisionChannel::ECC_Visibility;
   const FCollisionQueryParams collisionQueryParams;
   const FCollisionResponseParams collisionResponseParams;
   auto world = GetWorld();
   bool resFromQuery = world->LineTraceSingleByChannel(res, start, end, collisionChannel, collisionQueryParams, collisionResponseParams);
   if (resFromQuery) {
      pointImpact = res.ImpactPoint;
      // MY_LOG_UI(TEXT("Point impact : x = %f, y = %f, z = %f"), pointImpact.X, pointImpact.Y, pointImpact.Z);
   }


   MY_LOG_UI(TEXT("Position du clique = %f, %f"), locationX, locationY);
   ABFP_PlayerController* controller = GetController<ABFP_PlayerController>();
   ABFPHUD* HUD = Cast<ABFPHUD>(controller->GetHUD());
   HUD->AffichageRedArrow(locationX, locationY);

   // MY_LOG_UI(TEXT("C'est le move !!!"));
   ABFPCharacterLieutenant* lieutenantChoisi = HUD->GetLieutenantChoisi();
   MY_LOG_UI(TEXT("LieutenantChoisi : x = %f, y = %f, z = %f"), lieutenantChoisi->GetActorLocation().X,
      lieutenantChoisi->GetActorLocation().Y,
      lieutenantChoisi->GetActorLocation().Z);
   //HUD->addPositionIndicator(locationX,locationY);

   // pointImpact.Z += 50;
   FTransform trans = FTransform(pointImpact);
   MY_LOG_UI(TEXT("Position ajout�e : x = %f, y = %f, z = %f"), trans.GetLocation().X, trans.GetLocation().Y, trans.GetLocation().Z);
   // squad->getManeuver()->AddTargetPoint(trans);
   controller->AddTargetPointRPC(trans, HUD->GetLieutenantChoisi());
   MY_LOG_UI(TEXT("Position ajout�e : x = %f, y = %f, z = %f"), trans.GetLocation().X, trans.GetLocation().Y, trans.GetLocation().Z);


   HUD->HideStrategyWheel();
   HUD->HideStrategyButtons();
   //HUD->HidePositionIndicators();
}

void ABFPCharacterPlayable::ShowActionWheelInteractiveMap()
{
   ABFP_PlayerController* controller = GetController<ABFP_PlayerController>();
   ABFPHUD* HUD = Cast<ABFPHUD>(controller->GetHUD());
   HUD->HidePositionIndicators();
   MoveClickIsEnable = true;
   HUD->ShowStrategyWheel();
   HUD->AfficherLignes = true;
   HUD->LieutenantChoisi = true;
}

void ABFPCharacterPlayable::ApparitionInteractiveMap()
{
   isInInteractiveMap = true;
   ABFP_PlayerController* controller = GetController<ABFP_PlayerController>();
   ABFPHUD* HUD = Cast<ABFPHUD>(controller->GetHUD());
   MY_LOG_NETWORK(TEXT("ApparitionInteractiveMap HUD %u"), IsValid(HUD));
   HUD->AffichageMapInteractive();
}

void ABFPCharacterPlayable::DisparitionInteractiveMap()
{
   ABFP_PlayerController* controller = GetController<ABFP_PlayerController>();
   ABFPHUD* HUD = Cast<ABFPHUD>(controller->GetHUD());
   isInInteractiveMap = false;
   InteractiveMap = false;
   HUD->AfficherLignes = false;
   controller->bShowMouseCursor = false;
   HUD->DisparitionMapInteractive();
}




bool ABFPCharacterPlayable::GetIsInInteractiveMap()
{
   return isInInteractiveMap;
}


void ABFPCharacterPlayable::LockEnemy()
{
	ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
	//Mise 'a jour des cibles potentielles
	ABFP_PlayerController* controller = GetController<ABFP_PlayerController>();
	if (IsValid(controller)) {
		ABFPCharacterPlayable* joueur = Cast<ABFPCharacterPlayable>(controller->GetPawn());
		if (TargetLocked) {
			TargetLocked = false;
			NearestTarget->GetCrossHairLock()->SetHiddenInGame(true);
			return;
		}
		// Heros
		if (joueur->getCharacterAffiliation() == Affiliation::GOOD) {
			FoundActors.Empty();
			FoundActors.Append(gameState->GetAllMechant());
		}
		else if ((joueur->getCharacterAffiliation() == Affiliation::EVIL)) {//DAVROS
			FoundActors.Empty();
			FoundActors.Append(gameState->GetAllJoueursAllies());
		}
		if (FoundActors.Num() > 0) {
			minScore = ComputeScoreForLock(FoundActors[0]);
			float minDistance = FVector::Dist(FoundActors[0]->GetActorLocation(), GetActorLocation());

			FVector vecUnitCam = this->GetFollowCamera()->GetForwardVector();
			float xCam = this->GetFollowCamera()->GetComponentLocation().X;
			float yCam = this->GetFollowCamera()->GetComponentLocation().Y;
			float zCam = this->GetFollowCamera()->GetComponentLocation().Z;

            for (int i = 0; i != FoundActors.Num(); i++)
            {
                if (IsValid(FoundActors[i])) {
                    float score = ComputeScoreForLock(FoundActors[i]);
                    float distance = FVector::Dist(FoundActors[i]->GetActorLocation(), GetActorLocation());
                    if (distance <= MinimumDistanceToEnable && score <= minScore) {
                        minScore = score;
                        minDistance = distance;
                        NearestTarget = Cast<ABFPCharacter>(FoundActors[i]);
                    }
                }
            }

			if (minDistance < MinimumDistanceToEnable && IsValid(NearestTarget))
			{
				DRAW_DEBUG_LINE(GetWorld(), GetActorLocation(), GetActorLocation() + GetFollowCamera()->GetForwardVector() * 1000.0f, FColor::Red, true, -1.0F, (uint8)'\000', 10.0F);
				TargetLocked = true;
				LockCamera(NearestTarget);
				NearestTarget->GetCrossHairLock()->SetHiddenInGame(false);
			}
		}
	}
}

void ABFPCharacterPlayable::LockCamera(ABFPCharacter* NearestTarget)
{
   //A Tester
   if (IsValid(NearestTarget)) {
      FRotator current = this->GetController()->GetControlRotation();
      FVector posCamera = this->GetFollowCamera()->GetComponentLocation();
      FVector posTarget = NearestTarget->GetActorLocation();
      FRotator target = UKismetMathLibrary::FindLookAtRotation(posCamera, posTarget);
      FRotator temp = FMath::RInterpTo(current, target, GetWorld()->DeltaTimeSeconds * 2, 100.0f);
      FRotator finale = FRotator(current.Pitch, temp.Yaw, current.Roll);
      this->GetController()->SetControlRotation(finale);
   }

}

void ABFPCharacterPlayable::MoveActionInteractiveMap()
{
   MoveButtonClicked = true;
   ABFP_PlayerController* controller = GetController<ABFP_PlayerController>();
   ABFPHUD* HUD = Cast<ABFPHUD>(controller->GetHUD());
   HUD->HideStrategyButtons();
}

void ABFPCharacterPlayable::StopMoveInteractiveMap()
{
   // MY_LOG_UI(TEXT("C'est le move !!!"));
   ABFP_PlayerController* controller = GetController<ABFP_PlayerController>();
   ABFPHUD* HUD = Cast<ABFPHUD>(controller->GetHUD());
   ABFPCharacterLieutenant* lieutenantChoisi = HUD->GetLieutenantChoisi();
   MY_LOG_UI(TEXT("JE SUIS DANS LE STOP"));
   HUD->Stopped = true;
   FVector position{ lieutenantChoisi->GetActorLocation().X , lieutenantChoisi->GetActorLocation().Y, lieutenantChoisi->GetActorLocation().Z };
   controller->StopMovementRPC(FTransform(position), HUD->GetLieutenantChoisi());
   //squad->getManeuver()->StopMovementSquad(FTransform(position));
   //MY_LOG_UI(TEXT("Position ajout�e : x = %f, y = %f, z = %f"), position.X, position.Y, position.Z);


   HUD->HideStrategyWheel();
   HUD->HideStrategyButtons();
   HUD->HidePositionIndicators();
   HUD->DeleteAllPositionIndicator();
   HUD->LieutenantChoisi = false;


}

void ABFPCharacterPlayable::SetStrategieAgressiveInteractiveMap() {
   SetStrategieInteractiveMap(ManeuverType::AGRESSIVE);
}
void ABFPCharacterPlayable::SetStrategieDefensiveInteractiveMap() {
   SetStrategieInteractiveMap(ManeuverType::DEFENSIVE);
}
void ABFPCharacterPlayable::SetStrategieAssaultInteractiveMap() {
   SetStrategieInteractiveMap(ManeuverType::ASSAULT);
}
void ABFPCharacterPlayable::SetStrategieSurvieInteractiveMap() {
   SetStrategieInteractiveMap(ManeuverType::SURVIE);
}

void ABFPCharacterPlayable::SetStrategieInteractiveMap(ManeuverType maneuverType) {
   ABFP_PlayerController* controller = GetController<ABFP_PlayerController>();
   ABFPHUD* HUD = Cast<ABFPHUD>(controller->GetHUD());
   ABFPCharacterLieutenant* lieutenantChoisi = HUD->GetLieutenantChoisi();

	FVector positionLieutenant = lieutenantChoisi->GetActorLocation();
    FString str("On vient de changer de manoeuvre. Son type est = ");
    str.AppendInt(static_cast<int>(maneuverType));
    DEBUG_SCREEN(-1, 5.0f, FColor::Cyan, str);
    MY_LOG_IA(TEXT("On vient de changer de manoeuvre. Son type est = %d"), static_cast<int>(maneuverType));
	controller->SetStrategieRPC(maneuverType, lieutenantChoisi);

   HUD->HideStrategyWheel();
   HUD->HideStrategyButtons();
   HUD->HidePositionIndicators();
   HUD->DeleteAllPositionIndicator();
   HUD->LieutenantChoisi = false;
}


void ABFPCharacterPlayable::ExitGameMenu() {
   MY_LOG_NETWORK(TEXT("ABFPCharacterPlayable ExitGameMenu"));
   this->GetController<ABFP_PlayerController>()->ExitGameMenu();
}

void ABFPCharacterPlayable::UnRegisterToGameManager() {
   GetWorld()->GetGameState<ABFP_GameState>()->UnRegister(this);
}

void ABFPCharacterPlayable::RegisterToGameManager() {
   GetWorld()->GetGameState<ABFP_GameState>()->Register(this);
}

void ABFPCharacterPlayable::OnRep_SetGhostState()
{
   if (bGhosted)
   {
      SetGhosted();
      MY_LOG_GAME(TEXT("Call for : void ABFPCharacterPlayable::OnRep_SetGhostState() with bGhosted == TRUE"));
      TWeakObjectPtr<ABFP_PlayerController> lPlayerController = Cast<ABFP_PlayerController>(GetController());
      if (lPlayerController.IsValid() && IsValid(lPlayerController->SoundActorInstance)) {
         MY_LOG_GAME(TEXT("PLAYER CONTROLLER VALID, FLAG DEATH CALLED"));
         lPlayerController->SoundActorInstance->SoundStateFlag_Broadcast(lPlayerController->SoundActorInstance->SoundStateFlag_Death, ESoundState::SS_Death);
      }
   }
   else
   {
      SetUnGhosted();
      MY_LOG_GAME(TEXT("Call for : void ABFPCharacterPlayable::OnRep_SetGhostState() with bGhosted == FALSE"));
      TWeakObjectPtr<ABFP_PlayerController> lPlayerController = Cast<ABFP_PlayerController>(GetController());
      if (lPlayerController.IsValid() && IsValid(lPlayerController->SoundActorInstance)) {
         MY_LOG_GAME(TEXT("PLAYER CONTROLLER VALID, FLAG REVIVE CALLED"));
         lPlayerController->SoundActorInstance->SoundStateFlag_Broadcast(lPlayerController->SoundActorInstance->SoundStateFlag_Revive, ESoundState::SS_Revive);
      }
   }
}
float ABFPCharacterPlayable::ComputeScoreForLock(ABFPCharacter * target) const {
   float coefDistance = 1.0f;
   float coefForward = 30.0f;
   FVector posTarget = target->GetActorLocation();
   FVector posSelf = GetActorLocation();

   float d = FVector::Dist(posSelf, posTarget);
   d *= coefDistance;

   FVector forward = GetFollowCamera()->GetForwardVector();
   float angle = Util::AngleBetweenVectorsInXYPlane(forward, posTarget - posSelf);
   angle = (angle >= 180.0f) ? 360.0f - angle : angle; // on passe de 0-360 à 0-180-0
   float f = abs(angle) * coefForward;

   return d + f;
}
void ABFPCharacterPlayable::OnRep_VraiMort() {
   MY_LOG_NETWORK(TEXT("ABFPCharacterPlayable::SetDead begin "));
   ABFP_PlayerController* controller = GetController<ABFP_PlayerController>();
   MY_LOG_NETWORK(TEXT("ABFPCharacterPlayable::SetDead controller %u "), IsValid(controller));
   if (IsValid(controller)) {
      ABFPHUD* HUD = Cast<ABFPHUD>(controller->GetHUD());
      MY_LOG_NETWORK(TEXT("ABFPCharacterPlayable::SetDead HUD %u "), IsValid(HUD));
      HUD->SetDead(bVraiMort);
   }
}

void ABFPCharacterPlayable::PlayFlash() {
   MY_LOG_NETWORK(TEXT("ABFPCharacterPlayable::PlayFlash begin "));
   ABFP_PlayerController* controller = GetController<ABFP_PlayerController>();
   MY_LOG_NETWORK(TEXT("ABFPCharacterPlayable::PlayFlash controller %u "), IsValid(controller));
   if (IsValid(controller)) {
      ABFPHUD* HUD = Cast<ABFPHUD>(controller->GetHUD());
      MY_LOG_NETWORK(TEXT("ABFPCharacterPlayable::PlayFlash HUD %u "), IsValid(HUD));
      HUD->PlayRedFlash();
   }
}