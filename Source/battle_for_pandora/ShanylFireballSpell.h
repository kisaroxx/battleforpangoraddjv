// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Spell.h"
#include "Engine/TriggerCapsule.h"
#include "Engine/StaticMeshActor.h"
#include "ShanylFireballSpell.generated.h"

UCLASS()
class BATTLE_FOR_PANDORA_API AShanylFireballSpell : public ASpell
{
    GENERATED_BODY()

    /* Le rayon de trigger de la "sphere" de collision. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float distanceDeTrigger = 100.0f;

    /* La puissance (donc la vitesse) du spell. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float vitesseSpell = 5000.0f;

    /* La duree du spell. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float dureeSpell = 0.6f;

    /* Le spell a creer lors de l'explosion. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    TSubclassOf<ASpell> explosionSpellClass;

    FVector directionInitiale;

    bool bHasExplode = false;

protected:
   AShanylFireballSpell(const FObjectInitializer & ObjectInitializer);

   // Called when the game starts or when spawned
   virtual void BeginPlay() override;

public:
   // Called every frame
   virtual void Tick(float DeltaTime) override;

   UFUNCTION()
   void OnActorOverlap(AActor * OverlappedActor, AActor * OtherActor);

private:
    UFUNCTION()
    void GenerateExplosion();
};