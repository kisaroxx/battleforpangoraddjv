#include "Poussee.h"

FPoussee::FPoussee(float puissance, float duree) : puissance{ puissance }, duree{ duree } {}
FPoussee::FPoussee() : puissance{ 0.0f }, duree{ 0.0f } {}

float FPoussee::GetPuissance() const {
    return puissance;
}
float FPoussee::GetDuree() const {
    return duree;
}
void FPoussee::SetDirection(FVector direction_) {
    direction = direction_;
    direction.Normalize();
}
void FPoussee::Start(UWorld* world) {
    startTime = UGameplayStatics::GetRealTimeSeconds(world);
}
bool FPoussee::IsEnded(UWorld* world) {
    return (UGameplayStatics::GetRealTimeSeconds(world) - startTime) > duree;
}
void FPoussee::ApplyPoussee(APawn* pawn, float deltaTime) {
    if ((pawn->GetController()!= NULL) && (puissance != 0.0f))
    {
        // find out which way is forward
        const FRotator Rotation = pawn->GetController()->GetControlRotation();
        const FRotator YawRotation(0, Rotation.Yaw, 0);

        float value = puissance * deltaTime;

        // get forward vector
        FVector pos = pawn->GetTransform().GetLocation();
        pos = pos + value * direction;
        pawn->SetActorLocation(pos);
    }
}


