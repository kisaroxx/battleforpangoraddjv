// Fill out your copyright notice in the Description page of Project Settings.

#include "FuirSelected_BTTaskNode.h"
#include "SquadSbireController.h"
#include "Logger.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Maneuver.h"
#include "ManeuverDefensive.h"
#include "BFPCharacterPlayable.h"
#include "SquadDescriptor.h"
#include "MoveCommand.h"

UFuirSelected::UFuirSelected(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    NodeName = "FuirSelected";
}

EBTNodeResult::Type UFuirSelected::ExecuteTaskImpl(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, AManeuver* maneuvre) {
    if (IsValid(maneuvre->GetJoueurSelected())) {
        MY_LOG_IA(TEXT("%s MODE = FuirSelected"), *maneuvre->GetDescriptor()->GetName());
        maneuvre->FuirActor(maneuvre->GetJoueurSelected());
        return EBTNodeResult::InProgress;
    }
    else {
        return EBTNodeResult::Failed;
    }
}

FString UFuirSelected::GetStaticDescription() const
{
    return TEXT("Permet de s'eloigner le plus possible du personnage selectionne.");
}

void UFuirSelected::OnMessage(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, FName Message, int32 RequestID, bool bSuccess) {
    Super::OnMessage(OwnerComp, NodeMemory, Message, RequestID, bSuccess);

    // On distingue les messages
    AManeuver* maneuvre = Cast<AManeuver>(OwnerComp.GetOwner());
    switch (maneuvre->GetLastMessage()->GetType()) {
    case MessageType::IN_RUSH_RANGE:
        FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
        break;
    case MessageType::OUT_OF_VISION_RANGE:
    case MessageType::OUT_OF_AGRESSION_RANGE:
        FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
        break;
    default:
        break;
    }
}
