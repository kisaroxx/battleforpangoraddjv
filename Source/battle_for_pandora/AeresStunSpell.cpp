// Fill out your copyright notice in the Description page of Project Settings.

#include "AeresStunSpell.h"
#include "BFPCharacter.h"
#include "Public/TimerManager.h"
#include "BFP_GameState.h"

AAeresStunSpell::AAeresStunSpell(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    PrimaryActorTick.bCanEverTick = false;
}

void AAeresStunSpell::BeginPlay() {
   Super::BeginPlay();
   ABFPCharacter* aeres = Cast<ABFPCharacter>(GetOwner());
   if (IsValid(aeres)) {
       if (GetWorld()->IsServer()) {
           ABFP_GameState* gs = GetWorld()->GetGameState<ABFP_GameState>();
           TArray<ABFPCharacter*> mechants = gs->GetAllMechant();
           mechants = gs->GetAllInDistanceFrom(mechants, GetTransform(), rangeSpell);

           for (int i = 0; i < mechants.Num(); i++) {
               mechants[i]->TakeDamage(degats, dureeStun, FPoussee{}, false, false);
           }
       }

       DestroyIn(dureeStun);
   } else {
       DEBUG_SCREEN(-1, 5.f, FColor::Red, TEXT("Impossible de recuperer l'owner dans kiaraSpell !!"));
   }
}
