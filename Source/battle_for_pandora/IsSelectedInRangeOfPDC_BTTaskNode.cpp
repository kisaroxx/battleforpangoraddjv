// Fill out your copyright notice in the Description page of Project Settings.

#include "IsSelectedInRangeOfPDC_BTTaskNode.h"
#include "SquadSbireController.h"
#include "Logger.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Maneuver.h"
#include "BFPCharacterPlayable.h"
#include "SquadDescriptor.h"

UIsSelectedInRangeOfPDC::UIsSelectedInRangeOfPDC(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    NodeName = "IsSelectedInRangeOfPDC";
    distance = 2500.0f;
}

EBTNodeResult::Type UIsSelectedInRangeOfPDC::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8*
    NodeMemory)
{
    Super::ExecuteTask(OwnerComp, NodeMemory);

    // On recupere la manoeuvre !
    AManeuver* maneuvre = Cast<AManeuver>(OwnerComp.GetOwner());
    FVector posSelected = maneuvre->GetJoueurSelected()->GetActorLocation();
    FVector posPDC = maneuvre->GetDescriptor()->GetCheckpointAssociated()->GetActorLocation();
    float d = FVector::DistSquared(posSelected, posPDC);

    if (d <= distance * distance) {
        return EBTNodeResult::Succeeded;
    } else {
        return EBTNodeResult::Failed;
    }
}

FString UIsSelectedInRangeOfPDC::GetStaticDescription() const
{
    return TEXT("Permet de savoir si le personnage selectionne est assez proche du PDC associe.");
}