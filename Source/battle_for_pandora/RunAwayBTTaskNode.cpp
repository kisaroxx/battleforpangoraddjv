// Fill out your copyright notice in the Description page of Project Settings.

#include "RunAwayBTTaskNode.h"
#include "SquadSbireController.h"


URunAwayBTTaskNode::URunAwayBTTaskNode()
{
   // Le nom que prendra le noeud dans le BT
   NodeName = "RunAway";
   // Cette t�che appelle TickTask
   bNotifyTick = true;
}

/* Sera appel�e au d�marrage de la t�che et devra retourner Succeeded, Failed ou InProgress */
EBTNodeResult::Type URunAwayBTTaskNode::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8*
   NodeMemory)
{
   // Obtenir un pointeur sur notre AIEnemyController
   ASquadSbireController* sbireController = Cast<ASquadSbireController>(OwnerComp.GetOwner());

   // Nous pr�parons le r�sultat de la t�che. Elle doit retourner InProgress
   //EBTNodeResult::Type NodeResult = EBTNodeResult::InProgress;

   EPathFollowingRequestResult::Type MoveToActorResult = sbireController->RunAway();

   if (MoveToActorResult == EPathFollowingRequestResult::Failed) {
      return EBTNodeResult::Failed;
   } else if (MoveToActorResult == EPathFollowingRequestResult::AlreadyAtGoal) {
      return EBTNodeResult::Succeeded;
   }
   return EBTNodeResult::InProgress;
}

/* Sera appel�e constamment tant que la t�che n'est pas finie (tant que ExecuteTask retourne
InProgress) */
void URunAwayBTTaskNode::TickTask(class UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) {

   // Obtenir un pointeur sur notre AIEnemyController
   ASquadSbireController* sbireController = Cast<ASquadSbireController>(OwnerComp.GetOwner());

   if (sbireController->CheckTargetActorNearby() && !sbireController->HasSquad()) {
      EPathFollowingRequestResult::Type MoveToActorResult = sbireController->RunAway();
      if (MoveToActorResult == EPathFollowingRequestResult::AlreadyAtGoal)
      {
         FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
      }
      FinishLatentTask(OwnerComp, EBTNodeResult::InProgress);
   } else {
      EPathFollowingRequestResult::Type MoveToActorResult = sbireController->MoveTo(FAIMoveRequest(sbireController->runAwayGoal));
      if (MoveToActorResult == EPathFollowingRequestResult::AlreadyAtGoal)
      {
         FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
      }
      FinishLatentTask(OwnerComp, EBTNodeResult::InProgress);
   }

   
}

/** Retourne une chaine de description pour la t�che. Ce texte appara�tre dans le BT */
FString URunAwayBTTaskNode::GetStaticDescription() const
{
   return TEXT("Fuite du Sbire");
}

