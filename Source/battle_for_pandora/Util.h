// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <utility>

class Util {
public:
    /* Renvoie l'angle en degree entre 0 et 360. */
    static float AngleBetweenVectors(const FVector& v1, const FVector& v2);
	 static float AngleBetweenVectorsInXYPlane(const FVector& v1, const FVector& v2);
	 static float AngleBetweenVectors2D(const FVector2D& v1, const FVector2D& v2);
	 static FVector ProjectInXYPlane(const FVector& v1);
    static FVector PolaireToCartesian(float angle, float rayon); // La hauteur vaut 0
    static float ToRad(float angleDegree); // Entre 0 et 2pi (with modulo)
    static float ToDegree(float angleRadian); // Entre 0 et 360 (with modulo)
    static int Random(int minInclu, int maxInclu);
    static float Random(float min, float max);
	template <class T> static int Random(int minInclu, int maxInclu, const TArray<T>& nonTirable);
    template <class T> static T ModF(T value, T diviseur); // Enleve autant de fois que possible diviseur � value, puis retourne le reste !
    //static void SpawnDebugActor(UWorld* world, FVector place, float disparitionTime = -1.0f); // Fait spawner une sphere de couleur � l'endroit specifier pour permettre de debugger. Si le disparitionTime != -1, alors l'objet sera delete dans disparitionTime secondes.
    static std::pair<int, int> GetSecAndMilli(float dureeEnSecondes); // Les secondes en premier, les millisecondes en deuxieme
};

template<class T>
inline T Util::ModF(T value, T diviseur) {
    while (value - diviseur >= 0) {
        value -= diviseur;
    }
    return value;
}

template<class T>
inline int Util::Random(int minInclu, int maxInclu, const TArray<T>& nonTirable) {
	int val = -1;
	while (val < 0) {
		int randomInt = Util::Random(minInclu, maxInclu);
		if (!nonTirable.Contains(randomInt)) {
			val = randomInt;
		}
		else if (nonTirable.Num() == (minInclu - maxInclu + 1)) {
			// s'il est plein on agit comme s'il etait vide
			val = randomInt;
		}
	}
	return val;
}