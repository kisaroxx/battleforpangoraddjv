#pragma once
#include <vector>
#include <string>
#include <Runtime/Engine/Classes/GameFramework/Actor.h>
//#include <iomanip>
//#include <type_traits>

class LandscapeHeights
{

   static const int demiSizeLandscape = 25200;
   static const int pas = 100;
   static const int nbHeights = (demiSizeLandscape * 2 / pas + 1) * (demiSizeLandscape * 2 / pas + 1);
   std::vector<short> landscapeHeights{};

public:
   LandscapeHeights();
   ~LandscapeHeights();

   void generateLandscapeHeights(UWorld* world);
   void WriteLandscapeHeights(std::string filePath);
   void WriteLandscapeHeights2(std::string filePath);
   void ReadLandscapeHeights(std::string filePath, UWorld* world);
   void ReadLandscapeHeights2(std::string filePath, UWorld* world);
   
   float getHeight(float x, float y);

   int getDemiSizeLandscape();

 
private :

   template <class T> T normaliser(T val) {
      return val;
   }
   template<class T>
   char * serialiser(char *dest, T t) {
      //static_assert(is_trivially_copyable_v<T>);
      t = normaliser(t);
      auto p = reinterpret_cast<const char*>(&t);
      copy(p, p + sizeof t, dest); // Ecrit les 4 premiers bytes de f
      return dest + sizeof t;
   }

   template <class T> T denormaliser(T val) {
      return val;
   }
   template<class T>
   const char *deserialiser(T &t, const char *src) {
      //static_assert(is_trivially_copyable_v<T>);
      t = *reinterpret_cast<const T*>(src);
      t = denormaliser(t);
      return src + sizeof t;
   }

   char * serialiser(char *dest);
   const char * deserialiser(const char *src);
};

