// Fill out your copyright notice in the Description page of Project Settings.

#include "KiaraDashSpell.h"
#include "BFPCharacter.h"
#include "Public/TimerManager.h"

AKiaraDashSpell::AKiaraDashSpell(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    PrimaryActorTick.bCanEverTick = true;
}


void AKiaraDashSpell::BeginPlay() {
   Super::BeginPlay();
   ABFPCharacter* kiara = Cast<ABFPCharacter>(GetOwner());
   if (IsValid(kiara)) {
       FPoussee poussee(vitesseDash, dureeDash);
       poussee.SetDirection(kiara->GetTransform().GetRotation().GetForwardVector());
       kiara->AddPoussee(poussee);

       DestroyIn(dureeDash);
   } else {
       DEBUG_SCREEN(-1, 5.f, FColor::Red, TEXT("Impossible de recuperer l'owner dans kiaraSpell !!"));
   }
}

void AKiaraDashSpell::Tick(float DeltaTime) {
   Super::Tick(DeltaTime);
   ABFPCharacter* kiara = Cast<ABFPCharacter>(GetOwner());
   if (IsValid(kiara)) {
       SetActorLocation(kiara->GetActorLocation());
   }
}
