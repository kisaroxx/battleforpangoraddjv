// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"

#include "Components/WidgetComponent.h"
#include "BFPCharacter.h"
#include "CheckpointTriggerSphere.h"

#include "BFPHUD.generated.h"


USTRUCT()
struct FCiblesLieutenants
{
    GENERATED_USTRUCT_BODY()

   
    UPROPERTY()
    TArray<UUserWidget*> Widgets;
    
    UPROPERTY()
    TArray<FVector2D> Positions;
    FCiblesLieutenants()
    {
    }
};


/**
 *
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FAnimationRedArrow); 
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FApparitionProgressBar);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDisparitionProgressBar);

UCLASS()
class BATTLE_FOR_PANDORA_API ABFPHUD : public AHUD
{
    GENERATED_BODY()

public:
    ABFPHUD(const FObjectInitializer& ObjectInitializer);
    virtual void DrawHUD() override;

    void AffichagePourHeros(class ABFPCharacterPlayable* playerPawn);

    void AffichagePourDavros(class ABFPCharacterPlayable* playerPawn);

    void DessinerLignes();

    void DrawSpells();

	 void HideSpells();

    virtual void BeginPlay() override;

    void InitializeWidgets();

	void InitializeTimelineProgressBar();
	void AffichageMapInteractive();
	void DisparitionMapInteractive();

	void AffichageRedArrow(float positionx, float positiony);

	void AffichageProgressBarPDC(Affiliation CheckpointAffiliation);
	void DisparitionProgressBarPDC();

	UFUNCTION()
		void SetPDCValue();

	UFUNCTION()
		void SetPDCState();

	UFUNCTION()
		void UpdateProgressBarPDC(float PDCValueChange, Affiliation CheckpointAffiliation);

	UFUNCTION()
		void ChangeColorProgressBarPDC(FLinearColor Color);

	float CalculAngleRotationFlecheMap(ABFPCharacter* character, UUserWidget* widget);

	void CalculPositionFlecheMap(ABFPCharacterPlayable * character, UUserWidget* widgetfleche, UUserWidget* widgetHeros, FVector2D pos);

private:

    // Global Widgets
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "UI", meta = (AllowPrivateAccess = "true"))
        TSubclassOf<class UUserWidget>  InteractiveMap;
    UPROPERTY(EditAnywhere, Category = "UI")
        class UUserWidget* InteractiveMapWidget;

    //Died widgets
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "UI", meta = (AllowPrivateAccess = "true"))
        TSubclassOf<class UUserWidget> YouDied;
    UPROPERTY(EditAnywhere, Category = "UI")
        class UUserWidget* YouDiedWidget;

    //Win screen
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "UI", meta = (AllowPrivateAccess = "true"))
        TSubclassOf<class UUserWidget> WinScreen;
    UPROPERTY(EditAnywhere, Category = "UI")
        class UUserWidget* WinScreenWidget;

    //Lose screen
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "UI", meta = (AllowPrivateAccess = "true"))
        TSubclassOf<class UUserWidget> LooseScreen;
    UPROPERTY(EditAnywhere, Category = "UI")
        class UUserWidget* LooseScreenWidget;

   //Checkpoints
   UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "UI", meta = (AllowPrivateAccess = "true"))
   TSubclassOf<class UUserWidget> CPUncaptured;
   UPROPERTY(EditAnywhere, Category = "UI")
   TArray<class UUserWidget*> CPUncapturedWidget;

	//ProgressBar_PDC
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "UI", meta = (AllowPrivateAccess = "true"))
		TSubclassOf<class UUserWidget> ProgressBarPDC;
	UPROPERTY(EditAnywhere, Category = "UI")
		class UUserWidget* ProgressBarPDCWidget;


   /*UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "UI", meta = (AllowPrivateAccess = "true"))
   TSubclassOf<class UUserWidget> CPCapturedByHeros;
   UPROPERTY(EditAnywhere, Category = "UI")
   class UUserWidget* CPCapturedByHerosWidget;

   UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "UI", meta = (AllowPrivateAccess = "true"))
   TSubclassOf<class UUserWidget> CPCapturedByDavros;
   UPROPERTY(EditAnywhere, Category = "UI")
   class UUserWidget* CPCapturedByDavrosWidget;*/


   //redArrow
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "UI", meta = (AllowPrivateAccess = "true"))
		TSubclassOf<class UUserWidget>  RedArrow;
   UPROPERTY(EditAnywhere, Category = "UI")
      class UUserWidget* RedArrowWidget;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "UI", meta = (AllowPrivateAccess = "true"))
        TSubclassOf<class UUserWidget>  StrategyWheel;
    UPROPERTY(EditAnywhere, Category = "UI")
        class UUserWidget* StrategyWheelWidget;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "UI", meta = (AllowPrivateAccess = "true"))
        TSubclassOf<class UUserWidget>  StrategyButtons;
    UPROPERTY(EditAnywhere, Category = "UI")
        class UUserWidget* StrategyButtonsWidget;

    UPROPERTY(EditAnywhere, Category = "UI")
        TSubclassOf<class UUserWidget> HUDWidgetClass;
    UPROPERTY(EditAnywhere, Category = "UI")
        class UUserWidget* MainHUDWidget;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "UI", meta = (AllowPrivateAccess = "true"))
        TSubclassOf<class UUserWidget>  RedFlash;
    UPROPERTY(EditAnywhere, Category = "UI")
        class UUserWidget* RedFlashWidget;

    // Stockage
    UPROPERTY(EditAnywhere, Category = "UI")
        class UUserWidget* CurrentWidget;

    // Widgets Characters
    // Heros
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "UI", meta = (AllowPrivateAccess = "true"))
        TSubclassOf<class UUserWidget>  HerosIndicator;
    UPROPERTY(EditAnywhere, Category = "UI")
        TArray<UUserWidget*> HerosIndicatorWidgets;

    // Davros -> A changer TArray -> *
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "UI", meta = (AllowPrivateAccess = "true"))
        TSubclassOf<class UUserWidget> DavrosIndicator;
    UPROPERTY(EditAnywhere, Category = "UI")
        TArray<UUserWidget*> DavrosIndicatorWidgets;
    // Lieutenants
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "UI", meta = (AllowPrivateAccess = "true"))
        TSubclassOf<class UUserWidget>  LieutenantIndicator;
    UPROPERTY(EditAnywhere, Category = "UI")
        TArray<UUserWidget*> LieutenantIndicatorWidgets;
    // Positions de parcours pour lieutenants
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "UI", meta = (AllowPrivateAccess = "true"))
        TSubclassOf<class UUserWidget> PositionIndicator;

    UPROPERTY(EditAnywhere, Category = "UI")
        TArray<FCiblesLieutenants> ArrayCiblesLieutenants;
	 //fleche bleue pour les heros
	 UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "UI", meta = (AllowPrivateAccess = "true"))
		 TSubclassOf<class UUserWidget>  FlechePositionHerosIndicator;
	 UPROPERTY(EditAnywhere, Category = "UI")
		 TArray<UUserWidget*> FlechePositionHerosIndicatorWidgetArray;
	 //fleche rouge pour Davros
	 UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "UI", meta = (AllowPrivateAccess = "true"))
		 TSubclassOf<class UUserWidget>  FlechePositionDavrosIndicator;
	 UPROPERTY(EditAnywhere, Category = "UI")
		 TArray<UUserWidget*>  FlechePositionDavrosIndicatorWidgetArray;

    // Stockage
    UPROPERTY(EditAnywhere)
        class UUserWidget* lieutenantChoisiButton;

    UPROPERTY()
        bool isEndGame = false;
		bool bInitHUD1Time = false;



   UPROPERTY()
	   bool isDead = false;

	//ProgressBar PDC
	UPROPERTY(EditDefaultsOnly, Category = "PDC")
		UCurveFloat *PDCCurve;



	UPROPERTY(VisibleAnywhere, Category = "PDC")
		float PDCPercentage = 0.0f;

	UPROPERTY(VisibleAnywhere, Category = "PDC")
		float PreviousValuePDC = 0.0f;

	UPROPERTY(VisibleAnywhere, Category = "PDC")
		float PDCValue = 0.0f;

	float CurveFloatValue;
	float TimelineValue;

	bool changeColorAfterNeutralAffiliation = false;
	bool UpdateProgressBarTo0 = false;
	class	UTimelineComponent* MyTimeline;
	
	UPROPERTY(EditAnywhere)
		UTexture2D* PDCNeutre;
	UPROPERTY(EditAnywhere)
		UTexture2D* PDCEvil;
	UPROPERTY(EditAnywhere)
		UTexture2D* PDCGood;

	UPROPERTY(EditAnywhere)
		UTexture2D* ImageKiara;
	UPROPERTY(EditAnywhere)
		UTexture2D* ImageAeres;
	UPROPERTY(EditAnywhere)
		UTexture2D* ImageNeelf;
	UPROPERTY(EditAnywhere)
		UTexture2D* ImageShanyl;
	UPROPERTY(EditAnywhere)
		UTexture2D* ImageDavros;




    /* Le sprite du rond de couleur permettant de differencier les lieutenants. */
    UPROPERTY(EditAnywhere, Category = "SQUAD et PDC")
    TSubclassOf<class UUserWidget> lieutenantColorMarkClass;
    TArray<UUserWidget*> lieutenantColorMarkWidgets;

    void DrawMark(ABFPCharacterLieutenant* lieutenant);
    void DrawMark(ACheckpointTriggerSphere* checkpoint);

    /* Le sprite permettant d'afficher le nombre de sbires actuellement dans la squad. */
    UPROPERTY(EditAnywhere, Category = "SQUAD et PDC")
    TSubclassOf<class UUserWidget> lieutenantNombreSbireClass;
    TArray<UUserWidget*> lieutenantNombreSbireWidget;

    void DrawNbSbiresLieutenant(ABFPCharacterLieutenant* lieutenant);

public:
	UFUNCTION()
		void SetDead(bool vraiMort);
	UFUNCTION()
		void SetEndGameStatus(bool victory);


    UFUNCTION(BlueprintCallable)
        void SetLieutenantChoisiButton(class UUserWidget* lieutenantButton);

    UFUNCTION(BlueprintCallable)
        class UUserWidget* GetLieutenantChoisiButton();

    UFUNCTION(BlueprintCallable)
        class ABFPCharacterLieutenant* GetLieutenantChoisi();

	 UFUNCTION(BlueprintCallable)
		 float GetPDCPercentage();

    UPROPERTY(EditAnywhere)
    ABFPCharacterLieutenant* lieutenantChoisiCharacter;


    UFUNCTION()
        void Reset();

   UFUNCTION()
   void ResetCPs();

    UFUNCTION()
        void addHeros(ABFPCharacter* character);

    void addPositionIndicator(float positionX, float positionY);

    UFUNCTION()
        void addEvil(ABFPCharacter* character);
    UFUNCTION()
        void addLieutenant(ABFPCharacter* character);

   UFUNCTION()
   void addCP(ACheckpointTriggerSphere* CP);

    void RefreshPositionCiblesLieutenant(ABFPCharacterLieutenant * lieutenant);

    UFUNCTION()
        FVector2D fromWorldToMap(FVector vector);

    //UFUNCTION(BlueprintCallable)
    //	void StayInHUD();

    UFUNCTION(BlueprintCallable)
        void ShowStrategyWheel();

    UFUNCTION(BlueprintCallable)
        void HideStrategyWheel();


    UFUNCTION(BlueprintCallable)
        void HidePositionIndicators();

    UFUNCTION(BlueprintCallable)
        void ShowStrategyButtons();

    UFUNCTION(BlueprintCallable)
        void HideStrategyButtons();

	

    UFUNCTION()
        void RemoveWidgetFromHUD(class ABFPCharacter* character);

    void DeleteAllPositionIndicator();

    bool Stopped = false;

    bool AfficherLignes = false;

    bool LieutenantChoisi = false;
    void DeleteNextPositionIndicator(ABFPCharacterLieutenant* lieutenant, FTransform targetPoint);

	 bool bAllPicturesSet = false;

private:
    UPROPERTY(EditAnywhere, Category = "UI")
        class UWidgetAnimation *AnimApparition;

    UPROPERTY(EditAnywhere, Category = "UI")
        bool RedArrowAnimationFinie;

protected:

	UPROPERTY(BlueprintAssignable, Category = UI)
		FAnimationRedArrow AnimationRedArrow;

	UPROPERTY(BlueprintAssignable, Category = UI)
		FApparitionProgressBar ApparitionProgressBar;
	UPROPERTY(BlueprintAssignable, Category = UI)
		FDisparitionProgressBar DisparitionProgressBar;

	UPROPERTY(EditAnywhere, Category = "UI")
		UProgressBar *ProgressBar;

	UPROPERTY(EditAnywhere, Category = "UI")
		UProgressBar *ProgressBar_fond;

public:
	bool initSpawnFinished = false;

	UPROPERTY(BlueprintReadWrite, Category = UI)
	bool ProgressBarIsShow = false;

	UFUNCTION()
	void PlayRedFlash();
};