// Fill out your copyright notice in the Description page of Project Settings.

#include "MainMenuLevelScriptActor.h"
#include "Kismet/GameplayStatics.h"
#include "Network/GameInfoInstance.h"
#include "Logger.h"

void AMainMenuLevelScriptActor::BeginPlay() {
	Super::BeginPlay();
	TWeakObjectPtr<UWorld> world = GetWorld();
	if (!ensure(world.IsValid())) return;
	TWeakObjectPtr<UGameInfoInstance> gameInstanceRef = world->GetGameInstance<UGameInfoInstance>();
	if (!ensure(gameInstanceRef.IsValid())) return;
	if(!gameInstanceRef->IsDedicatedServerInstance()){
		MY_LOG_NETWORK(TEXT("AMainMenuLevelScriptActor BeginPlay CLIENT SHOW MAIN MENU"));
		gameInstanceRef->showMainMenuWidget(world->GetFirstPlayerController());
	}
}
