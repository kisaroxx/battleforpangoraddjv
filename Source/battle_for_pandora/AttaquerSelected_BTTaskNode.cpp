// Fill out your copyright notice in the Description page of Project Settings.

#include "AttaquerSelected_BTTaskNode.h"
#include "SquadSbireController.h"
#include "Logger.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Maneuver.h"
#include "BFPCharacterPlayable.h"
#include "SquadDescriptor.h"


UAttaquerSelected::UAttaquerSelected(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    NodeName = "AttaquerSelected";
}

EBTNodeResult::Type UAttaquerSelected::ExecuteTaskImpl(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, AManeuver* maneuvre) {
    MY_LOG_IA(TEXT("%s MODE = AttaquerSelected"), *maneuvre->GetDescriptor()->GetName());
    maneuvre->Attaquer(maneuvre->GetJoueurSelected());

    return EBTNodeResult::InProgress;
}

FString UAttaquerSelected::GetStaticDescription() const
{
    return TEXT("Attaque le player qui a ete precedement selectionne.");
}

void UAttaquerSelected::OnMessage(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, FName Message, int32 RequestID, bool bSuccess) {
    Super::OnMessage(OwnerComp, NodeMemory, Message, RequestID, bSuccess);

    // On distingue les messages
    AManeuver* maneuvre = Cast<AManeuver>(OwnerComp.GetOwner());
    ASquadMemberController * sbire = maneuvre->GetLastMessage()->GetSquadMemberController();
    ABFPCharacterPlayable * joueur = maneuvre->GetJoueurSelected();
    switch (maneuvre->GetLastMessage()->GetType()) {
    case MessageType::COMPLETED:
        // Si un sibre a finit sa courbe de menace alors on lui trouve une autre menace !
        maneuvre->FaireMenacerMember(sbire, joueur);
        break;
    case MessageType::DIED:
        // Si un sbire qui �tait attaquant meurt, il faut le remplacer par un autre !
        maneuvre->Attaquer(joueur); // OK c'est bourrin de ouf �a x) � refaire !
        break;
    case MessageType::KILLED:
        FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
        break;
    case MessageType::OUT_OF_VISION_RANGE:
    case MessageType::OUT_OF_AGRESSION_RANGE: 
        // Si le joueur est trop loin, alors on sort !
        FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
        break;
    case MessageType::OUT_OF_RUSH_RANGE:
        // Si le joueur est trop loin, alors on sort !
        if(shouldStopAttaqueOutOfRushRange)
            FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
        break;
    default:
        break;
    }
}
