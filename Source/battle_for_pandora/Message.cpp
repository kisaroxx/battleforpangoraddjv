// Fill out your copyright notice in the Description page of Project Settings.

#include "Message.h"

UMessage::UMessage()
{
}

UMessage::~UMessage()
{
}

void UMessage::SetParams(MessageType type_, ASquadMemberController * character_) {
    type = type_;
    character = character_;
}

MessageType UMessage::GetType() const
{
    return type;
}

ASquadMemberController * UMessage::GetSquadMemberController() const
{
    return character;
}
