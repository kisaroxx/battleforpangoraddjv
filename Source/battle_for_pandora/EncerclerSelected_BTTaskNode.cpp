// Fill out your copyright notice in the Description page of Project Settings.

#include "EncerclerSelected_BTTaskNode.h"
#include "SquadSbireController.h"
#include "Logger.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Maneuver.h"
#include "BFPCharacterPlayable.h"
#include "SquadDescriptor.h"
#include "SimpleAttackCommand.h"


UEncerclerSelected::UEncerclerSelected(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    NodeName = "EncerclerSelected";
}

EBTNodeResult::Type UEncerclerSelected::ExecuteTaskImpl(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, AManeuver* maneuvre) {
    MY_LOG_IA(TEXT("%s MODE = EncerclerSelected"), *maneuvre->GetDescriptor()->GetName());
    maneuvre->nbAttaquantsCourant = 0;

    maneuvre->EncerclerJoueur(maneuvre->GetJoueurSelected());

    return EBTNodeResult::InProgress;
}

FString UEncerclerSelected::GetStaticDescription() const
{
    return TEXT("Encercle la cible selectionne.");
}

void UEncerclerSelected::OnMessage(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, FName Message, int32 RequestID, bool bSuccess) {
    Super::OnMessage(OwnerComp, NodeMemory, Message, RequestID, bSuccess);

    // On distingue les messages
    AManeuver* maneuvre = Cast<AManeuver>(OwnerComp.GetOwner());
    switch (maneuvre->GetLastMessage()->GetType()) {
    case MessageType::COMPLETED:
        // Si c'est un sbire, attaquer le joueur !
        if (IsValid(Cast<ASquadSbireController>(maneuvre->GetLastMessage()->GetSquadMemberController()))) {
            if (maneuvre->nbAttaquantsCourant < maneuvre->GetNbAttaquants()) {
                USimpleAttackCommand* com = NewObject<USimpleAttackCommand>(this, USimpleAttackCommand::StaticClass());
                com->setCharacter(maneuvre->GetLastMessage()->GetSquadMemberController());
                com->SetEnemy(maneuvre->GetJoueurSelected());
                maneuvre->GetLastMessage()->GetSquadMemberController()->SetCommand(com);
                maneuvre->nbAttaquantsCourant++;
            } else {
                maneuvre->FaireMenacerMember(maneuvre->GetLastMessage()->GetSquadMemberController(), maneuvre->GetJoueurSelected());
            }
        }

        maneuvre->nbCompleted++;
        // Si on a re�u assez de messages alors on quitte la tache ! :3
        if (maneuvre->nbCompleted >= Cast<AManeuver>(OwnerComp.GetOwner())->GetDescriptor()->GetNbMembers() * 0.75f) {
            FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
        }
        break;
    case MessageType::DIED: {
        ASquadSbireController * sbireController = Cast<ASquadSbireController>(maneuvre->GetLastMessage()->GetSquadMemberController());
        if (IsValid(sbireController)) { // Il faut que ce message soit envoy� avant que le sbire ne soit d�tuir !
            if (IsValid(Cast<USimpleAttackCommand>(sbireController->GetCommand()))) { // S'il etait en train d'attaquer
                maneuvre->nbAttaquantsCourant--;
            }
        }
        break;
    }
    case MessageType::OUT_OF_VISION_RANGE: case MessageType::OUT_OF_AGRESSION_RANGE:
        // Si le joueur est trop loin, alors on sort !
        FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
        break;
    default:
        break;
    }
}
