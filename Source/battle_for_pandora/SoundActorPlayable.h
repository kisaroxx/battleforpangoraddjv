// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CheckpointTriggerSphere.h"
#include "GameFramework/Actor.h"
#include "SoundActorPlayable.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSoundStateDelegate);

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class ESoundState : uint8
{
   SS_Combat 	UMETA(DisplayName = "Combat"),
   SS_Exploration 	UMETA(DisplayName = "Exploration"),
   SS_Death	UMETA(DisplayName = "Death"),
   SS_Revive UMETA(DisplayName = "Revive"),
   SS_Victory UMETA(DisplayName = "Victory"),
   SS_Defeat UMETA(DisplayName = "Defeat")
};

UCLASS()
class BATTLE_FOR_PANDORA_API ASoundActorPlayable : public AActor
{
   GENERATED_BODY()


public:	
	// Sets default values for this actor's properties
	ASoundActorPlayable();
   UPROPERTY(BlueprintAssignable, Category = "SoundState")
      FSoundStateDelegate SoundStateFlag_Exploration;
   UPROPERTY(BlueprintAssignable, Category = "SoundState")
      FSoundStateDelegate SoundStateFlag_Combat;
   UPROPERTY(BlueprintAssignable, Category = "SoundState")
      FSoundStateDelegate SoundStateFlag_Death;
   UPROPERTY(BlueprintAssignable, Category = "SoundState")
      FSoundStateDelegate SoundStateFlag_Checkpoint;
   UPROPERTY(BlueprintAssignable, Category = "SoundState")
      FSoundStateDelegate SoundStateFlag_Revive;
   UPROPERTY(BlueprintAssignable, Category = "SoundState")
      FSoundStateDelegate SoundStateFlag_Victory;
   UPROPERTY(BlueprintAssignable, Category = "SoundState")
      FSoundStateDelegate SoundStateFlag_Defeat;

   void SoundStateFlag_Broadcast(FSoundStateDelegate& toFlag, ESoundState EnumValue);
   void setCurrentSoundState(ESoundState EnumValue);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

   UPROPERTY(BlueprintReadWrite, Category = "SoundState")
      ESoundState CurrentSoundState;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
