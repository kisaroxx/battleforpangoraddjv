// Fill out your copyright notice in the Description page of Project Settings.

#include "SquadLieutenantController.h"

#include "UObject/ConstructorHelpers.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeManager.h"
#include "Logger.h"
#include "UnrealNetwork.h"
#include "SquadDescriptor.h"
#include "SquadManager.h"
#include "BFP_GameState.h"
#include "Message.h"

ASquadLieutenantController::ASquadLieutenantController(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
	static ConstructorHelpers::FObjectFinder<UBehaviorTree> SquadLieutenantBT(TEXT("/Game/IA/IALieutenant/SquadLieutenantBT"));
	if (!ensure(IsValid(SquadLieutenantBT.Object))) return;
	lieutenantBT = SquadLieutenantBT.Object;

}

void ASquadLieutenantController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASquadLieutenantController, lieutenantBT);
}

void ASquadLieutenantController::PostInitializeComponents() {
	Super::PostInitializeComponents();

	// On set la manoeuvre
	if (IsValid(lieutenantBTClass.Get())) {
		//lieutenantBT = NewObject<UBehaviorTree>(this, lieutenantBTClass); // Don't touch this ! <3
	}
	else {
		MY_LOG_NETWORK(TEXT("ASquadLieutenantController PostInitializeComponents lieutenantBTClass non valide"));
	}
}

void ASquadLieutenantController::BeginPlay() {
	Super::BeginPlay();
   //GetSquad()->RegisterLieutenantController(this);
}

void ASquadLieutenantController::Tick(float DeltaTime) {
    Super::Tick(DeltaTime);

    // On detecte les joueurs en vue
    // S'ils sont plus proche que certaines distance, on envoie des messages !

    TArray<ABFPCharacterPlayable*> joueurs = GetWorld()->GetGameState<ABFP_GameState>()->GetAllJoueursAllies();
    float visionRange = GetSquad()->GetManeuver()->GetVisionRange();

    TArray<ABFPCharacterPlayable*> joueursInVisionRange = GetWorld()->GetGameState<ABFP_GameState>()->GetAllInDistanceFrom(joueurs, GetSquad()->GetFirstLieutenant()->GetPawn()->GetTransform(), visionRange);
    GetSquad()->GetManeuver()->SetJoueursVus(joueursInVisionRange);

    if (joueursInVisionRange.Num() > 0) {
        // Envoie d'un message de vision
        UMessage* message = NewObject<UMessage>();
        message->SetParams(MessageType::IN_VISION_RANGE, this);
        GetSquad()->GetSquadManager()->ReceiveMessage(message);
    } else {
        // Envoie d'un message de non-vision
        UMessage* message = NewObject<UMessage>();
        message->SetParams(MessageType::OUT_OF_VISION_RANGE, this);
        GetSquad()->GetSquadManager()->ReceiveMessage(message);
    }

    float agressionRange = GetSquad()->GetManeuver()->GetAgressionRange();
    TArray<ABFPCharacterPlayable*> joueursInAgressionRange = GetWorld()->GetGameState<ABFP_GameState>()->GetAllInDistanceFrom(joueursInVisionRange, GetSquad()->GetFirstLieutenant()->GetPawn()->GetTransform(), agressionRange);
    if(joueursInAgressionRange.Num() > 0) {
        // Envoie d'un message d'agression
        UMessage* message = NewObject<UMessage>();
        message->SetParams(MessageType::IN_AGRESSION_RANGE, this);
        GetSquad()->GetSquadManager()->ReceiveMessage(message);
    } else {
        // Envoie d'un message de non-vision
        UMessage* message = NewObject<UMessage>();
        message->SetParams(MessageType::OUT_OF_AGRESSION_RANGE, this);
        GetSquad()->GetSquadManager()->ReceiveMessage(message);
    }

    float rushRange = GetSquad()->GetManeuver()->GetRushRange();
    TArray<ABFPCharacterPlayable*> joueursInRushRange = GetWorld()->GetGameState<ABFP_GameState>()->GetAllInDistanceFrom(joueursInVisionRange, GetSquad()->GetFirstLieutenant()->GetPawn()->GetTransform(), rushRange);
    if(joueursInRushRange.Num() > 0) {
        // Envoie d'un message de rush
        UMessage* message = NewObject<UMessage>();
        message->SetParams(MessageType::IN_RUSH_RANGE, this);
        GetSquad()->GetSquadManager()->ReceiveMessage(message);
    } else {
        // Envoie d'un message de non-vision
        UMessage* message = NewObject<UMessage>();
        message->SetParams(MessageType::OUT_OF_RUSH_RANGE, this);
        GetSquad()->GetSquadManager()->ReceiveMessage(message);
    }
}

void ASquadLieutenantController::RunBT() {
	MY_LOG_NETWORK(TEXT("ASquadLieutenantController RunBT lieutenantBT ? %u"), IsValid(lieutenantBT));
	bool succes = RunBehaviorTree(lieutenantBT);
	MY_LOG_NETWORK(TEXT("ASquadLieutenantController RunBT result ? %u"), succes);
	UBehaviorTreeManager* BTManager = UBehaviorTreeManager::GetCurrent(GetWorld());
	if (BTManager)
	{
		BTManager->DumpUsageStats();
	}
}
