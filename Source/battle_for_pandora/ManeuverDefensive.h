// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Maneuver.h"
#include "ManeuverDefensive.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class BATTLE_FOR_PANDORA_API AManeuverDefensive : public AManeuver
{
    GENERATED_BODY()

    class AManeuverTargetPoint* lastTargetPoint;

    /* La vision maximum pour aggresser sans restrictions un joueur. */
    UPROPERTY(EditAnywhere, Category = "SensThinkAct")
    float distanceMaxFromLastTargetPoint = 1000.0f;

public:

	// Called when the game starts
	virtual void BeginPlay() override;

    void SetSquadDescriptor(ASquadDescriptor* descriptor) override;

    void RemoveFirstTargetPoint() override;

    FVector GetLastTargetPoint() const;

    float GetDistanceMaxFromLastTargetPoint() const;
};
