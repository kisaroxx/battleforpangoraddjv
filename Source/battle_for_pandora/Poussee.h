#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Pawn.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Poussee.generated.h"


USTRUCT(BlueprintType)
struct FPoussee {
   GENERATED_USTRUCT_BODY()
public:

    UPROPERTY(BlueprintReadWrite)
    float puissance = 1000.0f;
    UPROPERTY(BlueprintReadWrite)
    float duree = 0.2f;
    UPROPERTY(BlueprintReadWrite)
    FVector direction{};
    UPROPERTY(BlueprintReadWrite)
    float startTime;

public:
    FPoussee();
    FPoussee(float puissance, float duree);

    float GetPuissance() const;
    float GetDuree() const;

    void SetDirection(FVector direction);

    void Start(UWorld* world);
    bool IsEnded(UWorld* world);
    void ApplyPoussee(APawn* pawn, float deltaTime);
};
