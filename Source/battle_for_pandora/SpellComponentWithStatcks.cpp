// Fill out your copyright notice in the Description page of Project Settings.

#include "SpellComponentWithStatcks.h"
#include "Engine/World.h"
#include "BFPCharacter.h"
#include "BFPCharacterPlayable.h"
#include "Logger.h"
#include "Engine/Canvas.h"
#include "Engine/Texture2D.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "Animation/WidgetAnimation.h"
#include "Components/WidgetComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"
#include "WidgetBlueprintLibrary.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "Components/Image.h"
#include "Components/Border.h"
#include "Components/CanvasPanel.h"
#include "Components/CircularThrobber.h"
#include "UserWidget.h"
#include "Spell.h"
#include "Util.h"
#include "Engine/UserInterfaceSettings.h"
#include "Engine/RendererSettings.h"
#include <algorithm>


USpellComponentWithStatkcs::USpellComponentWithStatkcs(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    PrimaryComponentTick.bCanEverTick = true;
    SetIsReplicated(true);
}

void USpellComponentWithStatkcs::BeginPlay() {
    Super::BeginPlay();
    currentNbStacks = maxNbStacks;
}

void USpellComponentWithStatkcs::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction) {
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
    if (GetWorld()->IsServer()) {
        if (currentNbStacks < maxNbStacks) {
            if (GetWorld()->IsServer()) {
                currentTime = UGameplayStatics::GetRealTimeSeconds(GetWorld());
            }
            if (currentTime - lastTimeCastSpell >= dureeCooldown) {
                currentNbStacks++;
                lastTimeCastSpell = currentTime;
                spellState = SpellState::ACTIVATABLE;
                //MY_LOG_UI(TEXT("currentNbStacks Updated (Tick) = %d"), currentNbStacks);
            }
        }
    }
}

void USpellComponentWithStatkcs::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    //MY_LOG_UI(TEXT("CA REPLIQUE USpellComponentWithStacks !!!"));
    DOREPLIFETIME(USpellComponentWithStatkcs, currentNbStacks);
}

bool USpellComponentWithStatkcs::IsActivable() {
    ABFPCharacter* owner = Cast<ABFPCharacter>(GetOwner());
    return currentNbStacks > 0 && owner->CanCastSpell();
}

void USpellComponentWithStatkcs::OnActivateSpell(ASpell * spell) {
    currentNbStacks = std::max(currentNbStacks - 1, 0);
    //MY_LOG_UI(TEXT("currentNbStacks Updated = %d"), currentNbStacks);
    spellState = (currentNbStacks > 0) ? SpellState::ACTIVATABLE : SpellState::IN_COOLDOWN;
    if (currentNbStacks == maxNbStacks - 1) {
        if (GetWorld()->IsServer()) {
            currentTime = UGameplayStatics::GetRealTimeSeconds(GetWorld());
        }
        lastTimeCastSpell = currentTime;
    }
}

void USpellComponentWithStatkcs::DrawWidget() {
    if (IsValid(spellWidget)) {
        //if (spellWidget->IsInViewport()) {
        //    MY_LOG_UI(TEXT("currentNbStacks = %d owner->GetCombatState() = %d"), currentNbStacks, static_cast<int>(Cast<ABFPCharacterPlayable>(GetOwner())->GetCombatState()));
        //}
        //UImage* image = dynamic_cast<UImage*>(spellWidget->GetWidgetFromName(FName("Image")));
        spellWidget->SetVisibility(ESlateVisibility::Visible);
        UBorder* greyFilter = dynamic_cast<UBorder*>(spellWidget->GetWidgetFromName(FName("GreyFilter")));
        UWidget* circular = dynamic_cast<UWidget*>(spellWidget->GetWidgetFromName(FName("Circular")));
        UTextBlock* timer = dynamic_cast<UTextBlock*>(spellWidget->GetWidgetFromName(FName("Timer")));
        UTextBlock* counter = dynamic_cast<UTextBlock*>(spellWidget->GetWidgetFromName(FName("Counter")));
        if (greyFilter == nullptr || circular == nullptr || timer == nullptr || counter == nullptr) {
            DEBUG_SCREEN(-1, 5.f, FColor::Red, TEXT("Un spell widget avec statcks a de mauvais noms d'elements !"));
            return;
        }
        if (IsActivable()) {
            greyFilter->SetVisibility(ESlateVisibility::Hidden);
        } else {
            greyFilter->SetVisibility(ESlateVisibility::Visible);
        }
        if (currentNbStacks == maxNbStacks) {
            circular->SetVisibility(ESlateVisibility::Hidden);
        } else {
            circular->SetVisibility(ESlateVisibility::Visible);
        }
        if (currentNbStacks > 0) {
            timer->SetVisibility(ESlateVisibility::Hidden);
            counter->SetVisibility(ESlateVisibility::Visible);
            FString str; str.AppendInt(currentNbStacks);
            FText t = FText::AsCultureInvariant(str);
            counter->SetText(t);
        } else {
            timer->SetVisibility(ESlateVisibility::Visible);
            counter->SetVisibility(ESlateVisibility::Hidden);
            FString str = GetFStringCooldown();
            FText t = FText::AsCultureInvariant(str);
            timer->SetText(t);
        }
    }
    else {
        DEBUG_SCREEN(-1, 5.f, FColor::Red, TEXT("Le spellWidget n'est pas valide ! !"));
    }
}
