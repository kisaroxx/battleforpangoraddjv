// Fill out your copyright notice in the Description page of Project Settings.

#include "SquadMemberController.h"
#include "SquadSbireController.h"
#include "SquadLieutenantController.h"
#include "Logger.h"
#include "BFPCharacterNonPlayable.h"
#include "BFPCharacterPlayable.h"
#include "BFP_GameState.h"
#include "Util.h"
#include "DrawDebugHelpers.h"
#include "NoCommand.h"

ASquadMemberController::ASquadMemberController(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    GetPathFollowingComponent()->SetCanEverAffectNavigation(true);
}

void ASquadMemberController::BeginPlay() {
    Super::BeginPlay();
    commande = NewObject<UNoCommand>(this, UNoCommand::StaticClass());
    //if (HasSquad()) {
        RunBT();
        //bBTIsRunning = true;
    //}
}

UCommandReturn* ASquadMemberController::execute() {

    //// Si quelqu'un est trop proche, on l'attaque quelque soient les ordres de la squad, il en va d'une question de survie !
    //TArray<ABFPCharacterPlayable*> joueurs = GetWorld()->GetGameState<ABFP_GameState>()->GetAllJoueursAllies();

    if (commande) {
        return commande->execute();
    }
    // Si commande nulle ? Que faire ? ==> informer que �a ne va pas !!!
    // DEBUG_SCREEN(-1, 5.0f, FColor::Red, "Attention une commande est nullptr !!");
    return nullptr;
}

void ASquadMemberController::SetCommand(UCommand* command) {
    commande = command;
}

UCommand * ASquadMemberController::GetCommand()
{
    return commande;
}

void ASquadMemberController::SetSquad(ASquadDescriptor* _squad) {
    ABFPCharacterNonPlayable* pawn = Cast<ABFPCharacterNonPlayable>(GetPawn());
    pawn->SetSquad(_squad);

    //// Si le BT n'est pas lance, alors on le lance
    //if (!bBTIsRunning) {
    //    RunBT();
    //}
}

ASquadDescriptor* ASquadMemberController::GetSquad() {
    ABFPCharacterNonPlayable* pawn = Cast<ABFPCharacterNonPlayable>(GetPawn());
    return pawn->GetSquad();
}

bool ASquadMemberController::HasSquad() {
    ABFPCharacterNonPlayable* pawn = Cast<ABFPCharacterNonPlayable>(GetPawn());
    return pawn->HasSquad();
}

FPathFollowingRequestResult ASquadMemberController::MoveToSpiraledImpl(FAIMoveRequest request, FVector* outNewGoal) {
    ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
    FVector center = request.GetGoalLocation();
    request.SetProjectGoalLocation(true);
    //center.Z = gameState->landscapeHeights.getHeight(center.X, center.Y);
    FPathFollowingRequestResult result = MoveTo(request);
    if (result.Code != EPathFollowingRequestResult::Type::Failed) {
        if (outNewGoal != nullptr) *outNewGoal = request.GetGoalLocation();
        //DrawDebugLine(GetWorld(), GetPawn()->GetActorLocation(), request.GetGoalLocation(), FColor::Red, true, -1.0F, (uint8)'\000', 10.0F);
        return result;
    }

    // Si ca n'a pas marche, on commence la spirale ! :)
    double currentAngle = 0.0f;
    double angleInterval = 30.0f;
    double maxAngle = 360.0f;
    double currentDistance = 10.0f;
    double distanceInterval = 10.0f;
    int nbRequeteMax = 1000;
    int nbRequete = 1;
    FVector newGoal = center + Util::PolaireToCartesian(currentAngle, currentDistance);
    //newGoal.Z = gameState->landscapeHeights.getHeight(newGoal.X, newGoal.Y);
    FAIMoveRequest request2 = FAIMoveRequest(newGoal);
    request2.SetProjectGoalLocation(true);
    result = MoveTo(request2);
    while (result == EPathFollowingRequestResult::Type::Failed && nbRequete <= nbRequeteMax) {
        currentAngle = Util::ModF(currentAngle + angleInterval, maxAngle);
        currentDistance += distanceInterval;
        newGoal = center + Util::PolaireToCartesian(currentAngle, currentDistance);
        //newGoal.Z = gameState->landscapeHeights.getHeight(newGoal.X, newGoal.Y);
        request2 = FAIMoveRequest(newGoal);
        request2.SetProjectGoalLocation(true);
        result = MoveTo(request2);
        nbRequete++;
    }
    if (nbRequete > nbRequeteMax) {
        //DEBUG_SCREEN(-1, 5.f, FColor::Red, TEXT("Une spirale de pathfinding a echoue !!!"));
        DrawDebugLine(GetWorld(), GetPawn()->GetActorLocation(), center, FColor::Red, true, -1.0F, (uint8)'\000', 10.0F);
        //DrawDebugLine(GetWorld(), GetPawn()->GetActorLocation(), request2.GetGoalLocation(), FColor::Red, true, -1.0F, (uint8)'\000', 10.0F);
    }
    else {
        if (outNewGoal != nullptr) *outNewGoal = request2.GetGoalLocation();
        //DrawDebugLine(GetWorld(), GetPawn()->GetActorLocation(), request2.GetGoalLocation(), FColor::Red, true, -1.0F, (uint8)'\000', 10.0F);
    }
    return result;
}

FPathFollowingRequestResult ASquadMemberController::MoveToSpiraled(FAIMoveRequest request, FVector* outNewGoal, RadiusType radiusType) {
    // On change le radius de notre agent !
    float previousRadius = GetPawn()->GetMovementComponent()->NavAgentProps.AgentRadius;
    GetPawn()->GetMovementComponent()->SetUpdateNavAgentWithOwnersCollisions(false);
    GetPawn()->GetMovementComponent()->NavAgentProps.AgentRadius = (radiusType == RadiusType::SMALL) ? 35.0f : 1000.0f;

    // On effectue son mouvement
    FPathFollowingRequestResult result = MoveToSpiraledImpl(request, outNewGoal);

    // On remet son radius !
    GetPawn()->GetMovementComponent()->SetUpdateNavAgentWithOwnersCollisions(true);
    GetPawn()->GetMovementComponent()->NavAgentProps.AgentRadius = previousRadius;

    return result;
}

bool ASquadMemberController::IsSbire() const {
    return IsValid(Cast<ASquadSbireController>(this));
}
bool ASquadMemberController::IsLieutenant() const {
    return IsValid(Cast<ASquadLieutenantController>(this));
}


