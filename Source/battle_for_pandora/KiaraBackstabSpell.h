// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerCapsule.h"
#include "Engine/StaticMeshActor.h"
#include "Spell.h"
#include "KiaraBackstabSpell.generated.h"

UCLASS()
class BATTLE_FOR_PANDORA_API AKiaraBackstabSpell : public ASpell
{
    GENERATED_BODY()

    /* Le multiplicateur de degats de la prochaine attaque. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float degatMultiplicateur = 3.0f;

    /* Le nombre de fois que l'on peut cumuler des multiplicateurs de ce type. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    int nbStacksMax = 3;

    /* La taille de la boule de lumiere. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float facteurDAgrandissement = 1.0f;

public:
   AKiaraBackstabSpell(const FObjectInitializer & ObjectInitializer);

protected:
   virtual void BeginPlay() override;

public:
   virtual void Tick(float DeltaTime) override;
};