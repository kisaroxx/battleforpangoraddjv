// Fill out your copyright notice in the Description page of Project Settings.

#include "BackEndSaveGame.h"

UBackEndSaveGame::UBackEndSaveGame()
{
   SaveSlotName = TEXT("LocalSaveGame");
   UserIndex = 0;
   PlayerGuestToken = "";
   PlayerIndex = "";
   PlayerName = "";
   PlayerWins = 0;
   PlayerLoss = 0;   
}
