// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TargetPoint.h"
#include "AITargetPointCPP.generated.h"

/**
 * 
 */
UCLASS()
class BATTLE_FOR_PANDORA_API AAITargetPointCPP : public ATargetPoint
{
	GENERATED_BODY()

public :
   /** Représente l'ordre dans lequel le NPC parcoure le chemin de patrouille
   ( 0 est le point initial ) */
   UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Qualificateur")
   int32 Position;

   AAITargetPointCPP();

	
};
