// Fill out your copyright notice in the Description page of Project Settings.

#include "BFPCharacterNonPlayable.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "SquadMemberController.h"
#include "SquadDescriptor.h"
#include "Logger.h"

ABFPCharacterNonPlayable::ABFPCharacterNonPlayable(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer)
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;
}

void ABFPCharacterNonPlayable::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
   Super::GetLifetimeReplicatedProps(OutLifetimeProps);

   DOREPLIFETIME(ABFPCharacterNonPlayable, squad);
}

void ABFPCharacterNonPlayable::SetSquad(ASquadDescriptor* _squad) {
   squad = _squad;
}

ASquadDescriptor* ABFPCharacterNonPlayable::GetSquad() {
   //MY_LOG_NETWORK(TEXT("ABFPCharacterNonPlayable GetSquad %u"), IsValid(squad));
   return squad;
}

bool ABFPCharacterNonPlayable::HasSquad() {
    //MY_LOG_NETWORK(TEXT("ABFPCharacterNonPlayable HasSquad squad"));
    return IsValid(squad);
}

void ABFPCharacterNonPlayable::RegisterToSquad() {
}

void ABFPCharacterNonPlayable::UnRegisterToSquad() {
}