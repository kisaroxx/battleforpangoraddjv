// Fill out your copyright notice in the Description page of Project Settings.

#include "JoinSquadBTTaskNode.h"
#include "SquadSbireController.h"

UJoinSquadBTTaskNode::UJoinSquadBTTaskNode() {
   // Le nom que prendra le noeud dans le BT
   NodeName = "JoinSquad";
}

/* Sera appel�e au d�marrage de la t�che et devra retourner Succeeded, Failed ou InProgress */
EBTNodeResult::Type UJoinSquadBTTaskNode::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8*
   NodeMemory)
{
   // Obtenir un pointeur sur notre AIEnemyController
   ASquadSbireController* sbireController = Cast<ASquadSbireController>(OwnerComp.GetOwner());
   sbireController->JoinSquad();
   return EBTNodeResult::Succeeded;
}

/** Retourne une chaine de description pour la t�che. Ce texte appara�tre dans le BT */
FString UJoinSquadBTTaskNode::GetStaticDescription() const
{
   return TEXT("Sbire rejoint une escouade");
}