// Fill out your copyright notice in the Description page of Project Settings.

#include "OrienterToSelected_BTTaskNode.h"
#include "SquadSbireController.h"
#include "Logger.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Maneuver.h"
#include "BFPCharacterPlayable.h"
#include "SquadDescriptor.h"

UOrienterToSelected::UOrienterToSelected(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    NodeName = "OrienterToSelected";
    bUseCompletedMessages = true;
    bShouldFailOnAddTargetPoint = true;
}

FString UOrienterToSelected::GetStaticDescription() const
{
    return TEXT("Permet de s'orienter dans la direction de la cible selectionne.");
}

EBTNodeResult::Type UOrienterToSelected::ExecuteTaskImpl(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, AManeuver* maneuvre) {
    if (maneuvre->GetJoueursVus().Num() > 0) {
        MY_LOG_IA(TEXT("%s MODE = OrienterToSelected"), *maneuvre->GetDescriptor()->GetName());
        maneuvre->OrienterTo(maneuvre->GetJoueurSelected()->GetTransform());
        return EBTNodeResult::InProgress;
    } else {
        return EBTNodeResult::Failed;
    }
}

void UOrienterToSelected::OnMessage(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, FName Message, int32 RequestID, bool bSuccess) {
    Super::OnMessage(OwnerComp, NodeMemory, Message, RequestID, bSuccess);

    // On distingue les messages
    AManeuver* maneuvre = Cast<AManeuver>(OwnerComp.GetOwner());
    switch (maneuvre->GetLastMessage()->GetType()) {
    case MessageType::IN_AGRESSION_RANGE:
        if(!continueEvenIfInAgressionRange)
            FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
        break;
    case MessageType::IN_RUSH_RANGE:
        FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
        break;
    case MessageType::OUT_OF_VISION_RANGE:
        // Si le joueur est trop loin, alors on sort !
        FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
        break;
    default:
        break;
    }
}
