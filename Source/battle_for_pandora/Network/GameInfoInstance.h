// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "PlayerInfo.h"
#include "PlayerSaveGame.h"
//#include "UI/CharacterSelectWidget.h"
//#include "UI/ConnectedPlayerWidget.h"
//#include "UI/HostMenuWidget.h"
//#include "UI/LoadingScreenWidget.h"
//#include "UI/LobbyMenuWidget.h"
//#include "UI/MainMenuWidget.h"
//#include "UI/OptionMenuWidget.h"
//#include "UI/PlayerButtonWidget.h"
//#include "UI/ServerMenuWidget.h"
#include "UI/MenuWidget.h"
#include "OnlineSessionSettings.h"
#include "OnlineSessionInterface.h"
#include "GameInfoInstance.generated.h"

UCLASS()
class BATTLE_FOR_PANDORA_API UGameInfoInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UGameInfoInstance(const FObjectInitializer & ObjectInitializer);

	virtual void Init();
	virtual void Shutdown();

	// ------------------- WIDGET --------------------------
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	TSubclassOf<UUserWidget> CharacterSelectWD_Class;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	TWeakObjectPtr<class UCharacterSelectWidget> CharacterSelectWD;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	TSubclassOf<UUserWidget> ConnectedPlayerWD_Class;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	TWeakObjectPtr<class UConnectedPlayerWidget> ConnectedPlayerWD;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	TSubclassOf<UUserWidget> ErrorWD_Class;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	TWeakObjectPtr<class UErrorWidget> ErrorWD;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	TSubclassOf<UUserWidget> ExitWD_Class;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	TWeakObjectPtr<class UErrorWidget> ExitWD;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	TSubclassOf<UUserWidget> HostMenuWD_Class;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	TWeakObjectPtr<class UHostMenuWidget> HostMenuWD;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	TSubclassOf<UUserWidget> LoadingScreenWB_Class;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	TWeakObjectPtr<class ULoadingScreenWidget> LoadingScreenWB;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	TSubclassOf<UUserWidget> LobbyMenuWD_Class;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	TWeakObjectPtr<class ULobbyMenuWidget> LobbyMenuWD;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	TSubclassOf<UUserWidget> OptionMenuWD_Class;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	TWeakObjectPtr<class UOptionMenuWidget> OptionMenuWD;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	TSubclassOf<UUserWidget> MainMenuWD_Class;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	TWeakObjectPtr<class UMainMenuWidget> MainMenuWD;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	TSubclassOf<UUserWidget> PlayerButtonWD_Class;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	TWeakObjectPtr<class UPlayerButtonWidget> PlayerButtonWD;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	TSubclassOf<UUserWidget> ServerMenuWD_Class;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	TWeakObjectPtr<class UServerMenuWidget> ServerMenuWD;

	UFUNCTION(BlueprintCallable, Category = "Widget")
	void cleanWidget();

	UFUNCTION(BlueprintCallable, Category = "Widget")
	void showWidget(UMenuWidget* widget, TSubclassOf<UUserWidget> widgetClass, APlayerController* playerController = nullptr, const bool bShowMouseCursor = true, const EInputModeWidget& inputMode = EInputModeWidget::UI_Only);

	UMenuWidget* createWidget(TWeakObjectPtr<APlayerController> playerController, TSubclassOf<UUserWidget> widgetClass);
	UMenuWidget* createWidget(TSubclassOf<UUserWidget> widgetClass);

	UFUNCTION(BlueprintCallable, Category = "Widget")
	void showMainMenuWidget(APlayerController* playerController, const bool bShowMouseCursor = true, const EInputModeWidget& inputMode = EInputModeWidget::UI_Only);

	UFUNCTION(BlueprintCallable, Category = "Widget")
	void showHostMenuWidget(APlayerController* playerController, const bool bShowMouseCursor = true, const EInputModeWidget& inputMode = EInputModeWidget::UI_Only);

	UFUNCTION(BlueprintCallable, Category = "Widget")
	void showOptionMenuWidget(APlayerController* playerController, const bool bShowMouseCursor = true, const EInputModeWidget& inputMode = EInputModeWidget::UI_Only);

	UFUNCTION(BlueprintCallable, Category = "Widget")
	void showServerMenuWidget(APlayerController* playerController, const bool bShowMouseCursor = true, const EInputModeWidget& inputMode = EInputModeWidget::UI_Only);

	UFUNCTION(BlueprintCallable, Category = "Widget")
	void showLoadingScreenWidget(APlayerController* playerController, const bool bShowMouseCursor = true, const EInputModeWidget& inputMode = EInputModeWidget::UI_Only);

	UFUNCTION(BlueprintCallable, Category = "Widget")
	void showErrorWidget(APlayerController* playerController, const bool bShowMouseCursor = true, const EInputModeWidget& inputMode = EInputModeWidget::UI_Only);

	UFUNCTION(BlueprintCallable, Category = "Widget")
	void showLobbyMenuWidget(APlayerController* playerController, const bool bShowMouseCursor = true, const EInputModeWidget& inputMode = EInputModeWidget::UI_Only);

	UFUNCTION(BlueprintCallable, Category = "Widget")
	void showCharacterSelectWidget(APlayerController* playerController, const bool bShowMouseCursor = true, const EInputModeWidget& inputMode = EInputModeWidget::UI_Only);

	// ------------------- SAVE GAME --------------------------

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SaveGame")
	FString savingSlot = "saveGameSlot";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SaveGame")
	TWeakObjectPtr<UPlayerSaveGame> saveGameRef;

	UFUNCTION(BlueprintCallable, Category = "SaveGame")
	void savePlayerInfo(FPlayerInfo& playerInfo);

	UFUNCTION(BlueprintCallable, Category = "SaveGame")
	void deletePlayerInfo();

	UFUNCTION(BlueprintCallable, Category = "SaveGame")
	FPlayerInfo loadPlayerInfo();

	//UFUNCTION(BlueprintCallable, Category = "SaveGame")
	void savePlayerInfoCheck(FPlayerInfo& playerInfo);
	
	//UFUNCTION(BlueprintCallable, Category = "SaveGame")
	void saveGameCheck(TWeakObjectPtr<APlayerController> playerController);

	// ------------------ MANAGE INPUT/MENU -----------------------
	UFUNCTION(BlueprintCallable, Category = "ManageInput")
	void pauseGame(bool pause);

	UFUNCTION(BlueprintCallable, Category = "ManageInput")
	void quitGame(APlayerController* playerController = nullptr);

	UFUNCTION(BlueprintCallable, Category = "ManageInput")
	void mainMenu(APlayerController* playerController = nullptr);

	// ----------------- SERVER SETTINGS ---------------------
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SaveGame")
	int nbPlayer_;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SaveGame")
	FText serverName_;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SaveGame")
	bool saveGameCreated_;

	// ----------------- MANAGE SESSION ---------------------



	// Delegates
	DECLARE_DELEGATE_TwoParams(FOnCreateSessionCompleteDelegate, FName, bool);
	FOnCreateSessionCompleteDelegate onCreateSessionCompleteDelegate;
	DECLARE_DELEGATE_TwoParams(FOnStartSessionCompleteDelegate, FName, bool);
	FOnStartSessionCompleteDelegate onStartSessionCompleteDelegate;
	DECLARE_DELEGATE_OneParam(FOnFindSessionsCompleteDelegate, bool);
	FOnFindSessionsCompleteDelegate onFindSessionsCompleteDelegate;
	DECLARE_DELEGATE_TwoParams(FOnJoinSessionCompleteDelegate, FName, EOnJoinSessionCompleteResult::Type);
	FOnJoinSessionCompleteDelegate onJoinSessionCompleteDelegate;
	DECLARE_DELEGATE_TwoParams(FOnDestroySessionCompleteDelegate, FName, bool);
	FOnDestroySessionCompleteDelegate OnDestroySessionCompleteDelegate;


	DECLARE_EVENT_OneParam(UGameInfoInstance, FSessionFoundEvent, bool);
	FSessionFoundEvent sessionFoundEvent;
	DECLARE_EVENT_OneParam(UGameInfoInstance, FSessionCreatedEvent, bool);
	FSessionCreatedEvent sessionCreatedEvent;

	// Handlers pour garder en memoire le handle des delegates
	FDelegateHandle onCreateSessionCompleteDelegateHandle;
	FDelegateHandle onStartSessionCompleteDelegateHandle;
	FDelegateHandle onFindSessionsCompleteDelegateHandle;
	FDelegateHandle onJoinSessionCompleteDelegateHandle;
	FDelegateHandle onDestroySessionCompleteDelegateHandle;

	// Callbacks
	virtual void onCreateSessionComplete(FName SessionName, bool bWasSuccessful);
	void onStartSessionComplete(FName SessionName, bool bWasSuccessful);
	void onFindSessionsComplete(bool bWasSuccessful);
	void onJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);
	virtual void onDestroySessionComplete(FName SessionName, bool bWasSuccessful);

	TSharedPtr<class FOnlineSessionSettings> SessionSettings;
	TSharedPtr<class FOnlineSessionSearch> SessionSearch;
	TArray<class FOnlineSessionSearchResult> availableSessions;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ManageSession")
	FName nomSession_;

	
	void FindSessions(APlayerController* playerController, bool bIsLAN = true, bool bIsPresence = true);
	bool JoinSession(APlayerController* playerController, FName SessionName, const FOnlineSessionSearchResult& SearchResult);

	bool CreateSession(TSharedPtr<const FUniqueNetId> UserId, int nbPlayer, FString levelName, bool enableLAN);
	void FindSessions(TSharedPtr<const FUniqueNetId> UserId, bool bIsLAN, bool bIsPresence);
	bool JoinSession(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, const FOnlineSessionSearchResult& SearchResult);


	UFUNCTION(BlueprintCallable, Category = "ManageSession")
	void LaunchLobby(APlayerController* playerController, int nbPlayer, FText serverName, bool enableLAN = true);

	UFUNCTION(BlueprintCallable, Category = "ManageSession")
	bool CreateSession(APlayerController* playerController, int nbPlayer, FString levelName, bool enableLAN = true);
	
	UFUNCTION(BlueprintCallable, Category = "ManageSession")
	void JoinServer(APlayerController* playerController, uint8 idSession);

	UFUNCTION(BlueprintCallable, Category = "ManageSession")
	void DestroySession(APlayerController* playerController = nullptr);



};
