// Fill out your copyright notice in the Description page of Project Settings.

#include "MainMenuGM.h"
#include "Network/MainMenu/MainMenuGameSession.h"
#include "BFP_PlayerState.h"
#include "MainMenuPC.h"
#include "Logger.h"

AMainMenuGM::AMainMenuGM(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer)
{
	GameSessionClass = AMainMenuGameSession::StaticClass();
	PlayerControllerClass = AMainMenuPC::StaticClass();
	PlayerStateClass = ABFP_PlayerState::StaticClass();
}