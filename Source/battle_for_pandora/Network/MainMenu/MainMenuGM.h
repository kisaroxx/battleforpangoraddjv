// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "MainMenuGM.generated.h"

/**
 * 
 */
UCLASS(MinimalAPI)
class AMainMenuGM : public AGameMode
{
	GENERATED_BODY()

public:
	AMainMenuGM(const FObjectInitializer & ObjectInitializer);

};
