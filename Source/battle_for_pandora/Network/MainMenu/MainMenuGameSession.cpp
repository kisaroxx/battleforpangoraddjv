// Fill out your copyright notice in the Description page of Project Settings.

#include "MainMenuGameSession.h"
#include "Network/GameInfoInstance.h"
#include "OnlineSubsystem.h"
#include "Logger.h"

AMainMenuGameSession::AMainMenuGameSession(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer)
{
	nbJoueur = 2;
}

void AMainMenuGameSession::RegisterServer() {
	Super::RegisterServer();
	MY_LOG_NETWORK(TEXT("AMainMenuGameSession RegisterServer DEDICATED SERVER LAUNCH LOBBY"));
	TWeakObjectPtr<UWorld> world = GetWorld();
	if (!ensure(world.IsValid())) return;
	TWeakObjectPtr<UGameInfoInstance> gameInstanceRef = world->GetGameInstance<UGameInfoInstance>();
	if (!ensure(gameInstanceRef.IsValid())) return;
	MY_LOG_NETWORK(TEXT("AMainMenuGameSession RegisterServer GetNetMode ? %u"), GetNetMode());
	if (GetNetMode() == ENetMode::NM_DedicatedServer) {
		gameInstanceRef->LaunchLobby(world->GetFirstPlayerController(), nbJoueur, FText());
	}
}

