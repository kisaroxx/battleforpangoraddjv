// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

#include "MainMenuPC.generated.h"


/**
 * 
 */
UCLASS(Blueprintable)
class BATTLE_FOR_PANDORA_API AMainMenuPC : public APlayerController
{
	GENERATED_BODY()

public:
	AMainMenuPC(const FObjectInitializer & ObjectInitializer);
};
