// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "GameFramework/GameSession.h"
#include "MainMenuGameSession.generated.h"

/**
 * 
 */
UCLASS(MinimalAPI)
class AMainMenuGameSession : public AGameSession
{
	GENERATED_BODY()

public:
	AMainMenuGameSession(const FObjectInitializer & ObjectInitializer);

	virtual void RegisterServer() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameParams", meta = (AllowPrivateAccess = "true"))
	uint8 nbJoueur;
};
