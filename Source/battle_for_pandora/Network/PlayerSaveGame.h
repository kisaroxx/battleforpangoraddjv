// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "PlayerInfo.h"
#include "PlayerSaveGame.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class BATTLE_FOR_PANDORA_API UPlayerSaveGame : public USaveGame
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FPlayerInfo S_PlayerInfo;
	
};
