// Fill out your copyright notice in the Description page of Project Settings.

#include "MapLookUpTableLevelScriptActor.h"
#include "Network/GameInfoInstance.h"
#include "Engine/World.h"
#include "LandscapeHeights.h"
#include "TimerManager.h"
#include "Logger.h"

AMapLookUpTableLevelScriptActor::AMapLookUpTableLevelScriptActor(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {

}

void AMapLookUpTableLevelScriptActor::BeginPlay() {
	Super::BeginPlay();
	gameInstanceRef = GetGameInstance<UGameInfoInstance>();
	UWorld* world = GetWorld();
	if (IsValid(world)) {
		LandscapeHeights landScapeHeights{};
		MY_LOG_NETWORK(TEXT("AMapLookUpTableLevelScriptActor BeginPlay begin generateLandscapeHeights"));
		landScapeHeights.generateLandscapeHeights(world);
		MY_LOG_NETWORK(TEXT("AMapLookUpTableLevelScriptActor BeginPlay begin serialisation"));
		FString RelativePath = FPaths::ProjectContentDir();
		landScapeHeights.WriteLandscapeHeights(std::string(TCHAR_TO_UTF8(*RelativePath)) + "/LandscapeHeights");
		world->GetTimerManager().SetTimer(TimerHandle, this, &AMapLookUpTableLevelScriptActor::StopGame, 5.f, false);
	}
}

void AMapLookUpTableLevelScriptActor::StopGame() {
	MY_LOG_NETWORK(TEXT("AMapLookUpTableLevelScriptActor BeginPlay begin QUIT GAME"));
	gameInstanceRef->quitGame(GetWorld()->GetFirstPlayerController());
}
