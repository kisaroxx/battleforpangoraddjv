// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "MapLookUpTableLevelScriptActor.generated.h"

/**
 * 
 */
UCLASS()
class BATTLE_FOR_PANDORA_API AMapLookUpTableLevelScriptActor : public ALevelScriptActor
{
	GENERATED_BODY()
	
private:

	TWeakObjectPtr<class UGameInfoInstance> gameInstanceRef;
	struct FTimerHandle TimerHandle;

public:
	AMapLookUpTableLevelScriptActor(const FObjectInitializer & ObjectInitializer);

	virtual void BeginPlay() override;
	virtual void StopGame();
};
