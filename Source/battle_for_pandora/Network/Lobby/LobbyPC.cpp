// Fill out your copyright notice in the Description page of Project Settings.

#include "LobbyPC.h"

#include "UI/LobbyMenuWidget.h"
#include "UI/ErrorWidget.h"
#include "UI/LoadingScreenWidget.h"
#include "UI/ConnectedPlayerWidget.h"

#include "Network/Lobby/LobbyGM.h"

#include "UnrealNetwork.h"

#include "battle_for_pandora/BFP_PlayerState.h"

#include "Logger.h"

#include <string>

ALobbyPC::ALobbyPC(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
	if (GetWorld()) {
		gameInstanceRef = GetGameInstance<UGameInfoInstance>();
	}
	SelectedCharacterID = 0;
	PreviousCharacterID = 0;
}

void ALobbyPC::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ALobbyPC, AllConnectedPlayers);
	DOREPLIFETIME(ALobbyPC, AvailableCharacters);
}

void ALobbyPC::BeginPlay() {
	Super::BeginPlay();
#ifdef UE_BUILD_DEVELOPMENT
	ConsoleCommand("DisableAllScreenMessages");
#endif
}

void ALobbyPC::EndPlay(const EEndPlayReason::Type EndPlayReason) {
	Super::EndPlay(EndPlayReason);
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("PlayerController ALobbyPC EndPlay")));
	if (EndPlayReason != EEndPlayReason::LevelTransition && EndPlayReason != EEndPlayReason::Destroyed) {
		//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("PlayerController ALobbyPC EndPlay DestroySession")));
		this->DestroySession();
	}
	cleanWidget();
}

void ALobbyPC::logoutAllClients_Implementation()
{
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("logoutClients_Implementation")));
	this->DestroySession();
}

void ALobbyPC::DestroySession_Implementation() {
	UGameInfoInstance* gameInstance = GetGameInstance<UGameInfoInstance>();
	if (gameInstance) {
		showLoadingScreenWidget();
		gameInstance->DestroySession(this); 
	}
}

void ALobbyPC::showLoadingScreenWidget_Implementation(const bool bShowMouseCursor, const EInputModeWidget& inputMode) {
	if (!LoadingScreenWB.IsValid()) {
		LoadingScreenWB = Cast<ULoadingScreenWidget>(gameInstanceRef->createWidget(this, gameInstanceRef->LoadingScreenWB_Class));
	}
	UMenuWidget* widgetCast = Cast<UMenuWidget>(LoadingScreenWB);
	gameInstanceRef->showWidget(widgetCast, gameInstanceRef->LoadingScreenWB_Class, this, bShowMouseCursor, inputMode);
}

void ALobbyPC::showErrorWidget_Implementation(const bool bShowMouseCursor, const EInputModeWidget& inputMode) {
	gameInstanceRef->showErrorWidget(this, bShowMouseCursor, inputMode);
}

void ALobbyPC::showLobbyMenuWidget_Implementation(const bool bShowMouseCursor, const EInputModeWidget& inputMode) {
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("showLobbyMenuWidget_Implementation %s"), *this->GetFName().ToString()));
	if (!LobbyMenuWD.IsValid()) {
		LobbyMenuWD = Cast<ULobbyMenuWidget>(gameInstanceRef->createWidget(this, gameInstanceRef->LobbyMenuWD_Class));
	}
	UMenuWidget* widgetCast = Cast<UMenuWidget>(LobbyMenuWD);
	gameInstanceRef->showWidget(widgetCast, gameInstanceRef->LobbyMenuWD_Class, this, bShowMouseCursor, inputMode);
}

void ALobbyPC::createConnectedPlayerWidget(const FPlayerInfo newPlayer) {
	if (LobbyMenuWD.IsValid()) {
		UConnectedPlayerWidget* connectedWidget = Cast<UConnectedPlayerWidget>(gameInstanceRef->createWidget(this, gameInstanceRef->ConnectedPlayerWD_Class));
		connectedWidget->statutPlayer = newPlayer.MyPlayerStatus;
		connectedWidget->image = newPlayer.MyPlayerCharacterImage.Get();
		connectedWidget->nomCharacter = newPlayer.MyPlayerName;
		LobbyMenuWD->UpdatePlayerWindow(connectedWidget);
	}
}

void ALobbyPC::cleanWidget() {
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("cleanWidget_Implementation %s"), *this->GetFName().ToString()));
	if (LobbyMenuWD.IsValid()) {
		LobbyMenuWD->Clean(true);
	}
	if (LoadingScreenWB.IsValid()) {
		LoadingScreenWB->Clean(true);
	}
}

void ALobbyPC::AssignPlayer(int16 id, TSubclassOf<ACharacter> character, UTexture2D* image, const FText& name,
	                         UTexture2D* IconInGameImage, UTexture2D* IconOnMap) {
	FPlayerInfo* playerSettings = &GetPlayerState<ABFP_PlayerState>()->S_PlayerInfo;
	playerSettings->CharacterID = id;
	playerSettings->MyPlayerCharacter = character;
	playerSettings->MyPlayerCharacterImage = image;
	playerSettings->MyPlayerName = name;
	playerSettings->IconInGameImage = IconInGameImage;
	playerSettings->IconOnMapImage = IconOnMap;
	auto aaa = name.ToString();
	std::string boule(TCHAR_TO_UTF8(*aaa));
	MY_LOG_NETWORK(TEXT("AssignPlayer %s"), *aaa);
	Update(false);
}

//void ALobbyPC::AssignPlayer_Implementation(TSubclassOf<ACharacter> character, UTexture2D* image) {
//	FPlayerInfo* playerSettings = &GetPlayerState<ABFP_PlayerState>()->S_PlayerInfo;
//	playerSettings->MyPlayerCharacter = character;
//	playerSettings->MyPlayerCharacterImage = image;
//	Update(*playerSettings, false);
//}

//bool ALobbyPC::AssignPlayer_Validate(TSubclassOf<ACharacter> character, UTexture2D* image) {
//	return true;
//}

void ALobbyPC::AssignSelectedCharacter_Implementation(int16 characterId, UTexture2D* image, const FText& name, UTexture2D* IconInGameImage, UTexture2D* _IconOnMap) {
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("Character Selected %d"), characterId));
	PreviousCharacterID = SelectedCharacterID;
	SelectedCharacterID = characterId;
	CharacterImage = image;
	auto aaa = name.ToString();
   std::string boule (TCHAR_TO_UTF8(*aaa));
	MY_LOG_NETWORK(TEXT("AssignSelectedCharacter_Implementation %s"),*aaa );
	CharacterName = name;
	IconInGame = IconInGameImage;
	IconOnMap = _IconOnMap;
	CheckCharacter();
}

bool ALobbyPC::AssignSelectedCharacter_Validate(int16 characterId, UTexture2D* image, const FText& name, UTexture2D* IconInGameImage, UTexture2D* _IconOnMap) {
	return true;
}

void ALobbyPC::Update_Implementation(const bool changedStatusIn) {
	FPlayerInfo* playerSettings = &GetPlayerState<ABFP_PlayerState>()->S_PlayerInfo;
	TWeakObjectPtr<UWorld> world = GetWorld();
	TWeakObjectPtr<ALobbyGM> gameMode = world->GetAuthGameMode<ALobbyGM>();
	gameMode->SwapCharacter(this, playerSettings->MyPlayerCharacter, changedStatusIn);
	gameMode->EveryoneUpdate();
}

bool ALobbyPC::Update_Validate(const bool changedStatusIn) {
	return true;
}

void ALobbyPC::UpdateNumberOfPlayers_Implementation(const uint16& currentPlayers, const uint16& maxPlayers) {
	LobbyMenuWD->nbMaxPlayer = maxPlayers;
	LobbyMenuWD->NbPlayerModify(currentPlayers);
}

void ALobbyPC::AddPlayerInfo(const TArray<FPlayerInfo>& connectedPlayers) {
	AllConnectedPlayers = TArray<FPlayerInfo>(connectedPlayers);
	if (GetWorld()->IsServer()) {
		OnRep_AllConnectedPlayers();
	}
}


void ALobbyPC::UpdateAvailableCharacters(const TArray<bool>& availableCharacters) {
	AvailableCharacters = TArray<bool>(availableCharacters);
}

void ALobbyPC::CheckCharacter() {
	TWeakObjectPtr<UWorld> world = GetWorld();
	TWeakObjectPtr<ALobbyGM> gameMode = Cast<ALobbyGM>(world->GetAuthGameMode());
	if (gameMode.IsValid())
	{
		gameMode->UpdateAvailableCharacter(SelectedCharacterID, PreviousCharacterID);
		AssignPlayer(SelectedCharacterID, gameMode->Characters[SelectedCharacterID], CharacterImage.Get(), CharacterName,
			IconInGame.Get(), IconOnMap.Get());
	}
}

void ALobbyPC::PlayerReadyStatus_Implementation(const bool ready) {
	TWeakObjectPtr<UWorld> world = GetWorld();
	TWeakObjectPtr<ALobbyGM> gameMode = Cast<ALobbyGM>(world->GetAuthGameMode());
	GetPlayerState<ABFP_PlayerState>()->S_PlayerInfo.MyPlayerStatus = ready;
	gameMode->EveryoneUpdate();
}

bool ALobbyPC::PlayerReadyStatus_Validate(const bool ready) {
	return true;
}

void ALobbyPC::OnRep_AllConnectedPlayers() {
	if (LobbyMenuWD.IsValid()) {
		LobbyMenuWD->ClearPlayerList();
	}
	for (FPlayerInfo newPlayer : AllConnectedPlayers) {
		createConnectedPlayerWidget(newPlayer);
	}
}

void ALobbyPC::OnRep_AvailableCharacters() {
	for (int i = 0; i < AvailableCharacters.Num(); ++i) {
		MY_LOG_NETWORK(TEXT("LobbyPC OnRep_AvailableCharacters %u available ? %u"), i, AvailableCharacters[i]);
	}
	for (int i = 0; i < AvailableCharacters.Num(); ++i) {
		if (LobbyMenuWD.IsValid()) {
			TWeakObjectPtr<UCharacterSelectWidget> characterSelect = LobbyMenuWD->CharacterSelectWD;
			MY_LOG_NETWORK(TEXT("LobbyPC OnRep_AvailableCharacters characterSelect ?"), characterSelect.IsValid());
			if (characterSelect.IsValid()) {
				TWeakObjectPtr<UCharacterSelectButtonPoolWidget> buttonPool = characterSelect->CharacterPanel;
				MY_LOG_NETWORK(TEXT("LobbyPC OnRep_AvailableCharacters buttonPool"), buttonPool.IsValid());
				if (buttonPool.IsValid()) {
					TWeakObjectPtr<UCharacterSelectButton> button = buttonPool->characterSelectButtons[i];
					MY_LOG_NETWORK(TEXT("LobbyPC OnRep_AvailableCharacters button"), button.IsValid());
					if (button.IsValid()) {
						button->SetIsEnabled(AvailableCharacters[i]);
					}
				}
			}
		}
	}
}