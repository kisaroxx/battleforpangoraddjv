// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "LobbyPC.h"
#include "GameFramework/GameMode.h"
#include "GameFramework/PlayerStart.h"
#include "GameFramework/Character.h"
#include "Network/PlayerInfo.h"
#include "LobbyGM.generated.h"

/**
 * 
 */
UCLASS(MinimalAPI)
class ALobbyGM : public AGameMode
{
	GENERATED_BODY()

public:
	ALobbyGM(const FObjectInitializer & ObjectInitializer);

	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	virtual void PostLogin(APlayerController * NewPlayer) override;
	virtual void Logout(AController* Exiting) override;

	void logoutAllClients();

	void StartGame();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ServerSettings")
	bool canWeStart = false;
	UPROPERTY(EditAnywhere, Category = "ServerSettings")
	TArray<ALobbyPC*> AllPlayerControllers;
	UPROPERTY(EditAnywhere, Category = "ServerSettings")
	TArray<bool> PlayerReady;
	UPROPERTY(EditAnywhere, Category = "ServerSettings")
	TArray<APlayerStart*> SpawnPoints;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ServerSettings")
	FText ServerName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ServerSettings")
	int MaxPlayer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ServerSettings")
	int CurrentConnectedControllers;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ServerSettings")
	TArray<FPlayerInfo> AllConnectedPlayers;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ServerSettings")
	TArray<bool> AvailableCharacters;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ServerSettings")
	TArray<TSubclassOf<ACharacter>> Characters;

	UPROPERTY(EditAnywhere, Category = "ServerSettings")
	TWeakObjectPtr<ALobbyPC> hostPC;


	// ----------------------------UPDATE CHARACTER ----------------------
	void SwapCharacter(ALobbyPC* playerController, TSubclassOf<ACharacter> MyPlayerCharacter, const bool changedStatusIn);
	void EveryoneUpdate();

	void UpdateNumberOfPlayers();
	void UpdateAvailableCharacters();
	void UpdateCanWeStart();
	UFUNCTION()
	void UpdateAvailableCharacter(const uint16 SelectedCharacterID, const uint16 PreviousCharacterID);
};
