// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Network/PlayerSaveGame.h"
#include "Engine/Texture2D.h"
#include "UI/MenuWidget.h"
#include "Network/PlayerInfo.h"

#include "Network/GameInfoInstance.h"

#include "LobbyPC.generated.h"


/**
 * 
 */
UCLASS(Blueprintable)
class BATTLE_FOR_PANDORA_API ALobbyPC : public APlayerController
{
	GENERATED_BODY()
	
	TWeakObjectPtr<UGameInfoInstance> gameInstanceRef;

public:
	ALobbyPC(const FObjectInitializer & ObjectInitializer);

	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	void BeginPlay() override;

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	//-----------------------SESSION-----------------------------
	UFUNCTION(Reliable, Client)
	void logoutAllClients();

	UFUNCTION(Reliable, Client)
	void DestroySession();

	//--------------------------WIDGET-----------------------------
	UPROPERTY(EditAnywhere, Category = "Widget")
	TWeakObjectPtr<class UErrorWidget> ErrorWD;

	UPROPERTY(EditAnywhere, Category = "Widget")
	TWeakObjectPtr<class ULoadingScreenWidget> LoadingScreenWB;

	UPROPERTY(EditAnywhere, Category = "Widget")
	TWeakObjectPtr<class ULobbyMenuWidget> LobbyMenuWD;

	UFUNCTION(Reliable, Client)
	void showLoadingScreenWidget(const bool bShowMouseCursorIn = false, const EInputModeWidget& inputMode = EInputModeWidget::UI_Only);
	UFUNCTION(Reliable, Client)
	void showErrorWidget(const bool bShowMouseCursorIn = true, const EInputModeWidget& inputMode = EInputModeWidget::UI_Only);
	UFUNCTION(Reliable, Client)
	void showLobbyMenuWidget(const bool bShowMouseCursorIn = true, const EInputModeWidget& inputMode = EInputModeWidget::UI_Only);
	//UFUNCTION(Reliable, Client)
	void createConnectedPlayerWidget(const FPlayerInfo newPlayer);
	//UFUNCTION(Reliable, Client)
	void cleanWidget();

	// ----------------------------UPDATE CHARACTER ----------------------

	uint16 SelectedCharacterID;
	uint16 PreviousCharacterID;
	UPROPERTY(ReplicatedUsing = OnRep_AllConnectedPlayers, VisibleAnywhere, BlueprintReadOnly)
	TArray<FPlayerInfo> AllConnectedPlayers;
	UPROPERTY(ReplicatedUsing = OnRep_AvailableCharacters, VisibleAnywhere, BlueprintReadOnly)
	TArray<bool> AvailableCharacters;
	TWeakObjectPtr<UTexture2D> CharacterImage;
	FText CharacterName;
	TWeakObjectPtr<UTexture2D> IconInGame;
	TWeakObjectPtr<UTexture2D> IconOnMap;

	UFUNCTION(Reliable, Server, WithValidation)
	void AssignSelectedCharacter(int16 characterId, UTexture2D* image = nullptr, const FText& name = FText(), UTexture2D* IconInGameImage = nullptr, UTexture2D* IconOnMap = nullptr);
	//UFUNCTION(Reliable, Client)
	void AssignPlayer(int16 characterID, TSubclassOf<ACharacter> characterClass, UTexture2D* image = nullptr, const FText& name = FText(), 
		UTexture2D* IconInGameImage = nullptr, UTexture2D* IconOnMap = nullptr);
	UFUNCTION(Reliable, Server, WithValidation)
	void Update(const bool changedStatusIn);
	UFUNCTION(Reliable, Client)
	void UpdateNumberOfPlayers(const uint16& currentPlayers, const uint16& maxPlayers);

	void AddPlayerInfo(const TArray<FPlayerInfo>& connectedPlayers);
	void UpdateAvailableCharacters(const TArray<bool>& availableCharacters);

	UFUNCTION(Reliable, Server, WithValidation)
	void PlayerReadyStatus(const bool ready);

	void CheckCharacter();

	UFUNCTION()
	void OnRep_AllConnectedPlayers();
	UFUNCTION()
	void OnRep_AvailableCharacters();
};
