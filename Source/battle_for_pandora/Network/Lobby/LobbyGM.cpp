// Fill out your copyright notice in the Description page of Project Settings.

#include "LobbyGM.h"
#include "LobbyPC.h"
#include "Network/GameInfoInstance.h"
#include "battle_for_pandora/BFP_PlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"
#include "LobbyPC.h"
#include "Logger.h"

ALobbyGM::ALobbyGM(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer)
{
	PlayerControllerClass = ALobbyPC::StaticClass();
	PlayerStateClass = ABFP_PlayerState::StaticClass();

	bUseSeamlessTravel = true;

	int nbCharacter = 5;
	for (int i = 0; i < nbCharacter; ++i) {
		AvailableCharacters.Add(true);
	}

	ConstructorHelpers::FClassFinder<ACharacter> PlayerPawn0_Class(TEXT("/Game/BluePrints/Characters/0_Base"));
	if (!ensure(IsValid(PlayerPawn0_Class.Class))) return;
	ConstructorHelpers::FClassFinder<ACharacter> Aeres_Class(TEXT("/Game/Characters/Aeres/AeresPlayerCharacter"));
	if (!ensure(IsValid(Aeres_Class.Class))) return;
	ConstructorHelpers::FClassFinder<ACharacter> Davros_Class(TEXT("/Game/Characters/Davros/DavrosPlayerCharacter"));
	if (!ensure(IsValid(Davros_Class.Class))) return;
	ConstructorHelpers::FClassFinder<ACharacter> Kiara_Class(TEXT("/Game/Characters/Kiara/KiaraPlayerCharacter"));
	if (!ensure(IsValid(Kiara_Class.Class))) return;
	ConstructorHelpers::FClassFinder<ACharacter> Neelf_Class(TEXT("/Game/Characters/Neelf/NeelfPlayerCharacter"));
	if (!ensure(IsValid(Neelf_Class.Class))) return;
	ConstructorHelpers::FClassFinder<ACharacter> Shanyl_Class(TEXT("/Game/Characters/Shanyl/ShanylPlayerCharacter"));
	if (!ensure(IsValid(Shanyl_Class.Class))) return;

	Characters.Reserve(nbCharacter);
	Characters.Add(MoveTemp(PlayerPawn0_Class.Class));
	Characters.Add(MoveTemp(Aeres_Class.Class));
	Characters.Add(MoveTemp(Davros_Class.Class));
	Characters.Add(MoveTemp(Kiara_Class.Class));
	Characters.Add(MoveTemp(Neelf_Class.Class));
	Characters.Add(MoveTemp(Shanyl_Class.Class));

	DefaultPawnClass = Characters[0];

	CurrentConnectedControllers = 0;
}

void ALobbyGM::PostLogin(APlayerController * NewPlayer) {
	Super::PostLogin(NewPlayer);
	TWeakObjectPtr<ALobbyPC> playerController = Cast<ALobbyPC>(NewPlayer);
	MY_LOG_NETWORK(TEXT("ALobbyGM PostLogin playerController valide ? %u"), playerController.IsValid());
	if (playerController.IsValid()) {

		if (!IsRunningDedicatedServer()) {
			bool isHostPlayer = CurrentConnectedControllers == 0;

			//on ne sait jamais si on a besoin de le savoir plus tard
			ABFP_PlayerState* playerState = playerController->GetPlayerState<ABFP_PlayerState>();
			FPlayerInfo* playerSettings = &playerState->S_PlayerInfo;
			playerSettings->IsHost = isHostPlayer;

			MY_LOG_NETWORK(TEXT("ALobbyGM PostLogin isHostPlayer ? %u"), isHostPlayer);
			if (isHostPlayer) {
				hostPC = playerController;
				hostPC->StartSpectatingOnly();
			}
			else {
				AllPlayerControllers.Add(playerController.Get());
				PlayerReady.Add(false);

				FPlayerInfo* playerSettings = &playerController->GetPlayerState<ABFP_PlayerState>()->S_PlayerInfo;
				playerSettings->MyPlayerCharacter = Characters[0];
			}
		}
		else {
			AllPlayerControllers.Add(playerController.Get());
			PlayerReady.Add(false);

			FPlayerInfo* playerSettings = &playerController->GetPlayerState<ABFP_PlayerState>()->S_PlayerInfo;
			playerSettings->MyPlayerCharacter = Characters[0];
		}

		CurrentConnectedControllers++;

		MY_LOG_NETWORK(TEXT("ALobbyGM PostLogin showLobbyMenuWidget"));
		playerController->showLobbyMenuWidget(true, EInputModeWidget::GameAndUI);
	}

	//if (!MaxPlayer) {
	//	MaxPlayer = GetGameInstance<UGameInfoInstance>()->nbPlayer_;
	//}

	EveryoneUpdate();
}

void ALobbyGM::Logout(AController* Exiting) {
	Super::Logout(Exiting);
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("GameMode ALobbyGM Logout")));
	TWeakObjectPtr<ALobbyPC> playerController = Cast<ALobbyPC>(Exiting);
	int index;
	if (AllPlayerControllers.Find(playerController.Get(), index)) {
		AllPlayerControllers.RemoveAt(index);
		PlayerReady.RemoveAt(index);
		AllConnectedPlayers.RemoveAt(index);
		CurrentConnectedControllers--;
		EveryoneUpdate();
	}
}

void ALobbyGM::BeginPlay() {
	if (IsRunningDedicatedServer()) {
		MaxPlayer = GetGameInstance<UGameInfoInstance>()->nbPlayer_;
	}
	else {
		MaxPlayer = GetGameInstance<UGameInfoInstance>()->nbPlayer_ - 1; //le serveur est non joueur
	}
	UpdateNumberOfPlayers();
	UpdateCanWeStart();
}

void ALobbyGM::EndPlay(const EEndPlayReason::Type EndPlayReason) {
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("GameMode ALobbyGM EndPlay")));
	if (!EndPlayReason == EEndPlayReason::LevelTransition) {
		if (HasAuthority()) {
			logoutAllClients();
		}
	}
}

void ALobbyGM::StartGame() {
	TWeakObjectPtr<UWorld> world = GetWorld();
	if (!ensure(world.IsValid())) return;
	//if (world->IsServer()) {
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, TEXT("Start game"));
	for (int i = 0; i < AllPlayerControllers.Num(); ++i) {
		TWeakObjectPtr<ALobbyPC> playerController = AllPlayerControllers[i];
		//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("GameMode ALobbyGM start game %s"), *playerController->GetFName().ToString()));
		playerController->cleanWidget();
		playerController->showLoadingScreenWidget();

		if (hostPC.IsValid()) {
			hostPC->cleanWidget();
			hostPC->showLoadingScreenWidget();
		}
	}
	//world->ServerTravel("/Game/Maps/Arena", false, false);
	world->ServerTravel("/Game/Maps/GameMap_Final", false, false);
	//world->ServerTravel("/Game/Maps/GameMap", false, false);
	//world->ServerTravel("/Game/TestLevel", false, false);
	//}
	//else {
	//	//gameInstanceRef->showErrorWidget();
	//}
}

void ALobbyGM::logoutAllClients()
{
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("logoutClients_Implementation")));
	for (int i = 0; i < AllPlayerControllers.Num(); ++i) {
		AllPlayerControllers[i]->DestroySession();
	}
}

void ALobbyGM::SwapCharacter(ALobbyPC* playerController, TSubclassOf<ACharacter> MyPlayerCharacter, const bool changedStatusIn) {
	if (!changedStatusIn) {
		TWeakObjectPtr<APawn> pawn = playerController->GetPawn();
		FTransform position{};
		if (pawn.IsValid()) {
			position = pawn->GetActorTransform();
			pawn->Destroy();
		}
		TWeakObjectPtr<UWorld> world = GetWorld();
		playerController->Possess(world->SpawnActor<ACharacter>(MyPlayerCharacter, position, {}));
	}
}

void ALobbyGM::EveryoneUpdate() {

	UpdateNumberOfPlayers();
	UpdateAvailableCharacters();
	UpdateCanWeStart();
}

void ALobbyGM::UpdateAvailableCharacter(uint16 SelectedCharacterID, uint16 PreviousCharacterID) {
	if (SelectedCharacterID > 0) {
		if (AvailableCharacters[SelectedCharacterID - 1]) {
			AvailableCharacters[SelectedCharacterID - 1] = false;
		}
	}
	if (PreviousCharacterID > 0) {
		AvailableCharacters[PreviousCharacterID - 1] = true;
	}
}

void ALobbyGM::UpdateNumberOfPlayers() {
	int currentPlayerCount;
	if (IsRunningDedicatedServer()) {
		currentPlayerCount = CurrentConnectedControllers;
	}
	else {
		currentPlayerCount = CurrentConnectedControllers-1;
	}

	if (hostPC.IsValid()) {
		hostPC->UpdateNumberOfPlayers(currentPlayerCount, MaxPlayer);
	}

	AllConnectedPlayers.Empty();
	for (ALobbyPC* playerController : AllPlayerControllers) {
		AllConnectedPlayers.Add(playerController->GetPlayerState<ABFP_PlayerState>()->S_PlayerInfo);
		playerController->UpdateNumberOfPlayers(currentPlayerCount, MaxPlayer);
	}
}

void ALobbyGM::UpdateAvailableCharacters() {
	for (ALobbyPC* playerController : AllPlayerControllers) {
		playerController->AddPlayerInfo(AllConnectedPlayers);
		playerController->UpdateAvailableCharacters(AvailableCharacters);
	}
	if (hostPC.IsValid()) {
		hostPC->AddPlayerInfo(AllConnectedPlayers);
	}

}

void ALobbyGM::UpdateCanWeStart() {
	int currentPlayerCount;
	if (IsRunningDedicatedServer()) {
		currentPlayerCount = CurrentConnectedControllers;
	}
	else {
		currentPlayerCount = CurrentConnectedControllers - 1;
	}
	canWeStart = currentPlayerCount == MaxPlayer;
	MY_LOG_NETWORK(TEXT("ALobbyGM UpdateCanWeStart nb max player ? %u"), canWeStart);
	int i = 0;
	while (canWeStart && i < AllConnectedPlayers.Num()) {
		canWeStart = AllConnectedPlayers[i].MyPlayerCharacter != Characters[0];
		MY_LOG_NETWORK(TEXT("ALobbyGM UpdateCanWeStart character non null ? %u"), canWeStart);
		++i;
	}

	i = 0;
	while (canWeStart && i < AllPlayerControllers.Num()) {
		canWeStart = AllPlayerControllers[i]->GetPlayerState<ABFP_PlayerState>()->S_PlayerInfo.MyPlayerStatus;
		MY_LOG_NETWORK(TEXT("ALobbyGM UpdateCanWeStart joueur ready ? %u"), canWeStart);
		++i;
	}

	if (IsRunningDedicatedServer() && canWeStart) {
		StartGame();
	}
}