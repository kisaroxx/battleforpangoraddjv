// Fill out your copyright notice in the Description page of Project Settings.

#include "GameInfoInstance.h"

#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"
#include "Logger.h"

#include "UI/CharacterSelectWidget.h"
#include "UI/ConnectedPlayerWidget.h"
#include "UI/HostMenuWidget.h"
#include "UI/LoadingScreenWidget.h"
#include "UI/LobbyMenuWidget.h"
#include "UI/MainMenuWidget.h"
#include "UI/OptionMenuWidget.h"
#include "UI/PlayerButtonWidget.h"
#include "UI/ServerMenuWidget.h"
#include "UI/ErrorWidget.h"
#include "UI/MenuWidget.h"

#include "IPAddress.h"
#include "OnlineSubsystem.h"
#include "SocketSubsystem.h"
#include "CoreOnline.h"

#include "BFP_PlayerController.h"

UGameInfoInstance::UGameInfoInstance(const FObjectInitializer & ObjectInitializer)
{
	savingSlot = "saveGameSlot";

	nomSession_ = NAME_GameSession;

	// BINDS DELEGATES GESTION SESSION
	onCreateSessionCompleteDelegate = FOnCreateSessionCompleteDelegate::CreateUObject(this, &UGameInfoInstance::onCreateSessionComplete);
	onStartSessionCompleteDelegate = FOnStartSessionCompleteDelegate::CreateUObject(this, &UGameInfoInstance::onStartSessionComplete);
	onFindSessionsCompleteDelegate = FOnFindSessionsCompleteDelegate::CreateUObject(this, &UGameInfoInstance::onFindSessionsComplete);
	onJoinSessionCompleteDelegate = FOnJoinSessionCompleteDelegate::CreateUObject(this, &UGameInfoInstance::onJoinSessionComplete);
	OnDestroySessionCompleteDelegate = FOnDestroySessionCompleteDelegate::CreateUObject(this, &UGameInfoInstance::onDestroySessionComplete);

	// BINDS WIDGET BLUEPRINTS CLASS
	static ConstructorHelpers::FClassFinder<UUserWidget> CharacterSelectBPClass{ TEXT("/Game/UI/Lobby/CharacterSelectWidget_BP") };
	if (!ensure(IsValid(CharacterSelectBPClass.Class))) return;
	CharacterSelectWD_Class = CharacterSelectBPClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> ConnectedPlayerBPClass(TEXT("/Game/UI/Lobby/ConnectedPlayerWidget_BP"));
	if (!ensure(IsValid(ConnectedPlayerBPClass.Class))) return;
	ConnectedPlayerWD_Class = ConnectedPlayerBPClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> ErrorBPClass(TEXT("/Game/UI/MainMenu/ErrorWidget_BP"));
	if (!ensure(IsValid(ErrorBPClass.Class))) return;
	ErrorWD_Class = ErrorBPClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> ExitBPClass(TEXT("/Game/UI/MainMenu/ExitWidget_BP"));
	if (!ensure(IsValid(ExitBPClass.Class))) return;
	ExitWD_Class = ExitBPClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> HostMenuBPClass(TEXT("/Game/UI/MainMenu/HostMenuWidget_BP"));
	if (!ensure(IsValid(HostMenuBPClass.Class))) return;
	HostMenuWD_Class = HostMenuBPClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> LoadingScreenBPClass(TEXT("/Game/UI/MainMenu/LoadingScreenWidget_BP"));
	if (!ensure(IsValid(LoadingScreenBPClass.Class))) return;
	LoadingScreenWB_Class = LoadingScreenBPClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> LobbyMenuBPClass(TEXT("/Game/UI/Lobby/LobbyMenuWidget_BP"));
	if (!ensure(IsValid(LobbyMenuBPClass.Class))) return;
	LobbyMenuWD_Class = LobbyMenuBPClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> OptionMenuBPClass(TEXT("/Game/UI/MainMenu/OptionMenuWidget_BP"));
	if (!ensure(IsValid(OptionMenuBPClass.Class))) return;
	OptionMenuWD_Class = OptionMenuBPClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> MainMenuBPClass(TEXT("/Game/UI/MainMenu/MainMenuWidget_BP"));
	if (!ensure(IsValid(MainMenuBPClass.Class))) return;
	MainMenuWD_Class = MainMenuBPClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> PlayerButtonBPClass(TEXT("/Game/UI/Lobby/PlayerButtonWidget_BP"));
	if (!ensure(IsValid(PlayerButtonBPClass.Class))) return;
	PlayerButtonWD_Class = PlayerButtonBPClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> ServerMenuBPClass(TEXT("/Game/UI/MainMenu/ServerMenuWidget_BP"));
	if (!ensure(IsValid(ServerMenuBPClass.Class))) return;
	ServerMenuWD_Class = ServerMenuBPClass.Class;
}

void UGameInfoInstance::Init() {
	Super::Init();
}

void UGameInfoInstance::Shutdown() {
	Super::Shutdown();
	if (GetWorld()) {
		for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
		{
			APlayerController* Controller = Iterator->Get();
			if (Controller)
			{
				ABFP_PlayerController* PlayerController = Cast<ABFP_PlayerController>(Controller);
				if (PlayerController)
				{
					PlayerController->DestroySession();
				}
			}
		}
		DestroySession(GetWorld()->GetFirstPlayerController());
	}
}

void UGameInfoInstance::showWidget(UMenuWidget* widget, TSubclassOf<UUserWidget> widgetClass, APlayerController* playerController, const bool bShowMouseCursor, const EInputModeWidget& inputMode) {
	//on nettoie l'ecran
	//cleanWidget();
	//APlayerController* playerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (!IsDedicatedServerInstance()) {
		if (!IsValid(playerController)) {
			MY_LOG_NETWORK(TEXT("UGameInfoInstance showWidget playerController nullptr, on prend celui par defaut"));
			playerController = GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld());
		}
		if (!ensure(widgetClass != nullptr)) return;
		if (!ensure(IsValid(widget)))
		{
			widget = createWidget(playerController, widgetClass);
			if (!ensure(IsValid(widget))) return;
		}
		widget->Setup(bShowMouseCursor, inputMode);
	}
}

UMenuWidget* UGameInfoInstance::createWidget(TWeakObjectPtr<APlayerController> playerController, TSubclassOf<UUserWidget> widgetClass) {
	UMenuWidget* widget;
	widget = CreateWidget<UMenuWidget>(playerController.Get(), widgetClass);
	return widget;
}

UMenuWidget* UGameInfoInstance::createWidget(TSubclassOf<UUserWidget> widgetClass) {
	APlayerController* playerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	return createWidget(playerController, widgetClass);
}

void UGameInfoInstance::cleanWidget() {
	//on nettoie l'ecran
	CleanupGameViewport();
	//if (LoadingScreenWB.IsValid()) {
	//	LoadingScreenWB->Clean();
	//}
	//if (LobbyMenuWD.IsValid()) {
	//	LobbyMenuWD->Clean();
	//}
}

void UGameInfoInstance::showMainMenuWidget(APlayerController* playerController, const bool bShowMouseCursor, const EInputModeWidget& inputMode) {
	if (!MainMenuWD.IsValid()) {
		MainMenuWD = Cast<UMainMenuWidget>(createWidget(playerController, MainMenuWD_Class));
	}
	UMenuWidget* widgetCast = Cast<UMenuWidget>(MainMenuWD);
	showWidget(widgetCast, MainMenuWD_Class, playerController, bShowMouseCursor, inputMode);
}

void UGameInfoInstance::showHostMenuWidget(APlayerController* playerController, const bool bShowMouseCursor, const EInputModeWidget& inputMode) {
	if (!HostMenuWD.IsValid()) {
		HostMenuWD = Cast<UHostMenuWidget>(createWidget(playerController, HostMenuWD_Class));
	}
	UMenuWidget* widgetCast = Cast<UMenuWidget>(HostMenuWD);
	showWidget(widgetCast, HostMenuWD_Class, playerController, bShowMouseCursor, inputMode);
}

void UGameInfoInstance::showOptionMenuWidget(APlayerController* playerController, const bool bShowMouseCursor, const EInputModeWidget& inputMode) {
	if (!OptionMenuWD.IsValid()) {
		OptionMenuWD = Cast<UOptionMenuWidget>(createWidget(playerController, OptionMenuWD_Class));
	}
	UMenuWidget* widgetCast = Cast<UMenuWidget>(OptionMenuWD);
	showWidget(widgetCast, OptionMenuWD_Class, playerController, bShowMouseCursor, inputMode);
}

void UGameInfoInstance::showServerMenuWidget(APlayerController* playerController, const bool bShowMouseCursor, const EInputModeWidget& inputMode) {
	if (!ServerMenuWD.IsValid()) {
		ServerMenuWD = Cast<UServerMenuWidget>(createWidget(playerController, ServerMenuWD_Class));
	}
	UMenuWidget* widgetCast = Cast<UMenuWidget>(ServerMenuWD);
	showWidget(widgetCast, ServerMenuWD_Class, playerController, bShowMouseCursor, inputMode);
}

void UGameInfoInstance::showLoadingScreenWidget(APlayerController* playerController, const bool bShowMouseCursor, const EInputModeWidget& inputMode) {
	if (!LoadingScreenWB.IsValid()) {
		LoadingScreenWB = Cast<ULoadingScreenWidget>(createWidget(playerController, LoadingScreenWB_Class));
	}
	UMenuWidget* widgetCast = Cast<UMenuWidget>(LoadingScreenWB);
	showWidget(widgetCast, LoadingScreenWB_Class, playerController, bShowMouseCursor, inputMode);
}

void UGameInfoInstance::showErrorWidget(APlayerController* playerController, const bool bShowMouseCursor, const EInputModeWidget& inputMode) {
	if (!ErrorWD.IsValid()) {
		ErrorWD = Cast<UErrorWidget>(createWidget(playerController, ErrorWD_Class));
	}
	UMenuWidget* widgetCast = Cast<UMenuWidget>(ErrorWD);
	showWidget(widgetCast, ErrorWD_Class, playerController, bShowMouseCursor, inputMode);
}

void UGameInfoInstance::showLobbyMenuWidget(APlayerController* playerController, const bool bShowMouseCursor, const EInputModeWidget& inputMode) {
	if (!LobbyMenuWD.IsValid()) {
		LobbyMenuWD = Cast<ULobbyMenuWidget>(createWidget(playerController, LobbyMenuWD_Class));
	}
	UMenuWidget* widgetCast = Cast<UMenuWidget>(LobbyMenuWD);
	showWidget(widgetCast, LobbyMenuWD_Class, playerController, bShowMouseCursor, inputMode);
}

void UGameInfoInstance::showCharacterSelectWidget(APlayerController* playerController, const bool bShowMouseCursor, const EInputModeWidget& inputMode) {
	if (!CharacterSelectWD.IsValid()) {
		CharacterSelectWD = Cast<UCharacterSelectWidget>(createWidget(playerController, CharacterSelectWD_Class));
	}
	UMenuWidget* widgetCast = Cast<UMenuWidget>(CharacterSelectWD);
	showWidget(widgetCast, CharacterSelectWD_Class, playerController, bShowMouseCursor, inputMode);
}


void UGameInfoInstance::savePlayerInfo(FPlayerInfo& playerInfo) {
	if (!saveGameRef.IsValid()) {
		saveGameRef = Cast<UPlayerSaveGame>(UGameplayStatics::CreateSaveGameObject(UPlayerSaveGame::StaticClass()));
	}
	saveGameRef->S_PlayerInfo = playerInfo;
	UGameplayStatics::SaveGameToSlot(saveGameRef.Get(), savingSlot, 0);
}

FPlayerInfo UGameInfoInstance::loadPlayerInfo() {
	saveGameRef = Cast<UPlayerSaveGame>(UGameplayStatics::LoadGameFromSlot(savingSlot, 0));
	FPlayerInfo playerInfo = saveGameRef->S_PlayerInfo;
	return playerInfo;
}

void UGameInfoInstance::savePlayerInfoCheck(FPlayerInfo& playerInfo) {
	if (UGameplayStatics::DoesSaveGameExist(savingSlot, 0)) {
		playerInfo = loadPlayerInfo();
	}
	savePlayerInfo(playerInfo);
}

void UGameInfoInstance::saveGameCheck(TWeakObjectPtr<APlayerController> playerController) {
	saveGameCreated_ = UGameplayStatics::DoesSaveGameExist(savingSlot, 0);
	if (saveGameCreated_) {
		showMainMenuWidget(playerController.Get());
	}
	else {
		showOptionMenuWidget(playerController.Get());
	}
	//savePlayerInfo(playerInfo);
}

void UGameInfoInstance::deletePlayerInfo() {
	UGameplayStatics::DeleteGameInSlot(savingSlot, 0);
}

void UGameInfoInstance::pauseGame(bool pause) {
	APlayerController* playerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (pause) {
		playerController->SetInputMode(FInputModeUIOnly());
	}
	else {
		playerController->SetInputMode(FInputModeGameAndUI());
	}
	playerController->SetPause(pause);
}

void UGameInfoInstance::quitGame(APlayerController* playerController) {
	if (IsDedicatedServerInstance()) {
		FGenericPlatformMisc::RequestExit(false);
	}
	else {
		UKismetSystemLibrary::QuitGame(GetWorld(), playerController, EQuitPreference::Quit, false);
	}
}

void UGameInfoInstance::mainMenu(APlayerController* playerController) {
	if (IsValid(playerController)) {
		playerController->ClientTravel("/Game/Maps/MainMenu", ETravelType::TRAVEL_Absolute, false);
	}
	else {
		UGameplayStatics::OpenLevel(GetWorld(), FName("MainMenu"), true);
	}
}

void UGameInfoInstance::LaunchLobby(APlayerController* playerController, int nbPlayer, FText serverName, bool enableLAN) {
	nbPlayer_ = nbPlayer;
	serverName_ = serverName;
	if (!IsDedicatedServerInstance()) {
		showLoadingScreenWidget(playerController);
	}
	FString levelName = "Taverne";
	if (!CreateSession(playerController, nbPlayer_, levelName, enableLAN)) {
		showErrorWidget(playerController);
	}
}

bool UGameInfoInstance::CreateSession(APlayerController* playerController, int nbPlayer, FString levelName, bool enableLAN) {
	TSharedPtr<const FUniqueNetId> userId = 0;
	if (playerController) {
		ULocalPlayer* const Player = playerController->GetLocalPlayer();
		FUniqueNetIdRepl PlayerNetId = Player->GetPreferredUniqueNetId();
		userId = PlayerNetId.GetUniqueNetId();
	}
	return CreateSession(userId, nbPlayer, levelName, enableLAN);
}

bool UGameInfoInstance::CreateSession(TSharedPtr<const FUniqueNetId> UserId, int nbPlayer, FString levelName, bool enableLAN) {
	// Get the Online Subsystem to work with
	IOnlineSubsystem* const OnlineSub = IOnlineSubsystem::Get();

	if (OnlineSub)
	{
		MY_LOG_NETWORK(TEXT("UGameInfoInstance CreateSession OnlineSub OK"));
		// Get the Session Interface, so we can call the "CreateSession" function on it
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

		MY_LOG_NETWORK(TEXT("UGameInfoInstance CreateSession Sessions ? %u"), Sessions.IsValid());
		if (Sessions.IsValid())
		{
			MY_LOG_NETWORK(TEXT("UGameInfoInstance CreateSession Sessions et UserId OK"));
			/*
				Fill in all the Session Settings that we want to use.

				There are more with SessionSettings.Set(...);
				For example the Map or the GameMode/Type.
			*/
			SessionSettings = MakeShareable(new FOnlineSessionSettings());

			SessionSettings->bIsLANMatch = enableLAN;
			SessionSettings->bIsDedicated = IsDedicatedServerInstance();
			SessionSettings->bUsesPresence = IsDedicatedServerInstance();
			SessionSettings->NumPublicConnections = nbPlayer;
			SessionSettings->NumPrivateConnections = 1;
			SessionSettings->bAllowInvites = true;
			SessionSettings->bAllowJoinInProgress = true;
			SessionSettings->bShouldAdvertise = true;
			SessionSettings->bAllowJoinViaPresence = IsDedicatedServerInstance();
			SessionSettings->bAllowJoinViaPresenceFriendsOnly = false;

			levelName.RemoveFromStart(GetWorld()->StreamingLevelsPrefix);

			SessionSettings->Set(SETTING_MAPNAME, levelName, EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);

			// Set the delegate to the Handle of the SessionInterface
			onCreateSessionCompleteDelegateHandle = Sessions->AddOnCreateSessionCompleteDelegate_Handle(onCreateSessionCompleteDelegate);

			// Our delegate should get called when this is complete (doesn't need to be successful!)
			return Sessions->CreateSession(0, nomSession_, *SessionSettings);
		}
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, TEXT("No OnlineSubsytem found!"));
	}

	return false;
}

bool UGameInfoInstance::JoinSession(APlayerController* playerController, FName SessionName, const FOnlineSessionSearchResult& SearchResult) {
	ULocalPlayer* const Player = playerController->GetLocalPlayer();
	FUniqueNetIdRepl PlayerNetId = Player->GetPreferredUniqueNetId();
	return JoinSession(PlayerNetId.GetUniqueNetId(), SessionName, SearchResult);
}

bool UGameInfoInstance::JoinSession(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, const FOnlineSessionSearchResult& SearchResult) {
	// Return bool
	bool bSuccessful = false;

	// Get OnlineSubsystem we want to work with
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();

	if (OnlineSub)
	{
		// Get SessionInterface from the OnlineSubsystem
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();
		if (Sessions.IsValid() && UserId.IsValid())
		{
			// Set the Handle again
			onJoinSessionCompleteDelegateHandle = Sessions->AddOnJoinSessionCompleteDelegate_Handle(onJoinSessionCompleteDelegate);

			// Call the "JoinSession" Function with the passed "SearchResult". The "SessionSearch->SearchResults" can be used to get such a
			// "FOnlineSessionSearchResult" and pass it. Pretty straight forward!
			bSuccessful = Sessions->JoinSession(*UserId, SessionName, SearchResult);
		}
	}

	return bSuccessful;
}
void UGameInfoInstance::JoinServer(APlayerController* playerController, uint8 idSession) {
	if (!JoinSession(playerController, nomSession_, availableSessions[idSession])) {
		showErrorWidget(playerController);
		DestroySession();

	}
}

void UGameInfoInstance::FindSessions(APlayerController* playerController, bool bIsLAN, bool bIsPresence) {
	ULocalPlayer* const Player = playerController->GetLocalPlayer();
	FUniqueNetIdRepl PlayerNetId = Player->GetPreferredUniqueNetId();
	FindSessions(PlayerNetId.GetUniqueNetId(), bIsLAN, bIsPresence);
}
void UGameInfoInstance::FindSessions(TSharedPtr<const FUniqueNetId> UserId, bool bIsLAN, bool bIsPresence) {
	// Get the OnlineSubsystem we want to work with
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();

	if (OnlineSub)
	{
		// Get the SessionInterface from our OnlineSubsystem
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

		if (Sessions.IsValid() && UserId.IsValid())
		{
			/*
				Fill in all the SearchSettings, like if we are searching for a LAN game and how many results we want to have!
			*/
			SessionSearch = MakeShareable(new FOnlineSessionSearch());

			SessionSearch->SearchState = EOnlineAsyncTaskState::Done;
			SessionSearch->bIsLanQuery = bIsLAN;
			SessionSearch->MaxSearchResults = 20;
			SessionSearch->PingBucketSize = 50;

			// We only want to set this Query Setting if "bIsPresence" is true
			if (bIsPresence)
			{
				SessionSearch->QuerySettings.Set(SEARCH_PRESENCE, bIsPresence, EOnlineComparisonOp::Equals);
				//SessionSearch->QuerySettings.Set(SEARCH_DEDICATED_ONLY, true, EOnlineComparisonOp::Equals);
				SessionSearch->QuerySettings.Set(SEARCH_NONEMPTY_SERVERS_ONLY, true, EOnlineComparisonOp::Equals);
			}

			TSharedRef<FOnlineSessionSearch> SearchSettingsRef = SessionSearch.ToSharedRef();

			// Set the Delegate to the Delegate Handle of the FindSession function
			onFindSessionsCompleteDelegateHandle = Sessions->AddOnFindSessionsCompleteDelegate_Handle(onFindSessionsCompleteDelegate);

			// Finally call the SessionInterface function. The Delegate gets called once this is finished
			Sessions->FindSessions(*UserId, SearchSettingsRef);
		}
	}
	else
	{
		// If something goes wrong, just call the Delegate Function directly with "false".
		onFindSessionsComplete(false);
	}
}

void UGameInfoInstance::DestroySession(APlayerController* playerController) {
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

		if (Sessions.IsValid())
		{
			Sessions->AddOnDestroySessionCompleteDelegate_Handle(OnDestroySessionCompleteDelegate);
			FNamedOnlineSession* session = Sessions->GetNamedSession(nomSession_);
			//TArray<TSharedRef<const FUniqueNetId>> RegisteredPlayers = session->RegisteredPlayers;
			//if (Sessions->UnregisterPlayers(nomSession_, RegisteredPlayers)) {
			if ((IsValid(playerController) && playerController->IsLocalController())) {
				ULocalPlayer* Player = playerController->GetLocalPlayer();
				FUniqueNetIdRepl PlayerNetId = Player->GetPreferredUniqueNetId();
				Sessions->UnregisterPlayer(nomSession_, *PlayerNetId);
				Sessions->DestroySession(nomSession_);
			}
		}
	}
}



void UGameInfoInstance::onCreateSessionComplete(FName SessionName, bool bWasSuccessful) {
	cleanWidget();

	// Get the OnlineSubsystem so we can get the Session Interface
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		// Get the Session Interface to call the StartSession function
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

		if (Sessions.IsValid())
		{
			// Clear the SessionComplete delegate handle, since we finished this call
			Sessions->ClearOnCreateSessionCompleteDelegate_Handle(onCreateSessionCompleteDelegateHandle);
			if (bWasSuccessful)
			{
				// Set the StartSession delegate handle
				onStartSessionCompleteDelegateHandle = Sessions->AddOnStartSessionCompleteDelegate_Handle(onStartSessionCompleteDelegate);

				// Our StartSessionComplete delegate should get called after this
				Sessions->StartSession(SessionName);
			}
			else {
				sessionCreatedEvent.Broadcast(false);
			}
		}
	}
}

void UGameInfoInstance::onStartSessionComplete(FName SessionName, bool bWasSuccessful) {
	// Get the Online Subsystem so we can get the Session Interface
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		// Get the Session Interface to clear the Delegate
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();
		if (Sessions.IsValid())
		{
			// Clear the delegate, since we are done with this call
			Sessions->ClearOnStartSessionCompleteDelegate_Handle(onStartSessionCompleteDelegateHandle);
		}
	}

	// If the start was successful, we can open a NewMap if we want. Make sure to use "listen" as a parameter!
	if (bWasSuccessful)
	{
		sessionCreatedEvent.Broadcast(true);
		UGameplayStatics::OpenLevel(GetWorld(), FName("Taverne"), true, "listen");
	}
	else {
		sessionCreatedEvent.Broadcast(false);
	}
}
void UGameInfoInstance::onFindSessionsComplete(bool bWasSuccessful) {
	if (!bWasSuccessful) {
		sessionFoundEvent.Broadcast(false);
	}
	else {
		// Get OnlineSubsystem we want to work with
		IOnlineSubsystem* const OnlineSub = IOnlineSubsystem::Get();
		if (OnlineSub)
		{
			// Get SessionInterface of the OnlineSubsystem
			IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();
			if (Sessions.IsValid())
			{
				// Clear the Delegate handle, since we finished this call
				Sessions->ClearOnFindSessionsCompleteDelegate_Handle(onFindSessionsCompleteDelegateHandle);

				// Just debugging the Number of Search results. Can be displayed in UMG or something later on
				//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("Num Search Results: %d"), SessionSearch->SearchResults.Num()));

				// If we have found at least 1 session, we just going to debug them. You could add them to a list of UMG Widgets, like it is done in the BP version!
				if (SessionSearch->SearchResults.Num() > 0)
				{
					availableSessions.Empty();
					// "SessionSearch->SearchResults" is an Array that contains all the information. You can access the Session in this and get a lot of information.
					// This can be customized later on with your own classes to add more information that can be set and displayed
					for (int32 SearchIdx = 0; SearchIdx < SessionSearch->SearchResults.Num(); SearchIdx++)
					{
						// OwningUserName is just the SessionName for now. I guess you can create your own Host Settings class and GameSession Class and add a proper GameServer Name here.
						// This is something you can't do in Blueprint for example!
						FOnlineSessionSearchResult& searchResult = SessionSearch->SearchResults[SearchIdx];
						GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("Session Number: %d | SessionName: %s "), SearchIdx + 1, *(searchResult.Session.OwningUserName)));

						// RECUPERATION ADRESSE IP
						ISocketSubsystem* SS = ISocketSubsystem::Get();
						FString ConnectInfo;
						Sessions->GetResolvedConnectString(searchResult, GamePort, ConnectInfo);
						//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("Session Number: %d | ServerIP: %s "), SearchIdx + 1, *(ConnectInfo)));
						TSharedRef<FInternetAddr> Addr = SS->CreateInternetAddr();
						bool bIsValid;
						Addr->SetIp(*ConnectInfo, bIsValid);

						if (searchResult.Session.NumOpenPublicConnections > 0) {
							availableSessions.Add(searchResult);
						}

					}

					if (availableSessions.Num() > 0) {
						sessionFoundEvent.Broadcast(true);
					}
				}
				else {
					sessionFoundEvent.Broadcast(false);
				}
			}
		}
	}
}
void UGameInfoInstance::onJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result) {
	if (Result == EOnJoinSessionCompleteResult::Type::Success) {
		// Get the OnlineSubsystem we want to work with
		IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
		if (OnlineSub)
		{
			// Get SessionInterface from the OnlineSubsystem
			IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

			if (Sessions.IsValid())
			{
				// Clear the Delegate again
				Sessions->ClearOnJoinSessionCompleteDelegate_Handle(onJoinSessionCompleteDelegateHandle);

				// Get the first local PlayerController, so we can call "ClientTravel" to get to the Server Map
				// This is something the Blueprint Node "Join Session" does automatically!
				APlayerController * const PlayerController = GetFirstLocalPlayerController();

				// We need a FString to use ClientTravel and we can let the SessionInterface contruct such a
				// String for us by giving him the SessionName and an empty String. We want to do this, because
				// Every OnlineSubsystem uses different TravelURLs
				FString TravelURL;

				if (PlayerController && Sessions->GetResolvedConnectString(SessionName, TravelURL))
				{
					MY_LOG_NETWORK(TEXT("UGameInfoInstance onJoinSessionComplete TravelURL %s"), *TravelURL);
					// Finally call the ClienTravel. If you want, you could print the TravelURL to see
					// how it really looks like
					PlayerController->ClientTravel(TravelURL, ETravelType::TRAVEL_Absolute);
				}
			}
		}
	}
}
void UGameInfoInstance::onDestroySessionComplete(FName SessionName, bool bWasSuccessful) {
	// Get the OnlineSubsystem we want to work with
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		// Get the SessionInterface from the OnlineSubsystem
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

		if (Sessions.IsValid())
		{
			// Clear the Delegate
			Sessions->ClearOnDestroySessionCompleteDelegate_Handle(onDestroySessionCompleteDelegateHandle);

			// If it was successful, we just load another level (could be a MainMenu!)
			if (bWasSuccessful)
			{
				GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, TEXT("Session Destroyed !"));
				mainMenu();
			}
		}
	}
}
