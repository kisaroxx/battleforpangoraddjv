// Fill out your copyright notice in the Description page of Project Settings.

#include "GameplayGM.h"
#include "Network/GameInfoInstance.h"
#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"
#include "SquadDescriptor.h"
#include "SquadSbireController.h"
#include "SquadLieutenantController.h"

#include "battle_for_pandora/BFP_PlayerController.h"
#include "battle_for_pandora/BFP_PlayerState.h"
#include "battle_for_pandora/BFP_GameState.h"
#include "battle_for_pandora/BFPHUD.h"

#include "GameFramework/PlayerStart.h"
#include "Util.h"
#include "MedKit.h"

#include "Logger.h"

AGameplayGM::AGameplayGM(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer)
{
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/BluePrints/Characters/0_Base"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	PlayerControllerClass = ABFP_PlayerController::StaticClass();
	PlayerStateClass = ABFP_PlayerState::StaticClass();
	GameStateClass = ABFP_GameState::StaticClass();

	static ConstructorHelpers::FClassFinder<AHUD> HUDBP(TEXT("/Game/UI/BFPHUDBP"));
	if (HUDBP.Class != NULL)
	{
		HUDClass = HUDBP.Class;
	}

	if (GetWorld()) {
		gameInstanceRef = GetGameInstance<UGameInfoInstance>();
	}

	nbSpawnInitializedCount = 0;
}

void AGameplayGM::PostLogin(APlayerController * NewPlayer) {
	Super::PostLogin(NewPlayer);
}

void AGameplayGM::Logout(AController* Exiting) {
	Super::Logout(Exiting);
	TWeakObjectPtr<ABFP_PlayerController> playerController = Cast<ABFP_PlayerController>(Exiting);
	ABFP_GameState* gameState = GetGameState<ABFP_GameState>();
	logoutAllClients();
}

void AGameplayGM::PostInitializeComponents() {
	Super::PostInitializeComponents();
    UGameInfoInstance* gameInstance = GetGameInstance<UGameInfoInstance>();
    if (IsValid(gameInstance)) {
        ABFP_GameState* gameState = GetGameState<ABFP_GameState>();
        if (IsValid(gameState)) {
            gameState->nbJoueurs = gameInstance->nbPlayer_;
            AddSpawnCharacterCount(GetGameState<ABFP_GameState>()->nbJoueurs);
        }
    }

	//on recupere tous les points de controle
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerStart::StaticClass(), PlayerStarts);
	MY_LOG_NETWORK(TEXT("AGameplayGM PostInitializeComponents NbPlayerStart%d"), PlayerStarts.Num());
}

void AGameplayGM::BeginPlay() {
	Super::BeginPlay();
	MY_LOG_NETWORK(TEXT("AGameplayGM BeginPlay %d"), nbSpawnInitializedCount);
}

void AGameplayGM::EndPlay(const EEndPlayReason::Type EndPlayReason) {
	Super::EndPlay(EndPlayReason);
	if (!EndPlayReason == EEndPlayReason::LevelTransition) {
		logoutAllClients();
	}
}

void AGameplayGM::RespawnPlayer(APlayerController* playerController, TSubclassOf<ACharacter> MyPlayerCharacter) {
   MY_LOG_NETWORK(TEXT("RespawnPlayer MyPlayerCharacter %u"), IsValid(MyPlayerCharacter));
   MY_LOG_NETWORK(TEXT("RespawnPlayer MyPlayerCharacter %s"), *MyPlayerCharacter->GetName());
   TWeakObjectPtr<APawn> pawn = playerController->GetPawn();
   FTransform position{};
   if (pawn.IsValid()) {
      position = pawn->GetActorTransform();
      pawn->Destroy();
   }

   //on vide si tous les numeros ont deja ete tire
   if (playerStartAlreadyUsed.Num() == PlayerStarts.Num()) {
	   playerStartAlreadyUsed.Empty();
   }
   //on cherche un playerStart libre
   uint8 indexPlayerStart = Util::Random(0, PlayerStarts.Num() - 1, playerStartAlreadyUsed);
   playerStartAlreadyUsed.Add(indexPlayerStart);

	position = PlayerStarts[indexPlayerStart]->GetActorTransform();
	ABFPCharacterPlayable* joueur = SpawnPawn<ABFPCharacterPlayable>(position, MyPlayerCharacter, playerController, true, true);
	SetNamePlayer(joueur, playerController);
	nbPlayerSpawned++;
}



void AGameplayGM::SetNamePlayer(ABFPCharacterPlayable* joueur, APlayerController* playerController)
{
   if (IsValid(joueur)) {
      ABFP_PlayerController* MyController = Cast<ABFP_PlayerController>(playerController);
      ABFP_PlayerState* MyPlayerState = Cast<ABFP_PlayerState>(MyController->PlayerState);
      auto MyPlayerName = MyPlayerState->S_PlayerInfo.MyPlayerName;
      MY_LOG_NETWORK(TEXT("ABFPCharacterPlayable::BeginPlay Name character %s"), *MyPlayerState->S_PlayerInfo.MyPlayerCharacter->GetName());
      auto chaine = MyPlayerName.ToString();
      if (chaine.Equals("Kiara")) {
         joueur->Name = ECharacterName::KIARA;
         MY_LOG_NETWORK(TEXT("Name : KIARA"));
      }
      else if (chaine.Equals("Aeres")) {
         joueur->Name = ECharacterName::AERES;
         MY_LOG_NETWORK(TEXT("Name : AERES"));
      }
      else if (chaine.Equals("Neelf")) {
         joueur->Name = ECharacterName::NEELF;
         MY_LOG_NETWORK(TEXT("Name : NEELF"));
      }
      else if (chaine.Equals("Shanyl")) {
         joueur->Name = ECharacterName::SHANYL;
         MY_LOG_NETWORK(TEXT("Name : SHANYL"));
      }
      else {
         joueur->Name = ECharacterName::DAVROS;
      }
   }
}

void AGameplayGM::logoutAllClients()
{
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("logoutClients_Implementation")));
	ABFP_GameState* gameState = GetGameState<ABFP_GameState>();
	for (int i = 0; i < gameState->AllPlayerControllers.Num(); ++i) {
		MY_LOG_NETWORK(TEXT("AGameplayGM  logoutAllClients DestroySession PlayerController n %u"), i);
		ABFP_PlayerController* playerController = gameState->AllPlayerControllers[i];
		if (IsValid(playerController)) playerController->DestroySession();
	}
	ABFP_PlayerController* hostPC = GetWorld()->GetFirstPlayerController<ABFP_PlayerController>();
	if (IsValid(hostPC)) {
		MY_LOG_NETWORK(TEXT("AGameplayGM  logoutAllClients DestroySession host PC"));
		hostPC->DestroySession();
	}
}

void AGameplayGM::HandleStartingNewPlayer_Implementation(APlayerController * NewPlayer) {
	Super::HandleStartingNewPlayer_Implementation(NewPlayer);
	TWeakObjectPtr<ABFP_PlayerController> playerController = Cast<ABFP_PlayerController>(NewPlayer);
	if (playerController.IsValid()) {
		MY_LOG_IA(TEXT("AGameplayGM HandleStartingNewPlayer_Implementation Ajout PC"));
		ABFP_GameState* gameState = GetGameState<ABFP_GameState>();
		gameState->AllPlayerControllers.Add(playerController.Get());
		MY_LOG_IA(TEXT("AGameplayGM HandleStartingNewPlayer_Implementation character ? %s"), *playerController->GetPlayerState<ABFP_PlayerState>()->S_PlayerInfo.MyPlayerCharacter->GetName());
		playerController->InitPawn();
	}
}

void AGameplayGM::AddSpawnCharacterCount(const uint8& nbCharacterToSpawn) {
	MY_LOG_NETWORK(TEXT("AGameplayGM AddSpawnCharacterCount %u"), nbCharacterToSpawn);
	nbSpawnInitializedCount += nbCharacterToSpawn;
}

void AGameplayGM::SpawnMedicalKit(FTransform const& position, float valueOfHealing) {
	FActorSpawnParameters spawnParams{};
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	TWeakObjectPtr<AMedKit> medicalKit = SpawnActor<AMedKit>(position, AMedKit::StaticClass(), spawnParams);
	if (medicalKit.IsValid()) {
		medicalKit->SetPoucentageSoin((FMath::IsNearlyEqual(valueOfHealing, 0.f)) ? Util::Random(5.f, 25.f) : valueOfHealing);
	}
}