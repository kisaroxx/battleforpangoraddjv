// Fill out your copyright notice in the Description page of Project Settings.

#include "GameplayGMTest.h"
#include "Network/GameInfoInstance.h"
#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"
#include "SquadDescriptor.h"
#include "SquadSbireController.h"
#include "SquadLieutenantController.h"

#include "battle_for_pandora/BFP_PlayerController.h"
#include "battle_for_pandora/BFP_PlayerState.h"
#include "battle_for_pandora/BFP_GameState.h"
#include "battle_for_pandora/BFPHUD.h"

#include "Logger.h"

AGameplayGMTest::AGameplayGMTest(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer)
{
	ConstructorHelpers::FClassFinder<ACharacter> Aeres_Class(TEXT("/Game/Characters/Aeres/AeresPlayerCharacter"));
	if (!ensure(IsValid(Aeres_Class.Class))) return;
	ConstructorHelpers::FClassFinder<ACharacter> Davros_Class(TEXT("/Game/Characters/Davros/DavrosPlayerCharacter"));
	if (!ensure(IsValid(Davros_Class.Class))) return;
	ConstructorHelpers::FClassFinder<ACharacter> Kiara_Class(TEXT("/Game/Characters/Kiara/KiaraPlayerCharacter"));
	if (!ensure(IsValid(Kiara_Class.Class))) return;
	ConstructorHelpers::FClassFinder<ACharacter> Neelf_Class(TEXT("/Game/Characters/Neelf/NeelfPlayerCharacter"));
	if (!ensure(IsValid(Neelf_Class.Class))) return;
	ConstructorHelpers::FClassFinder<ACharacter> Shanyl_Class(TEXT("/Game/Characters/Shanyl/ShanylPlayerCharacter"));
	if (!ensure(IsValid(Shanyl_Class.Class))) return;

    if (!ensure(Characters.Num() == 5 || Characters.Num() == 0))
        return;
    if (Characters.Num() == 0) {
        Characters.Reserve(5);
        Characters.Add(MoveTemp(Neelf_Class.Class));
        Characters.Add(MoveTemp(Davros_Class.Class));
        Characters.Add(MoveTemp(Kiara_Class.Class));
        Characters.Add(MoveTemp(Shanyl_Class.Class));
        Characters.Add(MoveTemp(Aeres_Class.Class));
    }

	indexPlayerToSpawn = 0;

	nbJoueur = 2;
}

void AGameplayGMTest::Logout(AController* Exiting) {
}

void AGameplayGMTest::BeginPlay() {
   GetGameState<ABFP_GameState>()->nbJoueurs = nbJoueur;
   nbSpawnInitializedCount = 0;
   AddSpawnCharacterCount(GetGameState<ABFP_GameState>()->nbJoueurs);
}

void AGameplayGMTest::EndPlay(const EEndPlayReason::Type EndPlayReason) {
}

void AGameplayGMTest::RespawnPlayer(APlayerController* playerController, TSubclassOf<ACharacter> MyPlayerCharacter) {
	MY_LOG_NETWORK(TEXT("RespawnPlayer MyPlayerCharacter %u"), IsValid(MyPlayerCharacter))
	MyPlayerCharacter = Characters[indexPlayerToSpawn++];
	MY_LOG_NETWORK(TEXT("RespawnPlayer MyPlayerCharacter %s"), *MyPlayerCharacter->GetName())
	TWeakObjectPtr<APawn> pawn = playerController->GetPawn();
	FTransform position{};
	if (pawn.IsValid()) {
	//	position = pawn->GetActorTransform();
		pawn->Destroy();
	}
	position = PlayerStarts[nbPlayerSpawned%PlayerStarts.Num()]->GetActorTransform();
   SpawnPawn<ABFPCharacterPlayable>(position, MyPlayerCharacter, playerController, true, true);
   nbPlayerSpawned++;
}

void AGameplayGMTest::HandleStartingNewPlayer_Implementation(APlayerController * NewPlayer) {
	TWeakObjectPtr<ABFP_PlayerController> playerController = Cast<ABFP_PlayerController>(NewPlayer);
	if (playerController.IsValid()) {
		MY_LOG_IA(TEXT("AGameplayGM HandleStartingNewPlayer_Implementation Ajout PC"));
		ABFP_GameState* gameState = GetGameState<ABFP_GameState>();
		gameState->AllPlayerControllers.Add(playerController.Get());
		RespawnPlayer(playerController.Get(), nullptr);
	}
}