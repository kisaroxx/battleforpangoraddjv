// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayGM.h"
#include "Engine/World.h"
#include "SquadDescriptor.h"
#include "GameplayGMTest.generated.h"

/**
 * 
 */
UCLASS(MinimalAPI)
class AGameplayGMTest : public AGameplayGM
{
	GENERATED_BODY()
	
   uint8 indexPlayerToSpawn;

   UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameParams", meta = (AllowPrivateAccess = "true"))
   TArray<TSubclassOf<ACharacter>> Characters;
   
   UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameParams", meta = (AllowPrivateAccess = "true"))
	uint8 nbJoueur;

   //UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameParams", meta = (AllowPrivateAccess = "true"))
   //TArray<TSubclassOf<ACharacter>> charactersOrder;

public:
	AGameplayGMTest(const FObjectInitializer & ObjectInitializer);
	
   virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void Logout(AController* Exiting) override;

	void HandleStartingNewPlayer_Implementation(APlayerController * NewPlayer) override;

	void RespawnPlayer(APlayerController* playerController, TSubclassOf<ACharacter> MyPlayerCharacter) override;
};