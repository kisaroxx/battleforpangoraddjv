// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "Engine/World.h"
#include "SquadDescriptor.h"
#include "GameplayGM.generated.h"

/**
 * 
 */
UCLASS(MinimalAPI)
class AGameplayGM : public AGameMode
{
	GENERATED_BODY()

	TWeakObjectPtr<class UGameInfoInstance> gameInstanceRef;

protected:
	uint8 nbSpawnInitializedCount;	
	TArray<AActor*> PlayerStarts;
	uint8 nbPlayerSpawned = 0;
	TArray<uint8> playerStartAlreadyUsed;

public:

	AGameplayGM(const FObjectInitializer & ObjectInitializer);
	
	virtual void PostInitializeComponents() override;
	virtual void PostLogin(APlayerController * NewPlayer) override;
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void Logout(AController* Exiting) override;

	virtual void HandleStartingNewPlayer_Implementation(APlayerController * NewPlayer) override;

	

	void logoutAllClients();

	virtual void RespawnPlayer(APlayerController* playerController, TSubclassOf<ACharacter> MyPlayerCharacter);

	void SpawnMedicalKit(FTransform const& position, float valueOfHealing = 0.f);

	void SetNamePlayer(ABFPCharacterPlayable * joueur, APlayerController* playerController);
   void AddSpawnCharacterCount(const uint8& nbCharacterToSpawn);

public :
   template<class T>
   T* SpawnPawn(FTransform const& position, UClass* Class = nullptr, AController* controller = nullptr, const bool registerInGameState = true, const bool decrementeCharacterCount = false, const FActorSpawnParameters& SpawnParameters = FActorSpawnParameters())
   {
      if (!Class) Class = T::StaticClass();
      MY_LOG_NETWORK(TEXT("AGameplayGM SpawnPawn %s"), *Class->GetName());
      T* pawn = SpawnActor<T>(position, Class, SpawnParameters);
      if (IsValid(controller)) {
         controller->Possess(pawn);
      }
      return pawn;
   }

   template< class T >
   T* SpawnActor(FTransform const& Transform, UClass* Class = nullptr, const FActorSpawnParameters& SpawnParameters = FActorSpawnParameters())
   {
      MY_LOG_NETWORK(TEXT("AGameplayGM SpawnActor"));
      if (!Class) Class = T::StaticClass();
      return GetWorld()->SpawnActor<T>(Class, Transform, SpawnParameters);
   }

};