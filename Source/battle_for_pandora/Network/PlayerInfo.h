// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/UserDefinedStruct.h"
#include "Engine/Texture2D.h"
#include "GameFramework/Character.h"
#include "PlayerInfo.generated.h"

/**
 *
 */
USTRUCT(Blueprintable)
struct BATTLE_FOR_PANDORA_API FPlayerInfo
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, SaveGame)
	int CharacterID;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, SaveGame)
	FText MyPlayerName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, SaveGame)
	TWeakObjectPtr<UTexture2D> MyPlayerImage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, SaveGame)
	TSubclassOf<ACharacter> MyPlayerCharacter;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, SaveGame)
	TWeakObjectPtr<UTexture2D> MyPlayerCharacterImage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, SaveGame)
		TWeakObjectPtr<UTexture2D> IconInGameImage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, SaveGame)
		TWeakObjectPtr<UTexture2D> IconOnMapImage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, SaveGame)
	bool MyPlayerStatus;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, SaveGame)
	bool IsHost;

};
