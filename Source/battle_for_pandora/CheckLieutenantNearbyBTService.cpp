// Fill out your copyright notice in the Description page of Project Settings.

#include "CheckLieutenantNearbyBTService.h"
#include "SquadSbireController.h"

UCheckLieutenantNearbyBTService::UCheckLieutenantNearbyBTService() {
   // Nom du noeud dans le BT
   NodeName = "CheckLieutenantNearby";
   // Intervalle de mise � jour
   Interval = 0.5f;
   // Valeur al�atoire de d�viation de l'intervalle de mise � jour
   RandomDeviation = 0.1f;
}

/** Sera appel�e � chaque �update� du service */
void UCheckLieutenantNearbyBTService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float
   DeltaSeconds)
{
   // Appeler la fonction de la classe de base
   Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

   ASquadSbireController* sbireController = Cast<ASquadSbireController>(OwnerComp.GetOwner());
   sbireController->CheckLieutenantNearby();
}