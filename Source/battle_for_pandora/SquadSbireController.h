// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SquadMemberController.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BFPCharacterLieutenant.h"
#include "SquadSbireController.generated.h"

/**
 * 
 */
UCLASS()
class BATTLE_FOR_PANDORA_API ASquadSbireController : public ASquadMemberController
{
   GENERATED_BODY()

   UPROPERTY(EditAnywhere, Category = "BrainSbire")
   float visionRange = 1500.f;

   UPROPERTY(EditAnywhere, Category = "BrainSbire")
   float fleeDistance = 1700.f;

   

   TSubclassOf<UBehaviorTree> sbireBTClass;

   UPROPERTY(Replicated, VisibleAnywhere, Category = "BT")
	UBehaviorTree*  sbireBT;

public:
   UPROPERTY(EditAnywhere, Category = "BrainSbire")
   FVector wanderGoal;

   UPROPERTY(EditAnywhere, Category = "BrainSbire")
   FVector runAwayGoal;

   ASquadSbireController(const FObjectInitializer & ObjectInitializer);

   UFUNCTION()
   bool CheckTargetActorNearby();

   UFUNCTION()
   float CanAttack();

   UFUNCTION()
   EPathFollowingRequestResult::Type AttackEnemy();

   UFUNCTION()
   EPathFollowingRequestResult::Type RunAway();

   UFUNCTION()
   void CheckLieutenantNearby();

   UFUNCTION()
   void JoinSquad();

   UFUNCTION()
   EPathFollowingRequestResult::Type Wander();

   //DECLARE_DYNAMIC_MULTICAST_DELEGATE(FRunBtDelegate);

   //UPROPERTY(BlueprintAssignable)
   //FRunBtDelegate runBtFlag;

   void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

   virtual void PostInitializeComponents() override;
   virtual void BeginPlay() override;

	//UFUNCTION(BlueprintImplementableEvent, Reliable, Server, WithValidation, Category = "BaseCharacter")
	//UFUNCTION(BlueprintImplementableEvent, Category = "BaseCharacter")
   //UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "BaseCharacter")
   void RunBT() override;
};
