// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "battle_for_pandoraGameMode.h"
#include "BFPHUD.h"
#include "BFPCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Blueprint/UserWidget.h"
#include "UObject/ConstructorHelpers.h"

Abattle_for_pandoraGameMode::Abattle_for_pandoraGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		//DefaultPawnClass = PlayerPawnBPClass.Class;
        DefaultPawnClass = ABFPCharacter::StaticClass();
	}

	//// use our custom HUD class
   /*static ConstructorHelpers::FClassFinder<AHUD> HudClass(TEXT("/Game/UI/BFPHUDBP"));
   if (HudClass.Class != NULL) {
      HUDClass = HudClass.Class;
   }*/
   


}
