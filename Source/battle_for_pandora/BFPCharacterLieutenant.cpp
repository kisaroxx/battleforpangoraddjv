// Fill out your copyright notice in the Description page of Project Settings.

#include "BFPCharacterLieutenant.h"
#include "BFPHUD.h"
#include "Engine.h"
#include "BFP_GameState.h"
#include "Message.h"
#include "SquadDescriptor.h"
#include "SquadManager.h"
#include "SquadLieutenantController.h"
#include "SquadMemberController.h"
#include "Network/Gameplay/GameplayGM.h"


ABFPCharacterLieutenant::ABFPCharacterLieutenant(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
   bAlwaysRelevant = true;
   bReplicates = true;

   NetCullDistanceSquared *= 40;
}

// Called when the game starts
void ABFPCharacterLieutenant::BeginPlay()
{
   Super::BeginPlay();


	//ABFPHUD* hud = Cast<ABFPHUD>(GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld())->GetHUD());
	//hud->addLieutenant(this);
   if (GetWorld()->IsServer()) {
      ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
      if (IsValid(gameState)) {
         gameState->Register(this);
      }
	  else {
		  MY_LOG_NETWORK(TEXT("ABFPCharacterLieutenant BeginPlay game state non valide"));
	  }
   }
}

void ABFPCharacterLieutenant::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(ABFPCharacterLieutenant, checkpointAssociated);
}


void ABFPCharacterLieutenant::UnRegisterToGameManager() {
	GetWorld()->GetGameState<ABFP_GameState>()->UnRegister(this);
}

void ABFPCharacterLieutenant::RegisterToSquad() {
   squad->GetLieutenants().Add(Cast<ASquadLieutenantController>(this->GetController()));
}

void ABFPCharacterLieutenant::UnRegisterToSquad() {
   if (HasSquad()) {
      UMessage* message = NewObject<UMessage>();
      message->SetParams(MessageType::DIED, GetController<ASquadMemberController>());
      GetSquad()->GetSquadManager()->ReceiveMessage(message);
      GetSquad()->UnlinkFromCheckpoint();
      GetSquad()->RemoveLieutenant(Cast<ASquadLieutenantController>(this->GetController()));
   }
}

void ABFPCharacterLieutenant::Die() {
	DiedFlag.Broadcast(this->CharacterAffiliation);

	combatState = CombatState::DEAD;
	poussees.Empty();

	UnRegisterToSquad();

	UnRegisterToGameManager();

	// D�sactiver les collisions !
	SetActorEnableCollision(false);

	//on spawn un medicalKit
	GetWorld()->GetAuthGameMode<AGameplayGM>()->SpawnMedicalKit(GetActorTransform());

	// On declanche l'attaque apres un certain delais !
	FTimerHandle fuzeTimerHandle;
	FTimerDelegate timerDelegate;
	timerDelegate.BindUFunction(this, FName("DestroyCharacter"));
	GetWorld()->GetTimerManager().SetTimer(
		fuzeTimerHandle,
		timerDelegate,
		dureeDeath,
		false);
}
