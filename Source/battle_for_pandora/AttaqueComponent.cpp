// Fill out your copyright notice in the Description page of Project Settings.

#include "AttaqueComponent.h"
#include "Engine/World.h"
#include "BFPCharacter.h"
#include "Components/SkeletalMeshComponent.h"
#include "Attaque.h"
#include "Logger.h"
#include <algorithm>

using namespace std;
using std::vector;


// Sets default values for this component's properties
UAttaqueComponent::UAttaqueComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}

void UAttaqueComponent::ActivateAttack(ABFPCharacter* owner, UWorld* world) {
    FVector position{};
    FRotator rotation = FRotator::ZeroRotator;
    FActorSpawnParameters spawnParams;
    spawnParams.Owner = owner;
    spawnParams.Instigator = owner->Instigator;
    spawnParams.bNoFail = true;
    AAttaque* const attaque = world->SpawnActor<AAttaque>(attaqueClass, position, rotation, spawnParams);
    if (IsValid(attaque)) {
        int index = owner->GetMesh()->GetBoneIndex(attachedBone);
        attaque->InitPointAttache(owner->GetMesh(), index);
        attaque->SetOwner(owner, this);
        float dammages = baseDamages * owner->GetDegatMultiplicateur();
        float mult = max(owner->GetDegatMultiplicateur() * 0.65f, 1.0f);
        float puissance = puissancePoussee * min(mult, 3.0f);
        attaque->SetParams(dammages, weaponOffset, weaponOffsetRotation, dureeStun, dureeActivation, { puissance, dureePoussee });
        // On enregistre l'attaque pour les garder en m�moire
        attaquesEnCours.Push(attaque);
    } else {
		DEBUG_SCREEN(-1, 5.f, FColor::Green, TEXT("L'attaque n'a pas ete spawne !!!"));
    }
}

void UAttaqueComponent::CancelAll() {
    for (int i = 0; i < attaquesEnCours.Num(); i++) {
        attaquesEnCours[i]->DestroyAttaque();
        i--;
    }
}

void UAttaqueComponent::UntrackAttack(AAttaque * attaque) {
    attaquesEnCours.Remove(attaque);
}


// Called when the game starts
void UAttaqueComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UAttaqueComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}


float UAttaqueComponent::GetDureeArmement() {
   return dureeArmement;
}
float UAttaqueComponent::GetDureeActivation() {
   return dureeActivation;
}
float UAttaqueComponent::GetDureeDesarmement() {
   return dureeDesarmement;
}
float UAttaqueComponent::GetDureeArmementAnim() {
    return dureeArmementAnim;
}
float UAttaqueComponent::GetDureeActivationAnim() {
    return dureeActivationAnim;
}
float UAttaqueComponent::GetDureeDesarmementAnim() {
    return dureeDesarmementAnim;
}
