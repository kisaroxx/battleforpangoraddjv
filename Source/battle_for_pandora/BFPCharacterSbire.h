// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BFPCharacterNonPlayable.h"
#include "BFPCharacterSbire.generated.h"

/**
 * 
 */
UCLASS()
class BATTLE_FOR_PANDORA_API ABFPCharacterSbire : public ABFPCharacterNonPlayable
{
	GENERATED_BODY()

public :
   ABFPCharacterSbire(const FObjectInitializer & ObjectInitializer);

   // Called when the game starts
   virtual void BeginPlay() override;

public:

   virtual void RegisterToSquad() override;

   virtual void UnRegisterToSquad() override;

	virtual void RegisterToGameManager() override;

	virtual void UnRegisterToGameManager() override;

};
