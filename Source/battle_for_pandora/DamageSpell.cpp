﻿//// Fill out your copyright notice in the Description page of Project Settings.
//
//#include "DamageSpell.h"
//#include "BFPCharacter.h"
//#include "BFPCharacterPlayable.h"
//#include "BFPCharacterNonPlayable.h"
//#include "Util.h"
//#include "Message.h"
//#include "SquadMemberController.h"
//#include "SquadDescriptor.h"
//#include "SquadManager.h"
//
//ADamageSpell::ADamageSpell()
//{
//   bAlwaysRelevant = true;
//   bReplicates = true;
//   SetReplicates(true);
//}
//
//void ADamageSpell::BeginPlay()
//{
//   Super::BeginPlay();
//}
//
//void ADamageSpell::Tick(float DeltaTime)
//{
//   Super::Tick(DeltaTime);
//}
//
//void ADamageSpell::OnActorOverlap(AActor * OverlappedActor, AActor * OtherActor)
//{
//   if (Cast<ABFPCharacter>(owner)->getCharacterAffiliation() == Affiliation::NEUTRAL) {
//      return;
//   }
//   if (OtherActor != owner) {
//      ABFPCharacter* otherChar = Cast<ABFPCharacter>(OtherActor);
//      if (otherChar) {
//         ABFPCharacter* ownerCharacter = Cast<ABFPCharacter>(owner);
//         // Si on est de la même faction, on ne se touche pas
//         if (ownerCharacter && otherChar->getCharacterAffiliation() != ownerCharacter->getCharacterAffiliation()) {
//            if (IsValid(Cast<ABFPCharacterPlayable>(OtherActor)) && !Cast<ABFPCharacterPlayable>(OtherActor)->bGosted) {
//               otherChar->TakeDamageFromSpell(damage);
//               // Si la cible est morte et qu'elle a été tué par un squadmember, alors on envoie un message
//               if (otherChar->IsDead() && Cast<ABFPCharacterNonPlayable>(ownerCharacter) != nullptr) {
//                  UMessage* message = NewObject<UMessage>();
//                  message->SetParams(MessageType::KILLED, ownerCharacter->GetController<ASquadMemberController>());
//                  ASquadDescriptor* squad = ownerCharacter->GetController<ASquadMemberController>()->GetSquad();
//                  if (squad != nullptr) {
//                     squad->GetSquadManager()->ReceiveMessage(message);
//                  }
//               }
//            }
//            else if (!IsValid(Cast<ABFPCharacterPlayable>(OtherActor))) {
//               otherChar->TakeDamageFromSpell(damage);
//            }
//            else
//            {
//               DEBUG_SCREEN(-1, 5.f, FColor::Yellow, TEXT("AAttaque OnActorOverlap can NOT be damaged"));
//            }
//         }
//      }
//      else {
//         GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Black, TEXT("Deja touche !"));
//      }
//   }
//}