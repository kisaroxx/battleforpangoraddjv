// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BFPCharacterSbire.h"
#include "BFPCharacterLieutenant.h"
#include "Command.h"
#include "SquadMemberController.h"
#include "Logger.h"
#include "Formation.h"
#include "ManeuverAgressive.h"
#include "SquadLieutenantController.h"
#include "SquadSbireController.h"

#include <vector>

#include "SquadDescriptor.generated.h"

UCLASS()
class BATTLE_FOR_PANDORA_API ASquadDescriptor : public AActor
{
	GENERATED_BODY()

		/* La classe du lieutenant a instancier. */
		UPROPERTY(EditAnywhere, Category = "Deploiement")
		TSubclassOf<ABFPCharacterLieutenant> lieutenantPawnClass;

	/* La classe des sbires a instancier. */
	UPROPERTY(EditAnywhere, Category = "Deploiement")
		TSubclassOf<ABFPCharacterSbire> sbirePawnClass;

   UPROPERTY(EditAnywhere, Category = "Deploiement")
      TSubclassOf<ASquadLieutenantController> lieutenantControllerClass;
   UPROPERTY(EditAnywhere, Category = "Deploiement")
      TSubclassOf<ASquadSbireController> sbireControllerClass;

    /* Le nombre de lieutenants. */
    UPROPERTY(EditAnywhere, Category = "Deploiement")
    int nbLieutenants = 1;

    /* Le nombre de sbires. */
    UPROPERTY(EditAnywhere, Category = "Deploiement")
    int nbSbires = 29; // 30 - 1 = 29

		/* Les lieutenants actuellement dans la Squad. */
	UPROPERTY(Replicated, VisibleAnywhere, Category = "Deploiement")
		TArray<class ASquadLieutenantController*> lieutenants;

	/* Les sbires actuellement dans la Squad. */
	UPROPERTY(Replicated, VisibleAnywhere, Category = "Deploiement")
		TArray<class ASquadSbireController*> sbires;

	/* La hauteur de spawning par rapport au terrain. */
	UPROPERTY(EditDefaultsOnly, Category = "Deploiement")
		float spawningOffsetZ = 300.0f;

    /* Les differentes manoeuvres de la squad !!! <3 */
    UPROPERTY(EditAnywhere, Category = "Deploiement")
    TSubclassOf<AManeuver> maneuverAgressive = AManeuverAgressive::StaticClass();
    UPROPERTY(EditAnywhere, Category = "Deploiement")
    TSubclassOf<AManeuver> maneuverDefensive = AManeuverAgressive::StaticClass();
    UPROPERTY(EditAnywhere, Category = "Deploiement")
    TSubclassOf<AManeuver> maneuverSurvie = AManeuverAgressive::StaticClass();
    UPROPERTY(EditAnywhere, Category = "Deploiement")
    TSubclassOf<AManeuver> maneuverAssault = AManeuverAgressive::StaticClass();
    UPROPERTY(EditAnywhere, Category = "Deploiement")
    TSubclassOf<AManeuver> maneuverRecall = AManeuverAgressive::StaticClass();

	/* La manoeuvre a adopter de la squad. */
	UPROPERTY(ReplicatedUsing = OnRep_Man, VisibleAnywhere, Category = "Manoeuvre")
    class AManeuver* maneuver;
    /* Le type de manoeuvre courant de la squad. */
    UPROPERTY(VisibleAnywhere, Category = "Manoeuvre")
    ManeuverType maneuverType = ManeuverType::AGRESSIVE;


   UFUNCTION()
      void OnRep_Man() {
      MY_LOG_NETWORK(TEXT("ASquadDescriptor OnRep_Man : replication de man"));
      if (IsValid(maneuver)) {
         MY_LOG_NETWORK(TEXT("ASquadDescriptor OnRep_Man : replication de man, taille %u"), maneuver->GetTargetPoints().Num());
      }
   }

    /* Le cerveau de la Squad. */
    UPROPERTY(Replicated, VisibleAnywhere, Category = "Manoeuvre")
    class ASquadManager* squadManager;

    /* Le Checkpoint auquel la Squad est attache. */
    UPROPERTY(Replicated, VisibleAnywhere, Category = "Checkpoint")
    class ACheckpointTriggerSphere* checkpointAssociated = nullptr;

public:	
	// Sets default values for this actor's properties
	ASquadDescriptor(const FObjectInitializer & ObjectInitializer);

   void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

   TArray<class ASquadLieutenantController*> GetLieutenants();
   class ASquadLieutenantController* GetFirstLieutenant();
   TArray<class ASquadSbireController*> GetSbires();
   ASquadManager* GetSquadManager();

	int GetNbLieutenants();
	int GetNbSbires();
	int GetNbMembers();

   void AddSbire(ASquadSbireController* sbireController);

   UFUNCTION()
   AManeuver* GetManeuver();
   TSubclassOf<AManeuver> GetManeuverClass(ManeuverType type) const;
   void SetManeuver(AManeuver* nouvelleManeuvre, ManeuverType type); // DON'T CALL THIS ! Call the one in SquadManager instead ! :)
   ManeuverType GetManeuverType() const;

   void RemoveLieutenant(ASquadLieutenantController* lieutenant);

   void RemoveSbire(ASquadSbireController* sbire);

   void InitPawn();
   void InitPawnFinished();

   bool bSquadSpawned = false;
   bool bBTStarted = false;

   void RegisterLieutenantController(ASquadLieutenantController* lieutenantController);
   void RegisterSbireController(ASquadSbireController* sbireController);

   void SetCheckpointAssociated(class ACheckpointTriggerSphere* checkpoint);
   ACheckpointTriggerSphere* GetCheckpointAssociated() const;

    void UnlinkFromCheckpoint();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
    // Called every frame
    virtual void Tick(float DeltaTime) override;

	virtual void PostInitializeComponents() override;

private:
	UFUNCTION()
	void SpawnAllMembers();

	void sendCommandToMember(ASquadMemberController* squadMemberController, UCommand* command);
};
