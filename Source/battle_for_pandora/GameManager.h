// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SquadDescriptor.h"
#include "GameManager.generated.h"

/**
*
*/
UCLASS()
class BATTLE_FOR_PANDORA_API AGameManager : public AActor
{
    GENERATED_BODY()
//    static AGameManager* instance;
//
//    /* Les sbires. */
//    UPROPERTY(VisibleAnywhere, Category = "Unites")
//    TArray<class ABFPCharacterSbire*> sbires;
//    /* Les lieutenants. */
//    UPROPERTY(VisibleAnywhere, Category = "Unites")
//    TArray<class ABFPCharacterLieutenant*> lieutenants;
//    /* Les joueurs allies. */
//    UPROPERTY(VisibleAnywhere, Category = "Unites")
//    TArray<class ABFPCharacterPlayable*> joueursAllies; // Attention, cela n'inclu PAS Davros !
//    /* Davros. */
//    UPROPERTY(VisibleAnywhere, Category = "Unites")
//    class ABFPCharacterPlayable* davros; // Il est l� :)
//
//public:
//    // Je ne d�truit pas le constructeur de copie et l'affectation car c'est d�j� fait ! #Unreal
//    // AGameManager(const AGameManager&) = delete;
//    // AGameManager& operator=(const AGameManager&) = delete;
//
//    AGameManager();
//    static AGameManager* Get(); // Ceci est un singleton !
//
//    // S'enregistrer quand on est cr�e et se d�senregistrer quand on meurt
//    void Register(ABFPCharacterSbire*);
//    void Register(ABFPCharacterLieutenant*);
//    void Register(ABFPCharacterPlayable*);
//    void UnRegister(class ABFPCharacter*); // On est pas suppose appeler cette methode !
//    void UnRegister(ABFPCharacterSbire*);
//    void UnRegister(ABFPCharacterLieutenant*);
//    void UnRegister(ABFPCharacterPlayable*);
//
//    // Get all
//    TArray<ABFPCharacterSbire*> GetAllSbires() const;
//    TArray<ABFPCharacterLieutenant*> GetAllLieutenants() const;
//    TArray<ABFPCharacterPlayable*> GetAllJoueursAllies() const;
//    ABFPCharacterPlayable* GetDavros() const;
//
//    // Get nb
//    int GetNbSbires() const;
//    int GetNbLieutenants() const;
//    int GetNbJoueursAllies() const;
//
//
//    // R�cup�re les nbVoulu characters les plus proches de la source.
//    template<class Char>
//    TArray<Char*> GetAllNearestFrom(TArray<Char*> characters, const FTransform & source, int nbVoulu) const {
//        int k = nbVoulu;
//        int n = characters.Num();
//        if (n <= k) return characters;
//
//        // Pr�dicat indiquant qu'il faut trier selon la distance au point !
//        struct FSortByDistance {
//            FSortByDistance(const FVector& InSourceLocation) : SourceLocation(InSourceLocation) {}
//
//            /* The Location to use in our Sort comparision. */
//            FVector SourceLocation;
//
//            bool operator()(const AActor& A, const AActor& B) const {
//                float DistanceA = FVector::DistSquared(SourceLocation, A.GetActorLocation()); // DistSquared plus efficace car on utilise pas la racine !
//                float DistanceB = FVector::DistSquared(SourceLocation, B.GetActorLocation()); // DistSquared plus efficace car on utilise pas la racine !
//                return DistanceA < DistanceB; // On veut les plus proches en premier !
//            }
//        };
//
//        // On trie
//        TArray<Char*> res = characters;
//        res.Sort(FSortByDistance(source.GetLocation()));
//
//        // On veut les k premiers !
//        for (int i = 0; i < (n - k); i++)
//            res.Pop();
//
//        return res;
//    }
//
//    // R�cup�re tous les characters � moins de distance de la source.
//    template<class Char>
//    TArray<Char*> GetAllInDistanceFrom(TArray<Char*> characters, const FTransform & source, float distance) const {
//        int n = characters.Num();
//
//        TArray<Char*> res;
//        for (int i = 0; i < n; i++) {
//            // On veut juste les plus proches !
//            float d = FVector::DistSquared(characters[i]->GetActorLocation(), source.GetLocation());
//            if (d <= distance * distance) {
//                res.Push(characters[i]);
//            }
//        }
//
//        return res;
//    }
//
//    ~AGameManager();
};
