// Fill out your copyright notice in the Description page of Project Settings.

#include "MoveToPDC_BTTaskNode.h"
#include "SquadSbireController.h"
#include "Logger.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Maneuver.h"
#include "ManeuverDefensive.h"
#include "BFPCharacterPlayable.h"
#include "SquadDescriptor.h"
#include "MoveCommand.h"

UMoveToPDC::UMoveToPDC(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    NodeName = "MoveToPDC";
    bShouldFailOnAddTargetPoint = true;
}

EBTNodeResult::Type UMoveToPDC::ExecuteTaskImpl(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, AManeuver* maneuvre) {
    maneuvre->nbMemberCompleted = 0;
    maneuvre->lieutenantCompleted = false;

    ACheckpointTriggerSphere* checkpoint = maneuvre->GetDescriptor()->GetCheckpointAssociated();
    if (IsValid(checkpoint)) {
        MY_LOG_IA(TEXT("%s MODE = MoveToPDC"), *maneuvre->GetDescriptor()->GetName());
        maneuvre->MoveTo(checkpoint->GetTransform(), false);
        return EBTNodeResult::InProgress;
    }
    else {
        return EBTNodeResult::Failed;
    }
}

FString UMoveToPDC::GetStaticDescription() const
{
    return TEXT("Permet d'aller au PDC associe à cette Squad.");
}

void UMoveToPDC::OnMessage(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, FName Message, int32 RequestID, bool bSuccess) {
    Super::OnMessage(OwnerComp, NodeMemory, Message, RequestID, bSuccess);

    // On distingue les messages
    AManeuver* maneuvre = Cast<AManeuver>(OwnerComp.GetOwner());
    AManeuverDefensive* maneuvreDefensive = Cast<AManeuverDefensive>(OwnerComp.GetOwner());
    ASquadMemberController* member = maneuvre->GetLastMessage()->GetSquadMemberController();
    switch (maneuvre->GetLastMessage()->GetType()) {
    case MessageType::COMPLETED: {
        if (IsValid(Cast<ASquadLieutenantController>(member))) {
            maneuvre->lieutenantCompleted = true;
            // Pour tous les sbires, on leur dit qu'ils ne sont pas arriv�s !
            for (ASquadSbireController* sbire : maneuvre->GetDescriptor()->GetSbires()) {
                UMoveCommand* moveCommand = Cast<UMoveCommand>(sbire->GetCommand());
                if(IsValid(moveCommand))
                    moveCommand->SetNotArrived();
            }
        }
        else { // Sbire
            if (maneuvre->lieutenantCompleted) {
                maneuvre->nbMemberCompleted++;
                // Si on a re�u assez de messages alors on quitte la tache ! :3
                if (maneuvre->nbMemberCompleted >= Cast<AManeuver>(OwnerComp.GetOwner())->GetDescriptor()->GetNbMembers() * 0.75f) {
                    // On supprime le premier target point car on vient d'y arriver !
                    maneuvre->RemoveFirstTargetPoint();
                    FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
                }
            }
        }
        break;
    }
    case MessageType::IN_RUSH_RANGE:
        FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
        break;
    default:
        break;
    }
}
