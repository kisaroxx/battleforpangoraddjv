// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "battle_for_pandora.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, battle_for_pandora, "battle_for_pandora" );
 
//Log general
DEFINE_LOG_CATEGORY(MyLog);

//Log IA
DEFINE_LOG_CATEGORY(MyLogIA);

//Log UI
DEFINE_LOG_CATEGORY(MyLogUI);

//Log Network
DEFINE_LOG_CATEGORY(MyLogNetwork);

//Log Render
DEFINE_LOG_CATEGORY(MyLogRender);

//Log Gameplay
DEFINE_LOG_CATEGORY(MyLogGame);

//Log Son
DEFINE_LOG_CATEGORY(MyLogSon);