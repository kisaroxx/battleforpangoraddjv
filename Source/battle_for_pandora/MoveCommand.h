// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Command.h"
#include "Math/Vector.h"
#include "MoveCommand.generated.h"

/**
*
*/
UCLASS()
class BATTLE_FOR_PANDORA_API UMoveCommand : public UCommand
{
    GENERATED_BODY()

    bool arrived = false;
    bool pathCalculated = false;

    bool bTryUseLieutenantNavMesh = true;

    UPROPERTY(EditAnywhere, Category = "Command")
    float refreshTime = 2.0f;
    float lastTimeRefresh;

public:
    UMoveCommand();

    UFUNCTION(BlueprintCallable, Category = "Command")
        void SetTargetPoint(FVector _targetPoint);

    UFUNCTION(BlueprintCallable, Category = "Command")
        FVector GetTargetPoint();

    void SetTryUseLieutenantNavMesh(bool use);
    bool GetTryUseLieutenantNavMesh() const;

    void SetNotArrived(); // A utiliser avec precaution !!!

    class UCommandReturn* execute() override;

private:
    UPROPERTY(EditAnywhere, Category = "Command")
        FVector targetPoint;

    void SetRefreshTime(float refreshTime_);
};
