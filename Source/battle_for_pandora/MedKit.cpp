// Fill out your copyright notice in the Description page of Project Settings.

#include "MedKit.h"
#include "HealthComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/StaticMesh.h"

// Sets default values
AMedKit::AMedKit(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer)
{
	OnActorBeginOverlap.AddDynamic(this, &AMedKit::OnOverlap);
	
	static ConstructorHelpers::FObjectFinder<UStaticMesh> HeartMesh(TEXT("/Game/heart/Heart"));
	if (!ensure(IsValid(HeartMesh.Object))) return;
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("My Super Mesh"));
	StaticMeshComponent->SetStaticMesh(HeartMesh.Object);
	StaticMeshComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	StaticMeshComponent->SetCanEverAffectNavigation(false);

	pourcentageSoinMin = 15.f;
	pourcentageSoinMax = 50.f;

	ratioMin = 1.f / 25.f;
	ratioMax = 1.f / 20.f;

	pourcentageSoinParDefaut = pourcentageSoinMax;
	ratioParDefaut = ratioMax;

	coeff = (ratioMax - ratioMin) / (pourcentageSoinMax - pourcentageSoinMin);
	valeurOrigine = ratioMin - coeff * pourcentageSoinMin;

	SetReplicates(true);
}

void AMedKit::PostInitializeComponents() {
	Super::PostInitializeComponents();
	SetPoucentageSoin(pourcentageSoinParDefaut);
	if (IsValid(RootComponent)) {
		RootComponent->SetIsReplicated(true);
	}
	else {
		MY_LOG_NETWORK(TEXT("AMedKit PostInitializeComponents RootComponent invalide"));
	}
}

void AMedKit::OnOverlap(AActor* MyOverlappedActor, AActor* OtherActor)
{
	if (GetWorld()->IsServer()) {
		if (IsValid(OtherActor) && OtherActor != this)
		{
			TWeakObjectPtr<ABFPCharacter> MyCharacter = Cast<ABFPCharacter>(OtherActor);

			if (MyCharacter.IsValid() && MyCharacter->GetHealthSystem()->GetHealth() < MyCharacter->GetHealthSystem()->GetFullHealth() && MyCharacter->getCharacterAffiliation() == Affiliation::GOOD)
			{
				FTimerHandle handle;
				MyCharacter->GetHealthSystem()->StartHealing(handle, pourcentageSoin, true, 0.1f, false);
				StaticMeshComponent->DestroyComponent();
				Destroy();
			}
		}
	}
}

void AMedKit::SetPoucentageSoin(float valeurSoin) {
	pourcentageSoin = valeurSoin;
	ratio = coeff * valeurSoin + valeurOrigine;
	RootComponent->SetRelativeScale3D({ ratio,ratio,ratio });
}
