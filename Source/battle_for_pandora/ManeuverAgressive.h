// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Maneuver.h"
#include "ManeuverAgressive.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class BATTLE_FOR_PANDORA_API AManeuverAgressive : public AManeuver
{
    GENERATED_BODY()

    bool shouldCircle = true;

    UPROPERTY(EditAnywhere, Category = "Tactique")
    float pourcentageEncerclementAvantAttaque = 0.5f;

    UPROPERTY(EditAnywhere, Category = "Tactique")
    float distanceToPosEncerclementBeforeAttaque = 125.0f;

    UPROPERTY(EditAnywhere, Category = "Tactique")
    float distanceMaxToAttack = 1000.f;

    UPROPERTY(EditAnywhere, Category = "Tactique")
    float distanceMaxToCircle = 2000.f;

    class ABFPCharacterPlayable* cibleLaPlusProche = nullptr;

public:
    virtual void Execute() override;
	
private:
    void EncerclerPuisAttaquer(class ABFPCharacterPlayable* joueur);
};
