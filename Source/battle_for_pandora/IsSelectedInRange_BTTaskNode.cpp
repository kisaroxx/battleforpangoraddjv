// Fill out your copyright notice in the Description page of Project Settings.

#include "IsSelectedInRange_BTTaskNode.h"
#include "SquadSbireController.h"
#include "Logger.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Maneuver.h"
#include "BFPCharacterPlayable.h"
#include "SquadDescriptor.h"

UIsSelectedInRange::UIsSelectedInRange(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    NodeName = "IsSelectedInRange";
    distance = 1000.0f;
}

EBTNodeResult::Type UIsSelectedInRange::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8*
    NodeMemory)
{
    Super::ExecuteTask(OwnerComp, NodeMemory);

    // On recupere la manoeuvre !
    AManeuver* maneuvre = Cast<AManeuver>(OwnerComp.GetOwner());
    FVector posSelected = maneuvre->GetJoueurSelected()->GetActorLocation();
    FVector posLieutenant = maneuvre->GetDescriptor()->GetLieutenants()[0]->GetPawn()->GetActorLocation();
    float d = FVector::DistSquared(posSelected, posLieutenant);

    if (d <= distance * distance) {
        return EBTNodeResult::Succeeded;
    } else {
        return EBTNodeResult::Failed;
    }
}

FString UIsSelectedInRange::GetStaticDescription() const
{
    return TEXT("Permet de savoir si le personnage selectionne est assez proche d'une certaine distance ou non.");
}