// Fill out your copyright notice in the Description page of Project Settings.

#include "ShanylFireballSpell.h"
#include "BFPCharacter.h"
#include "Public/TimerManager.h"
#include "Components/SphereComponent.h"

AShanylFireballSpell::AShanylFireballSpell(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    PrimaryActorTick.bCanEverTick = true;
    OnActorBeginOverlap.AddDynamic(this, &AShanylFireballSpell::OnActorOverlap);
}


void AShanylFireballSpell::BeginPlay() {
   Super::BeginPlay();
   ABFPCharacter* shanyl = Cast<ABFPCharacter>(GetOwner());
   if (IsValid(shanyl)) {
       directionInitiale = shanyl->GetTransform().GetRotation().GetForwardVector();
       directionInitiale.Normalize();

       // On detruit cet acteur apr�s un certain temps ! :)
       FTimerHandle fuzeTimerHandle;
       FTimerDelegate timerDelegate;
       timerDelegate.BindUFunction(this, FName("GenerateExplosion"));
       GetWorld()->GetTimerManager().SetTimer(
           fuzeTimerHandle,
           timerDelegate,
           dureeSpell * 0.9f,
           false);

       DestroyIn(dureeSpell);
   } else {
       MY_LOG_GAME(TEXT("Impossible de recuperer l'owner dans ShanylFireballSpell !!"));
   }
}

void AShanylFireballSpell::Tick(float DeltaTime) {
   Super::Tick(DeltaTime);
   ABFPCharacter* shanyl = Cast<ABFPCharacter>(GetOwner());
   if (IsValid(shanyl)) {
       FVector pos = GetActorLocation();
       pos += directionInitiale * vitesseSpell * DeltaTime;
       SetActorLocation(pos);
   }
}

void AShanylFireballSpell::OnActorOverlap(AActor * OverlappedActor, AActor * OtherActor) {
    // On explose si on rentre en collision avec qui que ce soit, sauf si c'est shanyl lui-m�me !
    AActor* owner = GetOwner();
    if (IsValid(OtherActor)) {
        if (OtherActor != owner && OtherActor != this) {
            if (!OtherActor->GetName().Contains("Shanyl") && !OtherActor->GetName().Contains("Explo")) { // Je suis desole, mais �a fait 6h que je suis sur ce bug :/
                MY_LOG_GAME(TEXT("OVERLAP !!! of %s"), *GetName());
                DEBUG_SCREEN(-1, 5.0f, FColor::Green, TEXT("OVERLAP !!! of Fireball"));
                FString str = OtherActor->GetName();
                DEBUG_SCREEN(-1, 5.0f, FColor::Green, str);
                GetWorldTimerManager().ClearAllTimersForObject(this);

                // On cree l'explosion
                GenerateExplosion();

                // On s'auto-detruit !
                AutoDestroy();
            }
        }
    }
}

void AShanylFireballSpell::GenerateExplosion() {
    if (!bHasExplode) { // On ne veut exploser qu'une seule fois au maximum !
        bHasExplode = true;
        MY_LOG_GAME(TEXT("EXPLOSION !!!"));
        // On cree l'explosion
        FVector position = GetActorLocation();
        FRotator rotation = GetActorRotation();
        FActorSpawnParameters spawnParams;
        spawnParams.Owner = GetOwner();
        spawnParams.bNoFail = true;
        spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

        ASpell* explosion = GetWorld()->SpawnActor<ASpell>(explosionSpellClass, position, rotation, spawnParams);
        if (!IsValid(explosion)) {
            MY_LOG_GAME(TEXT("L'explosion de Shanyl n'a pas pu etre spawne !!"));
        }
    }
}
