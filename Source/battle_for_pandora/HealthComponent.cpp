﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "HealthComponent.h"
#include "Components/TimelineComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"
#include "Engine/World.h"
#include "BFPCharacter.h"
#include "BFPCharacterPlayable.h"
#include "BFP_GameState.h"
#include "Logger.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer)
{
   // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
   // off to improve performance if you don't need them.
   PrimaryComponentTick.bCanEverTick = true;
   MyCharacter = Cast<ABFPCharacter>(this->GetOwner());
   // ...

   SetIsReplicated(true); //autorise la replication des elements

   bWantsInitializeComponent = true; // permet l'appel a InitializeComponent()
}

void UHealthComponent::InitializeComponent() {
   Super::InitializeComponent();
   Health = FullHealth;
   HealthPercentage = 1.0f;
   PreviousHealth = HealthPercentage;
   HealthValue = 0.0f;
   if (HealthCurve)
   {
      FOnTimelineFloat TimelineCallback;
      FOnTimelineEventStatic TimelineFinishedCallback;
      TimelineCallback.BindUFunction(this, FName("SetHealthValue"));
      TimelineFinishedCallback.BindUFunction(this, FName("SetHealthState"));
      MyTimeline = NewObject<UTimelineComponent>(this, FName("Health_Animation"));
      MyTimeline->AddInterpFloat(HealthCurve, TimelineCallback);
      MyTimeline->SetTimelineFinishedFunc(TimelineFinishedCallback);
      MyTimeline->RegisterComponent();
   }
}

// Called when the game starts
void UHealthComponent::BeginPlay()
{
   Super::BeginPlay();
   MY_LOG_GAME(TEXT("UHealthComponent BeginPlay object valid : %u"), IsValid(this));
}


// Called every frame
void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
   Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

   if (IsValid(MyTimeline)) {
      MyTimeline->TickComponent(DeltaTime, ELevelTick::LEVELTICK_TimeOnly, nullptr);
   }
}

float UHealthComponent::GetHealthPercentage()
{
   return HealthPercentage;
}

float UHealthComponent::GetHealth()
{
   return Health;
}

float UHealthComponent::GetFullHealth()
{
   return FullHealth;
}

FText UHealthComponent::GetHealthIntText()
{
   int hpCourrant = HealthPercentage * FullHealth;
   int maxHp = FullHealth;
   FString str = FString::FromInt(hpCourrant);
   str.Append("/");
   str.AppendInt(maxHp);
   return FText::FromString(str);
}


void UHealthComponent::SetHealthValue()
{
   TimelineValue = MyTimeline->GetPlaybackPosition();
   CurveFloatValue = PreviousHealth + HealthValue * HealthCurve->GetFloatValue(TimelineValue);
   HealthPercentage = FMath::Clamp(CurveFloatValue, 0.0f, 1.0f);

   ChangeHealthEvent.Broadcast();
}

void UHealthComponent::SetHealthState()
{// Si le personnage est mort, on le tue ! :D
   //if (HealthPercentage <= 0) {
   //	EmptyHealthEvent.Broadcast();
   //}
   HealthValue = 0.0;
}

void UHealthComponent::UpdateHealth(float HealthChange, bool bAsPercentage)
{
   ABFPCharacter* owner = Cast<ABFPCharacter>(GetOwner());
   if (IsValid(owner)) {
      if (owner->GetCombatState() == CombatState::DEAD)
         return;
   }
   if (bAsPercentage) {
      Health = FMath::Clamp(Health += HealthChange * FullHealth / 100.f, 0.0f, FullHealth);
   }
   else {
      Health = FMath::Clamp(Health += HealthChange, 0.0f, FullHealth);
   }
}

void UHealthComponent::UpdateHealthPercentage() {
   PreviousHealth = HealthPercentage;
   HealthValue = (Health / FullHealth) - PreviousHealth;

   if (IsValid(MyTimeline)) {
      MyTimeline->PlayFromStart();
   }
}

void UHealthComponent::SetRedFlash(bool newVal)
{
   if (newVal) {
      bIsDamaged = true;
      FTimerDelegate timerDelegate;
      timerDelegate.BindUFunction(this, FName("SetRedFlash"), false);
      FTimerHandle handle;
      GetWorld()->GetTimerManager().SetTimer(handle, timerDelegate, 0.1f, false);
   }
   else {
      bIsDamaged = false;
   }
}

void UHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
   Super::GetLifetimeReplicatedProps(OutLifetimeProps);

   MY_LOG_UI(TEXT("CA REPLIQUE UHealthComponent !!!"));
   DOREPLIFETIME(UHealthComponent, Health);
   DOREPLIFETIME(UHealthComponent, bIsHealed);
   DOREPLIFETIME(UHealthComponent, bIsDamaged);
}

void UHealthComponent::SetTimer(FTimerHandle& InOutHandle, FTimerDelegate::TUObjectMethodDelegate<UHealthComponent>::FMethodPtr InTimerMethod, float InRate, bool InbLoop, float InFirstDelay)
{
   GetWorld()->GetTimerManager().SetTimer(InOutHandle, this, InTimerMethod, InRate, InbLoop, InFirstDelay);
}

float UHealthComponent::TakeDamage(float DamageAmount, bool bAsPercentage)
{
   if (GetWorld()->IsClient()) {
      DEBUG_SCREEN(-1, 5.f, FColor::Yellow, "ABFPCharacterPlayable SetUnGhosted Client");
   }
   else if (GetWorld()->IsServer()) {
      DEBUG_SCREEN(-1, 5.f, FColor::Yellow, "ABFPCharacterPlayable SetUnGhosted Serveur");
   }

   ABFPCharacter* owner = Cast<ABFPCharacter>(GetOwner());
   if (IsValid(owner)) {
      if (owner->GetCombatState() == CombatState::DEAD)
         return 0;
   }

   SetRedFlash(true);
   UpdateHealth(-DamageAmount, bAsPercentage);
   DEBUG_SCREEN(-1, 5.f, FColor::Yellow, *FString::SanitizeFloat(DamageAmount));
   DEBUG_SCREEN(-1, 5.f, FColor::Yellow, TEXT("Damage Taken"));

   //// Si le personnage est mort, on le tue ! :D
   if (Health <= 0) {
      HealthPercentage = 0.0f;
      EmptyHealthEvent.Broadcast();
   }
   return DamageAmount;
}

void UHealthComponent::OnRep_Health() {
   UpdateHealthPercentage();
}

void UHealthComponent::OnRep_IsHealed() {
   DEBUG_SCREEN(-1, 5.f, FColor::Yellow, FString::Printf(TEXT("UHealthComponent OnRep_IsHealed %u"), bIsHealed));
   MY_LOG_NETWORK(TEXT("UHealthComponent OnRep_IsHealed %u"), bIsHealed);
   ChangeHealingStateEvent.Broadcast();
}

void UHealthComponent::OnRep_IsDamaged() {
   DEBUG_SCREEN(-1, 5.f, FColor::Yellow, FString::Printf(TEXT("UHealthComponent OnRep_IsDamaged %u"), bIsDamaged));
   MY_LOG_NETWORK(TEXT("UHealthComponent OnRep_IsDamaged %u"), bIsDamaged);
   ChangeDamageStateEvent.Broadcast();
}


void UHealthComponent::StartHealing(FTimerHandle& timer, float amount, bool bAsPercentage, float delay, bool repeat) {
   if (delay <= 0.0f) {
      MY_LOG_GAME(TEXT("Timer non strictement positif dans StartHealing !"));
   }

   amountHeal = amount;
   bAsPercentageHealing = bAsPercentage;
   bRepeatHealing = repeat;
   bIsHealed = true;

   FTimerDelegate timerDelegate;
   timerDelegate.BindUFunction(this, FName("HealCallback"), timer);
   GetWorld()->GetTimerManager().SetTimer(timer, timerDelegate, delay, bRepeatHealing, -1.0f);
   //SetTimer(timer, &UHealthComponent::HealCallback, delay, repeat);
}

void UHealthComponent::StopHealing(FTimerHandle& timer) {
   MY_LOG_GAME(TEXT("STOPHEALING WESH !!!"));
   GetWorld()->GetTimerManager().ClearTimer(timer);
   bIsHealed = false;
}

void UHealthComponent::HealCallback(FTimerHandle timer) {
   MY_LOG_GAME(TEXT("HEALCALLBACK WESH !!!"));
   //if (Health == FullHealth) {
   //	StopHealing(timer);
   //}
   //else {
   Heal(amountHeal, bAsPercentageHealing);
   if (!bRepeatHealing) {
      StopHealing(timer);
   }
   //}
}

void UHealthComponent::Heal(float HealthChange, bool bAsPercentage) {
   UpdateHealth(HealthChange, bAsPercentage);
}

void UHealthComponent::StartDamaging(FTimerHandle& timer, float amount, bool bAsPercentage, float delay, bool repeat) {
   if (delay <= 0.0f) {
      MY_LOG_GAME(TEXT("Timer non strictement positif dans StartDamaging !"));
   }

   amountDamage = amount;
   bAsPercentageDamage = bAsPercentage;
   bRepeatDamage = repeat;
   SetRedFlash(true);

   FTimerDelegate timerDelegate;
   timerDelegate.BindUFunction(this, FName("DamageCallback"), timer);
   GetWorld()->GetTimerManager().SetTimer(timer, timerDelegate, delay, bRepeatDamage, -1.0f);
   //SetTimer(timer, &UHealthComponent::DamageCallback, delay, bRepeatDamage);
}

void UHealthComponent::StopDamaging(FTimerHandle& timer) {
   GetWorld()->GetTimerManager().ClearTimer(timer);
   bIsDamaged = false;
}

void UHealthComponent::DamageCallback(FTimerHandle timer) {
   if (Health <= 0.f) {
      StopDamaging(timer);
   }
   else {
      TakeDamage(amountDamage, bAsPercentageDamage);
      if (!bRepeatDamage) {
         StopDamaging(timer);
      }
   }
}

void UHealthComponent::Damage(float HealthChange, bool bAsPercentage) {
   TakeDamage(HealthChange, bAsPercentage);
}