// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Command.h"
#include "BFPCharacter.h"
#include "SimpleAttackCommand.generated.h"

/**
 * 
 */
UCLASS()
class BATTLE_FOR_PANDORA_API USimpleAttackCommand : public UCommand
{
	GENERATED_BODY()

public:
   USimpleAttackCommand();

   UFUNCTION(BlueprintCallable, Category = "Command")
   void SetEnemy(ABFPCharacter* _char);

   UFUNCTION(BlueprintCallable, Category = "Command")
   ABFPCharacter* GetEnemy();

   UFUNCTION(BlueprintCallable, Category = "Command")
   float GetDistanceToEnemy();

   class UCommandReturn* execute() override;

private:
   UPROPERTY(EditAnywhere, Category = "Command")
   ABFPCharacter* enemy;

   UPROPERTY(EditAnywhere, Category = "Command")
   FAIMoveRequest requete;

};
