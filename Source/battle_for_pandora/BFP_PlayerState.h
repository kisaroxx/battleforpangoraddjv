// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "Network/PlayerInfo.h"
#include "BFP_PlayerState.generated.h"

/**
 *
 */
UCLASS()
class BATTLE_FOR_PANDORA_API ABFP_PlayerState : public APlayerState
{
	GENERATED_BODY()


public:
	UPROPERTY(Replicated)
	FPlayerInfo S_PlayerInfo;

	ABFP_PlayerState(const FObjectInitializer & ObjectInitializer);

	void CopyProperties(APlayerState* PlayerState) override;
	void OverrideWith(APlayerState* PlayerState) override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void BeginPlay() override;

protected:
		
};
