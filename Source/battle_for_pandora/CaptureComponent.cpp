// Fill out your copyright notice in the Description page of Project Settings.

#include "CaptureComponent.h"

#include "Logger.h"

// Sets default values for this component's properties
UCaptureComponent::UCaptureComponent(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer)
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	//PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicated(true); //autorise la replication des elements
}

void UCaptureComponent::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UCaptureComponent, bShow);
	DOREPLIFETIME(UCaptureComponent, checkpointAffiliation);
	DOREPLIFETIME(UCaptureComponent, captureValue);
}

void UCaptureComponent::OnRep_Show() {
	ShowEvent.Broadcast();
}

void UCaptureComponent::OnRep_Affiliation() {
	ChangeAffiliationEvent.Broadcast();
}

void UCaptureComponent::OnRep_ValueChange() {
	MY_LOG_NETWORK(TEXT("Dans le OnRep_ValueChange, captureValue : %f"), captureValue);
	ChangeValueEvent.Broadcast();
}