// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Command.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Character.h"
#include "BFPCharacter.h"
#include "Command.h"
#include "SquadMemberController.generated.h"

UENUM(BlueprintType)
enum class RadiusType : uint8
{
    SMALL,         // Le rayon d'un agent permettant d'utiliser le navmesh complet pour son deplacement.
    LARGE          // Le rayon d'un agent permettant d'utiliser le navmesh partiel pour le deplacement d'un lieutenant de tel sorte � ce qu'il ne longe pas les murs !
};

/**
 * 
 */
UCLASS()
class BATTLE_FOR_PANDORA_API ASquadMemberController : public AAIController
{
    GENERATED_BODY()

    /* Si un ennemie rentre dans cette range, alors on passe automatiquement en mode attaque vers cet ennemi ! */
    UPROPERTY(EditAnywhere, Category = "SensThinkAct")
    float rangeAttaqueAutonome = 150.0f;

    UPROPERTY(EditAnywhere, Category = "Command")
    UCommand* commande;

    bool bBTIsRunning = false;

public :
   ASquadMemberController(const FObjectInitializer & ObjectInitializer);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

   UFUNCTION(BlueprintCallable, Category = "Command")
   UCommandReturn* execute();

   UFUNCTION(BlueprintCallable, Category = "Command")
   void SetCommand(UCommand* command);

   UFUNCTION(BlueprintCallable, Category = "Command")
   UCommand* GetCommand();

   UFUNCTION()
   void SetSquad(ASquadDescriptor* _squad);

   UFUNCTION()
   class ASquadDescriptor* GetSquad();

   UFUNCTION()
   bool HasSquad();

   bool IsSbire() const;
   bool IsLieutenant() const;

   virtual void RunBT() {};

   FPathFollowingRequestResult MoveToSpiraled(FAIMoveRequest request, FVector* outNewGoal = nullptr, RadiusType radiusType = RadiusType::SMALL);
private:
   FPathFollowingRequestResult MoveToSpiraledImpl(FAIMoveRequest request, FVector* outNewGoal = nullptr);

};
