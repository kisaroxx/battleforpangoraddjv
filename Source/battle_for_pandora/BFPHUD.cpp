// Fill out your copyright notice in the Description page of Project Settings.


#include "BFPHUD.h"
#include "Engine/Canvas.h"
#include "Engine/Texture2D.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "Util.h"
#include "Animation/WidgetAnimation.h"
#include "Camera/CameraComponent.h"
#include "Components/WidgetComponent.h"
#include "Components/ProgressBar.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"
#include "Logger.h"
#include "WidgetBlueprintLibrary.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "GameManager.h"
#include "BFPCharacterPlayable.h"
#include "BFPCharacterLieutenant.h"
#include "BFP_PlayerController.h"
#include "BFP_GameState.h"
#include "CheckpointTriggerSphere.h"

#include "TimerManager.h"
#include "Components/TimelineComponent.h"

#include "UI/RedFlashWidget.h"
#include "UserWidget.h"
#include "Image.h"
#include "SpellComponent.h"


ABFPHUD::ABFPHUD(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	static ConstructorHelpers::FClassFinder<UUserWidget> HealthBar(TEXT("/Game/UI/Health/HealthBar"));
	HUDWidgetClass = HealthBar.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> widgetClass(TEXT("/Game/UI/InteractiveMap/InteractiveMap"));
	InteractiveMap = widgetClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> HerosIndicatorClass(TEXT("/Game/UI/InteractiveMap/CharacterIndicator/HerosIndicator"));
	HerosIndicator = HerosIndicatorClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> redArrow(TEXT("/Game/UI/InteractiveMap/Redarrow_widget"));
	RedArrow = redArrow.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> DavrosIndicatorClass(TEXT("/Game/UI/InteractiveMap/CharacterIndicator/Davros_Indicator"));
	DavrosIndicator = DavrosIndicatorClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> LieutenantIndicatorClass(TEXT("/Game/UI/InteractiveMap/CharacterIndicator/LieutenantIndicator"));
	LieutenantIndicator = LieutenantIndicatorClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> StrategyWheelClass(TEXT("/Game/UI/InteractiveMap/Strategy/StrategyWheel"));
	StrategyWheel = StrategyWheelClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> StrategyButtonsClass(TEXT("/Game/UI/InteractiveMap/Strategy/StrategyButtons"));
	StrategyButtons = StrategyButtonsClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> RedFlashClass(TEXT("/Game/UI/Health/redFlash"));
	RedFlash = RedFlashClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> PositionIndicatorClass(TEXT("/Game/UI/InteractiveMap/CharacterIndicator/PositionIndicator"));
	PositionIndicator = PositionIndicatorClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> YouDiedClass(TEXT("/Game/UI/EndGame/YouDied"));
	YouDied = YouDiedClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> YouWinClass(TEXT("/Game/UI/EndGame/WinScreen"));
	WinScreen = YouWinClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> YouLooseClass(TEXT("/Game/UI/EndGame/LooseScreen"));
	LooseScreen = YouLooseClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> CPUncapturedClass(TEXT("/Game/UI/Checkpoints/CPUncapturedBP"));
	CPUncaptured = CPUncapturedClass.Class;


	static ConstructorHelpers::FClassFinder<UUserWidget> ProgressBarPDCClass(TEXT("/Game/UI/ProgressBar_PDC/PDC_ProgressBar"));
	ProgressBarPDC = ProgressBarPDCClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> FlecheIndicatiorHerosClass(TEXT("/Game/UI/InteractiveMap/CharacterIndicator/FlecheIndicator"));
	FlechePositionHerosIndicator = FlecheIndicatiorHerosClass.Class;


	static ConstructorHelpers::FClassFinder<UUserWidget> FlecheIndicatiorDavrosClass(TEXT("/Game/UI/InteractiveMap/CharacterIndicator/FlecheRougeIndicator"));
	FlechePositionDavrosIndicator = FlecheIndicatiorDavrosClass.Class;
	//static ConstructorHelpers::FClassFinder<UUserWidget> CPCapturedByHerosClass(TEXT("/Game/UI/Checkpoints/CPUncapturedBP"));
	//CPCapturedByHeros = CPCapturedByHerosClass.Class;

	//static ConstructorHelpers::FClassFinder<UUserWidget> CPCapturedByDavrosClass(TEXT("/Game/UI/Checkpoints/CPUncapturedBP"));
	//CPCapturedByDavros = CPCapturedByDavrosClass.Class;
}


void ABFPHUD::DrawHUD()
{
	Super::DrawHUD();

	// TODO ==> d�commenter les 3 lignes d'apr�s ! x)
	//if (isDead || isEndGame) {
	//    return;
	//}

	// On reinitialise les widgets des marks !
	for (auto widget : lieutenantColorMarkWidgets) {
		widget->RemoveFromViewport();
	}
	lieutenantColorMarkWidgets.Empty();
	// On reinitialise les widgets et des nb !
	for (auto widget : lieutenantNombreSbireWidget) {
		widget->RemoveFromViewport();
	}
	lieutenantNombreSbireWidget.Empty();

	// On positionne la wheel � l'endroit du lieutenant choisi s'il y en a un !
	ABFPCharacterLieutenant* lieutenantChoisi = GetLieutenantChoisi();
	if (IsValid(lieutenantChoisi)) {
		FVector2D pos2D = fromWorldToMap(lieutenantChoisi->GetActorLocation());
		if (IsValid(StrategyWheelWidget)) {
			StrategyWheelWidget->SetPositionInViewport(pos2D);
		}
		if (IsValid(StrategyButtonsWidget)) {
			StrategyButtonsWidget->SetPositionInViewport(pos2D);
		}
	}


	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

	// Recupere MON pion
	TWeakObjectPtr<APlayerController> MyController = GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld());
	TWeakObjectPtr<APawn> pawn = MyController->GetPawn();
	TWeakObjectPtr<ABFPCharacterPlayable> playerPawn = Cast<ABFPCharacterPlayable>(pawn);

	if (playerPawn.IsValid()) {
		// Suis-je dans ma carte interactive ?
		bool isInInteractivMap = playerPawn->GetIsInInteractiveMap();
		if (!isInInteractivMap) {
			MY_LOG_NETWORK(TEXT("DrawHUD isInInteractivMap %u"), isInInteractivMap);
			if (!bAllPicturesSet) {
				//Mise a jour du portrait a cote de la healthbar
				if (IsValid(MainHUDWidget)) {
					UImage* ImageInGame = dynamic_cast<UImage*>(MainHUDWidget->GetWidgetFromName(FName("Symbol")));
					if (IsValid(GetWorld())) {
						if (IsValid(GetWorld()->GetFirstLocalPlayerFromController())) {
							if (IsValid(GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld()))) {
								APlayerController* MyController = GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld());
								if (IsValid(MyController)) {
									ABFP_PlayerState* MyPlayerState = Cast<ABFP_PlayerState>(MyController->PlayerState);
									if (IsValid(MyPlayerState)) {
										if (IsValid(MyPlayerState->S_PlayerInfo.IconInGameImage.Get())) {
											ImageInGame->SetBrushFromTexture(MyPlayerState->S_PlayerInfo.IconInGameImage.Get(), true);
										}
										bAllPicturesSet = true;
									}
								}
							}
						}
					}
				}
			}
			if (IsValid(MainHUDWidget)) {
				MainHUDWidget->SetVisibility(ESlateVisibility::Visible);
			}

		}


		if (isInInteractivMap) {
			// Choix d'affichage
			//MY_LOG_NETWORK(TEXT("DrawHUD MyControllerPawn %u"), playerPawn.IsValid());
			if (IsValid(MainHUDWidget)) {
				MainHUDWidget->SetVisibility(ESlateVisibility::Hidden);
			}

			// Heros
			if (playerPawn->getCharacterAffiliation() == Affiliation::GOOD) {
				AffichagePourHeros(playerPawn.Get());
			}
			// Davros
			if (playerPawn->getCharacterAffiliation() == Affiliation::EVIL) {
				AffichagePourDavros(playerPawn.Get());
			}

			if (AfficherLignes && isInInteractivMap) {
				DessinerLignes();
			}
			ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
			TArray<ACheckpointTriggerSphere*> CPs = gameState->GetAllCPs();
			// Affichage des CPs
			for (int i = 0; i < CPs.Num(); ++i) {
				auto widget = CPUncapturedWidget[i];
				UImage* CPImage = dynamic_cast<UImage*>(widget->GetWidgetFromName(FName("CP")));
				if (CPs[i]->getCheckpointAffiliation() == Affiliation::NEUTRAL) {
					CPImage->SetBrushFromTexture(PDCNeutre, true);
				}
				else if (CPs[i]->getCheckpointAffiliation() == Affiliation::EVIL) {
					CPImage->SetBrushFromTexture(PDCEvil, true);
				}
				else {
					CPImage->SetBrushFromTexture(PDCGood, true);
				}
				DrawMark(CPs[i]);
			}
			HideSpells();
		}
		else {
			// Afficher les spells
         FString mapName = GetWorld()->GetMapName().Mid(GetWorld()->StreamingLevelsPrefix.Len());
         if (mapName == "GameMap_Final") {
            DrawSpells();
         }
		}
	}
	else {
		MY_LOG_NETWORK(TEXT("DrawHUD pawn %u"), pawn.IsValid());
		MY_LOG_NETWORK(TEXT("DrawHUD pawnCast %u"), playerPawn.IsValid());
	}
}


void ABFPHUD::AffichagePourHeros(ABFPCharacterPlayable* playerPawn) {

	// Recupere les acteurs
	ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
	TArray<ABFPCharacterPlayable*> Heros = gameState->GetAllJoueursAllies();
	TArray<ABFPCharacterLieutenant*> Lieutenant = gameState->GetAllLieutenants();
	ABFPCharacterPlayable* Davros = gameState->GetDavros();
	APlayerController* MyController = GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld());
	APawn* pawn = MyController->GetPawn();
	ABFPCharacterPlayable* playerPawnPerso = Cast<ABFPCharacterPlayable>(pawn);
	bool isInInteractivMap = playerPawn->GetIsInInteractiveMap();

	// Affichage des copains
	for (int i = 0; i < HerosIndicatorWidgets.Num(); ++i) {
		if (IsValid(Heros[i])) {
			auto widget = HerosIndicatorWidgets[i];
			FVector worldLocation = Heros[i]->GetActorLocation();
			FVector2D pos = fromWorldToMap(worldLocation);
			widget->SetPositionInViewport(pos, true);
			//UImage* Image = dynamic_cast<UImage*>(widget->GetWidgetFromName(FName("fleche")));
			if (playerPawnPerso == Heros[i]) {
				/*	Image->SetVisibility(ESlateVisibility::Visible);
				MY_LOG_NETWORK(TEXT("Angle %f"), angle);
				widget->SetRenderAngle(angle);*/
				float angle = CalculAngleRotationFlecheMap(Heros[i], widget);


				FlechePositionHerosIndicatorWidgetArray[0]->SetVisibility(ESlateVisibility::Visible);
				FlechePositionHerosIndicatorWidgetArray[0]->SetRenderAngle(angle);
				FlechePositionHerosIndicatorWidgetArray[0]->SetPositionInViewport(pos, true);
			}
			
				


		}
	}

    // Affichage des lieutenants visibles
    for (auto heros : Heros) {
       TArray<ABFPCharacterLieutenant*> lieutenantsVus = gameState->GetAllInDistanceFrom(Lieutenant,
          heros->GetActorTransform(), 5000);
       for (auto lieutenantVisible : lieutenantsVus) {
          if (IsValid(lieutenantVisible)) {
             int32 index = Lieutenant.Find(lieutenantVisible);
             if (index != INDEX_NONE) {
                auto widget = LieutenantIndicatorWidgets[index];
                FVector worldLocation = Lieutenant[index]->GetActorLocation();
                FVector2D pos = fromWorldToMap(worldLocation);
                widget->SetPositionInViewport(pos, true);
                if (isInInteractivMap) {
                   widget->SetVisibility(ESlateVisibility::HitTestInvisible);
                }
             }
          }
       }
    }

    for (int i = 0; i < Lieutenant.Num(); ++i) {
        if (IsValid(Lieutenant[i])) {
           for (auto heros : Heros) {
              auto distance = (heros->GetActorLocation() - Lieutenant[i]->GetActorLocation()).Size();
              auto widget = LieutenantIndicatorWidgets[i];
              if (distance > 5000 && isInInteractivMap) {
                 widget->SetVisibility(ESlateVisibility::Hidden);
              }
              else if (distance <= 5000) {
                 widget->SetVisibility(ESlateVisibility::HitTestInvisible);
                 DrawMark(Lieutenant[i]);
                 DrawNbSbiresLieutenant(Lieutenant[i]);
                 break;
              }
              // DrawMark(Lieutenant[i]);
           }
        }
    }
    // Affichage de Davros si visible

    if (IsValid(Davros)) {
        TArray<ABFPCharacterPlayable*> DavrosArray;
        DavrosArray.Add(Davros);

        for (auto heros : Heros) {
           TArray<ABFPCharacterPlayable*> DavrosVus = gameState->GetAllInDistanceFrom(DavrosArray,
              heros->GetActorTransform(), 5000);

           for (auto davros : DavrosVus) {
              if (davros == Davros) {

                 auto widget = DavrosIndicatorWidgets[0];
                 FVector worldLocation = Davros->GetActorLocation();
                 FVector2D pos = fromWorldToMap(worldLocation);
                 widget->SetPositionInViewport(pos, true);
                 if (isInInteractivMap) {
                    widget->SetVisibility(ESlateVisibility::Visible);
                    UImage* Image = dynamic_cast<UImage*>(widget->GetWidgetFromName(FName("fleche")));
                    Image->SetVisibility(ESlateVisibility::Hidden);
                 }
              }
           }
        }
        for (auto heros : Heros) {
           auto distance = (heros->GetActorLocation() - Davros->GetActorLocation()).Size();
           auto widget = DavrosIndicatorWidgets[0];
           if (distance > 5000 && isInInteractivMap) {
              widget->SetVisibility(ESlateVisibility::Hidden);
           }
           else {
              widget->SetVisibility(ESlateVisibility::Visible);
              break;
           }
        }
    }
	//InteractiveMapWidget->SetRenderOpacity(25.0f);
}

void ABFPHUD::AffichagePourDavros(ABFPCharacterPlayable* playerPawn) {
	// Recupere les acteurs
	ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
	TArray<ABFPCharacterPlayable*> Heros = gameState->GetAllJoueursAllies();
	TArray<ABFPCharacterLieutenant*> Lieutenant = gameState->GetAllLieutenants();
	ABFPCharacterPlayable* Davros = gameState->GetDavros();
	APlayerController* MyController = GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld());
	APawn* pawn = MyController->GetPawn();
	ABFPCharacterPlayable* playerPawnPerso = Cast<ABFPCharacterPlayable>(pawn);

	bool isInInteractivMap = playerPawn->GetIsInInteractiveMap();
	MY_LOG_NETWORK(TEXT("DrawHUD joueur : EVIL -- Heros"));
	// Affichage des heros visibles
	TArray<ABFPCharacterPlayable*> herosVus = gameState->GetAllInDistanceFrom(Heros,
		playerPawn->GetActorTransform(), 9999999); // remplacer par 5000 si on veut revenir comme avant
	for (auto heros : herosVus) {
		if (IsValid(heros)) {
			int32 index = Heros.Find(heros);
			auto widget = HerosIndicatorWidgets[index];
			FVector worldLocation = Heros[index]->GetActorLocation();
			FVector2D pos = fromWorldToMap(worldLocation);
			widget->SetPositionInViewport(pos, true);
			if (isInInteractivMap) {
				widget->SetVisibility(ESlateVisibility::Visible);
		/*		UImage* Image = dynamic_cast<UImage*>(widget->GetWidgetFromName(FName("fleche")));
				Image->SetVisibility(ESlateVisibility::Hidden);*/
			}
		}
	}
   for (auto lieutenant : Lieutenant) {
      TArray<ABFPCharacterPlayable*> herosVus = gameState->GetAllInDistanceFrom(Heros,
         lieutenant->GetActorTransform(), 9999999); // remplacer par 5000 si on veut revenir comme avant
      for (auto heros : herosVus) {
         if (heros != nullptr) {
            int32 index = Heros.Find(heros);
            auto widget = HerosIndicatorWidgets[index];
            FVector worldLocation = Heros[index]->GetActorLocation();
            FVector2D pos = fromWorldToMap(worldLocation);
            widget->SetPositionInViewport(pos, true);
            if (isInInteractivMap) {
               widget->SetVisibility(ESlateVisibility::Visible);
               UImage* Image = dynamic_cast<UImage*>(widget->GetWidgetFromName(FName("fleche")));
               Image->SetVisibility(ESlateVisibility::Hidden);
            }
         }
      }
   }

   TArray<bool> herosVusByDavros;
	for (int i = 0; i < Heros.Num(); ++i) {
		if (IsValid(Heros[i])) {
			auto distance = (playerPawn->GetActorLocation() - Heros[i]->GetActorLocation()).Size();
			auto widget = HerosIndicatorWidgets[i];
			if (distance > 9999999 && isInInteractivMap) { // remplacer par 5000 si on veut revenir comme avant
				widget->SetVisibility(ESlateVisibility::Hidden);
            herosVusByDavros.Add(false);
         }
         else {
            widget->SetVisibility(ESlateVisibility::Visible);
            herosVusByDavros.Add(true);
         }
		}
	}

   for (int i = 0; i < Heros.Num(); ++i) {
      if (Heros[i] != nullptr) {
         for (auto lieutenant : Lieutenant) {
            auto distance = (lieutenant->GetActorLocation() - Heros[i]->GetActorLocation()).Size();
            auto widget = HerosIndicatorWidgets[i];
            if (distance > 9999999 && isInInteractivMap && !herosVusByDavros[i]) { // remplacer par 5000 si on veut revenir comme avant
               widget->SetVisibility(ESlateVisibility::Hidden);
            }
            else {
               widget->SetVisibility(ESlateVisibility::Visible);
               break;
            }
         }
      }
   }

	// Affichage des lieutenants
	MY_LOG_NETWORK(TEXT("DrawHUD joueur : EVIL -- Lieutenants : %d"), LieutenantIndicatorWidgets.Num());
	for (int i = 0; i < LieutenantIndicatorWidgets.Num(); ++i) {
		if (IsValid(Lieutenant[i])) {
			MY_LOG_NETWORK(TEXT("DrawHUD joueur : EVIL -- Lieutenants %d"), i);
			auto widget = LieutenantIndicatorWidgets[i];
			FVector worldLocation = Lieutenant[i]->GetActorLocation();
			FVector2D pos = fromWorldToMap(worldLocation);
			widget->SetPositionInViewport(pos, true);
			DrawMark(Lieutenant[i]);
			DrawNbSbiresLieutenant(Lieutenant[i]);
		}
	}
	// Affichage de Davros
	MY_LOG_NETWORK(TEXT("DrawHUD joueur : EVIL -- Davros"));
	for (int i = 0; i < DavrosIndicatorWidgets.Num(); ++i) {
		if (IsValid(Davros)) {
			auto widget = DavrosIndicatorWidgets[i];
			FVector worldLocation = Davros->GetActorLocation();
			FVector2D pos = fromWorldToMap(worldLocation);
			widget->SetPositionInViewport(pos, true);
			if (playerPawnPerso == Davros) {
				/*	Image->SetVisibility(ESlateVisibility::Visible);
					MY_LOG_NETWORK(TEXT("Angle %f"), angle);
					widget->SetRenderAngle(angle);*/
				float angle = CalculAngleRotationFlecheMap(Davros, widget);
				FlechePositionDavrosIndicatorWidgetArray[0]->SetVisibility(ESlateVisibility::Visible);
				FlechePositionDavrosIndicatorWidgetArray[0]->SetRenderAngle(angle);
				FlechePositionDavrosIndicatorWidgetArray[0]->SetPositionInViewport(pos, true);
				//CalculPositionFlecheMap(Heros[i], FlechePositionIndicatorWidget, widget,pos);
			}

		}
	}
	// Affichage des lieutenants
	MY_LOG_NETWORK(TEXT("DrawHUD joueur : EVIL -- Lieutenants : %d"), LieutenantIndicatorWidgets.Num());
	for (int i = 0; i < LieutenantIndicatorWidgets.Num(); ++i) {
		if (IsValid(Lieutenant[i])) {
			MY_LOG_NETWORK(TEXT("DrawHUD joueur : EVIL -- Lieutenants %d"), i);
			auto widget = LieutenantIndicatorWidgets[i];
			FVector worldLocation = Lieutenant[i]->GetActorLocation();
			FVector2D pos = fromWorldToMap(worldLocation);
			widget->SetPositionInViewport(pos, true);
			DrawMark(Lieutenant[i]);
			DrawNbSbiresLieutenant(Lieutenant[i]);
			/*float angle = CalculAngleRotationFlecheMap(Lieutenant[i], widget);
			FlechePositionDavrosIndicatorWidgetArray[i+1]->SetVisibility(ESlateVisibility::Visible);
			FlechePositionDavrosIndicatorWidgetArray[i+1]->SetRenderAngle(angle);
			FlechePositionDavrosIndicatorWidgetArray[i+1]->SetPositionInViewport(pos, true);*/
		}
	}

}






void ABFPHUD::DessinerLignes() {
	if (LieutenantChoisi) {
		TArray<ABFPCharacterLieutenant*> lieutenantInstances = GetWorld()->GetGameState<ABFP_GameState>()->GetAllLieutenants();
		ABFPCharacterLieutenant* lieutenantchoisi = this->GetLieutenantChoisi();
		int32 lieutenantIndex = lieutenantInstances.Find(lieutenantchoisi);
		if (lieutenantIndex != INDEX_NONE) {
			if (ArrayCiblesLieutenants[lieutenantIndex].Positions.Num() > 1) {
				//Mise a jour de la position dans l'array de la position du widget du lieutenant
				FVector positiontemp = lieutenantchoisi->GetActorLocation();
				FVector2D positionTempInMap = fromWorldToMap(positiontemp);
				//Positions[0] correspond a la position du lieutenant car plus pratiques dans la prochaine boucle.
				ArrayCiblesLieutenants[lieutenantIndex].Positions[0] = positionTempInMap;
				if (FVector2D::Distance(ArrayCiblesLieutenants[lieutenantIndex].Positions[0],
					ArrayCiblesLieutenants[lieutenantIndex].Positions[1]) < 2) {
					// Widget
					ArrayCiblesLieutenants[lieutenantIndex].Widgets[0]->RemoveFromViewport();
					auto pepito = ArrayCiblesLieutenants[lieutenantIndex].Widgets[0];
					ArrayCiblesLieutenants[lieutenantIndex].Widgets.Remove(pepito);
					//Position
					auto pepito2 = ArrayCiblesLieutenants[lieutenantIndex].Positions[1];
					ArrayCiblesLieutenants[lieutenantIndex].Positions.Remove(pepito2);

				}
				for (int i = 0; i != (ArrayCiblesLieutenants[lieutenantIndex].Positions.Num() - 1); i++)
				{
					float xdebut = ArrayCiblesLieutenants[lieutenantIndex].Positions[i].X;
					float ydebut = ArrayCiblesLieutenants[lieutenantIndex].Positions[i].Y;
					float xfin = ArrayCiblesLieutenants[lieutenantIndex].Positions[i + 1].X;
					float yfin = ArrayCiblesLieutenants[lieutenantIndex].Positions[i + 1].Y;
					DrawLine(xdebut, ydebut, xfin, yfin, FColor::Red, 10.f);
				}
			}
		}
	}
}



void ABFPHUD::DrawSpells() {
	TWeakObjectPtr<APlayerController> MyController = GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld());
	TWeakObjectPtr<APawn> pawn = MyController->GetPawn();
	ABFPCharacter* character = Cast<ABFPCharacter>(pawn);
	if (IsValid(character)) {
		TArray<USpellComponent*> spellsComponents = character->GetSpellComponents();
		for (int i = 0; i < spellsComponents.Num(); i++) {
			spellsComponents[i]->DrawWidget();
		}
	}
}

void ABFPHUD::HideSpells() {
	APlayerController* MyController = GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld());
	TWeakObjectPtr<APawn> pawn = MyController->GetPawn();
	ABFPCharacterPlayable* character = Cast<ABFPCharacterPlayable>(pawn);
	if (IsValid(character)) {
		TArray<USpellComponent*> spellsComponents = character->GetSpellComponents();
		for (int i = 0; i < spellsComponents.Num(); i++) {
			spellsComponents[i]->HideWidget();
		}
	}
}


void ABFPHUD::BeginPlay()
{
	Super::BeginPlay();
	if (!GetWorld()->IsServer()) {
		InitializeWidgets();
	}
}

void ABFPHUD::InitializeWidgets() {

	if (!bInitHUD1Time) {
		// HealthBar
		MainHUDWidget = CreateWidget<UUserWidget>(GetWorld(), HUDWidgetClass);
		// InteractvieMap
		InteractiveMapWidget = CreateWidget<UUserWidget>(GetWorld(), InteractiveMap);
		InteractiveMapWidget->SetVisibility(ESlateVisibility::Hidden);
		// YouDied
		MY_LOG_NETWORK(TEXT("InitializeWidgets YouDied"));
		YouDiedWidget = CreateWidget<UUserWidget>(GetWorld(), YouDied);
		YouDiedWidget->SetVisibility(ESlateVisibility::Hidden);
		// WinScreen
		MY_LOG_NETWORK(TEXT("InitializeWidgets YouDied"));
		WinScreenWidget = CreateWidget<UUserWidget>(GetWorld(), WinScreen);
		WinScreenWidget->SetVisibility(ESlateVisibility::Hidden);
		// LooseScreen
		MY_LOG_NETWORK(TEXT("InitializeWidgets YouDied"));
		LooseScreenWidget = CreateWidget<UUserWidget>(GetWorld(), LooseScreen);
		LooseScreenWidget->SetVisibility(ESlateVisibility::Hidden);
		// Red Flash
		RedFlashWidget = CreateWidget<UUserWidget>(GetWorld(), RedFlash);
		RedFlashWidget->SetVisibility(ESlateVisibility::Visible);
		// Red Arrows
		RedArrowWidget = CreateWidget<UUserWidget>(GetWorld(), RedArrow);
		RedArrowWidget->SetVisibility(ESlateVisibility::Hidden);
		// StrategyWheel
		StrategyWheelWidget = CreateWidget<UUserWidget>(GetWorld(), StrategyWheel);
		StrategyWheelWidget->SetVisibility(ESlateVisibility::Hidden);
		// StrategyButton
		StrategyButtonsWidget = CreateWidget<UUserWidget>(GetWorld(), StrategyButtons);
		StrategyButtonsWidget->SetVisibility(ESlateVisibility::Hidden);

		// ProgressBarPDC
		ProgressBarPDCWidget = CreateWidget<UUserWidget>(GetWorld(), ProgressBarPDC);
		ProgressBarPDCWidget->SetVisibility(ESlateVisibility::Hidden);

	
	//for (int i = 0; i != 4; i++ ) {
	//		FlechePositionHerosIndicatorWidgetArray.Add(CreateWidget<UUserWidget>(GetWorld(), FlechePositionHerosIndicator));
	//		FlechePositionHerosIndicatorWidgetArray[i]->SetVisibility(ESlateVisibility::Hidden);
	//	}

	//	FlechePositionDavrosIndicatorWidgetArray.Add(CreateWidget<UUserWidget>(GetWorld(), FlechePositionDavrosIndicator));
	//	FlechePositionDavrosIndicatorWidgetArray[0]->SetVisibility(ESlateVisibility::Hidden);
		// CPs
		//CPUncapturedWidget = CreateWidget<UUserWidget>(GetWorld(), CPUncaptured);
		//CPUncapturedWidget->SetVisibility(ESlateVisibility::Hidden);

		// HelathBar actif
		CurrentWidget = MainHUDWidget;

		if (CurrentWidget)
		{
			// Ajouts au VP
			CurrentWidget->AddToViewport();
			InteractiveMapWidget->AddToViewport(1);
			RedArrowWidget->AddToViewport(5);
			StrategyWheelWidget->AddToViewport(13);
			StrategyButtonsWidget->AddToViewport(14);
			RedFlashWidget->AddToViewport(1);
			YouDiedWidget->AddToViewport(6);
			WinScreenWidget->AddToViewport(7);
			LooseScreenWidget->AddToViewport(7);
			ProgressBarPDCWidget->AddToViewport(1);
		/*	for (int i = 0; i != FlechePositionHerosIndicatorWidgetArray.Num(); i++) {
				FlechePositionHerosIndicatorWidgetArray[i]->AddToViewport(5);
			}
			FlechePositionDavrosIndicatorWidgetArray[0]->AddToViewport(5);*/
			// CPUncapturedWidget->AddToViewport(1);
		}
	
		bInitHUD1Time = true;
	}
}







FVector2D ABFPHUD::fromWorldToMap(FVector vector) {
	APlayerController* MyController = GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld());
	float worldSizeX = 28300 * 2.0f;
	float worldSizeY = 28300 * 2.0f;
	int screenSizeX, screenSizeY;
	MyController->GetViewportSize(screenSizeX, screenSizeY);
	float cordX = (vector.X + worldSizeX / 2) * (screenSizeX / worldSizeX);
	float cordY = (vector.Y + worldSizeY / 2) * (screenSizeY / worldSizeY);
	return FVector2D(cordX, cordY);
}

void ABFPHUD::AffichageMapInteractive()
{
    MY_LOG_NETWORK(TEXT("AffichageMapInteractive MainHUDWidget %u"), IsValid(MainHUDWidget));

    // Set des != parametres
    MY_LOG_NETWORK(TEXT("AffichageMapInteractive InteractiveMapWidget %u"), IsValid(InteractiveMapWidget));
    InteractiveMapWidget->SetVisibility(ESlateVisibility::Visible);
	 HideSpells();
    CurrentWidget = InteractiveMapWidget;
    APlayerController* MyController = GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld());
    MY_LOG_NETWORK(TEXT("AffichageMapInteractive MyController %u"), IsValid(MyController));
    FInputModeGameAndUI input{};
    MyController->bShowMouseCursor = true;
    input.SetLockMouseToViewportBehavior(EMouseLockMode::LockAlways);
    MyController->SetInputMode(input);
    float worldSizeX = 28300 * 2.0f;
    float worldSizeY = 28300 * 2.0f;

   // Recupere les acteurs  
   ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
   TArray<ABFPCharacterPlayable*> Heros = gameState->GetAllJoueursAllies();
   TArray<ABFPCharacterLieutenant*> Lieutenant = gameState->GetAllLieutenants();
   ABFPCharacterPlayable* Davros = gameState->GetDavros();
   TArray<ACheckpointTriggerSphere*> CPs = gameState->GetAllCPs();

   //MY_LOG_NETWORK(TEXT("Taille liste lieutenants : %u"), Lieutenant.Num());
   //MY_LOG_NETWORK(TEXT("Taille liste : %u"), Lieutenant[0]->GetSquad()->GetManeuver()->GetTargetPoint().Num());

   // Affichage des CPs
   for (int i = 0; i < CPs.Num(); ++i) {
      auto widget = CPUncapturedWidget[i];
      UImage* CPImage = dynamic_cast<UImage*>(widget->GetWidgetFromName(FName("CP")));
      if (CPs[i]->getCheckpointAffiliation() == Affiliation::NEUTRAL) {
         CPImage->SetBrushFromTexture(PDCNeutre, true);
      }
      else if (CPs[i]->getCheckpointAffiliation() == Affiliation::EVIL) {
         CPImage->SetBrushFromTexture(PDCEvil, true);
      }
      else {
         CPImage->SetBrushFromTexture(PDCGood, true);
      }
      widget->SetVisibility(ESlateVisibility::Visible);
   }

   // Choix d'affichage
   ABFPCharacterPlayable* joueur = Cast<ABFPCharacterPlayable>(MyController->GetPawn());
   // Heros
   if (joueur->getCharacterAffiliation() == Affiliation::GOOD) {
      // Affichage des copains
      GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("ABFPHUD AffichageMapInteractive %d"), HerosIndicatorWidgets.Num()));
      for (int i = 0; i < HerosIndicatorWidgets.Num(); ++i) {
         if (IsValid(Heros[i])) {
            auto widget = HerosIndicatorWidgets[i];
            FVector worldLocation = Heros[i]->GetActorLocation();
            FVector2D pos = fromWorldToMap(worldLocation);
            widget->SetPositionInViewport(pos, true);
				
            widget->SetVisibility(ESlateVisibility::Visible);
         }
      }
      // Affichage des lieutenants visibles
      for (auto heros : Heros) {
         TArray<ABFPCharacterLieutenant*> lieutenantsVus = gameState->GetAllInDistanceFrom(Lieutenant,
            heros->GetActorTransform(), 5000);
         for (auto lieutenantVisible : lieutenantsVus) {
            if (IsValid(lieutenantVisible)) {
               int32 index = Lieutenant.Find(lieutenantVisible);
               if (index != INDEX_NONE) {
                  auto widget = LieutenantIndicatorWidgets[index];
                  FVector worldLocation = Lieutenant[index]->GetActorLocation();
                  FVector2D pos = fromWorldToMap(worldLocation);
                  widget->SetPositionInViewport(pos, true);
                  widget->SetVisibility(ESlateVisibility::HitTestInvisible);
                  widget->SetIsEnabled(false);
               }
            }
         }
      }
      // Affichage de Davros si visible
      if (IsValid(Davros)) {
         TArray<ABFPCharacterPlayable*> DavrosArray;
         DavrosArray.Add(Davros);
         for (auto heros : Heros) {
            TArray<ABFPCharacterPlayable*> DavrosVus = gameState->GetAllInDistanceFrom(DavrosArray,
               heros->GetActorTransform(), 5000);
            for (auto davros : DavrosVus) {
               if (davros == Davros) {
                  auto widget = DavrosIndicatorWidgets[0];
                  FVector worldLocation = Davros->GetActorLocation();
                  FVector2D pos = fromWorldToMap(worldLocation);
                  widget->SetPositionInViewport(pos, true);
                  widget->SetVisibility(ESlateVisibility::Visible);
               }
            }
         }
      }
   }
   // Davros
   if (joueur->getCharacterAffiliation() == Affiliation::EVIL) {
      // Affichage des heros visibles
      TArray<ABFPCharacterPlayable*> herosVus = gameState->GetAllInDistanceFrom(Heros,
         joueur->GetActorTransform(), 5000);
      for (auto heros : herosVus) {
         if (IsValid(heros)) {
            int32 index = Heros.Find(heros);
            if (index != INDEX_NONE) {
               auto widget = HerosIndicatorWidgets[index];
               FVector worldLocation = Heros[index]->GetActorLocation();
               FVector2D pos = fromWorldToMap(worldLocation);
               widget->SetPositionInViewport(pos, true);
               widget->SetVisibility(ESlateVisibility::Visible);
            }
         }
      }
      for (auto lieutenant : Lieutenant) {
         TArray<ABFPCharacterPlayable*> herosVus = gameState->GetAllInDistanceFrom(Heros,
            lieutenant->GetActorTransform(), 5000);
         for (auto heros : herosVus) {
            if (IsValid(heros)) {
               int32 index = Heros.Find(heros);
               if (index != INDEX_NONE) {
                  auto widget = HerosIndicatorWidgets[index];
                  FVector worldLocation = Heros[index]->GetActorLocation();
                  FVector2D pos = fromWorldToMap(worldLocation);
                  widget->SetPositionInViewport(pos, true);
                  widget->SetVisibility(ESlateVisibility::Visible);
               }
            }
         }
      }
      // Affichage des lieutenants
      for (int i = 0; i < LieutenantIndicatorWidgets.Num(); ++i) {
         if (IsValid(Lieutenant[i])) {
            auto widget = LieutenantIndicatorWidgets[i];
            FVector worldLocation = Lieutenant[i]->GetActorLocation();
            FVector2D pos = fromWorldToMap(worldLocation);
            widget->SetPositionInViewport(pos, true);
            widget->SetVisibility(ESlateVisibility::Visible);
         }
         else {
             DEBUG_SCREEN(-1, 10.f, FColor::Red, TEXT("lieutenant null"));
         }
      }
      // Affichage de Davros
      for (int i = 0; i < DavrosIndicatorWidgets.Num(); ++i) {
         if (IsValid(Davros)) {
            auto widget = DavrosIndicatorWidgets[i];
            FVector worldLocation = Davros->GetActorLocation();
            FVector2D pos = fromWorldToMap(worldLocation);
            widget->SetPositionInViewport(pos, true);
            widget->SetVisibility(ESlateVisibility::Visible);
         }
      }

      /*auto tp = gameState->GetAllSquads()[0]->GetManeuver()->GetTargetPoint();
      MY_LOG_NETWORK(TEXT("AffichageMapInteractive tp"));
      if (tp.Num() != 0) {
         MY_LOG_NETWORK(TEXT("AffichageMapInteractive tvector %u"), tp.Num());
         if (tp[0] == nullptr) {
            MY_LOG_NETWORK(TEXT("AffichageMapInteractive tp[0] nullptr %u"), tp.Num());
         } else {
            FVector t = tp[0]->GetActorTransform().GetLocation();
            MY_LOG_NETWORK(TEXT("AffichageMapInteractive x : %f, y = %f, z = %f"), t.X, t.Y, t.Z);
         }
      }*/
   }
}

void ABFPHUD::DisparitionMapInteractive()
{
	InteractiveMapWidget->SetVisibility(ESlateVisibility::Visible);
	APlayerController* MyController = GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld());
	UWidgetBlueprintLibrary::SetInputMode_GameOnly(MyController);

	ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
	TArray<ACheckpointTriggerSphere*> CPs = gameState->GetAllCPs();
	for (int i = 0; i < CPs.Num(); ++i) {
		auto widget = CPUncapturedWidget[i];
		widget->SetVisibility(ESlateVisibility::Hidden);
	}

	for (auto widget : HerosIndicatorWidgets) {
		widget->SetVisibility(ESlateVisibility::Hidden);
	}
	for (auto widget : DavrosIndicatorWidgets) {
		widget->SetVisibility(ESlateVisibility::Hidden);
	}
	for (auto widget : LieutenantIndicatorWidgets) {
		widget->SetVisibility(ESlateVisibility::Hidden);
	}

	for (auto widget : lieutenantColorMarkWidgets) {
		widget->SetVisibility(ESlateVisibility::Hidden);
	}
	lieutenantColorMarkWidgets.Empty();

	for (auto widget : lieutenantNombreSbireWidget) {
		widget->SetVisibility(ESlateVisibility::Hidden);
	}
	lieutenantNombreSbireWidget.Empty();

	StrategyWheelWidget->SetVisibility(ESlateVisibility::Hidden);
	RedArrowWidget->SetVisibility(ESlateVisibility::Hidden);
	StrategyButtonsWidget->SetVisibility(ESlateVisibility::Hidden);
	InteractiveMapWidget->SetVisibility(ESlateVisibility::Hidden);
	for (int i = 0; i != FlechePositionHerosIndicatorWidgetArray.Num(); i++) {
		FlechePositionHerosIndicatorWidgetArray[i]->SetVisibility(ESlateVisibility::Hidden);
	}
	for (int i = 0; i != FlechePositionDavrosIndicatorWidgetArray.Num(); i++) {
		FlechePositionDavrosIndicatorWidgetArray[i]->SetVisibility(ESlateVisibility::Hidden);
	}
	
	HidePositionIndicators();
	MainHUDWidget->SetVisibility(ESlateVisibility::Visible);
	CurrentWidget = MainHUDWidget;
}

void ABFPHUD::DrawMark(ABFPCharacterLieutenant * lieutenant) {
	// Affichage de la marque
	UUserWidget* mark = CreateWidget<UUserWidget>(GetWorld(), lieutenantColorMarkClass);
	UImage* sprite = dynamic_cast<UImage*>(mark->GetWidgetFromName(FName("CircleSprite")));
	if (IsValid(sprite)) {
      if (IsValid(lieutenant)) {
         if (IsValid(lieutenant->GetSquad())) {
            ACheckpointTriggerSphere* checkpoint = lieutenant->GetSquad()->GetCheckpointAssociated();
            if (IsValid(checkpoint)) { // Une squad qui a ete pose a la main n'est pas associee � un checkpoint !
               FColor color = checkpoint->GetColor();
               sprite->SetColorAndOpacity(FLinearColor(color));
               FVector worldLocation = lieutenant->GetActorLocation();
               FVector2D pos = fromWorldToMap(worldLocation);
               FVector2D posMark = pos + FVector2D{ 10, -10 };
               mark->AddToViewport(10);
               mark->SetPositionInViewport(posMark, true);
               mark->SetVisibility(ESlateVisibility::Visible);
               lieutenantColorMarkWidgets.Add(mark);
            }
         }
      }
	}
}

void ABFPHUD::DrawMark(ACheckpointTriggerSphere * checkpoint) {
	UUserWidget* mark = CreateWidget<UUserWidget>(GetWorld(), lieutenantColorMarkClass);
	UImage* sprite = dynamic_cast<UImage*>(mark->GetWidgetFromName(FName("CircleSprite")));
	if (IsValid(sprite)) {
		if (IsValid(checkpoint)) {
			FColor color = checkpoint->GetColor();
			sprite->SetColorAndOpacity(FLinearColor(color));
			FVector worldLocation = checkpoint->GetActorLocation();
			FVector2D pos = fromWorldToMap(worldLocation);
			FVector2D posMark = pos + FVector2D{ 10, -10 };
			mark->AddToViewport(10);
			mark->SetPositionInViewport(posMark, true);
			mark->SetVisibility(ESlateVisibility::Visible);
			lieutenantColorMarkWidgets.Add(mark);
		}
	}
}

void ABFPHUD::DrawNbSbiresLieutenant(ABFPCharacterLieutenant* lieutenant) {
	// Affichage du nombre
	UUserWidget* mark = CreateWidget<UUserWidget>(GetWorld(), lieutenantNombreSbireClass);
	UTextBlock* texte = dynamic_cast<UTextBlock*>(mark->GetWidgetFromName(FName("TexteSprite")));
	if (IsValid(texte)) {
		ACheckpointTriggerSphere* checkpoint = lieutenant->GetSquad()->GetCheckpointAssociated();
		if (IsValid(checkpoint)) { // Une squad qui a ete pose a la main n'est pas associee � un checkpoint !
			int nbSbires = lieutenant->GetSquad()->GetNbSbires();
			if (nbSbires <= 9) {
				texte->SetColorAndOpacity(FLinearColor(1.0f, 0.1f, 0.1f));
			}
			else if (nbSbires <= 19) {
				texte->SetColorAndOpacity(FLinearColor(1.0f, 140.0f / 255.0f, 0.0f));
			}
			else if (nbSbires <= 29) {
				texte->SetColorAndOpacity(FLinearColor(0.2f, 1.0f, 0.0f));
			}
			else {
				texte->SetColorAndOpacity(FLinearColor(0.0f, 1.0f, 1.0f));
			}
			FString str; // O
			str.AppendInt(nbSbires); // M
			FText t = FText::AsCultureInvariant(str); // F
			texte->SetText(t); // G
			FVector worldLocation = lieutenant->GetActorLocation();
			FVector2D pos = fromWorldToMap(worldLocation);
			FVector2D posTexte = pos + FVector2D{ 10, 10 };
			mark->AddToViewport(10);
			mark->SetPositionInViewport(posTexte, true);
			mark->SetVisibility(ESlateVisibility::Visible);
			lieutenantNombreSbireWidget.Add(mark);
		}
	}
}


void ABFPHUD::SetDead(bool vraiMort)
{
	if (vraiMort && !isEndGame) {
		YouDiedWidget->SetVisibility(ESlateVisibility::Visible);
	}
	isDead = true;
}

void ABFPHUD::SetEndGameStatus(bool victory)
{
	MY_LOG_NETWORK(TEXT("SetEndGameStatus begin "));
	isEndGame = true;
	if (IsValid(YouDiedWidget)) {
		YouDiedWidget->SetVisibility(ESlateVisibility::Hidden);
		if (victory) {
			WinScreenWidget->SetVisibility(ESlateVisibility::Visible);
		}
		else {
			LooseScreenWidget->SetVisibility(ESlateVisibility::Visible);
		}
	}
}


void ABFPHUD::SetLieutenantChoisiButton(UUserWidget * lieutenantButton)
{
	lieutenantChoisiButton = lieutenantButton;
}

UUserWidget * ABFPHUD::GetLieutenantChoisiButton()
{
	return lieutenantChoisiButton;
}

ABFPCharacterLieutenant* ABFPHUD::GetLieutenantChoisi()
{
	ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
	if (IsValid(gameState)) {
		TArray<ABFPCharacterLieutenant*> lieutenantInstances = gameState->GetAllLieutenants();
		int32 lieutenantButtonIndex = LieutenantIndicatorWidgets.Find(lieutenantChoisiButton);
		if (lieutenantButtonIndex != INDEX_NONE) {
			lieutenantChoisiCharacter = Cast<ABFPCharacterLieutenant>(lieutenantInstances[lieutenantButtonIndex]);
			return lieutenantChoisiCharacter;
		}
		else {
			return nullptr;
		}
	}
	else {
		return nullptr;
	}
}

float ABFPHUD::GetPDCPercentage()
{
	return PDCPercentage;
}

void ABFPHUD::addHeros(ABFPCharacter* character) {

	UUserWidget* widget = CreateWidget<UUserWidget>(GetWorld(), HerosIndicator);
	widget->SetVisibility(ESlateVisibility::Hidden);
	widget->AddToViewport(1);
	ABFPCharacterPlayable*  heros = Cast<ABFPCharacterPlayable>(character);
	if (IsValid(heros))
	{
		UImage* ImageInGame = dynamic_cast<UImage*>(widget->GetWidgetFromName(FName("Icon")));
		HerosIndicatorWidgets.Add(widget);
		MY_LOG_NETWORK(TEXT("TEST DU NAME %u "), IsValid(heros));
		switch (heros->Name) {
		case ECharacterName::AERES:
			MY_LOG_NETWORK(TEXT("TEST DU AERES"));
			ImageInGame->SetBrushFromTexture(ImageAeres, true);
			break;
		case ECharacterName::DAVROS:
			ImageInGame->SetBrushFromTexture(ImageDavros, true);
			break;
		case ECharacterName::KIARA:
			MY_LOG_NETWORK(TEXT("TEST DU KIARA"));
			ImageInGame->SetBrushFromTexture(ImageKiara, true);
			break;
		case ECharacterName::NEELF:
			MY_LOG_NETWORK(TEXT("TEST DU NEELF"));
			ImageInGame->SetBrushFromTexture(ImageNeelf, true);
			break;
		case ECharacterName::SHANYL:
			MY_LOG_NETWORK(TEXT("TEST DU SHANYL"));
			ImageInGame->SetBrushFromTexture(ImageShanyl, true);
			break;
		default:
			ImageInGame->SetBrushFromTexture(ImageDavros, true);
			break;

		}
		APlayerController* MyController = GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld());
		APawn* pawn = MyController->GetPawn();
		ABFPCharacterPlayable* playerPawnPerso = Cast<ABFPCharacterPlayable>(pawn);
		if (playerPawnPerso->Name == heros->Name) {
			UUserWidget* widgetfleche = CreateWidget<UUserWidget>(GetWorld(), FlechePositionHerosIndicator);
			widgetfleche->AddToViewport(5);
			widgetfleche->SetVisibility(ESlateVisibility::Hidden);
			FlechePositionHerosIndicatorWidgetArray.Add(widgetfleche);
		}
	}
}

void ABFPHUD::addPositionIndicator(float positionX, float positionY) {
	ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
	UUserWidget* widget = CreateWidget<UUserWidget>(GetWorld(), PositionIndicator);
	widget->SetVisibility(ESlateVisibility::Visible);
	widget->AddToViewport(1);
	ABFPCharacterLieutenant* lieutenantChoisi = GetLieutenantChoisi();
	widget->SetPositionInViewport(FVector2D(positionX, positionY), true);
	TArray<ABFPCharacterLieutenant*> lieutenantInstances = gameState->GetAllLieutenants();
	int32 lieutenantIndex = lieutenantInstances.Find(lieutenantChoisi);
	if (lieutenantIndex != INDEX_NONE) {
		ArrayCiblesLieutenants[lieutenantIndex].Widgets.Add(widget);
		if (ArrayCiblesLieutenants[lieutenantIndex].Positions.Num() > 0) {
			DEBUG_SCREEN(-1, 5.0f, FColor::Green, "Positions.NUM()>0");
			ArrayCiblesLieutenants[lieutenantIndex].Positions.Add(FVector2D(positionX, positionY));
		}
		else {
			DEBUG_SCREEN(-1, 5.0f, FColor::Green, "Positions.NUM()=0");
			ArrayCiblesLieutenants[lieutenantIndex].Positions.Push(FVector2D(0.0f, 0.0f));
			ArrayCiblesLieutenants[lieutenantIndex].Positions.Add(FVector2D(positionX, positionY));
		}

	}
}


void ABFPHUD::AffichageRedArrow(float positionx, float positiony)
{

	RedArrowWidget->SetPositionInViewport(FVector2D(positionx, positiony), true);
	RedArrowWidget->SetVisibility(ESlateVisibility::Visible);
	AnimationRedArrow.Broadcast();
	addPositionIndicator(positionx, positiony);
}

float ABFPHUD::CalculAngleRotationFlecheMap(ABFPCharacter * character, UUserWidget* widget)
{

	FVector VectorForward = character->GetTransform().GetRotation().GetForwardVector();
	FVector2D VectorForward2D = FVector2D(Util::ProjectInXYPlane(VectorForward).X, Util::ProjectInXYPlane(VectorForward).Y);
	VectorForward2D.Normalize();
	//float angle = FMath::Acos(FVector2D::DotProduct(FVector2D(0,1), VectorForward2D))*(180/PI);
	float angle = Util::AngleBetweenVectors2D(FVector2D(0, 1), VectorForward2D);
	return angle;
}

void ABFPHUD::CalculPositionFlecheMap(ABFPCharacterPlayable * character, UUserWidget * widgetfleche, UUserWidget* widgetHeros, FVector2D pos)
{
	FVector2D taillewidgetHeros = widgetHeros->GetDesiredSize();
	FGeometry cachedGeometry = widgetHeros->GetCachedGeometry();
	FVector2D currentSize = cachedGeometry.GetLocalSize();

	//recuperation de l'angle
	FVector VectorForward = character->GetTransform().GetRotation().GetForwardVector();
	FVector2D VectorForward2D = FVector2D(Util::ProjectInXYPlane(VectorForward).X, Util::ProjectInXYPlane(VectorForward).Y);
	VectorForward2D.Normalize();
	//float angle = FMath::Acos(FVector2D::DotProduct(FVector2D(0,1), VectorForward2D))*(180/PI);
	float angle = Util::AngleBetweenVectors2D(FVector2D(1, 0), VectorForward2D);
	MY_LOG_NETWORK(TEXT("Angle %f"), angle);
	FVector CoordonneeRelativeFleche = Util::PolaireToCartesian(angle, 5.0f); // le rayon de 15 est un test
	MY_LOG_NETWORK(TEXT("CoordonneeRelativeFleche : x : %f , y : %f"), CoordonneeRelativeFleche.X, CoordonneeRelativeFleche.Y);

	FVector2D PositionFinaleFleche = FVector2D(pos.X + CoordonneeRelativeFleche.X, pos.Y + CoordonneeRelativeFleche.Y);
	widgetfleche->SetPositionInViewport(PositionFinaleFleche, true);



}



void ABFPHUD::addEvil(ABFPCharacter* character) {
	UUserWidget* widget = CreateWidget<UUserWidget>(GetWorld(), DavrosIndicator);
	widget->SetVisibility(ESlateVisibility::Hidden);
	widget->AddToViewport(1);
	DavrosIndicatorWidgets.Add(widget);

	UUserWidget* widgetfleche = CreateWidget<UUserWidget>(GetWorld(), FlechePositionDavrosIndicator);
	widgetfleche->AddToViewport(5);
	widgetfleche->SetVisibility(ESlateVisibility::Hidden);
	FlechePositionDavrosIndicatorWidgetArray.Add(widgetfleche);
}

void ABFPHUD::Reset() {

	for (auto widget : LieutenantIndicatorWidgets) {
		widget->RemoveFromViewport();
	}

	for (auto cible : ArrayCiblesLieutenants) {
		for (auto widget : cible.Widgets) {
			widget->RemoveFromViewport();
		}
	}

	for (auto widget : DavrosIndicatorWidgets) {
		widget->RemoveFromViewport();
	}

	for (auto widget : HerosIndicatorWidgets) {
		widget->RemoveFromViewport();
	}

	for (auto widget : lieutenantColorMarkWidgets) {
		widget->RemoveFromViewport();
	}

	for (auto widget : lieutenantNombreSbireWidget) {
		widget->RemoveFromViewport();
	}
	for (auto widget : FlechePositionHerosIndicatorWidgetArray) {
		widget->RemoveFromViewport();
	}
	for (auto widget : FlechePositionDavrosIndicatorWidgetArray) {
		widget->RemoveFromViewport();
	}
	FlechePositionHerosIndicatorWidgetArray.Empty();
	FlechePositionDavrosIndicatorWidgetArray.Empty();
	LieutenantIndicatorWidgets.Empty();
	ArrayCiblesLieutenants.Empty();
	DavrosIndicatorWidgets.Empty();
	HerosIndicatorWidgets.Empty();
	lieutenantColorMarkWidgets.Empty();
	lieutenantNombreSbireWidget.Empty();
}

void ABFPHUD::ResetCPs() {
	CPUncapturedWidget.Empty();
}

void ABFPHUD::addLieutenant(ABFPCharacter* character) {
	UUserWidget* widget = CreateWidget<UUserWidget>(GetWorld(), LieutenantIndicator);
	widget->SetVisibility(ESlateVisibility::Hidden);
	widget->AddToViewport(12);
	MY_LOG_NETWORK(TEXT("ABFPHUD addLieutenant"));
	LieutenantIndicatorWidgets.Add(widget);
	FCiblesLieutenants temp;
	ArrayCiblesLieutenants.Add(temp);
	ArrayCiblesLieutenants.Last().Positions.Push(FVector2D(0.0f, 0.0f));

	/*UUserWidget* widgetfleche = CreateWidget<UUserWidget>(GetWorld(), FlechePositionDavrosIndicator);
	widgetfleche->AddToViewport(5);
	widgetfleche->SetVisibility(ESlateVisibility::Hidden);
	FlechePositionDavrosIndicatorWidgetArray.Add(widgetfleche);*/

}

void ABFPHUD::addCP(ACheckpointTriggerSphere* CP) {
	UUserWidget* widget = CreateWidget<UUserWidget>(GetWorld(), CPUncaptured);
	DEBUG_SCREEN(-1, 5.0f, FColor::Green, "ADDCP");
	//if (CP->getCheckpointAffiliation() == Affiliation::EVIL) {
	//   DEBUG_SCREEN(-1, 5.0f, FColor::Green, "EVIL !!!");
	//   UImage* CPImage = dynamic_cast<UImage*>(widget->GetWidgetFromName(FName("CP")));
	//   CPImage->SetBrushFromTexture(PDCEvil,true);
	//}
	widget->SetVisibility(ESlateVisibility::Hidden);
	FVector worldLocation = CP->GetActorLocation();
	FVector2D pos = fromWorldToMap(worldLocation);
	widget->AddToViewport(1);
	widget->SetPositionInViewport(pos, true);
	MY_LOG_NETWORK(TEXT("ABFPHUD addCP"));
	CPUncapturedWidget.Add(widget);
}

void ABFPHUD::RefreshPositionCiblesLieutenant(ABFPCharacterLieutenant* lieutenant) {
	//recuperation des targets Points
	TArray<AManeuverTargetPoint*> TargetPoints;
	ASquadDescriptor* squad = lieutenant->GetSquad();
	if (IsValid(squad)) {
		AManeuver* man = squad->GetManeuver();
		if (IsValid(man)) {
			TargetPoints = man->GetTargetPoints();
			//recuperation de l'index du lieutenant choisi
			TArray<ABFPCharacterLieutenant*> lieutenantInstances = GetWorld()->GetGameState<ABFP_GameState>()->GetAllLieutenants();
			int32 lieutenantIndex = lieutenantInstances.Find(lieutenant);
			if (lieutenantIndex != INDEX_NONE) {
				//creation des widgets pour chaque target point
				for (int i = 0; i != TargetPoints.Num(); i++) {
					UUserWidget* widget = CreateWidget<UUserWidget>(GetWorld(), PositionIndicator);
					widget->SetVisibility(ESlateVisibility::Hidden);
					widget->AddToViewport(3);
					FVector2D positionTargetInMap = fromWorldToMap(TargetPoints[i]->GetActorTransform().GetLocation());
					widget->SetPositionInViewport(positionTargetInMap, true);
					ArrayCiblesLieutenants[lieutenantIndex].Widgets.Add(widget);
					ArrayCiblesLieutenants.Last().Positions.Push(positionTargetInMap);
				}
			}
		}
		else {
			MY_LOG_NETWORK(TEXT("ABFPHUD RefreshPositionCiblesLieutenant man nulle"));
		}
	}
	else {
		MY_LOG_NETWORK(TEXT("ABFPHUD RefreshPositionCiblesLieutenant squad nulle"));
	}
	MY_LOG_NETWORK(TEXT("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"));
	MY_LOG_NETWORK(TEXT("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB"));
}


//void ABFPHUD::StayInHUD() {
//	AffichageMapInteractive();
//}

void ABFPHUD::ShowStrategyWheel()
{
	float positionx, positiony;
	APlayerController* MyController = GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld());
	MyController->GetMousePosition(positionx, positiony);

	ABFPCharacterLieutenant* lieutenantChoisi = GetLieutenantChoisi();
	TArray<ABFPCharacterLieutenant*> lieutenantInstances = GetWorld()->GetGameState<ABFP_GameState>()->GetAllLieutenants();
	int32 lieutenantIndex = lieutenantInstances.Find(lieutenantChoisi);
	if (lieutenantIndex != INDEX_NONE) {
		FVector2D pos2D = fromWorldToMap(lieutenantChoisi->GetActorLocation());

		StrategyWheelWidget->SetPositionInViewport(pos2D, true);
		StrategyWheelWidget->SetVisibility(ESlateVisibility::Visible);
		StrategyButtonsWidget->SetVisibility(ESlateVisibility::Hidden);

		MY_LOG_NETWORK(TEXT("Draw des cibles - taille des widgets %u"), ArrayCiblesLieutenants[lieutenantIndex].Widgets.Num());

		for (int i = 0; i != ArrayCiblesLieutenants[lieutenantIndex].Widgets.Num(); i++) {
			if (IsValid(ArrayCiblesLieutenants[lieutenantIndex].Widgets[i])) {
				ArrayCiblesLieutenants[lieutenantIndex].Widgets[i]->SetVisibility(ESlateVisibility::Visible);
			}
		}
	}
}

void ABFPHUD::HideStrategyWheel()
{
	//FVector2D pos = fromWorldToMap(worldLocation);
	StrategyWheelWidget->SetVisibility(ESlateVisibility::Hidden);
}




void ABFPHUD::ShowStrategyButtons()
{
	float positionx, positiony;
	APlayerController* MyController = GetWorld()->GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld());
	MyController->GetMousePosition(positionx, positiony);

	//FVector2D pos = fromWorldToMap(worldLocation);
	//StrategyButtonsWidget->SetPositionInViewport(FVector2D(positionx+50, positiony), true);
	StrategyButtonsWidget->SetVisibility(ESlateVisibility::Visible);
	StrategyWheelWidget->SetVisibility(ESlateVisibility::Hidden);


}

void ABFPHUD::HideStrategyButtons()
{
	//FVector2D pos = fromWorldToMap(worldLocation);
	StrategyButtonsWidget->SetVisibility(ESlateVisibility::Hidden);
}



void ABFPHUD::RemoveWidgetFromHUD(class ABFPCharacter* character) {
	MY_LOG_NETWORK(TEXT("ABFPHUD::RemoveWidgetFromHUD begin"));
	ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();

	MY_LOG_NETWORK(TEXT("ABFPHUD::RemoveWidgetFromHUD test Lieutenant"));
	if (IsValid(Cast<ABFPCharacterLieutenant>(character))) {
		MY_LOG_NETWORK(TEXT("ABFPHUD::RemoveWidgetFromHUD est lieutenant"));
		// On est lieutenant
		TArray<ABFPCharacterLieutenant*> lieutenantInstances = gameState->GetAllLieutenants();
		int32 lieutenantIndex = lieutenantInstances.Find(Cast<ABFPCharacterLieutenant>(character));
		//if (lieutenantIndex >= 0) {
		   //auto widget = LieutenantIndicatorWidgets[lieutenantIndex];
		   //LieutenantIndicatorWidgets.Remove(widget);
		//}
	}

	MY_LOG_NETWORK(TEXT("ABFPHUD::RemoveWidgetFromHUD test playable"));
	if (IsValid(Cast<ABFPCharacterPlayable>(character))) {
		MY_LOG_NETWORK(TEXT("ABFPHUD::RemoveWidgetFromHUD is playable"));
		auto characterPlayable = Cast<ABFPCharacterPlayable>(character);
		MY_LOG_NETWORK(TEXT("ABFPHUD::RemoveWidgetFromHUD test Evil"));
		if (characterPlayable->getCharacterAffiliation() == Affiliation::EVIL) {
			MY_LOG_NETWORK(TEXT("ABFPHUD::RemoveWidgetFromHUD is evil"));
			auto widget = DavrosIndicatorWidgets[0];
			DavrosIndicatorWidgets.Remove(widget);
		}
		else if (characterPlayable->getCharacterAffiliation() == Affiliation::GOOD) {
			MY_LOG_NETWORK(TEXT("ABFPHUD::RemoveWidgetFromHUD is good"));
			TArray<ABFPCharacterPlayable*> herosInstances = gameState->GetAllJoueursAllies();
			MY_LOG_NETWORK(TEXT("ABFPHUD::RemoveWidgetFromHUD herosInstances count : %u"), herosInstances.Num());
			int32 herosIndex = herosInstances.Find(characterPlayable);
			//if (herosIndex >= 0) {
			if (herosIndex != INDEX_NONE) {
				MY_LOG_NETWORK(TEXT("ABFPHUD::RemoveWidgetFromHUD herosIndex : %u"), herosIndex);
				MY_LOG_NETWORK(TEXT("ABFPHUD::RemoveWidgetFromHUD HerosIndicatorWidget count : %u"), HerosIndicatorWidgets.Num());
				auto widget = HerosIndicatorWidgets[herosIndex];
				MY_LOG_NETWORK(TEXT("ABFPHUD::RemoveWidgetFromHUD widget : %u"), IsValid(widget));
				HerosIndicatorWidgets.Remove(widget);
				//}
			}
		}
	}
}

void ABFPHUD::HidePositionIndicators()
{
	ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
	//ABFPCharacterLieutenant* lieutenantChoisi = GetLieutenantChoisi();
	TArray<ABFPCharacterLieutenant*> lieutenantInstances = gameState->GetAllLieutenants();
	//int32 lieutenantIndex = lieutenantInstances.Find(lieutenantChoisi);
	for (int j = 0; j != ArrayCiblesLieutenants.Num(); j++) {
		for (int i = 0; i != ArrayCiblesLieutenants[j].Widgets.Num(); i++) {
			if (IsValid(ArrayCiblesLieutenants[j].Widgets[i])) {
				ArrayCiblesLieutenants[j].Widgets[i]->SetVisibility(ESlateVisibility::Hidden);
			}
		}
	}

}

void ABFPHUD::DeleteAllPositionIndicator()
{
	ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
	TArray<ABFPCharacterLieutenant*> lieutenantInstances = gameState->GetAllLieutenants();
	int32 lieutenantIndex = lieutenantInstances.Find(GetLieutenantChoisi());
	//On vide les widgets
	if (lieutenantIndex != INDEX_NONE) {
		ArrayCiblesLieutenants[lieutenantIndex].Widgets.Empty();
		//On vide les positions (sauf le premier)
		ArrayCiblesLieutenants[lieutenantIndex].Positions.Empty();
		ArrayCiblesLieutenants.Last().Positions.Push(FVector2D(0.0f, 0.0f));
	}
}


void ABFPHUD::DeleteNextPositionIndicator(ABFPCharacterLieutenant * lieutenant, FTransform targetPoint)
{
	ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
	if (!Stopped) {
		TArray<ABFPCharacterLieutenant*> lieutenantInstances = gameState->GetAllLieutenants();
		int32 lieutenantIndex = lieutenantInstances.Find(lieutenant);
		if (lieutenantIndex != INDEX_NONE) {
			if (ArrayCiblesLieutenants[lieutenantIndex].Widgets.Num() > 1 && IsValid(ArrayCiblesLieutenants[lieutenantIndex].Widgets[0])) {

				UUserWidget* widget = ArrayCiblesLieutenants[lieutenantIndex].Widgets[0];
				widget->SetVisibility(ESlateVisibility::Hidden);
				ArrayCiblesLieutenants[lieutenantIndex].Widgets.Remove(widget);
				//ArrayPositionIndicatorWidgets[lieutenantIndex].PositionIndicatorWidgets->HeapPop();
				FVector2D temp = ArrayCiblesLieutenants[lieutenantIndex].Positions[1];
				ArrayCiblesLieutenants[lieutenantIndex].Positions.Remove(temp);
			}
		}
	}
	else {
		Stopped = false;
	}

}

void ABFPHUD::InitializeTimelineProgressBar()
{
	if (PDCCurve)
	{
		FOnTimelineFloat TimelineCallback;
		FOnTimelineEventStatic TimelineFinishedCallback;
		TimelineCallback.BindUFunction(this, FName("SetPDCValue"));
		TimelineFinishedCallback.BindUFunction(this, FName("SetPDCState"));
		MyTimeline = NewObject<UTimelineComponent>(this, FName("ProgressBarPDC_Animation"));
		MyTimeline->AddInterpFloat(PDCCurve, TimelineCallback);
		MyTimeline->SetTimelineFinishedFunc(TimelineFinishedCallback);
		MyTimeline->RegisterComponent();
	}

}

void ABFPHUD::SetPDCValue()
{
	MY_LOG_NETWORK(TEXT("On est dans le PDC Value"));
	TimelineValue = MyTimeline->GetPlaybackPosition();
	MY_LOG_NETWORK(TEXT("PDCValue du PDC %f"), PDCValue);
	
	CurveFloatValue = PreviousValuePDC + (PDCValue - PreviousValuePDC) * PDCCurve->GetFloatValue(TimelineValue);
	MY_LOG_NETWORK(TEXT("CurveFloatValue %f"), CurveFloatValue);
	PDCPercentage = FMath::Clamp(CurveFloatValue, 0.0f, 1.0f);

	MY_LOG_NETWORK(TEXT("Pourcentage du PDC %f"), PDCPercentage);
	ProgressBar->SetPercent(PDCPercentage);
	//Cast<ABFPCharacter>(this->GetOwner())->UpdateHealthBar(HealthPercentage);
}

void ABFPHUD::SetPDCState()
{
	if (changeColorAfterNeutralAffiliation) {

		APlayerController* MyController = GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld());
		APawn* pawn = MyController->GetPawn();
		ABFPCharacterPlayable* playerPawn = Cast<ABFPCharacterPlayable>(pawn);
		if (playerPawn->getCharacterAffiliation() == Affiliation::GOOD) {
			ProgressBar->SetFillColorAndOpacity(FLinearColor::Blue);
			ProgressBar_fond->SetFillColorAndOpacity(FLinearColor::Gray);
		}
		if (playerPawn->getCharacterAffiliation() == Affiliation::EVIL) {
			ProgressBar->SetFillColorAndOpacity(FLinearColor::Red);
			ProgressBar_fond->SetFillColorAndOpacity(FLinearColor::Gray);
		}
	}
	else if (UpdateProgressBarTo0) {
		ProgressBar->SetPercent(0.0f);
		PDCPercentage = 0.0f;
	}
	PDCValue = 0.0;
}

void ABFPHUD::UpdateProgressBarPDC(float PDCValueChange, Affiliation CheckpointAffiliation)
{
	MY_LOG_NETWORK(TEXT("Dans le UpdateProgressBarPDC, captureValue : %f"), PDCValueChange); 
	APlayerController* MyController = GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld());
	APawn* pawn = MyController->GetPawn();
	ABFPCharacterPlayable* playerPawn = Cast<ABFPCharacterPlayable>(pawn);
	changeColorAfterNeutralAffiliation = false;
	UpdateProgressBarTo0 = false;
	if (playerPawn->getCharacterAffiliation() != CheckpointAffiliation) {
		switch (CheckpointAffiliation) {

		case Affiliation::EVIL:
			DEBUG_SCREEN(-1, 5.0f, FColor::Yellow, "Je suis dans le update mechant ");
			ProgressBar_fond->SetFillColorAndOpacity(FLinearColor::Red);
			ProgressBar->SetFillColorAndOpacity(FLinearColor::Gray);
			break;
		case Affiliation::GOOD:
			DEBUG_SCREEN(-1, 5.0f, FColor::Yellow, "Je suis dans le update gentil");
			ProgressBar_fond->SetFillColorAndOpacity(FLinearColor::Blue);
			ProgressBar->SetFillColorAndOpacity(FLinearColor::Gray);
			break;
		default:
			ProgressBar_fond->SetFillColorAndOpacity(FLinearColor::Gray);
			if (playerPawn->getCharacterAffiliation() == Affiliation::GOOD) {
				ProgressBar->SetFillColorAndOpacity(FLinearColor::Blue);
			}
			else
			{
				ProgressBar->SetFillColorAndOpacity(FLinearColor::Red);
			}

		}

	}


	PreviousValuePDC = PDCPercentage;
	PDCValue = FMath::Clamp((PDCValueChange / 100), 0.0f, 1.0f);
	MY_LOG_NETWORK(TEXT("Dans le UpdateProgressBarPDC, PDCValue(percentage) : %f"), PDCValue);
	if (PDCValue >= 1.0f) {
		if (CheckpointAffiliation == Affiliation::NEUTRAL) {
			changeColorAfterNeutralAffiliation = true;
			DEBUG_SCREEN(-1, 5.0f, FColor::Yellow, "JE CHANGE LE BOOL A TRUE LA  ");
		}
		else {
			
			UpdateProgressBarTo0 = true;
			DEBUG_SCREEN(-1, 5.0f, FColor::Yellow, "JE CHANGE LE BOOL du pourcentage  A TRUE LA  ");

		}
	}
	else if (IsValid(MyTimeline)) {
			MyTimeline->PlayFromStart();
		 }
	
	if (PDCPercentage > 0.94f) {
		ProgressBar->SetPercent(1.0f);
	}
	

}

void ABFPHUD::ChangeColorProgressBarPDC(FLinearColor Color)
{
	ProgressBar->SetFillColorAndOpacity(Color);
}



void ABFPHUD::AffichageProgressBarPDC(Affiliation CheckpointAffiliation)
{
		ProgressBar = dynamic_cast<UProgressBar*>(ProgressBarPDCWidget->GetWidgetFromName(FName("ProgressBar_PDC")));
		ProgressBar_fond = dynamic_cast<UProgressBar*>(ProgressBarPDCWidget->GetWidgetFromName(FName("ProgressBar_fond")));
	
		if (!ProgressBarIsShow) {
			APlayerController* MyController = GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld());
			APawn* pawn = MyController->GetPawn();
			ABFPCharacterPlayable* playerPawn = Cast<ABFPCharacterPlayable>(pawn);
			//le pdc t'appartient
			if (playerPawn->getCharacterAffiliation() == CheckpointAffiliation)
			{
				ProgressBar_fond->SetFillColorAndOpacity(FLinearColor::Gray);
				
				if (playerPawn->getCharacterAffiliation() == Affiliation::GOOD) {
					DEBUG_SCREEN(-1, 5.0f, FColor::Red, "Je suis le gentil sur un point de controle gentil");
					ProgressBar->SetFillColorAndOpacity(FLinearColor::Blue);
					ProgressBar->SetPercent(1.0f);
				}
				else {
					ProgressBar->SetFillColorAndOpacity(FLinearColor::Red);
					ProgressBar->SetPercent(1.0f);
					DEBUG_SCREEN(-1, 5.0f, FColor::Red, "Je suis le mechant sur un point de controle mechant");
				}
					
			}
			//le pdc est neutre
			else if (CheckpointAffiliation == Affiliation::NEUTRAL) 
			{
				
				if (playerPawn->getCharacterAffiliation() == Affiliation::GOOD) {
					DEBUG_SCREEN(-1, 5.0f, FColor::Red, "Je suis le gentil sur un point de controle neutre");
					ProgressBar->SetFillColorAndOpacity(FLinearColor::Blue);
					
				}
				else {
					DEBUG_SCREEN(-1, 5.0f, FColor::Red, "Je suis le mechant sur un point de controle neutre");
					ProgressBar->SetFillColorAndOpacity(FLinearColor::Red);
				}
			}
			//le pdc est a l'ennemi
			else {
				ProgressBar->SetFillColorAndOpacity(FLinearColor::Gray);
				if (playerPawn->getCharacterAffiliation() == Affiliation::GOOD) {
					DEBUG_SCREEN(-1, 5.0f, FColor::Red, "Je suis le gentil sur un point de controle mechant");
					ProgressBar_fond->SetFillColorAndOpacity(FLinearColor::Red);

				}
				else {
					DEBUG_SCREEN(-1, 5.0f, FColor::Red, "Je suis le mechant sur un point de controle gentil");
					ProgressBar_fond->SetFillColorAndOpacity(FLinearColor::Blue);
				}
			}

		}
		else {
			ProgressBar->SetPercent(0.0f);
			PDCPercentage = 0.0f;
		}
		ProgressBarPDCWidget->SetVisibility(ESlateVisibility::Visible);
		ApparitionProgressBar.Broadcast();
		ProgressBarIsShow = true;

	}


void ABFPHUD::DisparitionProgressBarPDC()
{
	if (ProgressBarIsShow) {
		APlayerController* MyController = GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld());
		DEBUG_SCREEN(-1, 5.0f, FColor::Red, "On va faire disparaitre");
		DisparitionProgressBar.Broadcast();
		ProgressBarIsShow = false;
	}
}

void ABFPHUD::PlayRedFlash() {
	Cast<URedFlashWidget>(RedFlashWidget)->Activate();
}