// Fill out your copyright notice in the Description page of Project Settings.

#include "BFP_PlayerController.h"
#include "Network/GameInfoInstance.h"
#include "Network/Gameplay/GameplayGM.h"
#include "BFP_PlayerState.h"
#include "BFPHUD.h"
#include "Message.h"
#include "SquadManager.h"
#include "BFP_GameState.h"
#include "UObject/ConstructorHelpers.h"
#include "UI/ExitWidget.h"
#include "CaptureComponent.h"
#include "SquadDescriptor.h"
#include "SquadManager.h"
#include "Message.h"
#include "Spell.h"


void ABFP_PlayerController::SetEndGameStatus_Implementation(bool victory)
{
   Cast<ABFPHUD>(GetHUD())->SetEndGameStatus(victory);
   if (victory) {
      if (IsValid(SoundActorInstance))
         SoundActorInstance->SoundStateFlag_Broadcast(SoundActorInstance->SoundStateFlag_Victory, ESoundState::SS_Victory);
      if (IsValid(HttpActorInstance))
         HttpActorInstance->UpdateWinsRequest();
   }
   else {
      if (IsValid(SoundActorInstance))
         SoundActorInstance->SoundStateFlag_Broadcast(SoundActorInstance->SoundStateFlag_Defeat, ESoundState::SS_Defeat);
      if (IsValid(HttpActorInstance))
         HttpActorInstance->UpdateLossRequest();
   }
}

void ABFP_PlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
   Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

ABFP_PlayerController::ABFP_PlayerController(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
   CaptureSystem = CreateDefaultSubobject<UCaptureComponent>(TEXT("CaptureSystem"));
   PrimaryActorTick.bCanEverTick = true;
}

void ABFP_PlayerController::Tick(float DeltaTime) {
   Super::Tick(DeltaTime);
   CheckMusicTransitions();
}

void ABFP_PlayerController::PostInitializeComponents() {
   Super::PostInitializeComponents();

   gameInstanceRef = GetGameInstance<UGameInfoInstance>();

   CaptureSystem->ShowEvent.AddUFunction(this, "ChangeCaptureVisibility");
   CaptureSystem->ChangeValueEvent.AddUFunction(this, "UpdateCaptureValue");
   CaptureSystem->ChangeAffiliationEvent.AddUFunction(this, "UpdateCaptureValue");
}

void ABFP_PlayerController::BeginPlay()
{
   Super::BeginPlay();
#ifdef UE_BUILD_DEVELOPMENT
   ConsoleCommand("DisableAllScreenMessages");
#endif
   PlayerCameraManager->ViewPitchMax = MaxPitch;
   PlayerCameraManager->ViewPitchMin = MinPitch;
   SpawnSoundActor();
   SpawnHttpActor();
}

void ABFP_PlayerController::EndPlay(const EEndPlayReason::Type EndPlayReason) {
   Super::EndPlay(EndPlayReason);
   if (EndPlayReason != EEndPlayReason::LevelTransition) {
      this->DestroySession();
   }
}

void ABFP_PlayerController::InitPawn() {
   AGameplayGM* gameMode = GetWorld()->GetAuthGameMode<AGameplayGM>();
   FPlayerInfo playerSettings = GetPlayerState<ABFP_PlayerState>()->S_PlayerInfo;
   TSubclassOf<ACharacter> characterClass = playerSettings.MyPlayerCharacter;
   MY_LOG_NETWORK(TEXT("InitPawn Character valide = %u"), IsValid(playerSettings.MyPlayerCharacter))
      gameMode->RespawnPlayer(this, characterClass);
}

void ABFP_PlayerController::UpdateHUDFromGameStateModification() {
   ABFPHUD* hud = Cast<ABFPHUD>(GetHUD());
   // Constructions des widgets globaux
   hud->InitializeWidgets();
   hud->InitializeTimelineProgressBar();
   // Creation des widgets de BFPCharacters
   // Reset du HUD
   hud->Reset();
   // Recuperation du GameState
   ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
   // Heros
   TArray<class ABFPCharacterPlayable*> Heros = gameState->GetAllJoueursAllies();
   MY_LOG_NETWORK(TEXT("ABFP_PlayerController UpdateHUDFromGameStateModification - Num Heros %d"), Heros.Num());
   for (auto heros : Heros) {
      hud->addHeros(heros);
   }
   // Davros
   ABFPCharacterPlayable* Davros = gameState->GetDavros();
   MY_LOG_NETWORK(TEXT("ABFP_PlayerController UpdateHUDFromGameStateModification - Num Davros %d"), IsValid(Davros));
   hud->addEvil(Davros);
   // Lieutenants
   TArray<class ABFPCharacterLieutenant*> Lieutenants = gameState->GetAllLieutenants();
   MY_LOG_NETWORK(TEXT("ABFP_PlayerController InitPawnFinished_Implementation - Num Loeutenants %d"), Lieutenants.Num());
   DEBUG_SCREEN(-1, 10.f, FColor::Red, TEXT("MAJ LIEUTENANTS"));
   for (auto lieutenant : Lieutenants) {
      hud->addLieutenant(lieutenant);
      MY_LOG_NETWORK(TEXT("ABFP_PlayerController UpdateHUDFromGameStateModification Lieutenant = %u"), IsValid(lieutenant));
   }

   // CP
   TArray<class ACheckpointTriggerSphere*> CPs = gameState->GetAllCPs();
   MY_LOG_NETWORK(TEXT("ABFP_PlayerController InitPawnFinished_Implementation - Num CPs %d"), CPs.Num());
   for (auto cp : CPs) {
      hud->addCP(cp);
   }
   // Affichage possible car tout est bien initialise
   Cast<ABFPHUD>(GetHUD())->initSpawnFinished = true;
}

void ABFP_PlayerController::UpdateHUDCheckpointsFromGameStateModification() {
   ABFPHUD* hud = Cast<ABFPHUD>(GetHUD());
   hud->InitializeWidgets();
   hud->ResetCPs();
   ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
   TArray<class ACheckpointTriggerSphere*> CPs = gameState->GetAllCPs();
   MY_LOG_NETWORK(TEXT("ABFP_PlayerController UpdateHUDCheckpointsFromGameStateModification - Num CPs %d"), CPs.Num());
   for (auto cp : CPs) {
      hud->addCP(cp);
   }
}

void ABFP_PlayerController::DestroySession_Implementation() {
   gameInstanceRef->DestroySession(this);
}

void ABFP_PlayerController::cleanWidget_Implementation() {
   gameInstanceRef->cleanWidget();
}

void ABFP_PlayerController::showExitMenuWidget(const bool bShowMouseCursor, const EInputModeWidget& inputMode) {
   if (!ExitWD.IsValid()) {
      ExitWD = Cast<UExitWidget>(gameInstanceRef->createWidget(GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld()), gameInstanceRef->ExitWD_Class));
   }
   UMenuWidget* widgetCast = Cast<UMenuWidget>(ExitWD);
   gameInstanceRef->showWidget(widgetCast, gameInstanceRef->ExitWD_Class, this, bShowMouseCursor, inputMode);
}

void ABFP_PlayerController::ExitGameMenu_Implementation() {
   MY_LOG_NETWORK(TEXT("ABFP_PlayerController ExitGameMenu"));
   showExitMenuWidget(true, EInputModeWidget::UI_Only);
}

void ABFP_PlayerController::AffichageProgressBarRPC(Affiliation CheckpointAffiliation) {
   ABFPHUD* hud = Cast<ABFPHUD>(GetHUD());
   if (!hud->ProgressBarIsShow) {
      hud->AffichageProgressBarPDC(CheckpointAffiliation);
   }
}

void ABFP_PlayerController::DisparitionProgressBarRPC() {
   ABFPHUD* hud = Cast<ABFPHUD>(GetHUD());
   if (hud->ProgressBarIsShow) {
      hud->DisparitionProgressBarPDC();
   }
}

void ABFP_PlayerController::UpdateProgressBarRPC(float PDCValueChange, Affiliation CheckpointAffiliation) {
   ABFPHUD* hud = Cast<ABFPHUD>(GetHUD());
   if (hud->ProgressBarIsShow) {
      MY_LOG_NETWORK(TEXT("Dans le UpdateProgressBarRPC, captureValue : %f"), PDCValueChange);
      hud->UpdateProgressBarPDC(PDCValueChange, CheckpointAffiliation);
   }
}

void ABFP_PlayerController::ChangeColorProgressBarRPC_Implementation(FLinearColor Color) {
   ABFPHUD* hud = Cast<ABFPHUD>(GetHUD());
   if (hud->ProgressBarIsShow) {
      hud->ChangeColorProgressBarPDC(Color);
   }
}

void ABFP_PlayerController::AddTargetPointRPC_Implementation(FTransform point, ABFPCharacterLieutenant* lieutenant)
{
   if (IsValid(GetWorld())) {
      AGameplayGM* gameMode = GetWorld()->GetAuthGameMode<AGameplayGM>();
      FActorSpawnParameters params;
      params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
      AManeuverTargetPoint* targetPoint = gameMode->SpawnActor<AManeuverTargetPoint>(point, nullptr, params);
      if (IsValid(targetPoint)) {
         lieutenant->GetSquad()->GetManeuver()->AddTargetPoint(targetPoint);
         UMessage* message = NewObject<UMessage>();
         message->SetParams(MessageType::ADD_TARGET_POINT, Cast<ASquadMemberController>(lieutenant->GetController()));
         lieutenant->GetSquad()->GetSquadManager()->ReceiveMessage(message);
      }
   }
}

bool ABFP_PlayerController::AddTargetPointRPC_Validate(FTransform point, ABFPCharacterLieutenant* lieutenant) {
   return true;
}

void ABFP_PlayerController::StopMovementRPC_Implementation(FTransform point, ABFPCharacterLieutenant* lieutenant) {
   UMessage* message = NewObject<UMessage>();
   message->SetParams(MessageType::STOP, Cast<ASquadMemberController>(lieutenant->GetController()));
   lieutenant->GetSquad()->GetSquadManager()->ReceiveMessage(message);
   MY_LOG_NETWORK(TEXT("ABFP_PlayerController StopMovementRPC_Implementation STOP Message Send"));
}

bool ABFP_PlayerController::StopMovementRPC_Validate(FTransform point, ABFPCharacterLieutenant* lieutenant) {
   return true;
}

void ABFP_PlayerController::SetStrategieRPC_Implementation(ManeuverType maneuverType, ABFPCharacterLieutenant* lieutenant) {
   MY_LOG_NETWORK(TEXT("Envoie d'un ordre de changement de strategy a une squad = %d"), static_cast<int>(maneuverType));
   lieutenant->GetSquad()->GetSquadManager()->SetManeuver(maneuverType);
}

bool ABFP_PlayerController::SetStrategieRPC_Validate(ManeuverType maneuverType, ABFPCharacterLieutenant* lieutenant) {
   return true;
}

void ABFP_PlayerController::ChangeCaptureVisibility() {
   if (CaptureSystem->bShow) {
      AffichageProgressBarRPC(CaptureSystem->checkpointAffiliation);
   }
   else {
      DisparitionProgressBarRPC();
   }
}

void ABFP_PlayerController::UpdateCaptureValue() {
   MY_LOG_NETWORK(TEXT("Dans le UpdateCaptureValue, captureValue : %f"), CaptureSystem->captureValue);
   UpdateProgressBarRPC(CaptureSystem->captureValue, CaptureSystem->checkpointAffiliation);
}

void ABFP_PlayerController::Spell1RPC_Implementation(FTransform posLanceur, ECharacterName Name) {
   //if (GetWorld() != nullptr) {
   //   AGameplayGM* gameMode = GetWorld()->GetAuthGameMode<AGameplayGM>();
   //   FActorSpawnParameters params;
   //   params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
   //   
   //   DEBUG_SCREEN(-1, 5.f, FColor::Yellow, TEXT("SORT"));

   //   if (Name == ECharacterName::DAVROS) {
   //      DEBUG_SCREEN(-1, 5.f, FColor::Yellow, TEXT("DAVROS"));
   //      posLanceur.SetLocation(posLanceur.GetLocation() + FVector(0, 0, 2500));
   //      ASpell* spell = gameMode->SpawnActor<ASpell>(posLanceur , meteoriteSpell, params);
   //      spell->SetOwner(this->GetPawn());
   //      FVector dep_0 = this->GetPawn()->GetTransform().GetRotation().GetForwardVector();
   //      FVector dep = FVector(2000.0f * dep_0.X, 2000.0f * dep_0.Y, 1000.0f * dep_0.Z);
   //      //spell->SetInitVector(dep);
   //      auto meteor = spell->GetComponentsByTag(UStaticMeshComponent::StaticClass(), "Meteor2");
   //      Cast<UStaticMeshComponent>(meteor[0])->AddImpulse(dep + FVector(0, 0, -1000), NAME_None, true);
   //   }
   //   if (Name == ECharacterName::SHANYL) {
   //      DEBUG_SCREEN(-1, 5.f, FColor::Yellow, TEXT("SHANYL"));
   //      ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
   //      FVector dep_0 = this->GetPawn()->GetTransform().GetRotation().GetForwardVector();
   //      FVector dep = FVector(1000.0f * dep_0.X, 1000.0f * dep_0.Y, 1000.0f * dep_0.Z);
   //      FVector tempo = dep + posLanceur.GetLocation();
   //      tempo.Z = gameState->landscapeHeights.getHeight(tempo.X, tempo.Y);
   //      ASpell* spell = gameMode->SpawnActor<ASpell>(FTransform(tempo) , tourbillonFlammesSpell, params);
   //   }
   //   if (Name == ECharacterName::AERES) {
   //      DEBUG_SCREEN(-1, 5.f, FColor::Yellow, TEXT("AERES"));
   //      ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
   //      FVector tempo = this->GetPawn()->GetTransform().GetLocation();
   //      ASpell* spell = gameMode->SpawnActor<ASpell>(FTransform(tempo), healingSpell, params);
   //   }
   //   if (Name == ECharacterName::NEELF) {
   //      DEBUG_SCREEN(-1, 5.f, FColor::Yellow, TEXT("NEELF"));
   //      ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
   //      FVector tempo = this->GetPawn()->GetTransform().GetLocation();
   //      ASpell* spell = gameMode->SpawnActor<ASpell>(FTransform(tempo), explosionSpell, params);
   //   }
   //}
}

bool ABFP_PlayerController::Spell1RPC_Validate(FTransform posLanceur, ECharacterName Name) {
   return true;
}

void ABFP_PlayerController::SpawnSoundActor() {
   if (GetWorld()->IsClient()) {
      TWeakObjectPtr<ABFPCharacterPlayable> pion = Cast<ABFPCharacterPlayable>(GetPawn());
      if (pion.IsValid()) {
         SoundActor = pion->SoundActor;
         SoundActorInstance = GetWorld()->SpawnActor<ASoundActorPlayable>(SoundActor);
      }
   }
}

void ABFP_PlayerController::SpawnHttpActor()
{
   if (GetWorld()->IsClient()) {
      TWeakObjectPtr<ABFPCharacterPlayable> pion = Cast<ABFPCharacterPlayable>(GetPawn());
      if (pion.IsValid()) {
         HttpActor = pion->HttpActor;
         HttpActorInstance = GetWorld()->SpawnActor<AHttpBackEndActor>(HttpActor);
      }
   }
}

void ABFP_PlayerController::CheckMusicTransitions() {
   if (!GetWorld()->IsServer()) { // Si on est sur le client
      ABFP_GameState* gs = GetWorld()->GetGameState<ABFP_GameState>();
      if (gs == nullptr) return;
      TArray<ABFPCharacter*> opposants;
      ABFPCharacter* pawn = Cast<ABFPCharacter>(GetPawn());
      if (pawn->getCharacterAffiliation() == Affiliation::EVIL) {
         TArray<ABFPCharacterPlayable*> tmp = gs->GetAllJoueursAllies();
         for (int i = 0; i < tmp.Num(); i++)
            opposants.Add(tmp[i]);
      }
      else {
         opposants = gs->GetAllMechant();
      }

      // Si on est en exploration
      if (musicState == MusicState::EXPLORATION) {
         // On regarde si il y a des opposants assez proches
         TArray<ABFPCharacter*> proches = gs->GetAllInDistanceFrom(opposants, GetPawn()->GetActorTransform(), distanceMusiqueCombat);
         if (proches.Num() >= 2) {
            SoundActorInstance->SoundStateFlag_Broadcast(SoundActorInstance->SoundStateFlag_Combat, ESoundState::SS_Combat);
            // RAISE FLAG MUSIQUE COMBAT
            musicState = MusicState::COMBAT;
         }

         // Si on est en combat
      }
      else {
         // On regarde si il n'y a pas d'opposants assez proches
         TArray<ABFPCharacter*> proches = gs->GetAllInDistanceFrom(opposants, GetPawn()->GetActorTransform(), distanceMusiqueExploration);
         if (proches.Num() == 0) {
            // RAISE FLAG MUSIQUE EXPLORATION
            SoundActorInstance->SoundStateFlag_Broadcast(SoundActorInstance->SoundStateFlag_Exploration, ESoundState::SS_Exploration);
            musicState = MusicState::EXPLORATION;
         }
      }
   }


}
