// Fill out your copyright notice in the Description page of Project Settings.

#include "FormationRectangle.h"
#include <exception>

void UFormationRectangle::ComputeAllPositions(int nbSbires, int nbLieutenants) {
    transformsRelativesSbires.Empty();
    transformsRelativesLieutenants.Empty();

    // Les lieutenants sont au centre
    // Je ne gere pas le multi-lieutenant pour le moment
    for (int i = 0; i < nbLieutenants; ++i) {
        FVector pos{ 0.0f, 0.0f, 0.0f };
        FQuat quat = FQuat::Identity;
        FVector scale{ 0.0f, 0.0f, 0.0f };
        FTransform t{ quat, pos, scale };
        transformsRelativesLieutenants.Add(t);
    }

    int nbLignes = GetNbLignes(nbSbires);
    // On remplit les anneaux de sbires 1 � 1
    for (int i = 0; i < nbLignes; ++i) {
        // La premiere ligne est differente des autres !
        if (i == 0) {
            int tailleLigne = nbFrontLane - 2;
            FVector posInitiale{ - espacement * (tailleLigne + 2) / 2 + espacement / 2, 0.0f, 0.0f };
            for (int j = 0; j < tailleLigne; ++j) {
                int indDecalage = (j < tailleLigne / 2) ? j : j + 2;
                FVector pos = posInitiale + FVector{ espacement * indDecalage, 0.0f, 0.0f };
                FQuat quat = FQuat::Identity;
                FVector scale{ 1.0f, 1.0f, 1.0f };
                FTransform t{ quat, pos, scale };
                transformsRelativesSbires.Add(t);

            }

        // Et la derniere aussi ! x)
        } else if (i == nbLignes - 1) {
            int tailleLigne = nbSbires - (nbFrontLane - 2) - (nbLignes - 2) * nbFrontLane;
            FVector posInitiale{ - espacement * tailleLigne / 2 + espacement / 2, 0.0f, 0.0f };
            for (int j = 0; j < tailleLigne; ++j) {
                FVector pos = posInitiale + FVector{ espacement * j, espacement * i, 0.0f };
                FQuat quat = FQuat::Identity;
                FVector scale{ 1.0f, 1.0f, 1.0f };
                FTransform t{ quat, pos, scale };
                transformsRelativesSbires.Add(t);
            }

        // Une ligne normale ! :)
        } else {
            int tailleLigne = nbFrontLane;
            FVector posInitiale{ - espacement * tailleLigne / 2 + espacement / 2, 0.0f, 0.0f };
            for (int j = 0; j < tailleLigne; ++j) {
                FVector pos = posInitiale + FVector{ espacement * j, espacement * i, 0.0f };
                FQuat quat = FQuat::Identity;
                FVector scale{ 1.0f, 1.0f, 1.0f };
                FTransform t{ quat, pos, scale };
                transformsRelativesSbires.Add(t);
            }
        }
    }
}

int UFormationRectangle::GetNbLignes(int nbSbires) {
    if (nbSbires == 0) return 0;
    if (nbFrontLane < 2) throw new _exception{};

    int taillePremiereLigne = nbFrontLane - 2;
    int nbLignes = 1;

    while (nbSbires > taillePremiereLigne) {
        nbSbires -= taillePremiereLigne;
        taillePremiereLigne = nbFrontLane;
        nbLignes++;
    }

    return nbLignes;
}
