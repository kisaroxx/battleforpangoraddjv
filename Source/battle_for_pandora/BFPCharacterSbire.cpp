// Fill out your copyright notice in the Description page of Project Settings.

#include "BFPCharacterSbire.h"
#include "BFP_GameState.h"
#include "Message.h"
#include "SquadDescriptor.h"
#include "SquadManager.h"
#include "SquadSbireController.h"
#include "SquadMemberController.h"


ABFPCharacterSbire::ABFPCharacterSbire(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {


}

// Called when the game starts
void ABFPCharacterSbire::BeginPlay()
{
    Super::BeginPlay();

    if (GetWorld()->IsServer()) {
       ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
       if (IsValid(gameState)) {
          gameState->Register(this);
       }
	   else {
		   MY_LOG_NETWORK(TEXT("ABFPCharacterSbire BeginPlay game state non valide"));
	   }
    }
}

void ABFPCharacterSbire::UnRegisterToGameManager() {
    GetWorld()->GetGameState<ABFP_GameState>()->UnRegister(this);
}

void ABFPCharacterSbire::RegisterToGameManager() {
   GetWorld()->GetGameState<ABFP_GameState>()->Register(this);
}

void ABFPCharacterSbire::RegisterToSquad() {  
   squad->AddSbire(Cast<ASquadSbireController>(this->GetController()));
   UMessage* message = NewObject<UMessage>();
   message->SetParams(MessageType::JOIN, GetController<ASquadMemberController>());
   GetSquad()->GetSquadManager()->ReceiveMessage(message);
}

void ABFPCharacterSbire::UnRegisterToSquad() {
   if (HasSquad()) {
      UMessage* message = NewObject<UMessage>();
      message->SetParams(MessageType::DIED, GetController<ASquadMemberController>());
      GetSquad()->GetSquadManager()->ReceiveMessage(message);
      squad->RemoveSbire(Cast<ASquadSbireController>(this->GetController()));
   }
}
