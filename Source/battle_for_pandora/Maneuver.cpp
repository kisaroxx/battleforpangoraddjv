// Fill out your copyright notice in the Description page of Project Settings.

#include "Maneuver.h"
#include "Logger.h"
#include "MoveCommand.h"
#include "MoveToActorCommand.h"
#include "SimpleAttackCommand.h"
#include "SquadDescriptor.h"
#include "SquadMemberController.h"
#include "SquadLieutenantController.h"
#include "SquadSbireController.h"
#include "SquadDescriptor.h"
#include "BFPCharacterPlayable.h"
#include "FormationCercle.h"
#include "Util.h"
#include "BFP_PlayerController.h"
#include <algorithm>
#include <cmath>
#include "BFP_GameState.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTTaskNode.h"
#include "BehaviorTree/BTTaskNode.h"
#include "AIController.h"
#include "VisualLogger/VisualLogger.h"
#include "GameplayTasksComponent.h"
#include "Message.h"
#include "MoveToActorCommand.h"
#include "runtime/AIModule/Classes/BehaviorTree/BlackboardComponent.h"
#include "Runtime/AIModule/Classes/BrainComponent.h"
#include "Message.h"
#include "SquadManager.h"
#include "NoCommand.h"


AManeuver::AManeuver() {
    PrimaryActorTick.bCanEverTick = false;
    // AddTargetPoint(FTransform{ FVector{-7930.0f, -26270.0f, 290.0f} });
    bReplicates = true;
    bAlwaysRelevant = true;
}

void AManeuver::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
   Super::GetLifetimeReplicatedProps(OutLifetimeProps);

   DOREPLIFETIME(AManeuver, targetPoints);
}

void AManeuver::SetSquadDescriptor(ASquadDescriptor* newSquadDescriptor) {
    desc = newSquadDescriptor;
}


void AManeuver::Execute() {
    DEBUG_SCREEN(-1, 5.0f, FColor::Cyan, "Utilisation de l'execute de la maneuvre par defaut, il ne se passera rien, bande de pingouins :)");
}

void AManeuver::ClearMessages() {
    messages.Empty();
}


void AManeuver::ReceiveMessage(UMessage * message) {

   if (message->GetType() == MessageType::STOP) {
      EmptyTargetPoints();
      //SetNoCommands();
      //RunBehaviorTree(behaviorTree);
      GetDescriptor()->GetSquadManager()->SetManeuver(GetDescriptor()->GetManeuverType());
      MY_LOG_NETWORK(TEXT("ABFP_PlayerController StopMovementRPC_Implementation STOP Message Receive"));
   }

   // On enregistre le message
   messages.Add(message);

   // On transmet le message � la tache, c'est � elle de voir si �a l'int�resse ou pas haha
   UBehaviorTreeComponent* BTComp = Cast<UBehaviorTreeComponent>(BrainComponent);
   if (IsValid(BTComp)) {
      UBTTaskNode* currentNode = (UBTTaskNode*)BTComp->GetActiveNode();
      currentNode->ReceivedMessage(BrainComponent, FAIMessage{});
   }

   if (message->GetType() == MessageType::JOIN) {
      UMoveToActorCommand* com = NewObject<UMoveToActorCommand>(this, UMoveToActorCommand::StaticClass());
      com->setCharacter(message->GetSquadMemberController());
      com->SetActorTarget(GetDescriptor()->GetFirstLieutenant()->GetPawn());
      com->SetOffsetType(OffsetType::ABSOLU);
      message->GetSquadMemberController()->SetCommand(com);
   }
}


UMessage * AManeuver::GetLastMessage() const
{
    return messages.Last();
}

TArray<UMessage*> AManeuver::GetAllMessages() const
{
    return messages;
}

void AManeuver::PostInitProperties() {
    Super::PostInitProperties();

    // On set la formation
    formation = NewObject<UFormation>(this, formationClass.Get()); // Don't touch this ! <3

    // La formation d'encerclement
    formationDEncerclement = NewObject<UFormationCercle>(this, UFormationCercle::StaticClass());
    formationDEncerclement->SetParams(10, GetAgressionRange() / 2.0f, 270.0f);
}

// Called when the game starts
void AManeuver::BeginPlay()
{
	Super::BeginPlay();
   if(IsValid(behaviorTree))
       RunBehaviorTree(behaviorTree);
   else
       DEBUG_SCREEN(-1, 5.0f, FColor::Red, FString("Le behavior tree de la maneuvre n'est pas set !!!"));
}

TMap<ABFPCharacterPlayable*, TArray<ASquadSbireController*>> AManeuver::RepartirSelonEnnemis(TArray<ABFPCharacterPlayable*> ennemis) {
    ABFP_GameState* gs = GetWorld()->GetGameState<ABFP_GameState>();

    int E = ennemis.Num();
    int N = desc->GetNbSbires();
    TArray<ASquadSbireController*> sbires = desc->GetSbires();
    TMap<ABFPCharacterPlayable*, TArray<ASquadSbireController*>> nearest;
    TMap<ABFPCharacterPlayable*, TArray<ASquadSbireController*>> res;

    if (E == 0) {
       return res;
       MY_LOG_IA(TEXT("AManeuver::RepartirSelonEnnemis erreur ! ennemis == 0 !"));
    }

    // On calcul les plus proches de chaque ennemi
    for (int i = 0; i < E; i++) {
        nearest.Add(ennemis[i], gs->GetAllNearestFrom(sbires, ennemis[i]->GetTransform(), N));
        //nearest[ennemis[i]] = gs->GetAllNearestFrom(sbires, ennemis[i]->GetTransform(), N);
    }

    // Puis on les associe équitablement !
    for (int i = 0, j = 0; i < N; i++, j = (j + 1) % E) {
        // On ajoute le plus proche de notre ennemi
        ABFPCharacterPlayable* ennemi = ennemis[j];
        ASquadSbireController* plusProche = nearest[ennemi][0];
        if (res.Find(ennemi) == nullptr) {
            res.Add(ennemi, TArray<ASquadSbireController*>{ plusProche });
            //res[ennemi] = TArray<ASquadSbireController*>{ plusProche };
        } else {
            res[ennemi].Push( plusProche );
        }

        // Puis on le retire de toutes les listes des nearest !
        for (int k = 0; k < E; k++) {
            ABFPCharacterPlayable* ennemiK = ennemis[k];
            nearest[ennemiK].Remove(plusProche);
        }
    }

    return res;
}

// Called every frame
void AManeuver::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AManeuver::DetecterJoueurs() {
    TArray<ABFPCharacterPlayable*> joueurs = GetWorld()->GetGameState<ABFP_GameState>()->GetAllJoueursAllies();
    joueursVus = GetWorld()->GetGameState<ABFP_GameState>()->GetAllInDistanceFrom(joueurs, desc->GetFirstLieutenant()->GetPawn()->GetTransform(), visionRange);
}

void AManeuver::MoveTo(const FTransform & positionOrientee, bool tryUseLieutenantNavMesh) {
    TArray<ASquadLieutenantController*> lieutenants = desc->GetLieutenants();
    TArray<ASquadSbireController*> sbires = desc->GetSbires();
    float angle = Util::AngleBetweenVectorsInXYPlane(FVector::ForwardVector, lieutenants[0]->GetPawn()->GetActorLocation() - positionOrientee.GetLocation()) - 90.0f;
    TArray<FTransform> lieutenantTransforms = formation->GetRelativesPositionsLieutenants(desc->GetNbSbires(), desc->GetNbLieutenants(), angle);
    TArray<FTransform> sbiresTransforms = formation->GetRelativesPositionsSbires(desc->GetNbSbires(), desc->GetNbLieutenants(), angle);

    for (int i = 0; i < desc->GetNbLieutenants(); ++i) {
        FTransform t = lieutenantTransforms[i];
        UMoveCommand* com = NewObject<UMoveCommand>(this, UMoveCommand::StaticClass());
        FVector posFinale = t.GetLocation() + positionOrientee.GetLocation();
        com->SetTryUseLieutenantNavMesh(tryUseLieutenantNavMesh);
        com->SetTargetPoint(posFinale);
        com->setCharacter(lieutenants[i]);
        //MY_LOG_UI(TEXT("BLOUBLOU MOVE %f, %f, %f"), com->GetTargetPoint().X, com->GetTargetPoint().Y, com->GetTargetPoint().Z);
        lieutenants[i]->SetCommand(com);
    }
    // Compute sources
    TArray<FVector> sources;
    for (int i = 0; i < desc->GetNbSbires(); i++) {
        sources.Push(sbires[i]->GetPawn()->GetActorLocation());
    }
    // Compute dest
    TArray<FVector> cibles;
    for (int i = 0; i < desc->GetNbSbires(); i++) {
        //cibles.Push(sbiresTransforms[i].GetLocation() + positionOrientee.GetLocation());
        cibles.Push(sbiresTransforms[i].GetLocation());
    }
    // Associer
    TArray<FVector> positionsFinales = associerEfficacement(sources, cibles); // En fait si ! ^^'
    //TArray<FVector> positionsFinales = cibles; // On ne veut pas r�affecter les sbires si ils sont d�j� en formation !
    for (int i = 0; i < desc->GetNbSbires(); ++i) {
        UMoveToActorCommand* com = NewObject<UMoveToActorCommand>(this, UMoveToActorCommand::StaticClass());
        com->SetActorTarget(lieutenants[0]->GetPawn());
        com->SetOffsetType(OffsetType::RELATIF_TO_POINT);
        com->SetOffset(positionsFinales[i]);
        com->SetPointForAngle(positionOrientee.GetLocation());
        com->setCharacter(sbires[i]);
        sbires[i]->SetCommand(com);
        //UMoveCommand* com = NewObject<UMoveCommand>(this, UMoveCommand::StaticClass());
        //com->setTargetPoint(positionsFinales[i]);
        //com->setCharacter(sbires[i]);
        //sbires[i]->SetCommand(com);
    }
}

void AManeuver::MoveToActor(AActor* actor) {
    TArray<ASquadLieutenantController*> lieutenants = desc->GetLieutenants();
    TArray<ASquadSbireController*> sbires = desc->GetSbires();
    float angle = Util::AngleBetweenVectorsInXYPlane(FVector::ForwardVector, lieutenants[0]->GetPawn()->GetActorLocation() - actor->GetActorLocation()) - 90.0f;
    TArray<FTransform> lieutenantTransforms = formation->GetRelativesPositionsLieutenants(desc->GetNbSbires(), desc->GetNbLieutenants(), angle);
    TArray<FTransform> sbiresTransforms = formation->GetRelativesPositionsSbires(desc->GetNbSbires(), desc->GetNbLieutenants(), angle);

    for (int i = 0; i < desc->GetNbLieutenants(); ++i) {
        FTransform t = lieutenantTransforms[i];
        UMoveToActorCommand* com = NewObject<UMoveToActorCommand>(this, UMoveToActorCommand::StaticClass());
        FVector posFinale = t.GetLocation() + actor->GetActorLocation();
        com->SetActorTarget(actor);
        com->SetOffset(t.GetLocation());
        com->setCharacter(lieutenants[i]);
        lieutenants[i]->SetCommand(com);
    }
    // Compute sources
    TArray<FVector> sources;
    for (int i = 0; i < desc->GetNbSbires(); i++) {
        sources.Push(sbires[i]->GetPawn()->GetActorLocation());
    }
    // Compute dest
    TArray<FVector> cibles;
    for (int i = 0; i < desc->GetNbSbires(); i++) {
        cibles.Push(sbiresTransforms[i].GetLocation() + actor->GetActorLocation());
    }
    // Associer
    TArray<FVector> positionsFinales = associerEfficacement(sources, cibles); // En fait si ^^
    //TArray<FVector> positionsFinales = cibles; // On ne veut pas r�affecter les sbires si ils sont d�j� en formation !
    for (int i = 0; i < desc->GetNbSbires(); ++i) {
        UMoveToActorCommand* com = NewObject<UMoveToActorCommand>(this, UMoveToActorCommand::StaticClass());
        com->SetActorTarget(actor);
        com->SetOffset(positionsFinales[i] - actor->GetActorLocation());
        com->setCharacter(sbires[i]);
        sbires[i]->SetCommand(com);
    }
}

void AManeuver::FuirActor(AActor * actor) {
    TArray<ASquadLieutenantController*> lieutenants = desc->GetLieutenants();
    TArray<ASquadSbireController*> sbires = desc->GetSbires();
    float angle = Util::AngleBetweenVectorsInXYPlane(FVector::ForwardVector, lieutenants[0]->GetPawn()->GetActorLocation() - actor->GetActorLocation()) - 90.0f;
    TArray<FTransform> lieutenantTransforms = formation->GetRelativesPositionsLieutenants(desc->GetNbSbires(), desc->GetNbLieutenants(), angle);
    TArray<FTransform> sbiresTransforms = formation->GetRelativesPositionsSbires(desc->GetNbSbires(), desc->GetNbLieutenants(), angle);

    FVector posToFlee = actor->GetActorLocation();
    FVector posLieutenant = GetDescriptor()->GetFirstLieutenant()->GetPawn()->GetActorLocation();
    FVector directionFuite = FVector::ForwardVector * ((posLieutenant - posToFlee).Size2D() + 500.0f);

    for (int i = 0; i < desc->GetNbLieutenants(); ++i) {
        FTransform t = lieutenantTransforms[i];
        UMoveToActorCommand* com = NewObject<UMoveToActorCommand>(this, UMoveToActorCommand::StaticClass());
        FVector posFinale = t.GetLocation() + actor->GetActorLocation();
        com->SetActorTarget(actor);
        com->SetOffset(t.GetLocation() + directionFuite);
        com->setCharacter(lieutenants[i]);
        lieutenants[i]->SetCommand(com);
    }
    // Compute sources
    TArray<FVector> sources;
    for (int i = 0; i < desc->GetNbSbires(); i++) {
        sources.Push(sbires[i]->GetPawn()->GetActorLocation());
    }
    // Compute dest
    TArray<FVector> cibles;
    for (int i = 0; i < desc->GetNbSbires(); i++) {
        cibles.Push(sbiresTransforms[i].GetLocation() + actor->GetActorLocation());
    }
    // Associer
    TArray<FVector> positionsFinales = associerEfficacement(sources, cibles); // En fait si ^^
                                                                              //TArray<FVector> positionsFinales = cibles; // On ne veut pas r�affecter les sbires si ils sont d�j� en formation !
    for (int i = 0; i < desc->GetNbSbires(); ++i) {
        UMoveToActorCommand* com = NewObject<UMoveToActorCommand>(this, UMoveToActorCommand::StaticClass());
        com->SetActorTarget(actor);
        com->SetOffset(positionsFinales[i] - actor->GetActorLocation() + directionFuite);
        com->setCharacter(sbires[i]);
        sbires[i]->SetCommand(com);
    }
}

void AManeuver::OrienterTo(const FTransform & pointCible) {
    TArray<ASquadLieutenantController*> lieutenants = desc->GetLieutenants();
    TArray<ASquadSbireController*> sbires = desc->GetSbires();
    float angle = Util::AngleBetweenVectorsInXYPlane(FVector::ForwardVector, lieutenants[0]->GetPawn()->GetActorLocation() - pointCible.GetLocation()) - 90.0f;
    TArray<FTransform> lieutenantTransforms = formation->GetRelativesPositionsLieutenants(desc->GetNbSbires(), desc->GetNbLieutenants(), angle);
    TArray<FTransform> sbiresTransforms = formation->GetRelativesPositionsSbires(desc->GetNbSbires(), desc->GetNbLieutenants(), angle);
    FVector positionDeRotation = desc->GetFirstLieutenant()->GetPawn()->GetActorLocation();

    for (int i = 0; i < desc->GetNbLieutenants(); ++i) {
        FTransform t = lieutenantTransforms[i];
        UMoveCommand* com = NewObject<UMoveCommand>(this, UMoveCommand::StaticClass());
        FVector posFinale = t.GetLocation() + positionDeRotation;
        com->SetTryUseLieutenantNavMesh(false);
        com->SetTargetPoint(posFinale);
        com->setCharacter(lieutenants[i]);
        lieutenants[i]->SetCommand(com);
    }
    // Compute sources
    TArray<FVector> sources;
    for (int i = 0; i < desc->GetNbSbires(); i++) {
        sources.Push(sbires[i]->GetPawn()->GetActorLocation());
    }
    // Compute dest
    TArray<FVector> cibles;
    for (int i = 0; i < desc->GetNbSbires(); i++) {
        cibles.Push(sbiresTransforms[i].GetLocation() + positionDeRotation);
    }
    // Associer
    TArray<FVector> positionsFinales = associerEfficacement(sources, cibles); // En fait si ^^
    //TArray<FVector> positionsFinales = cibles; // On ne veut pas r�affecter les sbires si ils sont d�j� en formation !
    for (int i = 0; i < desc->GetNbSbires(); ++i) {
        UMoveCommand* com = NewObject<UMoveCommand>(this, UMoveCommand::StaticClass());
        com->SetTargetPoint(positionsFinales[i]);
        com->setCharacter(sbires[i]);
        sbires[i]->SetCommand(com);
    }
}

void AManeuver::RushTo(const FTransform & positionOrientee, bool tryUseLieutenantNavMesh) {
    TArray<ASquadLieutenantController*> lieutenants = desc->GetLieutenants();
    TArray<ASquadSbireController*> sbires = desc->GetSbires();

    for (int i = 0; i < desc->GetNbLieutenants(); ++i) {
        UMoveCommand* com = NewObject<UMoveCommand>(this, UMoveCommand::StaticClass());
        FVector posFinale = positionOrientee.GetLocation();
        com->SetTryUseLieutenantNavMesh(tryUseLieutenantNavMesh);
        com->SetTargetPoint(posFinale);
        com->setCharacter(lieutenants[i]);
        lieutenants[i]->SetCommand(com);
    }
    for (int i = 0; i < desc->GetNbSbires(); ++i) {
        UMoveCommand* com = NewObject<UMoveCommand>(this, UMoveCommand::StaticClass());
        FVector posFinale = positionOrientee.GetLocation();
        com->SetTargetPoint(posFinale);
        com->setCharacter(sbires[i]);
        sbires[i]->SetCommand(com);
    }
}

void AManeuver::MoveToNextPoint() {
    MoveTo(targetPoints[0]->GetActorTransform());
}

void AManeuver::Attaquer(ABFPCharacterPlayable* joueur) {
   TArray<ASquadLieutenantController*> lieutenants = desc->GetLieutenants();
   TArray<ASquadSbireController*> sbires = desc->GetSbires();

   ABFP_GameState* gs = GetWorld()->GetGameState<ABFP_GameState>();
   TArray<ABFPCharacterPlayable*> ennemis = gs->GetAllJoueursAllies();
   ennemis = gs->GetAllInDistanceFrom(ennemis, desc->GetFirstLieutenant()->GetPawn()->GetTransform(), GetRushRange() + 500.0f);

   if (ennemis.Num() == 0)
      return;

   TMap<ABFPCharacterPlayable*, TArray<ASquadSbireController*>> repartition = RepartirSelonEnnemis(ennemis);

   // Pour chaque ennemi dans la zone, on fait attaquer les sbires les plus proches !
   for (const TPair<ABFPCharacterPlayable*, TArray<ASquadSbireController*>>& pair : repartition) {
       ABFPCharacterPlayable* ennemi = pair.Key;
       TArray<ASquadSbireController*> sbiresAssocies = pair.Value;

       for (int i = 0; i < sbiresAssocies.Num(); i++) {
           // Les premiers attaquent
           if (i < nbAttaquants) {
              USimpleAttackCommand* com = NewObject<USimpleAttackCommand>(this, USimpleAttackCommand::StaticClass());
              com->SetEnemy(ennemi);
              com->setCharacter(sbiresAssocies[i]);
              sbiresAssocies[i]->SetCommand(com);

           // Les autre regardent et menacent !
           } else {
               FaireMenacerMember(sbiresAssocies[i], ennemi);
           }
       }
   }

   // Le lieutenant attaque son adversaire le plus proche !
   for (int i = 0; i < desc->GetNbLieutenants(); ++i) {
      USimpleAttackCommand* com = NewObject<USimpleAttackCommand>(this, USimpleAttackCommand::StaticClass());
      com->SetEnemy(joueur);
      com->setCharacter(lieutenants[i]);
      lieutenants[i]->SetCommand(com);
   }

   //// Les sbires les plus proches attaquent
   //sbires = GetWorld()->GetGameState<ABFP_GameState>()->GetAllNearestFromController(sbires, joueur->GetTransform(), nbAttaquants);
   //for (int i = 0; i < desc->GetNbLieutenants(); ++i) {
   //   USimpleAttackCommand* com = NewObject<USimpleAttackCommand>(this, USimpleAttackCommand::StaticClass());
   //   com->SetEnemy(joueur);
   //   com->setCharacter(lieutenants[i]);
   //   lieutenants[i]->SetCommand(com);
   //}
   //for (int i = 0; i < sbires.Num(); ++i) {
   //   USimpleAttackCommand* com = NewObject<USimpleAttackCommand>(this, USimpleAttackCommand::StaticClass());
   //   com->SetEnemy(joueur);
   //   com->setCharacter(sbires[i]);
   //   sbires[i]->SetCommand(com);
   //}

   //// Les autres regardent en menacant !
   //TArray<ASquadSbireController*> autresSbires;
   //for (int i = 0; i < desc->GetNbSbires(); i++) {
   //    if (sbires.Find(desc->GetSbires()[i]) == INDEX_NONE) {
   //        autresSbires.Push(desc->GetSbires()[i]);
   //    }
   //}
   //for (int i = 0; i < autresSbires.Num(); ++i) {
   //    FaireMenacerMember(autresSbires[i], joueur);
   //}
}

void AManeuver::FaireMenacerMember(ASquadMemberController* member, ABFPCharacterPlayable* target) {
    // On cherche l'offset, sur le meme rayon que le sbire par rapport au joueur, voir un peu plus proche
    // Et decale d'entre 15 et 75 degre
    float rayon = FVector::Distance(target->GetActorLocation(), member->GetPawn()->GetActorLocation());
    rayon = std::min(std::max(rayon, rayonDeMenaceMin), rayonDeMenaceMax);
    rayon = Util::Random(0.8, 1.1f) * rayon;
    float angle = Util::AngleBetweenVectorsInXYPlane(FVector::ForwardVector, member->GetPawn()->GetActorLocation() - target->GetActorLocation());
    angle += (Util::Random(0, 1) ? -1 : 1) * Util::Random(15.0f, 60.0f);
    FVector destination = Util::PolaireToCartesian(angle, rayon);
    UMoveToActorCommand * com = NewObject<UMoveToActorCommand>(this, UMoveToActorCommand::StaticClass());
    com->SetActorTarget(target);
    com->SetOffset(destination);
    com->SetOffsetType(OffsetType::ABSOLU);
    com->setCharacter(member);
    member->SetCommand(com);
}

void AManeuver::EncerclerJoueur(ABFPCharacter * character) {
    TArray<ASquadLieutenantController*> lieutenants = desc->GetLieutenants();
    TArray<ASquadSbireController*> sbires = desc->GetSbires();
    float angle = Util::AngleBetweenVectorsInXYPlane(FVector::ForwardVector, lieutenants[0]->GetPawn()->GetActorLocation() - character->GetActorLocation()) + 180.0f;
    TArray<FTransform> sbiresTransforms = formationDEncerclement->GetRelativesPositionsSbires(desc->GetNbSbires(), desc->GetNbLieutenants(), angle);

    for (int i = 0; i < desc->GetNbLieutenants(); ++i) {
        UMoveToActorCommand* com = NewObject<UMoveToActorCommand>(this, UMoveToActorCommand::StaticClass());
        FVector lieutToChar = character->GetActorLocation() - lieutenants[i]->GetPawn()->GetActorLocation();
        lieutToChar.Normalize();
        FVector posFinale = character->GetActorLocation() - lieutToChar * (750.0f);
        com->SetActorTarget(character);
        com->SetOffset(FVector::ForwardVector * (750.0f));
        com->setCharacter(lieutenants[i]);
        lieutenants[i]->SetCommand(com);
    }
    // Compute sources
    TArray<FVector> sources;
    for (int i = 0; i < desc->GetNbSbires(); i++) {
        sources.Push(sbires[i]->GetPawn()->GetActorLocation());
    }
    // Compute dest
    TArray<FVector> cibles;
    for (int i = 0; i < desc->GetNbSbires(); i++) {
        cibles.Push(sbiresTransforms[i].GetLocation() + character->GetActorLocation());
    }
    // Associer
    TArray<FVector> positionsFinales = associerEfficacement(sources, cibles);
    for (int i = 0; i < desc->GetNbSbires(); ++i) {
        UMoveToActorCommand* com = NewObject<UMoveToActorCommand>(this, UMoveToActorCommand::StaticClass());
        com->SetActorTarget(character);
        com->SetOffset(positionsFinales[i] - character->GetActorLocation());
        com->setCharacter(sbires[i]);
        sbires[i]->SetCommand(com);
    }
}

bool AManeuver::DejaEncercle(ABFPCharacter * character, float pourcentageDeCompletion, float distToPosMax) {
    TArray<ASquadLieutenantController*> lieutenants = desc->GetLieutenants();
    TArray<ASquadSbireController*> sbires = desc->GetSbires();
    float angle = Util::AngleBetweenVectorsInXYPlane(FVector::ForwardVector, lieutenants[0]->GetPawn()->GetActorLocation() - character->GetActorLocation()) + 180.0f;
    TArray<FTransform> sbiresTransforms = formationDEncerclement->GetRelativesPositionsSbires(desc->GetNbSbires(), desc->GetNbLieutenants(), angle);

    for (int i = 0; i < desc->GetNbLieutenants(); ++i) {
        UMoveCommand* com = NewObject<UMoveCommand>(this, UMoveCommand::StaticClass());
        FVector lieutToChar = character->GetActorLocation() - lieutenants[i]->GetPawn()->GetActorLocation();
        lieutToChar.Normalize();
        FVector posFinale = character->GetActorLocation() - lieutToChar * 1000.0f;
        com->SetTryUseLieutenantNavMesh(false);
        com->SetTargetPoint(posFinale);
        com->setCharacter(lieutenants[i]);
        lieutenants[i]->SetCommand(com);
    }
    // Compute sources
    TArray<FVector> sources;
    for (int i = 0; i < desc->GetNbSbires(); i++) {
        sources.Push(sbires[i]->GetPawn()->GetActorLocation());
    }
    // Compute dest
    TArray<FVector> cibles;
    for (int i = 0; i < desc->GetNbSbires(); i++) {
        cibles.Push(sbiresTransforms[i].GetLocation() + character->GetActorLocation());
    }
    // Associer
    TArray<FVector> positionsFinales = associerEfficacement(sources, cibles);

    // Trouver les distances
    int nb = 0;
    float distCarre = distToPosMax * distToPosMax;
    for (int i = 0; i < desc->GetNbSbires(); i++) {
        FVector posSbire = sbires[i]->GetPawn()->GetActorLocation();
        FVector posCible = positionsFinales[i];
        float dist = FVector::DistSquared(posSbire, posCible);
        if (dist <= distCarre) {
            nb++;
        }
    }
    float pourcentage = static_cast<float>(nb) / desc->GetNbSbires();
    FString str("pourcentage = "); str.AppendInt(pourcentage * 100);
    DEBUG_SCREEN(-1, 5.0f, FColor::Cyan, str);
    return  pourcentage >= pourcentageDeCompletion;
}

UFormation* AManeuver::GetFormation() {
    return formation;
}

void AManeuver::AddTargetPoint(AManeuverTargetPoint* maneuverTargetPoint) {
   targetPoints.Add(MoveTemp(maneuverTargetPoint));
}

TArray<AManeuverTargetPoint*>& AManeuver::GetTargetPoints()
{
   return targetPoints;
}

void AManeuver::SetTargetPoints(TArray<AManeuverTargetPoint*> newTargetPoints) {
    targetPoints = newTargetPoints;
}

void AManeuver::RemoveFirstTargetPoint() {
    if (targetPoints.Num() > 0) targetPoints.RemoveAt(0);
}

void AManeuver::EmptyTargetPoints() {
   int nbTargetPoints = targetPoints.Num();
   for (int i = 0; i < nbTargetPoints ; ++i) {
      RemoveFirstTargetPoint();
   }
}

void AManeuver::SetJoueursVus(TArray<ABFPCharacterPlayable*> joueursVus_) {
    joueursVus = joueursVus_;

    if (joueursVus.Num() > 0) {
        // On set le joueur le plus proche
        joueurSelected = GetWorld()->GetGameState<ABFP_GameState>()->GetAllNearestFrom(joueursVus, GetDescriptor()->GetFirstLieutenant()->GetPawn()->GetTransform(), 1)[0];
    }
}

TArray<ABFPCharacterPlayable*> AManeuver::GetJoueursVus() {
    return joueursVus;
}

float AManeuver::GetVisionRange() const {
    return visionRange;
}

float AManeuver::GetAgressionRange() const
{
    return agressionRange;
}

float AManeuver::GetRushRange() const
{
    return rushRange;
}

ASquadDescriptor * AManeuver::GetDescriptor() const
{
    return desc;
}

void AManeuver::SetJoueurSelected(ABFPCharacterPlayable * joueurSelected_) {
    joueurSelected = joueurSelected_;
}

ABFPCharacterPlayable * AManeuver::GetJoueurSelected() const
{
    return joueurSelected;
}

void AManeuver::SetCurrentTaskBT(UBTTaskNode * node) {
    currentTask = node;
}

int AManeuver::GetNbAttaquants() const {
    return nbAttaquants;
}

void AManeuver::SetNbAttaquants(int nb) {
    nbAttaquants = nb;
}

void AManeuver::SetNoCommands() {
   for (auto lieutenant : GetDescriptor()->GetLieutenants()) {
      lieutenant->StopMovement();
      UNoCommand* com = NewObject<UNoCommand>(this, UNoCommand::StaticClass());
      com->setCharacter(lieutenant);
      lieutenant->SetCommand(com);
   }
   for (auto sbire : GetDescriptor()->GetSbires()) {
      sbire->StopMovement();
      UNoCommand* com = NewObject<UNoCommand>(this, UNoCommand::StaticClass());
      com->setCharacter(sbire);
      sbire->SetCommand(com);
   }
}

TArray<FVector> AManeuver::associerEfficacement(const TArray<FVector>& sources, const TArray<FVector>& cibles) {
    int n = sources.Num();
    // Les indices associees !
    TArray<int> associes;
    for (int i = 0; i < n; i++) associes.Push(i);

    // Tant que l'on trouve des permutations, on continue � swapper ! :D
    bool continuer = true;
    while (continuer) {
        continuer = false;
        for (int i = 0; i < n; i++) {
            float dii = FVector::DistSquared(sources[i], cibles[associes[i]]);
            for (int j = i + 1; j < n; j++) {
                float djj = FVector::DistSquared(sources[j], cibles[associes[j]]);
                float dij = FVector::DistSquared(sources[i], cibles[associes[j]]);
                float dji = FVector::DistSquared(sources[j], cibles[associes[i]]);
                // Si le max d'avant est plus grand que le max d'apr�s, alors on swap !
                if (std::max(dii, djj) > std::max(dij, dji) + 50.0f) { // + 50 Pour �viter les oscillations
                    dii = dij;
                    std::swap(associes[i], associes[j]);
                    continuer = true;
                }
            }
        }
    }

    // On construit le r�sultat !
    TArray<FVector> res;
    for (int i = 0; i < n; i++) {
        res.Push(cibles[associes[i]]);
    }
    return res;
}