// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GameFramework/Controller.h"
#include "AIController.h"
#include "SquadSbireController.h"
//#include "BFPCharacterPlayable.h"
#include "GameFramework/Actor.h"
#include "Formation.h"
#include "Message.h"
#include "Logger.h"
#include "ManeuverTargetPoint.h"
#include "Maneuver.generated.h"

UENUM(BlueprintType)
enum class ManeuverType : uint8
{
    AGRESSIVE,
    DEFENSIVE,
    SURVIE,
    ASSAULT,
    RECALL
};

UCLASS( Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BATTLE_FOR_PANDORA_API AManeuver : public AAIController
{
	GENERATED_BODY()
protected:

    /* L'ensemble des points ou devra se rendre la squad.
     * Ce TArray se comportera comme une file. */
    UPROPERTY(ReplicatedUsing = OnRep_TP, VisibleAnywhere, Category = "Deplacement", meta = (AllowPrivateAccess = "true"))
    TArray<AManeuverTargetPoint*> targetPoints;

    UFUNCTION()
    void OnRep_TP() {
       MY_LOG_NETWORK(TEXT("UManeuver OnRep_TP : replication des points -- num %u"), targetPoints.Num());
    }

    /* L'ensemble des messages enregistres pour l'etat courant.
     * Ils sont classes par ordre d'arrive. */
    UPROPERTY(VisibleAnywhere, Category = "Messages")
    TArray<class UMessage*> messages;

    /* Le pointeur vers la squad pour pouvoir donner les ordres. */
    class ASquadDescriptor* desc;

    /* La formation que devront adopter les unites dans la Squad. */
    UPROPERTY(EditAnywhere, Category = "Deploiement")
    TSubclassOf<UFormation> formationClass = UFormation::StaticClass();

    /* La formation actuelle des unites. */
    UPROPERTY(VisibleAnywhere, Category = "Deploiement")
    class UFormation* formation;

    /* La vision maximum pour d�tecter des joueurs de la squad. */
    UPROPERTY(EditAnywhere, Category = "SensThinkAct")
    float visionRange = 5000.0f;

    /* La vision maximum pour aggresser un joueur. */
    UPROPERTY(EditAnywhere, Category = "SensThinkAct")
    float agressionRange = 2500.0f;

    /* La vision maximum pour aggresser sans restrictions un joueur. */
    UPROPERTY(EditAnywhere, Category = "SensThinkAct")
    float rushRange = 1500.0f;

    /* La distance de la cible � laquelle se tiennent les sbires qui la menacent. */
    UPROPERTY(EditAnywhere, Category = "SensThinkAct")
    float rayonDeMenaceMin = 750.0f;
    UPROPERTY(EditAnywhere, Category = "SensThinkAct")
    float rayonDeMenaceMax = 1500.0f;

    /* Les joueurs actuellement detectes par la maneoeuvre. */
    UPROPERTY(VisibleAnywhere, Category = "SensThinkAct")
    TArray<class ABFPCharacterPlayable*> joueursVus;

    /* Le joueur choisis par la Squad. */
    UPROPERTY(VisibleAnywhere, Category = "SensThinkAct")
    ABFPCharacterPlayable* joueurSelected;

    /* Le nombre de sbires pouvant attaquer simultan�ment ! */
    UPROPERTY(EditAnywhere, Category = "SensThinkAct")
    class UBehaviorTree* behaviorTree;

    /* La tache actuellement en cours du behaviorTree. Utilise pour transmettre les mesages.*/
    class UBTTaskNode* currentTask;

    /* La formation utilisee pour encercler un personnage. */
    class UFormationCercle* formationDEncerclement;

    /* Le nombre de sbires pouvant attaquer simultan�ment ! */
    UPROPERTY(EditAnywhere, Category = "Attaque")
    int nbAttaquants = 5;

public:	
	// Sets default values for this component's properties
	AManeuver();
   void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;


    virtual void SetSquadDescriptor(ASquadDescriptor* newSquadDescriptor);

    virtual void Execute();

    void ClearMessages();
    virtual void ReceiveMessage(UMessage * message);
    UMessage* GetLastMessage() const;
    TArray<UMessage*> GetAllMessages() const;

    UFormation* GetFormation();

    void AddTargetPoint(AManeuverTargetPoint* maneuverTargetPoint);

    TArray<AManeuverTargetPoint*>& GetTargetPoints();
    void SetTargetPoints(TArray<AManeuverTargetPoint*> newTargetPoints);
    virtual void RemoveFirstTargetPoint();
    void EmptyTargetPoints();

    void SetJoueursVus(TArray<ABFPCharacterPlayable*>);
    TArray<ABFPCharacterPlayable*> GetJoueursVus();
    float GetVisionRange() const;
    float GetAgressionRange() const;
    float GetRushRange() const;
    ASquadDescriptor* GetDescriptor() const;
    void SetJoueurSelected(ABFPCharacterPlayable*);
    ABFPCharacterPlayable* GetJoueurSelected() const;
    void SetCurrentTaskBT(UBTTaskNode* node);
    int GetNbAttaquants() const;
    void SetNbAttaquants(int nb);

    void SetNoCommands();

	// Called when the game starts
	virtual void BeginPlay() override;

	virtual void PostInitProperties() override;

    void DetecterJoueurs();

    void MoveTo(const FTransform & positionOrientee, bool tryUseLieutenantNavMesh = true);

    void MoveToActor(AActor* actor);

    void FuirActor(AActor* actor);

    void OrienterTo(const FTransform & pointCible);

    void RushTo(const FTransform & positionOrientee, bool tryUseLieutenantNavMesh = true);

    void MoveToNextPoint();

    void Attaquer(ABFPCharacterPlayable* joueur);

    void EncerclerJoueur(class ABFPCharacter* character);

    bool DejaEncercle(class ABFPCharacter* character, float pourcentageDeCompletion, float distToPosMax);

    /* Renvoie les positions que doivent prendre les sources pour aller le plus rapidement possibles aux cibles.
     * Les positions sont ordonn�es dans le m�me ordre que les sources. */
    TArray<FVector> associerEfficacement(const TArray<FVector>& sources, const TArray<FVector>& cibles);

    // Envoie une commande de menace � un membre particulier
    void FaireMenacerMember(ASquadMemberController* member, ABFPCharacterPlayable* target);

    // Permet de repartir les sbires de la squad selon les ennemis les plus proches.
    // On peut ainsi les attaquer efficacement ! :)
    TMap<ABFPCharacterPlayable*, TArray<ASquadSbireController*>> RepartirSelonEnnemis(TArray<class ABFPCharacterPlayable*> ennemis);

	// Called every frame
	virtual void Tick(float DeltaTime) override;

   //void IsStopped();

   // /!\ /!\ Attention la section qui suit n'est pas une bonne pratique !
   // J'expose ici des variables en publique pour pouvoir y accéder rapidement et facilement dans les Noeuds du BT
   // Je fais ça car il y en a beaucoup x)
public:
    int nbCompleted = 0; // DON'T USE !
    int nbAttaquantsCourant = 0; // DON'T USE !
    bool lieutenantCompleted = false; // DON'T USE !
    int nbMemberCompleted = 0; // DON'T USE !
};
