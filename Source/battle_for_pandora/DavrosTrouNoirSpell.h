// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerCapsule.h"
#include "Engine/StaticMeshActor.h"
#include "Spell.h"
#include "DavrosTrouNoirSpell.generated.h"

UCLASS()
class BATTLE_FOR_PANDORA_API ADavrosTrouNoirSpell : public ASpell {
    GENERATED_BODY()

    /* La distance au centre du trou noir en dessous de laquelle les joueurs se font toucher ! */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float porteeInitiale = 200.0f;

    /* La taille initial du vortex relativement � la taille de son mesh. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float tailleInitiale = 1.0f;

    /* La taille finale du vortex relativement � la taille de son mesh. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float tailleFinale = 10.0f;

    /* La taille finale du vortex. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float dureeTrouNoir = 10.0f;
    float debutTrouNoir = 0.0f;

    /* La frequence de rafraichissement des degats du trou noir. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float frequenceRafraichissement = 0.5f;
    float lastTimeRafraichissement = 0.0f;

    /* Les degats � chaque rafraichissemet du trou noir. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float degats = 10.0f;

    /* La force avec laquelle on est attire vers le trou noir. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float forceAttraction = 600.0f;

public:
   ADavrosTrouNoirSpell(const FObjectInitializer & ObjectInitializer);

protected:
   virtual void BeginPlay() override;

public:
   virtual void Tick(float DeltaTime) override;

   float GetPorteeActuelle() const;

private:
    void SetSize();
    void TryRafraichir();
    void Rafraichir();
};