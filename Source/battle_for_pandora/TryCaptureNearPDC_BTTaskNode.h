// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "Base_BTTaskNode.h"
#include "Maneuver.h"
#include "TryCaptureNearPDC_BTTaskNode.generated.h"

UCLASS()
class BATTLE_FOR_PANDORA_API UTryCaptureNearPDC : public UBaseBTTaskNode
{
    GENERATED_BODY()

    /* La distance maximum jusqu'à laquelle le lieutenant peut decider d'aller prendre le PDC. */
    UPROPERTY(EditAnywhere, Category = "SensThinkAct")
    float distanceMaxToCapturePDC = 5000.0f; // Par default c'est la vision range ! :)
    /* La distance en dessous de laquelle on n'a pas besoin d'aller capturer le PDC car on est deja dessus ! :) */
    UPROPERTY(EditAnywhere, Category = "SensThinkAct")
    float distanceMinToCapturePDC = 350.0f;

public:
    UTryCaptureNearPDC(const FObjectInitializer & ObjectInitializer);

    /* Fonction d'ex�cution de la t�che, cette t�che devra retourner Succeeded, Failed ou InProgress */
    /* Sera appel�e au d�marrage de la t�che et devra retourner Succeeded, Failed ou InProgress */
    virtual EBTNodeResult::Type ExecuteTaskImpl(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, AManeuver* maneuvre) override;

    /** Permet de d�finir une description pour la t�che. C'est ce texte qui
    appara�tra dans le noeud que nous ajouterons au Behavior Tree */
    virtual FString GetStaticDescription() const override;

    /* Permet de recuperer les messages de la squad :) */
    virtual void OnMessage(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, FName Message, int32 RequestID, bool bSuccess) override;
};
