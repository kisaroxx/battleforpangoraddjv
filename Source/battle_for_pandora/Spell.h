// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerSphere.h"
#include "Engine/StaticMeshActor.h"
#include "Spell.generated.h"

UCLASS()
class BATTLE_FOR_PANDORA_API ASpell : public AStaticMeshActor
{
   GENERATED_BODY()

public:
   ASpell(const FObjectInitializer & ObjectInitializer);

   // Called when the game starts or when spawned
   virtual void BeginPlay() override;

   void DestroyIn(float seconds);

   UFUNCTION()
   void AutoDestroy();

   // Called every frame
   virtual void Tick(float DeltaTime) override;
};