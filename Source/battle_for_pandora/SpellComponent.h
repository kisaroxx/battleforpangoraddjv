// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SpellComponent.generated.h"

UENUM(BlueprintType)
enum class SpellState : uint8
{
    ACTIVATABLE,    // On peut activer le spell
    IN_COOLDOWN     // On ne peut pas l'activer
};


UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BATTLE_FOR_PANDORA_API USpellComponent : public UActorComponent
{
	GENERATED_BODY()
protected:

    /* Le Spell � d�clancher. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    TSubclassOf<class ASpell> spellClass;

    /* L'offset a appliquer lors du spawning du spell. Cet offset est relatif au forward du personnage. */
    /* Le X est utilise pour determiner la distance avec le forward. Le Z pour la hauteur. Le y n'est pas utilise. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    FVector offsetSpawning;

    /* Impulse au depart */
    UPROPERTY(EditAnywhere, Category = "Spell")
    FVector impulseSpawning;

    /* Le temps de cooldown du Spell en secondes. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float dureeCooldown = 10.0f;

    /* L'etat actuel du spell. */
    UPROPERTY(Replicated, Transient, VisibleAnywhere, Category = "Spell")
    SpellState spellState = SpellState::ACTIVATABLE;

    /* La derni�re fois que le spell a ete lance. */
    UPROPERTY(Replicated, Transient, VisibleAnywhere, Category = "Spell")
    float lastTimeCastSpell{};

    /* Utilise pour synchroniser les temps entre le serveur et le client. */
    UPROPERTY(Replicated, Transient, VisibleAnywhere, Category = "Spell")
    float currentTime{};

    /* L'ensemble des spells qui ont ete instancies avec ce SpellComponent. */
    UPROPERTY(VisibleAnywhere, Category = "Spell")
    TArray<class ASpell*> activatedSpells{};

    /* La classe du widget � afficher sur le HUD. */
    UPROPERTY(EditAnywhere, Category = "UI")
    TSubclassOf<class UUserWidget> spellWidgetClass;
    /* Le widget � afficher sur le HUD. */
    UPROPERTY(VisibleAnywhere, Category = "UI")
    class UUserWidget* spellWidget;
    /* L'indice auquel est associe le bouton du spell. */
    UPROPERTY(VisibleAnywhere, Category = "UI")
    int indiceBouton = 0;

public:	
	USpellComponent(const FObjectInitializer & ObjectInitializer);
	virtual void BeginPlay() override;
	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;
    virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

    /* Permet de savoir quel bouton est associe a ce spell et donc de savoir ou l'afficher !*/
    void SetIndiceInHUD(int indice);

    /* Pour savoir si l'on peut activer le Spell. */
    /* La methode n'est pas const car elle met � jour l'�tat spellState � chaque appel. */
    UFUNCTION()
    virtual bool IsActivable();

    /* Tente d'activer le spell si celui-ci est activatable, ne fait rien sinon. */
    UFUNCTION()
    void TryActivate();

    /* Affiche le widget sur le HUD. */
    virtual void DrawWidget();

	 void HideWidget();
	 void PositionnerWidget();
    float GetDureeCooldown() const;
    void SetDureeCooldown(float duree);
    float GetRemainingCooldown();
    SpellState GetSpellState() const;

protected:
  
    FString GetFStringCooldown();

    /* Cette fonction est appell�e quand le sort est r��llement d�clanch� ! :)*/
    virtual void OnActivateSpell(ASpell* spell);
private:
    void ApplyImpulseIfMeteor(ASpell* spell);
};
