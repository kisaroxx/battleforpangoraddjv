// Fill out your copyright notice in the Description page of Project Settings.

#include "SimpleAttackCommand.h"
#include "SquadMemberController.h"
#include "BFPCharacterNonPlayable.h"
#include "CommandReturn.h"

USimpleAttackCommand::USimpleAttackCommand() {

}

void USimpleAttackCommand::SetEnemy(ABFPCharacter* _char)
{
   enemy = _char;
}

ABFPCharacter* USimpleAttackCommand::GetEnemy()
{
   return enemy;
}

float USimpleAttackCommand::GetDistanceToEnemy() {
   return (enemy->GetActorLocation() - Cast<AController>(character)->GetPawn()->GetActorLocation()).Size();
}

UCommandReturn* USimpleAttackCommand::execute() {
   UCommandReturn* comRet = NewObject<UCommandReturn>(this, UCommandReturn::StaticClass());
   EPathFollowingRequestResult::Type resultatFromMove;

   float distanceToAttack = character->IsLieutenant() ? 400 : 200;
   if (GetDistanceToEnemy() < distanceToAttack) {
      Cast<ABFPCharacter>(Cast<AController>(character)->GetPawn())->SimpleAttack();
      comRet->SetAttackLaunched(true);
   }
   else {
       ABFPCharacterNonPlayable* sbire = Cast<ABFPCharacterNonPlayable>(character->GetPawn());
       if (IsValid(sbire)) {
           if (sbire->GetAttackState() != AttackState::ARMING) {
               character->StopMovement();
           }
           else {
               requete = FAIMoveRequest(enemy->GetActorLocation());
               resultatFromMove = character->MoveToSpiraled(requete);
               if (!(character->HasSquad())) {
                   character->StopMovement();
               }
               comRet->SetAttackLaunched(false);
           }
       }
   }
   comRet->setReturnSimpleAttack(resultatFromMove);
   return comRet;
}

