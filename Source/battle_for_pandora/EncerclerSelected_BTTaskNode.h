// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "Base_BTTaskNode.h"
#include "Maneuver.h"
#include "EncerclerSelected_BTTaskNode.generated.h"

UCLASS()
class BATTLE_FOR_PANDORA_API UEncerclerSelected : public UBaseBTTaskNode
{
    GENERATED_BODY()

public:
    UEncerclerSelected(const FObjectInitializer & ObjectInitializer);

    /* Fonction d'ex�cution de la t�che, cette t�che devra retourner Succeeded, Failed ou InProgress */
    /* Sera appel�e au d�marrage de la t�che et devra retourner Succeeded, Failed ou InProgress */
    virtual EBTNodeResult::Type ExecuteTaskImpl(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, AManeuver* maneuvre) override;

    /** Permet de d�finir une description pour la t�che. C'est ce texte qui
    appara�tra dans le noeud que nous ajouterons au Behavior Tree */
    virtual FString GetStaticDescription() const override;

    /* Permet de recuperer les messages de la squad :) */
    virtual void OnMessage(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, FName Message, int32 RequestID, bool bSuccess) override;
};
