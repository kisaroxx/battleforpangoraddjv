// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "BFPCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Components/CapsuleComponent.h"
#include "HealthComponent.h "
#include "Components/InputComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/World.h"
#include "Components/ProgressBar.h"
#include "Components/WidgetComponent.h"
#include "Blueprint/UserWidget.h"
#include "BFPCharacterPlayable.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Public/TimerManager.h"
#include "Components/SkeletalMeshComponent.h"
#include "Attaque.h"
#include "AttaqueComponent.h"
#include "SpellComponent.h"
#include "Logger.h"
#include "BFP_GameState.h"
#include "BFP_PlayerController.h"
#include "BFPHUD.h"
#include "Particles/ParticleSystemComponent.h"


//////////////////////////////////////////////////////////////////////////

ABFPCharacter::ABFPCharacter(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer)
{
	MY_LOG_GAME(TEXT("ABFPCharacter ABFPCharacter Character name : %s"), *this->GetName());
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	HealthSystem = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthSystem"));
	HealthSystem->ChangeHealthEvent.AddUFunction(this, "UpdateHealth");	
	HealthSystem->ChangeHealingStateEvent.AddUFunction(this, "HealingAction");

	//HealthSystem->RegisterComponent();
	/*if (IsValid(this->GetHealthSystem())) {
		MY_LOG_GAME(TEXT("ABFPCharacter ABFPCharacter HealthSystem valid"));
		MY_LOG_GAME(TEXT("ABFPCharacter ABFPCharacter HealthSystem owner : %s"), *HealthSystem->GetOwner()->GetName());
	}*/

	HealthBarTopHead = CreateDefaultSubobject<UWidgetComponent>(TEXT("HealthBarTopHead"));
	static ConstructorHelpers::FClassFinder<UUserWidget> widgetClass(TEXT("/Game/UI/Health/HealthBarInWorld"));
	HealthBarTopHead->SetWidgetClass(widgetClass.Class);

	CrossHairLock = CreateDefaultSubobject<UWidgetComponent>(TEXT("CrossHairLock"));
	static ConstructorHelpers::FClassFinder<UUserWidget> widgetClassLock(TEXT("/Game/UI/LockEnemy/LockCrossHair"));
	CrossHairLock->SetWidgetClass(widgetClassLock.Class);

	//nbMaximumAttaquesCombo = attaquesSimplesComponent.Num();

	//HUDWidgetClass->GetComponentTransform()->SetScale3D(FVector(0.5f, 0.5f, 0.5f));
	//class UUserWidget* test = Cast<widgetClass>(HUDWidgetClass->GetUserWidgetObject());

	static ConstructorHelpers::FObjectFinder<UParticleSystem> HealEffect(TEXT("/Game/FXVarietyPack/Particles/P_ky_healAura"));
	if (!ensure(IsValid(HealEffect.Object))) return;
	EffectHealCheckpoint = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("My Super ParticleSystem"));
	EffectHealCheckpoint->SetupAttachment(RootComponent);
	EffectHealCheckpoint->bAutoActivate = false;
	EffectHealCheckpoint->SetTemplate(HealEffect.Object);
}

void ABFPCharacter::PostInitializeComponents() {
	Super::PostInitializeComponents();
	if (GetWorld()->IsServer()) {
		HealthSystem->EmptyHealthEvent.AddUFunction(this, "Die");
	}
	/*HealthSystem = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthSystem"));
	HealthSystem->ChangeHealthEvent.AddUFunction(this, "UpdateHealth");*/	
}

void ABFPCharacter::OnClickLeft()
{
}

// Called when the game starts
void ABFPCharacter::BeginPlay()
{
	Super::BeginPlay();
	MY_LOG_GAME(TEXT("ABFPCharacter begiPlay Character name : %s"), *this->GetName());
	//affichage de la barre de vie au dessus du mesh
	//HealthBarTopHead->AttachTo(this->GetMesh());
	if (!IsRunningDedicatedServer()) {
		MY_LOG_GAME(TEXT("ABFPCharacter begiPlay IsNotRunningDedicatedServer"));
		HealthBarTopHead->AttachToComponent(this->GetMesh(), FAttachmentTransformRules::KeepWorldTransform);
		float z_mesh = this->GetMesh()->CalcBounds(this->GetMesh()->GetRelativeTransform()).BoxExtent.Z;
		HealthBarTopHead->SetRelativeLocation(FVector(0.0f, 0.0f, (z_mesh * 2) + offsetHUDInWorld));
		HealthBarTopHead->SetRelativeScale3D(FVector(0.5f, 0.5f, 0.5f));

		HealthBarTopHead->bOwnerNoSee = true;
		Widget = HealthBarTopHead->GetUserWidgetObject();
		ProgressBar = dynamic_cast<UProgressBar*>(Widget->GetWidgetFromName(FName("Health_Bar")));  // hpbar is the widget progress bar's name in Blueprint
		UpdateHealthBar(1.0);

		CrossHairLock->AttachToComponent(this->GetMesh(), FAttachmentTransformRules::KeepWorldTransform);
		CrossHairLock->SetRelativeLocation(FVector(0.0f, 0.0f, (z_mesh * 2) + offsetHUDInWorld));
		CrossHairLock->SetRelativeScale3D(FVector(2.0f, 2.0f, 2.0f));
		CrossHairLock->SetDrawAtDesiredSize(true);
		CrossHairLock->bHiddenInGame = true;
	}

	FVector worldLocation = GetRootComponent()->GetRelativeTransform().GetLocation();
	UE_LOG(MyLogUI, Warning, TEXT("worldpos a la construction = %f, %f"), worldLocation.X, worldLocation.Y);
	nbMaximumAttaquesCombo = attaquesSimplesComponentClass.Num();
	
	MY_LOG_GAME(TEXT("ABFPCharacter begiPlay HealthSystem valid : %u"), IsValid(HealthSystem));
	MY_LOG_GAME(TEXT("ABFPCharacter begiPlay end"));
}
#pragma optimize("", on)

void ABFPCharacter::PostInitProperties() {
	Super::PostInitProperties();

	// On initialise les attaquesComponents !
	for (int i = 0; i < attaquesSimplesComponentClass.Num(); i++) {
		if (IsValid(attaquesSimplesComponentClass[i])) {
			UAttaqueComponent * attaqueComponent = NewObject<UAttaqueComponent>(this, attaquesSimplesComponentClass[i].Get());
			attaquesSimplesComponent.Push(attaqueComponent);
		}
		else {
			DEBUG_SCREEN(-1, 5.f, FColor::Yellow, this->GetName().Append("  a son attaque simple nullptr !"));
		}
	}
	if (IsValid(attaqueLourdeComponentClass)) {
		attaqueLourdeComponent = NewObject<UAttaqueComponent>(this, attaqueLourdeComponentClass.Get());
	}
	else {
		//DEBUG_SCREEN(-1, 5.f, FColor::Yellow, this->GetName().Append("  a son attaque lourde nullptr !"));
		MY_LOG_GAME(TEXT("%s a son attaque lourde nullptr !"), *this->GetName());
	}
    if (GetNetMode() == ENetMode::NM_DedicatedServer || GetNetMode() == ENetMode::NM_ListenServer) { // IsServer
        // On initialise les spellComponents ! :)
        for (int i = 0; i < spellComponentsClass.Num(); i++) {
            if (IsValid(spellComponentsClass[i])) {
                USpellComponent * spellComponent = NewObject<USpellComponent>(this, spellComponentsClass[i].Get());
                spellComponents.Push(spellComponent);
                spellComponents.Last()->SetIndiceInHUD(spellComponents.Num() - 1);
            }
            else {
                DEBUG_SCREEN(-1, 5.f, FColor::Yellow, this->GetName().Append(" a un SpellComponentClass nullptr !"));
            }
        }
    }
}

// Called every frame
void ABFPCharacter::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

   // TEST UNIQUEMENT SI ON EST DANS LA GAME MAP C'est pas beau mais faut que ça marchegit status
   FString mapName = GetWorld()->GetMapName().Mid(GetWorld()->StreamingLevelsPrefix.Len());
   if (mapName == "GameMap_Final") {
      // TP si en dehors de la carte
      ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
      FVector pos = GetActorLocation();
      FVector posOnMap = { pos.X, pos.Y, gameState->landscapeHeights.getHeight(pos.X, pos.Y) };

      //Trop haut ou trop bas ou trop a gauche u trop a droite dans le plan vu de dessus
      if (pos.X > 28300 || pos.X < -28300 || pos.Y >28300 || pos.Y < -28300) {
         if (this->GetTransform().GetLocation().Z < -10000) {
            this->SetActorTransform(FTransform(this->GetTransform().GetRotation(),
               FVector(-6840, 22770, 840),
               this->GetTransform().GetScale3D()));
         }
      }
      else if ((pos - posOnMap).Z < -1000) {
         this->SetActorTransform(FTransform(this->GetTransform().GetRotation(),
            FVector(posOnMap.X, posOnMap.Y, posOnMap.Z + 1000),
            this->GetTransform().GetScale3D()));
      }
   }

	if (!IsRunningDedicatedServer()) {
		RotateWidget(GetHealthBarTopHead());
	}
	ApplyPoussees(DeltaTime);
}

void ABFPCharacter::OnRep_AttackState()
{
	if (attackState == AttackState::ACTIVATING) {
		if (attackType == AttackType::SIMPLE_ATTACK) {
			SimpleAttackActivate();
		}
		else {
			HeavyAttackActivate();
		}
	}
}

void ABFPCharacter::RotateWidget(UWidgetComponent *widget)
{
	FVector WidgetLocation = widget->GetComponentLocation();
	UWorld* world = GetWorld();
	APlayerController* FirstPlayer = world->GetFirstLocalPlayerFromController()->GetPlayerController(world);
	APlayerCameraManager* PCM = FirstPlayer->PlayerCameraManager;
	FVector FirstPlayerCameraLocation = PCM->GetCameraLocation();
	widget->SetWorldRotation(UKismetMathLibrary::FindLookAtRotation(WidgetLocation, FirstPlayerCameraLocation));
}

bool ABFPCharacter::IsAttacking() {
	return combatState == CombatState::ATTACK;
}

bool ABFPCharacter::IsHeavyAttacking() {
	return combatState == CombatState::ATTACK && attackType == AttackType::HEAVY_ATTACK;
}

void ABFPCharacter::Die() {
	DEBUG_SCREEN(-1, 5.f, FColor::Yellow, this->GetName().Append(" DIIIEDD !"));
	MY_LOG_NETWORK(TEXT("ABFPCharacter::Die begin "));
	//DiedFlag.Broadcast(this->CharacterAffiliation);

	combatState = CombatState::DEAD;
	poussees.Empty();

	MY_LOG_NETWORK(TEXT("ABFPCharacter::Die unregiter from squad "));
	UnRegisterToSquad();
	
	MY_LOG_NETWORK(TEXT("ABFPCharacter::Die unregiter from Manager "));
	UnRegisterToGameManager();

	// Désactiver les collisions !
	SetActorEnableCollision(false);

	// On declanche l'attaque apres un certain delais !
	FTimerHandle fuzeTimerHandle;
	FTimerDelegate timerDelegate;
	timerDelegate.BindUFunction(this, FName("DestroyCharacter"));
	GetWorld()->GetTimerManager().SetTimer(
		fuzeTimerHandle,
		timerDelegate,
		dureeDeath,
		false);
}

void ABFPCharacter::DestroyCharacter() {
	Destroy();
}

bool ABFPCharacter::IsDead() {
	return combatState == CombatState::DEAD;
}

bool ABFPCharacter::IsGhost() {
   return false;
}

bool ABFPCharacter::IsStun() {
   return bStunState;
}

void ABFPCharacter::UpdateHealthBar(float HealtPercentage)
{
	if (ProgressBar) {
		ProgressBar->SetPercent(HealtPercentage);
	}
}

void ABFPCharacter::MoveForward(float Value)
{
	if (combatState == CombatState::HIT || combatState == CombatState::DODGING || IsDead())
		return;

	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// On ralentit dans certaines situations
		if (combatState == CombatState::ATTACK || combatState == CombatState::BLOCKING)
			Value = Value / 2.0f;

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ABFPCharacter::MoveRight(float Value)
{
	if (combatState == CombatState::HIT || combatState == CombatState::DODGING || IsDead())
		return;

	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// On ralentit dans certaines situations
		if (combatState == CombatState::ATTACK || combatState == CombatState::BLOCKING)
			Value = Value / 2.0f;

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}



void ABFPCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    MY_LOG_UI(TEXT("CA REPLIQUE ABFPCharacter !!!"));
	DOREPLIFETIME(ABFPCharacter, combatState);
	DOREPLIFETIME(ABFPCharacter, attackState);
	DOREPLIFETIME(ABFPCharacter, attackType);
	DOREPLIFETIME(ABFPCharacter, numeroAttackInCombo);
	DOREPLIFETIME(ABFPCharacter, spellComponents);
	DOREPLIFETIME(ABFPCharacter, degatMultiplicateur);
	DOREPLIFETIME(ABFPCharacter, bStunState);
}

void ABFPCharacter::Run()
{
	// ce petit tric devrait permettre d'arreter le decalage sur le sprint
	GetCharacterMovement()->MaxWalkSpeed = this->RunSpeed;
	RunServer();
}

void ABFPCharacter::RunServer_Implementation()
{
	GetCharacterMovement()->MaxWalkSpeed = this->RunSpeed;
	movementState = MovementState::RUNNING;
	//DEBUG_SCREEN(-1, 5.f, FColor::Yellow, this->GetName().Append(" Begin Run !"));
}

bool ABFPCharacter::RunServer_Validate()
{
	return true;
}
void ABFPCharacter::StopRunning_Implementation()
{
	GetCharacterMovement()->MaxWalkSpeed = this->WalkSpeed;
	movementState = MovementState::WALKING;
	//DEBUG_SCREEN(-1, 5.f, FColor::Yellow, this->GetName().Append(" End Run !"));
}
bool ABFPCharacter::StopRunning_Validate()
{
	return true;
}

float ABFPCharacter::TakeDamage(float DamageAmount, float dureeHit, FPoussee poussee, bool shouldDeduceArmor, bool couldBeDodge)
{
	MY_LOG_IA(TEXT("ABFPCharacter TakeDamage"));
	// On regarde si le personnage est en train d'esquiper
	if (couldBeDodge && combatState == CombatState::DODGING) {
		return 0.0f; // si oui rien ne se passe !
	}

	// On applique la reduction de degats de l'armure !
	if (combatState == CombatState::BLOCKING && shouldDeduceArmor) {
		DamageAmount = DamageAmount * (1.0f - coefParade);
	}

	if (combatState != CombatState::BLOCKING && !IsDead()) { // Bloquer permet d'esquiver la destabilisation !
        if (dureeHit > 0.0f) {
            ReceiveAttackHit(dureeHit, poussee);
        }
	}

    // On fait les degats !
	//HealthSystem->TakeDamage(DamageAmount, GetWorld());
	//HealthSystem->StartDamage(DamageAmount, false, 0.1f, false);
    HealthSystem->Damage(DamageAmount);

	return DamageAmount;
}

float ABFPCharacter::TakeDamageFromSpell(float DamageAmount) {
   HealthSystem->Damage(DamageAmount);
   return DamageAmount;
}

float ABFPCharacter::TakeDamageFromSpellWithPousse(float DamageAmount, FPoussee poussee) {

   if (combatState != CombatState::BLOCKING && !IsDead()) {
         ReceiveAttackHit(0.1f, poussee);
   }

   HealthSystem->Damage(DamageAmount);

   return DamageAmount;
}


float ABFPCharacter::HealFromSpell(float healAmount) {
    HealthSystem->Heal(healAmount);
    return healAmount;
}

bool ABFPCharacter::CanCastSpell() const {
    return GetCombatState() != CombatState::HIT && GetCombatState() != CombatState::DEAD;
}

CombatState ABFPCharacter::GetCombatState() const {
	return combatState;
}
AttackState ABFPCharacter::GetAttackState() {
	return attackState;
}
AttackType ABFPCharacter::GetAttackType() {
   return attackType;
}
MovementState ABFPCharacter::GetMovementState() {
	return movementState;
}

float ABFPCharacter::GetDegatMultiplicateur()
{
    return degatMultiplicateur;
}

void ABFPCharacter::ModifyDegatMultiplicateur(float multiplicateur) {
    if (multiplicateur <= 0.0f) {
        MY_LOG_GAME(TEXT("ModifyDegatMultiplicateur a ete modifie avec une valeur non strictement positive !"));
    } else {
        degatMultiplicateur *= multiplicateur;
    }
}

int ABFPCharacter::GetNumeroAttackInCombo() {
	return numeroAttackInCombo;
}

int ABFPCharacter::GetNbMaxAtttackInCombo() {
	return nbMaximumAttaquesCombo;
}

void ABFPCharacter::AddAttackInCombo() {
	numeroAttackInCombo = (numeroAttackInCombo + 1) % nbMaximumAttaquesCombo;
}

int ABFPCharacter::PreviousAttackInCombo() {

   int numero_tmp = numeroAttackInCombo;
   if (Cast<ABFPCharacterPlayable>(this)) {
      numero_tmp = (numeroAttackInCombo + nbMaximumAttaquesCombo - 1) % nbMaximumAttaquesCombo;
   }
   return numero_tmp;
}

bool ABFPCharacter::IsSimpleAttackStartPossible() {
	// Si le personnage n'est pas dans l'etat idle, alors il ne peut pas attaquer !
	if (attackState == AttackState::DISARMING || IsDead()) {
		return false;
	}
	if (combatState != CombatState::IDLE
		&& combatState != CombatState::BLOCKING
		&& (combatState != CombatState::ATTACK || attackType == AttackType::HEAVY_ATTACK)) {
		putInInputBuffer(InputTypes::SIMPLE_ATTACK_INPUT);
		return false;
	}
   /*if (combatState == CombatState::HIT
      || combatState == CombatState::DODGING
      || (combatState == CombatState::ATTACK && attackType == AttackType::HEAVY_ATTACK)) {
      putInInputBuffer(InputTypes::SIMPLE_ATTACK_INPUT);
      return false;
   }*/
	return true;
}

void ABFPCharacter::SimpleAttack_Implementation() {
	if (IsSimpleAttackStartPossible()) {
		if (isAttacking) {
			saveAttack = true;
		}
		else {
			isAttacking = true;
			SimpleAttackStart();
		}
	}
}
bool ABFPCharacter::SimpleAttack_Validate()
{
	return true;
}

TArray<USpellComponent*> ABFPCharacter::GetSpellComponents() const
{
    return spellComponents;
}

void ABFPCharacter::SimpleAttackStart() {
	if (combatState == CombatState::BLOCKING) {
		BlockingStop();
	}

	//DEBUG_SCREEN(-1, 5.f, FColor::Yellow, this->GetName().Append(" : Attaque simple numéro ") + FString::FromInt(numeroAttackInCombo));

   // On change l'etat du personnage
	combatState = CombatState::ATTACK;
	attackState = AttackState::ARMING;
	attackType = AttackType::SIMPLE_ATTACK;

	// on lance l'animation sur les clients
	StartAttackAnim(attackType);

	// On declanche l'attaque apres un certain delais !
	FTimerDelegate timerDelegate;
   timerDelegate.BindUFunction(this, FName("SimpleAttackActivate"));
	GetWorld()->GetTimerManager().SetTimer(
      fuzeAttackTimerHandle,
      timerDelegate,
		attaquesSimplesComponent[PreviousAttackInCombo()]->GetDureeArmement(),
		false);
}

void ABFPCharacter::HeavyAttackStart_Implementation() {
	// Si le personnage est mort, on ne fait rien
	if (IsDead() || IsGhost() || !IsValid(GetController<ABFP_PlayerController>())) {
		return;
	}
	// Si le personnage n'est pas dans l'etat idle, alors il ne peut pas attaquer !
	if (combatState != CombatState::IDLE && combatState != CombatState::BLOCKING) {
		putInInputBuffer(InputTypes::HEAVY_ATTACK_INPUT);
		return;
	}

	if (combatState == CombatState::BLOCKING) {
		BlockingStop();
	}

	//DEBUG_SCREEN(-1, 5.f, FColor::Yellow, this->GetName().Append(" : Attaque LOURDE !"));

	// On change l'etat du personnage
	combatState = CombatState::ATTACK;
	attackState = AttackState::ARMING;
	attackType = AttackType::HEAVY_ATTACK;

	StartAttackAnim(attackType);

	// On declanche l'attaque apres un certain delais !
	FTimerDelegate timerDelegate;
   timerDelegate.BindUFunction(this, FName("HeavyAttackActivate"));
	GetWorld()->GetTimerManager().SetTimer(
      fuzeAttackTimerHandle,
      timerDelegate,
		attaqueLourdeComponent->GetDureeArmement(),
		false);
}
bool ABFPCharacter::HeavyAttackStart_Validate()
{
	return true;
}

void ABFPCharacter::SimpleAttackActivate() {
	//DEBUG_SCREEN(-1, 5.f, FColor::Green, this->GetName().Append(" attaque ACTIVATE !!!"));
	// On change l'etat du personage
	attackState = AttackState::ACTIVATING;
	// On va lancer une attaque !
	attaquesSimplesComponent[PreviousAttackInCombo()]->ActivateAttack(this, GetWorld());

	// On declanche l'attaque apres un certain delais !
	FTimerDelegate timerDelegate;
   timerDelegate.BindUFunction(this, FName("SimpleAttackCombo"));
	GetWorld()->GetTimerManager().SetTimer(
      fuzeAttackTimerHandle,
      timerDelegate,
		attaquesSimplesComponent[PreviousAttackInCombo()]->GetDureeActivation(),
		false);
}


void ABFPCharacter::HeavyAttackActivate() {
	//DEBUG_SCREEN(-1, 5.f, FColor::Green, this->GetName().Append(" attaque lourde ACTIVATE !!!"));

	// On change l'etat du personage
	attackState = AttackState::ACTIVATING;

	// On va lancer une attaque !
	attaqueLourdeComponent->ActivateAttack(this, GetWorld());

	// On declanche l'attaque apres un certain delais !
	FTimerDelegate timerDelegate;
   timerDelegate.BindUFunction(this, FName("HeavyAttackEnd"));
	GetWorld()->GetTimerManager().SetTimer(
      fuzeAttackTimerHandle,
      timerDelegate,
		attaqueLourdeComponent->GetDureeActivation(),
		false);

}

void ABFPCharacter::SimpleAttackCombo() {
	if (Cast<ABFPCharacterPlayable>(this)) {
		if (saveAttack) {
			//AddAttackInCombo();
			saveAttack = false;
			SimpleAttackStart();
		}
		else {
			SimpleAttackEnd();
		}
	}
	else {
		SimpleAttackEnd();
	}

}

void ABFPCharacter::SimpleAttackEnd() {
	//DEBUG_SCREEN(-1, 5.f, FColor::Red, this->GetName().Append(" fin de l'attaque !"));
	// On change l'etat du personage
	attackState = AttackState::DISARMING;

	// On arrete l'attaque apres un certain delais
	FTimerDelegate timerDelegate;
   timerDelegate.BindUFunction(this, FName("SetCombatStateIdle"));
	GetWorld()->GetTimerManager().SetTimer(
      fuzeAttackTimerHandle,
      timerDelegate,
		attaquesSimplesComponent[PreviousAttackInCombo()]->GetDureeDesarmement(),
		false);
}

void ABFPCharacter::HeavyAttackEnd() {
	//DEBUG_SCREEN(-1, 5.f, FColor::Red, this->GetName().Append(" Fin de l'attaque lourde!"));
	// On change l'etat du personage
	attackState = AttackState::DISARMING;

	// On arrete l'attaque apres un certain delais
	FTimerDelegate timerDelegate;
   timerDelegate.BindUFunction(this, FName("SetCombatStateIdle"));
	GetWorld()->GetTimerManager().SetTimer(
      fuzeAttackTimerHandle,
      timerDelegate,
		attaqueLourdeComponent->GetDureeDesarmement(),
		false);
}

void ABFPCharacter::SetCombatStateIdle() {
	if (!IsDead()) {
		//DEBUG_SCREEN(-1, 5.f, FColor::Black, this->GetName().Append(" set state !"));   
      //numeroAttackInCombo = (Cast<ABFPCharacterPlayable>(this)) ? 0 : numeroAttackInCombo;
		if (Cast<ABFPCharacterPlayable>(this)) {
			numeroAttackInCombo = 0;
		}
		else {
			AddAttackInCombo();
		}
		isAttacking = false;
		saveAttack = false;
		combatState = CombatState::IDLE;
		attackState = AttackState::ARMING; // Au cas où
		attackType = AttackType::SIMPLE_ATTACK;


		if (movementState == MovementState::WALKING)
			GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
		else if (movementState == MovementState::RUNNING)
			GetCharacterMovement()->MaxWalkSpeed = RunSpeed;
		GetCharacterMovement()->MaxAcceleration = accelerationDeBase;

		// Si il y a un event pas trop vieux dans le buffer, essayer de l'utiliser
		tryUseBufferInput();
	}
}

void ABFPCharacter::ReceiveAttackHit(float dureeHit, FPoussee poussee) {

   if (combatState == CombatState::ATTACK && GetWorld()->IsServer()) {
      GetWorld()->GetTimerManager().ClearTimer(fuzeAttackTimerHandle);
      GetWorld()->GetTimerManager().ClearTimer(fuzeAnimationTimerHandle);
      //GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
   }

	combatState = CombatState::HIT;
    GetCharacterMovement()->MaxWalkSpeed = 0; // Pour les sbires et le lieutenant
	attackState = AttackState::ARMING; // Au cas où
	isAttacking = false;
	saveAttack = false;

	// Supprimer toutes les attaques en cours
	CancelAllAttacks();

	// On declanche le flash
	MY_LOG_NETWORK(TEXT("ABFPCharacter ReceiveAttackHit"));
	if (IsValid(HealthSystem))
	{
		//HealthSystem->SetRedFlash();
	}
	else
	{
		DEBUG_SCREEN(-1, 5.f, FColor::Yellow, TEXT("HealthSystem invalid"));
	}

	// On rajoute la poussée
	poussee.Start(GetWorld());
    AddPoussee(poussee);

	FTimerHandle fuzeTimerHandle;
	GetWorld()->GetTimerManager().SetTimer(
		fuzeTimerHandle,
		this,
		&ABFPCharacter::SetCombatStateIdle,
		dureeHit,
		false);

	// On s'occupe du boolean bStunState
	bStunState = false;
	if (dureeHit > dureeAnimationHit) {
		// On le set dans dureeAnimationHit
		FTimerHandle fuzeTimerHandle;
		FTimerDelegate timerDelegate;
		timerDelegate.BindUFunction(this, FName("SetBStunState"), true);
		GetWorld()->GetTimerManager().SetTimer(
			fuzeTimerHandle,
			timerDelegate,
			dureeAnimationHit,
			false);

		// Puis on le unset dans dureeHit
		FTimerHandle fuzeTimerHandle2;
		FTimerDelegate timerDelegate2;
		timerDelegate2.BindUFunction(this, FName("SetBStunState"), false);
		GetWorld()->GetTimerManager().SetTimer(
			fuzeTimerHandle2,
			timerDelegate2,
			dureeHit,
			false);
	}
}

void ABFPCharacter::BlockingStart_Implementation() {
   // Si le personnage est mort, on ne fait rien
   if (IsDead() || IsGhost() || !IsValid(GetController<ABFP_PlayerController>())) {
      return;
   }
	if (combatState != CombatState::IDLE && combatState != CombatState::BLOCKING) {
		putInInputBuffer(InputTypes::BLOCK_START_INPUT);
		return;
	}

	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("On pare !"));

	// On change l'etat du personnage
	combatState = CombatState::BLOCKING;
	attackState = AttackState::ARMING;
}
bool ABFPCharacter::BlockingStart_Validate()
{
	//DEBUG_SCREEN(-1, 5.f, FColor::Yellow, "Parade Validated");
	return true;
}

void ABFPCharacter::BlockingStop_Implementation() {
	if (combatState != CombatState::BLOCKING) {
		putInInputBuffer(InputTypes::BLOCK_END_INPUT);
		return;
	}

	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("On arrete de parer"));

	// On change l'etat du personnage
	SetCombatStateIdle();
}
bool ABFPCharacter::BlockingStop_Validate()
{
	return true;
}

void ABFPCharacter::DodgingStart_Implementation() {
   // Si le personnage est mort, on ne fait rien
	if (UGameplayStatics::GetRealTimeSeconds(GetWorld()) - tempsFinRoulade < cooldownRoulade || IsDead() || IsGhost() || !IsValid(GetController<ABFP_PlayerController>())) {
		return;
	}
	if (combatState != CombatState::IDLE && combatState != CombatState::BLOCKING) {
		putInInputBuffer(InputTypes::DODGE_INPUT);
		return;
	}

	//DEBUG_SCREEN(-1, 5.f, FColor::Yellow, TEXT("On fait une roulade ! <3"));

	// On change l'etat du personnage
	combatState = CombatState::DODGING;
	attackState = AttackState::ARMING;
	GetCharacterMovement()->MaxWalkSpeed = vitesseRoulade;
	GetCharacterMovement()->MaxAcceleration = accelerationRoulade;

	// On retient les paramètres necessaires au bon fonctionnement de la roulade :)
	tempsDebutRoulade = UGameplayStatics::GetRealTimeSeconds(GetWorld());
	directionRoulade = GetTransform().GetRotation().GetForwardVector();

	ApplyDodging();
}
bool ABFPCharacter::DodgingStart_Validate()
{
	return true;
}

void ABFPCharacter::TryCastSpell_Implementation(int index) {
    spellComponents[index]->TryActivate();
}
bool ABFPCharacter::TryCastSpell_Validate(int index) {
    return true;
}


void ABFPCharacter::putInInputBuffer(InputTypes input) {
	// Si on avait un input prioritaire, on le garde a moins que ce soit un autre input prioritaire
	if (lastInput == InputTypes::BLOCK_START_INPUT || lastInput == InputTypes::DODGE_INPUT) { // Ces inputs sont prioritaires, ils ne peuvent etre effaces que par eux-memes !
		if (input == InputTypes::BLOCK_START_INPUT || input == InputTypes::DODGE_INPUT || input == InputTypes::BLOCK_END_INPUT) { // Ces inputs sont prioritaires, ils ne peuvent etre effaces que par eux-memes !
			if (input == InputTypes::BLOCK_END_INPUT) {
				DEBUG_SCREEN(-1, 5.f, FColor::Yellow, TEXT("On arrete de bloquer"));
			}
			lastInput = input;
			tempsLastInput = UGameplayStatics::GetRealTimeSeconds(GetWorld());
		}
	}
	else {
		lastInput = input;
		tempsLastInput = UGameplayStatics::GetRealTimeSeconds(GetWorld());
	}
}

bool ABFPCharacter::tryUseBufferInput() {
	if (lastInput == InputTypes::NO_INPUT) {
		return false;
	}

	float now = UGameplayStatics::GetRealTimeSeconds(GetWorld());
	if (lastInput == InputTypes::BLOCK_START_INPUT || now - tempsLastInput < dureeInputBuffer) { // la parade est infinie !
		switch (lastInput) {
		case InputTypes::SIMPLE_ATTACK_INPUT:
			SimpleAttack();
			break;
		case InputTypes::HEAVY_ATTACK_INPUT:
			HeavyAttackStart();
			break;
		case InputTypes::BLOCK_START_INPUT:
			BlockingStart();
			break;
		case InputTypes::DODGE_INPUT:
			DodgingStart();
			break;
		}
	}

	lastInput = InputTypes::NO_INPUT;
	return true;
}

void ABFPCharacter::ApplyDodging() {
	if (combatState != CombatState::DODGING) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("CE N'EST PAS NORMAL ON NE DOIT PAS ETRE ICI !!!"));
		return;
	}

    // On s'ajoute une poussee, puis on repasse en Idle à la fin de la roulade !
    FPoussee poussee{ vitesseRoulade, dureeRoulade };
    poussee.SetDirection(directionRoulade);
    AddPoussee(poussee);

    FTimerHandle handle;
    GetWorld()->GetTimerManager().SetTimer(handle, this, &ABFPCharacter::FinishDodging, dureeRoulade, false, -1.0f);

	//if (Controller != NULL) {
	//	AddMovementInput(directionRoulade, vitesseRoulade);
	//}

	//// Si il reste du temps, il faudra continuer à appliquer la roulade aux temps suivants !
	//float now = UGameplayStatics::GetRealTimeSeconds(GetWorld());
	//if (now - tempsDebutRoulade < dureeRoulade) {
	//	FTimerDelegate TimerDelegate;
	//	TimerDelegate.BindUFunction(this, FName("ApplyDodging"));
	//	GetWorld()->GetTimerManager().SetTimerForNextTick(TimerDelegate);
	//}
	//else {
	//	// Si on a enfin fini, on revient en Idel !
	//	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("fin de la roulade"));
	//	tempsFinRoulade = UGameplayStatics::GetRealTimeSeconds(GetWorld());
	//	SetCombatStateIdle();
	//}
}

void ABFPCharacter::FinishDodging() {
	tempsFinRoulade = UGameplayStatics::GetRealTimeSeconds(GetWorld());
    SetCombatStateIdle();
}

void ABFPCharacter::CancelAllAttacks() {
	for (int i = 0; i < attaquesSimplesComponent.Num(); i++) {
		attaquesSimplesComponent[i]->CancelAll();
	}
	if (IsValid(attaqueLourdeComponent))
		attaqueLourdeComponent->CancelAll();
}

void ABFPCharacter::RegisterToGameManager() {
}

void ABFPCharacter::RegisterToSquad() {
}

void ABFPCharacter::UnRegisterToGameManager() {
	GetWorld()->GetGameState<ABFP_GameState>()->UnRegister(this);
}
void ABFPCharacter::UnRegisterToSquad() {
}

void ABFPCharacter::SetBStunState(bool newState) {
	bStunState = newState;
}

void ABFPCharacter::ApplyPoussees(float deltaTime) {
	int n = poussees.Num();
	if (n == 0) return;
	UWorld* world = GetWorld();
	for (int i = 0; i < poussees.Num(); i++) {
		if (poussees[i].IsEnded(world)) {
			poussees.RemoveAt(i);
			i--;
		}
		else {
			poussees[i].ApplyPoussee(this, deltaTime);
		}
	}
}

void ABFPCharacter::AddPoussee(FPoussee poussee) {
    poussee.Start(GetWorld());
    poussees.Push(poussee);
}

void ABFPCharacter::UpdatePlayrateAnimation(UAttaqueComponent* attaqueComponent, AttackState attackState) {
	float tempsAnim, tempsVoulu;
	switch (attackState) {
	case AttackState::ARMING:
		tempsAnim = attaqueComponent->GetDureeArmementAnim();
		tempsVoulu = attaqueComponent->GetDureeArmement();
		break;
	case AttackState::ACTIVATING:
		tempsAnim = attaqueComponent->GetDureeActivationAnim();
		tempsVoulu = attaqueComponent->GetDureeActivation();
		break;
	case AttackState::DISARMING:
		tempsAnim = attaqueComponent->GetDureeDesarmementAnim();
		tempsVoulu = attaqueComponent->GetDureeDesarmement();
		break;
	default:
		break;
	}

	playrateAttack = 1.0f / (tempsVoulu / tempsAnim);

	MY_LOG_GAME(TEXT("ABFPCharacter UpdatePlayrateAnimation tempsAnim %f"), tempsAnim);
	MY_LOG_GAME(TEXT("ABFPCharacter UpdatePlayrateAnimation tempsVoulu %f"), tempsVoulu);
	MY_LOG_GAME(TEXT("ABFPCharacter UpdatePlayrateAnimation playrate %f"), playrateAttack);

	UpdatePlayrateFlag.Broadcast(playrateAttack);
}

void ABFPCharacter::UpdateHealth() {	
	MY_LOG_NETWORK(TEXT("ABFPCharacter UpdateHealth healthsystem valid %u"), IsValid(HealthSystem));
	UpdateHealthBar(HealthSystem->GetHealthPercentage());
}

void ABFPCharacter::HealingAction() {
	if (HealthSystem->IsHealed()) {
		DEBUG_SCREEN(-1, 5.f, FColor::Yellow, "ABFPCharacter HealingAction activate healing");
		MY_LOG_NETWORK(TEXT("ABFPCharacter HealingAction activate healing"));
		EffectHealCheckpoint->ActivateSystem();
	}
	else {
		DEBUG_SCREEN(-1, 5.f, FColor::Yellow, "ABFPCharacter HealingAction deactivate healing");
		MY_LOG_NETWORK(TEXT("ABFPCharacter HealingAction deactivate healing"));
		EffectHealCheckpoint->DeactivateSystem();
	}
}


void ABFPCharacter::StartAttackAnim_Implementation(const AttackType typeAttack) {
	// On supprime tous les timers
   GetWorld()->GetTimerManager().ClearTimer(fuzeAttackTimerHandle);
   GetWorld()->GetTimerManager().ClearTimer(fuzeAnimationTimerHandle);
	//GetWorld()->GetTimerManager().ClearAllTimersForObject(this);

	switch (typeAttack) {
	case AttackType::SIMPLE_ATTACK: SimpleAttackStartAnim(); break;
	case AttackType::HEAVY_ATTACK: HeavyAttackStartAnim(); break;
	default:break;
	}
}

bool ABFPCharacter::StartAttackAnim_Validate(const AttackType typeAttack) {
	return true;
}

void ABFPCharacter::SimpleAttackStartAnim() {
	UpdatePlayrateAnimation(attaquesSimplesComponent[numeroAttackInCombo], AttackState::ARMING);
	SimpleAttackFlag.Broadcast();
   if (Cast<ABFPCharacterPlayable>(this)) {
      AddAttackInCombo();
   }
	// On declanche l'attaque apres un certain delais !
	FTimerDelegate timerDelegate;
	timerDelegate.BindUFunction(this, FName("SimpleAttackActivateAnim"));
	GetWorld()->GetTimerManager().SetTimer(
      fuzeAnimationTimerHandle,
		timerDelegate,
		attaquesSimplesComponent[PreviousAttackInCombo()]->GetDureeArmement(),
		false);
}

void ABFPCharacter::HeavyAttackStartAnim() {
	UpdatePlayrateAnimation(attaqueLourdeComponent, AttackState::ARMING);
	HeavyAttackFlag.Broadcast();

	// On declanche l'attaque apres un certain delais !
	FTimerDelegate timerDelegate;
	timerDelegate.BindUFunction(this, FName("HeavyAttackActivateAnim"));
	GetWorld()->GetTimerManager().SetTimer(
      fuzeAnimationTimerHandle,
		timerDelegate,
		attaqueLourdeComponent->GetDureeArmement(),
		false);
}

void ABFPCharacter::SimpleAttackActivateAnim() {
	UpdatePlayrateAnimation(attaquesSimplesComponent[PreviousAttackInCombo()], AttackState::ACTIVATING);
	// On declanche l'attaque apres un certain delais !
	FTimerDelegate timerDelegate;
	timerDelegate.BindUFunction(this, FName("SimpleAttackEndAnim"));
	GetWorld()->GetTimerManager().SetTimer(
      fuzeAnimationTimerHandle,
		timerDelegate,
		attaquesSimplesComponent[PreviousAttackInCombo()]->GetDureeActivation(),
		false);
}

void ABFPCharacter::HeavyAttackActivateAnim() {
	UpdatePlayrateAnimation(attaqueLourdeComponent, AttackState::ACTIVATING);

	// On declanche l'attaque apres un certain delais !
	FTimerDelegate timerDelegate;
	timerDelegate.BindUFunction(this, FName("HeavyAttackEndAnim"));
	GetWorld()->GetTimerManager().SetTimer(
      fuzeAnimationTimerHandle,
		timerDelegate,
		attaqueLourdeComponent->GetDureeActivation(),
		false);
}

void ABFPCharacter::SimpleAttackEndAnim() {
	UpdatePlayrateAnimation(attaquesSimplesComponent[PreviousAttackInCombo()], AttackState::DISARMING);
}

void ABFPCharacter::HeavyAttackEndAnim() {
	UpdatePlayrateAnimation(attaqueLourdeComponent, AttackState::DISARMING);
}