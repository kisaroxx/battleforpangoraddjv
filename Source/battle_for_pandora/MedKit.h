// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BFPCharacter.h"
#include "Components/StaticMeshComponent.h"
#include "MedKit.generated.h"

UCLASS()
class BATTLE_FOR_PANDORA_API AMedKit : public AActor
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	AMedKit(const FObjectInitializer & ObjectInitializer);
	void PostInitializeComponents() override;

	UFUNCTION()
	void OnOverlap(AActor* MyOverlappedActor, AActor* OtherActor);

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* StaticMeshComponent;

	void SetPoucentageSoin(float valeurSoin);

private:

	UPROPERTY(EditAnywhere)
	float ratioMin;
	UPROPERTY(EditAnywhere)
	float ratioMax;

	float coeff;
	float valeurOrigine;

	UPROPERTY(EditAnywhere)
	float pourcentageSoinMin;
	UPROPERTY(EditAnywhere)
	float pourcentageSoinMax;

	UPROPERTY(EditAnywhere)
	float pourcentageSoin;

	UPROPERTY(EditAnywhere)
	float ratio;

	UPROPERTY(EditAnywhere)
	float pourcentageSoinParDefaut;

	UPROPERTY(EditAnywhere)
	float ratioParDefaut;

};
