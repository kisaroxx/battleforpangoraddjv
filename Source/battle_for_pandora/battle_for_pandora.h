// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/GameplayStatics.h"

//Log general
DECLARE_LOG_CATEGORY_EXTERN(MyLog, Log, All);

//Log IA
DECLARE_LOG_CATEGORY_EXTERN(MyLogIA, Log, All);

//Log UI
DECLARE_LOG_CATEGORY_EXTERN(MyLogUI, Log, All);

//Log Network
DECLARE_LOG_CATEGORY_EXTERN(MyLogNetwork, Log, All);

//Log Render
DECLARE_LOG_CATEGORY_EXTERN(MyLogRender, Log, All);

//Log Gameplay
DECLARE_LOG_CATEGORY_EXTERN(MyLogGame, Log, All);

//Log Son
DECLARE_LOG_CATEGORY_EXTERN(MyLogSon, Log, All);
