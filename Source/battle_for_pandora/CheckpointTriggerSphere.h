// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core.h"
#include "Engine/TriggerSphere.h"
#include "BFPCharacter.h"
#include "GameFramework/Actor.h"
#include "Net/UnrealNetwork.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "Runtime/Engine/Classes/Materials/Material.h"
#include "Runtime/Core/Public/Math/BasicMathExpressionEvaluator.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "SquadDescriptor.h"
#include "CheckpointTriggerSphere.generated.h"


UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class ECheckpointState : uint8
{
    CS_NEUTRAL 		UMETA(DisplayName = "Neutral"),
    CS_CAPTURING 	UMETA(DisplayName = "Capturing"),
    CS_CONTESTED	UMETA(DisplayName = "Contested"),
    CS_HELD			UMETA(DisplayName = "Held ")
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FCaptured, Affiliation, CAffiliationOld, Affiliation, CAffiliationNew);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSoundDelegate);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FStateChange, Affiliation, CAffiliationOld, ECheckpointState, _CheckpointState, Affiliation, CAffiliationCapturingTeam);
/**
 *
 */
UCLASS()
class BATTLE_FOR_PANDORA_API ACheckpointTriggerSphere : public ATriggerSphere
{
    GENERATED_BODY()

    /* Utilise pour definir les couleurs des checkpoints ! :) */
    static TArray<FColor> COULEURS_BANK;
    static int indiceCouleur;

    /* La couleur representant le tandem Checkpoint/Squad ! :) */
    UPROPERTY(Replicated, VisibleAnywhere, Category = "Squad")
    FColor couleurSquadCheckpoint;

    UPROPERTY(EditAnywhere, Category = "Squad")
    TSubclassOf<ASquadDescriptor> squadClass;

    UPROPERTY(VisibleAnywhere, Category = "Squad")
    ASquadDescriptor* squadAssociee;

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;
	virtual void PostInitializeComponents() override;

    UPROPERTY(ReplicatedUsing = OnRep_State, EditAnywhere, BlueprintReadWrite, Category = Enum)
        ECheckpointState CurrentCheckpointState;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Enum)
        ECheckpointState OldCheckpointState;
    UPROPERTY(ReplicatedUsing = OnRep_Affiliation, EditAnywhere, BlueprintReadWrite, Category = Enum)
        Affiliation CurrentCheckpointAffiliation;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Enum)
        Affiliation OldCheckpointAffiliation;
    UPROPERTY(VisibleAnywhere)
        UStaticMeshComponent* StaticMeshComponent;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CheckpointMesh)
        UMaterial* MaterialEvil;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CheckpointMesh)
        UMaterial* MaterialNeutral;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CheckpointMesh)
        UMaterial* MaterialContest;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CheckpointMesh)
        UMaterial* MaterialGood;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CheckpointCaptureVariables)
	float capturePercentage = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CheckpointCaptureVariables)
	float captureRate = 10.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CheckpointCaptureVariables)
	float captureBonus = 1.12f;
   UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CheckpointCaptureVariables)
      float captureSpeed = 0.1f;


    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CheckpointCaptureVariables)
        TArray<AActor*> ListeActorOnCheckpoint;
    TMap<Affiliation, uint32> charOnCheckpoint;
    TMap<Affiliation, UMaterial*> affiliationMaterial;

    UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = CheckpointCaptureVariables)
    Affiliation capturingTeam;
    bool isTimerPaused = false;
    /*
    What I will need :
    - A Capture percentage
    - A Capture rate (List of capture rates ?)
    - A timer that multiply the percentage by the rate everysecond
    - To Increase the timer rate
    */

public:
	UPROPERTY(BlueprintAssignable, Category = "EventOnRun")
	FCaptured CapturedFlag;
   UPROPERTY(BlueprintAssignable, Category = "EventOnRun")
   FSoundDelegate SoundFlagGood;
   UPROPERTY(BlueprintAssignable, Category = "EventOnRun")
   FSoundDelegate SoundFlagEvil;

   UPROPERTY(BlueprintAssignable, Category = "EventOnRun")
      FStateChange StateChange;

	ACheckpointTriggerSphere(const FObjectInitializer & ObjectInitializer);

    void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;

    // declare overlap begin function
    // overlap begin function
    UFUNCTION(BlueprintCallable)
        void OnOverlapBegin(class AActor* OverlappedActor, class AActor* OtherActor);

    // overlap end function
    UFUNCTION(BlueprintCallable)
        void OnOverlapEnd(class AActor* OverlappedActor, class AActor* OtherActor);


	// Heroes
	void Tick(float DeltaTime);

    int32 CountdownTime;
    FTimerHandle CountdownTimerHandle;

    void InitiateTimer();

	void setCheckpointAffiliation(Affiliation EnumValue);
   void captureCheckpoint(Affiliation EnumValue);

    void setMeshMaterial(UMaterial* Material);
    void setMeshMaterial(Affiliation EnumValue);
    void setMeshMaterial(ECheckpointState EnumValue);

    void setCheckpointState(ECheckpointState EnumValue);
    void InitiateCapture(Affiliation EnumValue);
    void ResetCapture();
    void InitiateContest();
    void ReleaseContest();
    void KeepCapturing(Affiliation EnumValue);
    void KeepCapturing();
    void KeepContesting();
    void UpdateTimerDisplay();
    void AdvanceTimer();
    void CountdownHasFinished();
    void CountdownHasBeenInterupted();

    float getCapturePercentage();
    float getCaptureRate();
    float getCaptureBonus();

    Affiliation getCheckpointAffiliation();

    bool isThereSomeoneOnTheCheckpoint();
    bool isThereTwoFactionsOnTheCheckpoint();
    bool isThereEvilOnTheCheckpoint();
    bool isThereGoodOnTheCheckpoint();
    bool isFactionOnCheckpoint(Affiliation EnumValue);
    bool wasFactionCapturing(Affiliation EnumValue);
    bool isFactionCapturing(Affiliation EnumValue);
    bool isOwnedByFaction(Affiliation EnumValue);

    UFUNCTION()
        void OnRep_Affiliation();
    UFUNCTION()
        void OnRep_State();

    void UnlinkFromSquad();

    FColor GetColor() const;
    ASquadDescriptor* GetSquadAssociee() const;

private:
    void SpawnerAssociatedSquad();
    void RecallAssociatedSquad();
    void StopRecallAssociatedSquad();

    TMap<ABFPCharacterPlayable*, FTimerHandle> handles;
	void StartHealing(ABFPCharacterPlayable* playable);
	void StopHealing(ABFPCharacterPlayable* playable);
};