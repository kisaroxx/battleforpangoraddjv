// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "Logger.h"
#include "Network/Gameplay/GameplayGM.h"
#include "BFPCharacter.h"
#include "BFPCharacterPlayable.h"
#include "BFP_PlayerState.h"
#include "CheckpointTriggerSphere.h"
#include "LandscapeHeights.h"
#include "BFP_GameState.generated.h"

/**
 *
 */
UCLASS()
class BATTLE_FOR_PANDORA_API ABFP_GameState : public AGameState
{
	GENERATED_BODY()

	/* Donnees Static */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "GameState", meta = (AllowPrivateAccess = "true"))
	uint8 MAX_CONTROL_POINTS_COUNT = 9;

	/* Les informations sur L'etat general du jeu */
	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "GameState", meta = (AllowPrivateAccess = "true"))
	uint8 HerosCount = 0;
	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "GameState", meta = (AllowPrivateAccess = "true"))
	uint8 FiendCount = 0;
	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "GameState", meta = (AllowPrivateAccess = "true"))
	uint8 ControlPointsCount = 0;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "GameState", meta = (AllowPrivateAccess = "true"))
	uint8 ControlPointsToVictory = 6;

public:

   LandscapeHeights landscapeHeights{};

   UPROPERTY(ReplicatedUsing = OnRep_UpdateHUD, VisibleAnywhere, BlueprintReadOnly, Category = "Unites", meta = (AllowPrivateAccess = "true"))
   uint8 nbJoueurs;
   UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "Unites", meta = (AllowPrivateAccess = "true"))
   TArray<class ABFP_PlayerController*> AllPlayerControllers;

private:

	/* Les sbires. */
	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "Unites", meta = (AllowPrivateAccess = "true"))
	TArray<class ABFPCharacterSbire*> sbires;
	/* Les lieutenants. */
	UPROPERTY(ReplicatedUsing = OnRep_UpdateHUD, VisibleAnywhere, BlueprintReadOnly, Category = "Unites", meta = (AllowPrivateAccess = "true"))
	TArray<class ABFPCharacterLieutenant*> lieutenants;
	/* Les joueurs allies. */
	UPROPERTY(ReplicatedUsing = OnRep_UpdateHUD, VisibleAnywhere, BlueprintReadOnly, Category = "Unites", meta = (AllowPrivateAccess = "true"))
	TArray<class ABFPCharacterPlayable*> joueursAllies; // Attention, cela n'inclu PAS Davros !
	/* Davros. */
	UPROPERTY(ReplicatedUsing = OnRep_UpdateHUD, VisibleAnywhere, BlueprintReadOnly, Category = "Unites", meta = (AllowPrivateAccess = "true"))
	class ABFPCharacterPlayable* davros; // Il est la :)
   /* Les Squads */
   UPROPERTY(ReplicatedUsing = OnRep_UpdateHUDSquads, VisibleAnywhere, BlueprintReadOnly, Category = "Unites", meta = (AllowPrivateAccess = "true"))
   TArray<class ASquadDescriptor*> squads;
   /* Les CP. */
   UPROPERTY(ReplicatedUsing = OnRep_UpdateHUDCheckpoints, VisibleAnywhere, BlueprintReadOnly, Category = "CP", meta = (AllowPrivateAccess = "true"))
   TArray<class ACheckpointTriggerSphere*> checkpoints;

   UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "Unites", meta = (AllowPrivateAccess = "true"))
	Affiliation etatForces = Affiliation::NEUTRAL;

public:
	ABFP_GameState(const FObjectInitializer & ObjectInitializer);

   void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void BeginPlay() override;

	UFUNCTION()
	void ChangePlayerCount(Affiliation aff);
	UFUNCTION()
	void CheckIfEndOfGame();

   UFUNCTION()
   virtual void OnRep_UpdateHUD();
   UFUNCTION()
   virtual void OnRep_UpdateHUDSquads();
   UFUNCTION()
   virtual void OnRep_UpdateHUDCheckpoints();

	UFUNCTION()
	void AddCheckpointListener(ACheckpointTriggerSphere* checkpoint);
	UFUNCTION()
	void AddPlayer(ABFPCharacter* character);
	UFUNCTION()
	void CheckpointStateChange(Affiliation affOld, Affiliation affNew);
	
	UFUNCTION()
	void SetEndOfGame(Affiliation aff);

	// S'enregistrer quand on est cree et se desenregistrer quand on meurt
	void Register(ABFPCharacterSbire*);
	void Register(ABFPCharacterLieutenant*);
	void Register(ABFPCharacterPlayable*);
   void Register(ASquadDescriptor*);
   void ReRegister(ABFPCharacterPlayable*);
   void Register(ACheckpointTriggerSphere *CP);
	void UnRegister(class ABFPCharacter*); // On est pas suppose appeler cette methode !
	void UnRegister(ABFPCharacterSbire*);
	void UnRegister(ABFPCharacterLieutenant*);
   void UnRegister(ABFPCharacterPlayable*);
	void UnRegister(ASquadDescriptor*);

	TArray<ABFPCharacterSbire*> GetAllSbires() const;
	TArray<ABFPCharacterLieutenant*> GetAllLieutenants() const;
	TArray<ABFPCharacterPlayable*> GetAllJoueursAllies() const;
   TArray<ASquadDescriptor*> GetAllSquads() const;
	ABFPCharacterPlayable* GetDavros() const;
	TArray<ABFPCharacter*> GetAllMechant() const ;
	TArray<ACheckpointTriggerSphere*> GetAllCPs() const ;

	// Get nb
	int GetNbSbires() const;
	int GetNbLieutenants() const;
	int GetNbJoueursAllies() const;

	// Recupere les nbVoulu characters les plus proches de la source.
	template<class Char>
	TArray<Char*> GetAllNearestFrom(TArray<Char*> characters, const FTransform & source, int nbVoulu) const {
		int k = nbVoulu;
		int n = characters.Num();
		if (n <= k) return characters;

		// Pr�dicat indiquant qu'il faut trier selon la distance au point !
		struct FSortByDistance {
			FSortByDistance(const FVector& InSourceLocation) : SourceLocation(InSourceLocation) {}

			/* The Location to use in our Sort comparision. */
			FVector SourceLocation;

			bool operator()(const AActor& A, const AActor& B) const {
				// On essaye de mettre le non valide le plus vers la fin
				bool isValidA = IsValid(&A);
				bool isValidB = IsValid(&B);
				if (!isValidA || !isValidB) {
					return isValidA;
				}
				float DistanceA = FVector::DistSquared(SourceLocation, A.GetActorLocation()); // DistSquared plus efficace car on utilise pas la racine !
				float DistanceB = FVector::DistSquared(SourceLocation, B.GetActorLocation()); // DistSquared plus efficace car on utilise pas la racine !
				return DistanceA < DistanceB; // On veut les plus proches en premier !
			}
		};

		// On trie
		TArray<Char*> res = characters;
		res.Sort(FSortByDistance(source.GetLocation()));

        TArray<float> distances;
        for (int i = 0; i < res.Num(); i++) {
            AActor* a = Cast<AActor>(res[i]);
            distances.Add(FVector::Dist(source.GetLocation(), a->GetActorLocation()));
        }

		// On veut les k premiers !
		for (int i = 0; i < (n - k); i++)
			res.Pop();

		return res;
	}
	// Recupere les nbVoulu characters les plus proches de la source mais pour un controlleur cette fois
	template<class Controller>
	TArray<Controller*> GetAllNearestFromController(TArray<Controller*> characters, const FTransform & source, int nbVoulu) const {
		int k = nbVoulu;
		int n = characters.Num();
		if (n <= k) return characters;

		// Pr�dicat indiquant qu'il faut trier selon la distance au point !
		struct FSortByDistance {
			FSortByDistance(const FVector& InSourceLocation) : SourceLocation(InSourceLocation) {}

			/* The Location to use in our Sort comparision. */
			FVector SourceLocation;

			bool operator()(const AController& A, const AController& B) const {
				float DistanceA = FVector::DistSquared(SourceLocation, A.GetPawn()->GetActorLocation()); // DistSquared plus efficace car on utilise pas la racine !
				float DistanceB = FVector::DistSquared(SourceLocation, B.GetPawn()->GetActorLocation()); // DistSquared plus efficace car on utilise pas la racine !
				return DistanceA < DistanceB; // On veut les plus proches en premier !
			}
		};

		// On trie
		TArray<Controller*> res = characters;
		res.Sort(FSortByDistance(source.GetLocation()));

        TArray<float> distances;
        for (int i = 0; i < res.Num(); i++) {
            AController* a = Cast<AController>(res[i]);
            distances.Add(FVector::Dist(source.GetLocation(), a->GetPawn()->GetActorLocation()));
        }

		// On veut les k premiers !
		for (int i = 0; i < (n - k); i++)
			res.Pop();

		return res;
	}

	// R�cup�re tous les characters � moins de distance de la source.
	template<class Char>
	TArray<Char*> GetAllInDistanceFrom(TArray<Char*> characters, const FTransform & source, float distance) const {
		int n = characters.Num();

		TArray<Char*> res{};
		
		for (int i = 0; i < n; i++) {
				// On veut juste les plus proches !

			Char* character = characters[i];
			//MY_LOG_NETWORK(TEXT("ABFP_GameState GetAllInDistanceFrom - character ? %u"), character != nullptr);
			if (IsValid(character)) {
				float d = FVector::DistSquared(character->GetActorLocation(), source.GetLocation());
				if (d <= distance * distance) {
					res.Push(character);
				}
			}
		}

		return res;
	}

private:
    void InitAffiliationsCheckpoints();

};
