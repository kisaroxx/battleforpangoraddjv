// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BFPCharacter.h"
#include "BFPCharacterNonPlayable.generated.h"

/**
 * 
 */
UCLASS()
class BATTLE_FOR_PANDORA_API ABFPCharacterNonPlayable : public ABFPCharacter
{
   GENERATED_BODY()

protected:
   UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "IA", meta = (AllowPrivateAccess = "true"))
   class ASquadDescriptor* squad;

public:
   ABFPCharacterNonPlayable(const FObjectInitializer & ObjectInitializer);

   void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;

   UFUNCTION()
   void SetSquad(class ASquadDescriptor* _squad);

   UFUNCTION()
   class ASquadDescriptor* GetSquad();

   UFUNCTION()
   bool HasSquad();

   virtual void UnRegisterToSquad() override;

   virtual void RegisterToSquad() override;

   
};
