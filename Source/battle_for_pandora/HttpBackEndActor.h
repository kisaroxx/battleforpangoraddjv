// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "BackEndSaveGame.h"
#include "GameFramework/Actor.h"
#include "HttpBackEndActor.generated.h"

UCLASS()
class BATTLE_FOR_PANDORA_API AHttpBackEndActor : public AActor
{
   GENERATED_BODY()

protected:
   const FString adress = "http://inf714-h201902-env.sa4t9cj8pp.us-west-2.elasticbeanstalk.com/";
   bool bLoggedIn = false;
   short requestCount = 0;
public:
   FHttpModule* Http;
   UPROPERTY(BlueprintReadWrite, Category = Basic)
      UBackEndSaveGame* mySavedGame;
   UPROPERTY(BlueprintReadWrite, Category = Basic)
      UBackEndSaveGame* SaveGameInstance;

   /* Invite Creation Request */
   UFUNCTION(BlueprintCallable)
      void InviteAccountCreateRequest();
   void OnInviteAccountCreateResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

   /* Session Creation Request */
   UFUNCTION(BlueprintCallable)
      void SessionCreateRequest();
   void OnSessionCreateResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

   /* Session Refresh Request */
   UFUNCTION(BlueprintCallable)
      void SessionRefreshRequest();
   void OnSessionRefreshResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

   /* Profile Information Request */
   UFUNCTION(BlueprintCallable)
      void ProfileRequest();
   void OnProfileRequestResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

   /* Nickname Update Request */
   UFUNCTION(BlueprintCallable)
      void NicknameEditRequest(const FString playerName);
   void OnNicknameEditResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

   /* Nickname Update Request */
   UFUNCTION(BlueprintCallable)
      void UpdateWinsRequest();
   void OnUpdateWinsRequestResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

   /* Nickname Update Request */
   UFUNCTION(BlueprintCallable)
      void UpdateLossRequest();
   void OnUpdateLossRequestResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

   bool isLoggedIn() { return bLoggedIn; }
public:
   // Sets default values for this actor's properties
   AHttpBackEndActor(const class FObjectInitializer& ObjectInitializer);

protected:
   // Called when the game starts or when spawned
   virtual void BeginPlay() override;

public:
   // Called every frame
   virtual void Tick(float DeltaTime) override;

   void LoadSaveFile();

};
