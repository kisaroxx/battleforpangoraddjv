#include "LandscapeHeights.h"
#include <fstream>
#include <iostream>
#include <algorithm>
#include <Landscape/Classes/Landscape.h>
using namespace std;


LandscapeHeights::LandscapeHeights()
{
   landscapeHeights.reserve(nbHeights);
}

LandscapeHeights::~LandscapeHeights()
{
}

//***** ECRITURE ******

void LandscapeHeights::generateLandscapeHeights(UWorld* world) {
   for (float y = -demiSizeLandscape ; y <= demiSizeLandscape ; y = y + pas) {
      for (float x = -demiSizeLandscape ; x <= demiSizeLandscape ; x = x + pas) {
         FHitResult res;
         FVector start = FVector{ x , y, -32767.0f };
         FVector end = FVector{ x , y, +32767.0f };
         ECollisionChannel collisionChannel = ECollisionChannel::ECC_Visibility;
         const FCollisionQueryParams collisionQueryParams;
         const FCollisionResponseParams collisionResponseParams;
         bool resFromQuery = world->LineTraceSingleByChannel(res, start, end, collisionChannel, collisionQueryParams, collisionResponseParams);
         if (resFromQuery) {
            landscapeHeights.push_back(res.ImpactPoint.Z);
         }
         else {
            landscapeHeights.push_back(0.f);
         }

      }
   }
}

void LandscapeHeights::WriteLandscapeHeights2(std::string filePath) {
   ofstream fichier(filePath, ios::binary);
   if (fichier) {
      for (int j = 0; j <= (demiSizeLandscape * 2 / pas); ++j) {
         for (int i = 0; i <= (demiSizeLandscape * 2 / pas); ++i) {
            fichier << landscapeHeights[j * (demiSizeLandscape * 2 / pas + 1) + i] << " ";
         }
         fichier << "\n";
      }
      fichier.close();
   }
   else {
      cerr << "Impossible d'enregistrer le fichier landscapesHeights !" << endl;
   }
}

void LandscapeHeights::ReadLandscapeHeights2(string filePath, UWorld* world) {
   ifstream fichier(filePath, ios::binary);
   if (fichier) {
      landscapeHeights = vector<short>(nbHeights);
      for (int i = 0; i < landscapeHeights.size(); ++i) {
         fichier >> landscapeHeights[i];
      }
      fichier.close();
   }
   else {
      generateLandscapeHeights(world);
      WriteLandscapeHeights(filePath);
   }
}

//***** GETTER ******

float LandscapeHeights::getHeight(float x, float y) {
   x = std::min(std::max(x, -float(demiSizeLandscape)), float(demiSizeLandscape));
   y = std::min(std::max(y, -float(demiSizeLandscape)), float(demiSizeLandscape));
   int xScaled = (x + demiSizeLandscape) / pas;
   int yScaled = (y + demiSizeLandscape) / pas;
   int ind = yScaled * (demiSizeLandscape * 2 / pas + 1) + xScaled;
   return landscapeHeights[ind];
}

int LandscapeHeights::getDemiSizeLandscape() {
   return demiSizeLandscape;
}

//***** SERIALISATION ******

char * LandscapeHeights::serialiser(char *dest) {
   for (short landscapeHeight : landscapeHeights) {
      dest = serialiser(dest, landscapeHeight);
   }
   return dest;
}

const char * LandscapeHeights::deserialiser(const char *src) {
   landscapeHeights = vector<short>(nbHeights);
   for (short& landscapeHeight : landscapeHeights) {
      src = deserialiser(landscapeHeight, src);
   }
   return src;
}

void LandscapeHeights::WriteLandscapeHeights(std::string filePath) {
   
   ofstream fichier(filePath, ios::binary);
   if (fichier) {
      char buf[nbHeights * sizeof(short)]{};
      serialiser(buf);
      for (char c : buf) {
         fichier.put(c);
      }
      //WriteLandscapeHeights2("./LandscapeHeightsValWrite");
   }
   else {
      cerr << "Impossible d'enregistrer le fichier landscapesHeights !" << endl;
   }
}

void LandscapeHeights::ReadLandscapeHeights(string filePath, UWorld* world) {
   ifstream fichier(filePath, ios::binary);
   if (fichier) {
      char buf[nbHeights * sizeof(short)]{};
      for (char& c : buf) {
         fichier.get(c);
      }
      deserialiser(buf);
      //WriteLandscapeHeights2("./LandscapeHeightsValRead");
   }
   else {
      generateLandscapeHeights(world);
      WriteLandscapeHeights(filePath);
   }
}