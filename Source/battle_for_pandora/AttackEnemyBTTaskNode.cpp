// Fill out your copyright notice in the Description page of Project Settings.

#include "AttackEnemyBTTaskNode.h"
#include "SquadSbireController.h"

UAttackEnemyBTTaskNode::UAttackEnemyBTTaskNode()
{
   // Le nom que prendra le noeud dans le BT
   NodeName = "AttackEnemy";
   // Cette t�che appelle TickTask
   bNotifyTick = true;
}

/* Sera appel�e au d�marrage de la t�che et devra retourner Succeeded, Failed ou InProgress */
EBTNodeResult::Type UAttackEnemyBTTaskNode::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8*
   NodeMemory)
{
   // Obtenir un pointeur sur notre AIEnemyController
   ASquadSbireController* sbireController = Cast<ASquadSbireController>(OwnerComp.GetOwner());
   EPathFollowingRequestResult::Type MoveToActorResult = sbireController->AttackEnemy();
   if (MoveToActorResult == EPathFollowingRequestResult::Type::AlreadyAtGoal)
      return EBTNodeResult::Succeeded;
   if (MoveToActorResult == EPathFollowingRequestResult::Type::Failed)
      return EBTNodeResult::Failed;
   return EBTNodeResult::Succeeded;
}

/* Sera appel�e constamment tant que la t�che n'est pas finie (tant que ExecuteTask retourne
InProgress) */
void UAttackEnemyBTTaskNode::TickTask(class UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) {

   // Obtenir un pointeur sur notre AIEnemyController
   ASquadSbireController* sbireController = Cast<ASquadSbireController>(OwnerComp.GetOwner());
   if (sbireController->CanAttack()) {
      EPathFollowingRequestResult::Type MoveToActorResult = sbireController->AttackEnemy();
      if (MoveToActorResult == EPathFollowingRequestResult::AlreadyAtGoal)
         FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
      if (MoveToActorResult == EPathFollowingRequestResult::RequestSuccessful)
         FinishLatentTask(OwnerComp, EBTNodeResult::InProgress);
      if (MoveToActorResult == EPathFollowingRequestResult::Failed)
         FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
   }
   else {
      sbireController->StopMovement();
      FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
   }
}

/** Retourne une chaine de description pour la t�che. Ce texte appara�tre dans le BT */
FString UAttackEnemyBTTaskNode::GetStaticDescription() const
{
   return TEXT("Declenche l'attaque d'un Sbire");
}
