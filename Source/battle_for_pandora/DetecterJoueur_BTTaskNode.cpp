// Fill out your copyright notice in the Description page of Project Settings.

#include "DetecterJoueur_BTTaskNode.h"
#include "SquadSbireController.h"
#include "Logger.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Maneuver.h"
#include "BFPCharacterPlayable.h"
#include "SquadDescriptor.h"
#include "BFP_GameState.h"

UDetecterJoueur::UDetecterJoueur(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    NodeName = "DetecterJoueurs";
}

EBTNodeResult::Type UDetecterJoueur::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8*
    NodeMemory)
{
    Super::ExecuteTask(OwnerComp, NodeMemory);

    // On recupere la manoeuvre !
    AManeuver* maneuvre = Cast<AManeuver>(OwnerComp.GetOwner());
    if (maneuvre->GetDescriptor()->GetLieutenants().Num() <= 0) { // Si on a pas de lieutenant, on fail tout de suite !
        return EBTNodeResult::Failed;
    }

    maneuvre->DetecterJoueurs();

    return EBTNodeResult::Succeeded;
}

FString UDetecterJoueur::GetStaticDescription() const
{
    return TEXT("Detecte et enregistre les joueurs visibles par la squad.");
}