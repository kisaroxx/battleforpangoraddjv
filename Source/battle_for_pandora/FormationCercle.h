// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Formation.h"
#include "FormationCercle.generated.h"

/**
*
*/
UCLASS(Blueprintable)
class BATTLE_FOR_PANDORA_API UFormationCercle : public UFormation
{
    GENERATED_BODY()

        /* Indique la taille du premier anneau. */
        UPROPERTY(EditDefaultsOnly, Category = "Parametres")
        int taillePremierAnneau = 5;

    /* Indique la taille du premier anneau. */
    UPROPERTY(EditDefaultsOnly, Category = "Parametres")
        float largeurAnneau = 500.0f;

    /* Indique quelle partie du cercle les unites doivent couvrir. */
    UPROPERTY(EditDefaultsOnly, Category = "Parametres")
        float angleEncerclement = 360.0f;

public:
    void SetParams(int taillePremierAnneau_, float largeurAnneau_, float angleEncerclement_);

protected:
    virtual void ComputeAllPositions(int nbSbires, int nbLieutenants) override;

private:
    int GetNbRings(int nbSbires);

};
