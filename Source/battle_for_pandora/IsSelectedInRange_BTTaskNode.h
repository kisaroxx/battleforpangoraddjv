// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "Maneuver.h"
#include "IsSelectedInRange_BTTaskNode.generated.h"

UCLASS()
class BATTLE_FOR_PANDORA_API UIsSelectedInRange : public UBTTaskNode
{
    GENERATED_BODY()

    /** La distance qui nous int�resse. */
    UPROPERTY(Category = Range, EditAnywhere)
    float distance;

public:
    UIsSelectedInRange(const FObjectInitializer & ObjectInitializer);

    /* Fonction d'ex�cution de la t�che, cette t�che devra retourner Succeeded, Failed ou InProgress */
    /* Sera appel�e au d�marrage de la t�che et devra retourner Succeeded, Failed ou InProgress */
    virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

    /** Permet de d�finir une description pour la t�che. C'est ce texte qui
    appara�tra dans le noeud que nous ajouterons au Behavior Tree */
    virtual FString GetStaticDescription() const override;

};
