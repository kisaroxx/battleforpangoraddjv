// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core.h"
#include "SoundActorPlayable.h"
#include "HttpBackEndActor.h"
#include "GameFramework/PlayerController.h"
#include "Logger.h"
#include "GameFramework/Character.h"
#include "Net/UnrealNetwork.h"
#include "UI/MenuWidget.h"
#include "BFPCharacter.h"
#include "CaptureComponent.h"
#include "Maneuver.h"
#include "BFP_PlayerController.generated.h"

UENUM(BlueprintType)
enum class MusicState : uint8
{
    EXPLORATION,    // La musique d'exploration
    COMBAT          // La musique de combat
};

UCLASS()
class BATTLE_FOR_PANDORA_API ABFP_PlayerController : public APlayerController
{
	GENERATED_BODY()
protected:

private:
	UPROPERTY(Category = "Camera : Pitch", EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "0", UIMin = "0", AllowPrivateAccess = "true"))
	float MaxPitch = 10.f;
	UPROPERTY(Category = "Camera : Pitch", EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "0", UIMin = "0", AllowPrivateAccess = "true"))
	float MinPitch = -45.f;

   UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Spell", meta = (AllowPrivateAccess = "true"))
   TSubclassOf<class ASpell>  meteoriteSpell;

   UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Spell", meta = (AllowPrivateAccess = "true"))
   TSubclassOf<class ASpell> tourbillonFlammesSpell;

   UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Spell", meta = (AllowPrivateAccess = "true"))
   TSubclassOf<class ASpell> healingSpell;

   UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Spell", meta = (AllowPrivateAccess = "true"))
   TSubclassOf<class ASpell> explosionSpell;

   UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Musique", meta = (AllowPrivateAccess = "true"))
   MusicState musicState = MusicState::EXPLORATION;

   UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Musique", meta = (AllowPrivateAccess = "true"))
   float distanceMusiqueCombat = 1500.0f;

   UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Musique", meta = (AllowPrivateAccess = "true"))
   float distanceMusiqueExploration = 5000.0f;

	TWeakObjectPtr<class UGameInfoInstance> gameInstanceRef;

public:
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(Reliable, Client)
		void SetEndGameStatus(bool victory);

	ABFP_PlayerController(const FObjectInitializer & ObjectInitializer);
    virtual void Tick(float DeltaTime) override;

	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void BeginPlay() override;
	virtual void PostInitializeComponents() override;

	//UFUNCTION(Reliable, Server, WithValidation)
	void InitPawn();

   UFUNCTION()
   void UpdateHUDFromGameStateModification();
   UFUNCTION()
   void UpdateHUDCheckpointsFromGameStateModification();

	UFUNCTION(Reliable, Client)
	void DestroySession();

	UFUNCTION(Reliable, Client)
	void cleanWidget();

	UFUNCTION(Reliable, Client)
	void ExitGameMenu();

	//--------------------------WIDGET-----------------------------
	UPROPERTY(EditAnywhere, Category = "Widget")
	TWeakObjectPtr<class UExitWidget> ExitWD;

	void showExitMenuWidget(const bool bShowMouseCursorIn = false, const EInputModeWidget& inputMode = EInputModeWidget::UI_Only);

	//--------------------------PROGRESS BAR ----------------------

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Capture", meta = (AllowPrivateAccess = "true"))
	UCaptureComponent *CaptureSystem;
	
	UFUNCTION()
	void UpdateCaptureValue();
	UFUNCTION()
	void ChangeCaptureVisibility();

	//Les methodes pour la progress Bar du PDC
	void AffichageProgressBarRPC(Affiliation CheckpointAffiliation);

	void DisparitionProgressBarRPC();

	void UpdateProgressBarRPC(float PDCValueChange, Affiliation CheckpointAffiliation);

	UFUNCTION(Reliable, Client)
	void ChangeColorProgressBarRPC(FLinearColor Color);

   UFUNCTION(Reliable, Server, WithValidation)
   void AddTargetPointRPC(FTransform point, ABFPCharacterLieutenant* lieutenant);

   UFUNCTION(Reliable, Server, WithValidation)
   void StopMovementRPC(FTransform point, ABFPCharacterLieutenant* lieutenant);

   UFUNCTION(Reliable, Server, WithValidation)
   void Spell1RPC(FTransform posLanceur, ECharacterName Name);
   
   UFUNCTION(Reliable, Server, WithValidation)
   void SetStrategieRPC(ManeuverType maneuverType, ABFPCharacterLieutenant* lieutenant);

   // Http Actor
   UPROPERTY(EditAnywhere)
      TSubclassOf<ASoundActorPlayable> SoundActor;
   UPROPERTY(EditAnywhere)
      ASoundActorPlayable* SoundActorInstance;
   UFUNCTION()
      void SpawnSoundActor();
   // Http Actor
   UPROPERTY(EditAnywhere)
      TSubclassOf<AHttpBackEndActor> HttpActor;
   UPROPERTY(EditAnywhere)
      AHttpBackEndActor* HttpActorInstance;
   UFUNCTION()
      void SpawnHttpActor();

private:
    void CheckMusicTransitions();
};
