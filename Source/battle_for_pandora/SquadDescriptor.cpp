// Fill out your copyright notice in the Description page of Project Settings.

#include "SquadDescriptor.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "Engine/ActorChannel.h"
#include "Formation.h"
#include "FormationCercle.h"
#include "Maneuver.h"
#include "ManeuverAgressive.h"
#include "TimerManager.h"
#include "SquadLieutenantController.h"
#include "SquadSbireController.h"
#include "SquadManager.h"
#include "Logger.h"
#include "SquadDescriptor.h"
#include "EndCommand.h"
#include "Network/Gameplay/GameplayGM.h"
#include "BFP_GameState.h"
#include "UObject/ConstructorHelpers.h"

using namespace std;
using std::vector;

// Sets default values
ASquadDescriptor::ASquadDescriptor(const FObjectInitializer & ObjectInitializer)
{
   // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
   PrimaryActorTick.bCanEverTick = false;
   bAlwaysRelevant = true;
   bReplicates = true;

   NetCullDistanceSquared *= 40;
}

void ASquadDescriptor::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(ASquadDescriptor, lieutenants);
    DOREPLIFETIME(ASquadDescriptor, sbires);
    DOREPLIFETIME(ASquadDescriptor, maneuver);
    DOREPLIFETIME(ASquadDescriptor, squadManager);
    DOREPLIFETIME(ASquadDescriptor, checkpointAssociated);
}

void ASquadDescriptor::PostInitializeComponents() {
    Super::PostInitializeComponents();

    if (!ensure(IsValid(lieutenantControllerClass))) return;
    if (!ensure(IsValid(sbireControllerClass))) return;
    if (!ensure(IsValid(lieutenantPawnClass))) return;
    if (!ensure(IsValid(sbirePawnClass))) return;

    // On set la manoeuvre
    if (IsValid(maneuverAgressive.Get())) {
        FActorSpawnParameters params{};
        params.Owner = this;
        //FName name("ManoeuvreOf "); FString
        //params.Name = name;
        maneuver = GetWorld()->SpawnActor<AManeuver>(maneuverAgressive.Get(), params);
        maneuver->SetSquadDescriptor(this);
        //maneuver = NewObject<AManeuver>(this, maneuverAgressive.Get()); // Don't touch this ! <3
        //maneuver->SetSquadDescriptor(this);
    }
    else {
        DEBUG_SCREEN(-1, 5.0f, FColor::Red, "Attention la maneuverAgressive.Get() est nul !");
    }
}

void ASquadDescriptor::Tick(float DeltaTime) {
    Super::Tick(DeltaTime);
}

void ASquadDescriptor::BeginPlay()
{
    Super::BeginPlay();
    MY_LOG_NETWORK(TEXT("ASquadDescriptor BeginPlay COTE SERVEUR OU COTE CLIENT ? %u"), GetWorld()->IsServer());

    //squadManager = NewObject<ASquadManager>(this, ASquadManager::StaticClass());
    //squadManager = GetWorld()->SpawnActor<ASquadManager>();
    if (GetWorld()->IsServer()) {
        MY_LOG_NETWORK(TEXT("ASquadDescriptor BeginPlay COTE SERVEUR"));
        AGameplayGM* gameMode = GetWorld()->GetAuthGameMode<AGameplayGM>();
        MY_LOG_NETWORK(TEXT("ASquadDescriptor BeginPlay COTE SERVEUR %u"), IsValid(gameMode));
		if (IsValid(gameMode)) {
			gameMode->GetGameState<ABFP_GameState>()->Register(this);
			InitPawn();
			InitPawnFinished();
		}
    }
}

void ASquadDescriptor::InitPawn() {
    squadManager = GetWorld()->GetAuthGameMode<AGameplayGM>()->SpawnActor<ASquadManager>({});
    if (IsValid(squadManager)) {
        squadManager->SetSquadDescriptor(this);
    }
    else {
        DEBUG_SCREEN(-1, 5.0f, FColor::Red, "Attention le squadManager est nul !");
    }

    // On spawn les membres de la squad
    //if (GetWorld()->IsServer()) {
    AGameplayGM* gameMode = GetWorld()->GetAuthGameMode<AGameplayGM>();
    gameMode->AddSpawnCharacterCount(nbLieutenants + nbSbires);
    bSquadSpawned = true;
    SpawnAllMembers();
}

void ASquadDescriptor::sendCommandToMember(ASquadMemberController* squadMemberController, UCommand* command) {
    squadMemberController->SetCommand(command);
}

TArray<class ASquadLieutenantController*> ASquadDescriptor::GetLieutenants()
{
    return lieutenants;
}

ASquadLieutenantController * ASquadDescriptor::GetFirstLieutenant()
{
    return lieutenants[0];
}

TArray<class ASquadSbireController*> ASquadDescriptor::GetSbires()
{
    return sbires;
}

void ASquadDescriptor::AddSbire(ASquadSbireController* sbireController)
{
   sbires.Add(sbireController);
}

ASquadManager * ASquadDescriptor::GetSquadManager()
{
    return squadManager;
}

int ASquadDescriptor::GetNbLieutenants() {
    return GetLieutenants().Num();
}

int ASquadDescriptor::GetNbSbires() {
    return GetSbires().Num();
}

int ASquadDescriptor::GetNbMembers() {
    return GetSbires().Num() + GetLieutenants().Num();
}

AManeuver * ASquadDescriptor::GetManeuver()
{
    return maneuver;
}

void ASquadDescriptor::SpawnAllMembers() {
    if (lieutenantPawnClass == nullptr || sbirePawnClass == nullptr || lieutenantControllerClass == nullptr || sbireControllerClass == nullptr || maneuver == nullptr) {
        DEBUG_SCREEN(-1, 5.0f, FColor::Red, "Attention des param�tres de SquadDescriptor sont nuls et emp�chent la squad de spawner !");
        return;
    }
    MY_LOG_NETWORK(TEXT("ASquadDescriptor SpawnAllMembers BEGIN"));
    // Instancier tous les lieutenants et tous les sbires !
    FRotator rotation = FRotator::ZeroRotator;
    FActorSpawnParameters spawnParams{};
    spawnParams.Owner = this;
    spawnParams.Instigator = this->Instigator;
    spawnParams.bNoFail = true;
    spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

    TArray<FTransform> transformsLieutenants = maneuver->GetFormation()->GetRelativesPositionsLieutenants(nbSbires, nbLieutenants);
    TArray<FTransform> transformsSbires = maneuver->GetFormation()->GetRelativesPositionsSbires(nbSbires, nbLieutenants);

    for (int i = 0; i < nbLieutenants; ++i) {
        FVector positionFinale = transformsLieutenants[i].GetLocation() + GetActorLocation() + FVector{ 0.0f, 0.0f, spawningOffsetZ };
        FRotator rotationFinale = GetActorRotation() + transformsLieutenants[i].GetRotation().Rotator();
        FTransform transformFinale{};
        transformFinale.SetLocation(positionFinale);
        transformFinale.SetRotation(FQuat(rotationFinale));

        ASquadLieutenantController* lieutenantControllerInstance = nullptr;
        ABFPCharacterLieutenant* const lieutenant = GetWorld()->GetAuthGameMode<AGameplayGM>()->SpawnPawn<ABFPCharacterLieutenant>(transformFinale, lieutenantPawnClass, lieutenantControllerInstance, true, true, spawnParams);

        //lieutenant->SpawnDefaultController();
        lieutenant->SetSquad(this);

        MY_LOG_IA(TEXT("lieutenant %s"), *lieutenant->GetName());
        //AController* controllerNonCast = lieutenant->GetController();
        //MY_LOG_IA(TEXT("controllerNonCast %s"), *controllerNonCast->GetName());
        ASquadLieutenantController* controller = lieutenant->GetController<ASquadLieutenantController>();
        if (controller == nullptr) {
            MY_LOG_IA(TEXT("Le controller n'est pas set correctement !"));
        }
        else {
           MY_LOG_IA(TEXT("controller %s"), *controller->GetName());
           lieutenants.Add(controller);
        }
    }
    for (int i = 0; i < nbSbires; ++i) {
        FVector positionFinale = transformsSbires[i].GetLocation() + GetActorLocation() + FVector{ 0.0f, 0.0f, spawningOffsetZ };
        FRotator rotationFinale = GetActorRotation() + transformsSbires[i].GetRotation().Rotator();

        FTransform transformFinale{};
        transformFinale.SetLocation(positionFinale);
        transformFinale.SetRotation(FQuat(rotationFinale));

        ASquadSbireController* sbireControllerInstance = nullptr;
        ABFPCharacterSbire* const sbire = GetWorld()->GetAuthGameMode<AGameplayGM>()->SpawnPawn<ABFPCharacterSbire>(transformFinale, sbirePawnClass, sbireControllerInstance, true, true, spawnParams);

        //sbire->SpawnDefaultController();
        sbire->SetSquad(this);

        ASquadSbireController* controller = sbire->GetController<ASquadSbireController>();
        if (controller == nullptr) {
            MY_LOG_IA(TEXT("Le controller n'est pas set correctement !"));
        }
        else {
           sbires.Add(controller);
        }
    }
}

void ASquadDescriptor::RemoveLieutenant(ASquadLieutenantController* lieutenant) {
    lieutenants.Remove(lieutenant);
    if (lieutenants.Num() == 0) {
        for (auto sbire : sbires) {
            UEndCommand* com = NewObject<UEndCommand>(this, UEndCommand::StaticClass());
            sbire->SetCommand(com);
            // sbire->StopMovement();
            sbire->SetSquad(nullptr);
        }
        lieutenant->Destroy();
        GetWorld()->GetAuthGameMode<AGameplayGM>()->GetGameState<ABFP_GameState>()->UnRegister(this);
        squadManager->Destroy();
        Destroy();
    }
}

void ASquadDescriptor::RemoveSbire(ASquadSbireController* sbire) {
    sbires.Remove(sbire);
    sbire->Destroy();
}

void ASquadDescriptor::InitPawnFinished() {
    //MY_LOG_IA(TEXT("ASquadDescriptor InitPawnFinished server ? %u"), GetWorld()->IsServer());
    //for (auto lieutenant : lieutenants) {
    //    MY_LOG_IA(TEXT("ASquadDescriptor InitPawnFinished runBtFlag %s"), *lieutenant->GetName());
    //    lieutenant->RunBT();
    //}
    //for (auto sbire : sbires) {
    //    MY_LOG_IA(TEXT("ASquadDescriptor InitPawnFinished runBtFlag %s"), *sbire->GetName());
    //    sbire->RunBT();
    //}
    //bBTStarted = true;
}

void ASquadDescriptor::RegisterLieutenantController(ASquadLieutenantController * lieutenantController)
{
   lieutenants.Add(lieutenantController);
}

void ASquadDescriptor::RegisterSbireController(ASquadSbireController * sbireController)
{
   sbires.Add(sbireController);
}

void ASquadDescriptor::SetCheckpointAssociated(ACheckpointTriggerSphere * checkpoint) {
    checkpointAssociated = checkpoint;
}

ACheckpointTriggerSphere * ASquadDescriptor::GetCheckpointAssociated() const {
    return checkpointAssociated;
}

TSubclassOf<AManeuver> ASquadDescriptor::GetManeuverClass(ManeuverType type) const {
    switch (type) {
    case ManeuverType::AGRESSIVE:
        return maneuverAgressive;
        break;
    case ManeuverType::DEFENSIVE:
        return maneuverDefensive;
        break;
    case ManeuverType::SURVIE:
        return maneuverSurvie;
        break;
    case ManeuverType::ASSAULT:
        return maneuverAssault;
        break;
    case ManeuverType::RECALL:
        return maneuverRecall;
        break;
    default:
        break;
    }
    return nullptr;
}

void ASquadDescriptor::SetManeuver(AManeuver * nouvelleManeuvre, ManeuverType type) {
    maneuver = nouvelleManeuvre;
    maneuverType = type;
}
ManeuverType ASquadDescriptor::GetManeuverType() const
{
   return maneuverType;
}
void ASquadDescriptor::UnlinkFromCheckpoint() {
	if (IsValid(checkpointAssociated)) {
		checkpointAssociated->UnlinkFromSquad();
		checkpointAssociated = nullptr;
	}
}
