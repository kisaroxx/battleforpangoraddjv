﻿// Fill out your copyright notice in the Description page of Project Settings.
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core.h"
#include "BFPCharacter.h"
#include "BFPHUD.h"
#include "SoundActorPlayable.h"
#include "HttpBackEndActor.h"
#include "Components/StaticMeshComponent.h"
#include "BFPCharacterPlayable.generated.h"


/**
 *
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FUnGhost);

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class ECharacterName : uint8
{
	AERES,
	DAVROS,
	KIARA,
	NEELF,
	SHANYL
};


UCLASS()
class ABFPCharacterPlayable : public ABFPCharacter
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable, Category = "EventOnRun")
	FUnGhost UnGhost;

	UPROPERTY(VisibleAnywhere, ReplicatedUsing = OnRep_SetGhostState, Transient, Category = "Ghost")
    bool bGhosted = false;

	UFUNCTION()
	void OnRep_SetGhostState();

private:
	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;


	UPROPERTY(EditAnywhere, Category = "LockCamera")
	bool TargetLocked;

    float ComputeScoreForLock(ABFPCharacter* target) const;

	UPROPERTY()
	ABFPCharacter* NearestTarget;

	float minScore = 0;
	float MinimumDistanceToEnable = 5000.0f;

	/* Permet de savoir si on est dans la carte ou pas */
	bool isInInteractiveMap;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Ghost", meta = (AllowPrivateAccess = "true"))
    uint8 NbMortMax = 2;
	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "Ghost", meta = (AllowPrivateAccess = "true"))
    uint8 NbMort = 0;

	UPROPERTY()
	Affiliation oldAffiliation;

	UFUNCTION()
	void PlayFlash();

public:
	ABFPCharacterPlayable(const FObjectInitializer & ObjectInitializer);

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	void Die() override;

	UFUNCTION()
	void DiedFunction();

	UFUNCTION()
	Affiliation getOldAffiliation() { return oldAffiliation; }
	UFUNCTION()
	void setOldAffiliation(Affiliation aff) { oldAffiliation = aff; }

	UFUNCTION()
	uint8 getNbMort() { return NbMort; }
	UFUNCTION()
	uint8 getNbMortMax() { return NbMortMax; }

	UFUNCTION()
	void AddNbMort() { NbMort++; }

	UPROPERTY(ReplicatedUsing = OnRep_VraiMort, Transient)
	bool bVraiMort = false;
	UFUNCTION()
	void OnRep_VraiMort();

	//UFUNCTION(Reliable, Client)
	void SetDead(bool vraiMort);
	//UFUNCTION(Reliable, NetMulticast)
	void SetGhosted();
	UFUNCTION()
	void SetUnGhosted();

   bool IsGhost() override;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;

	UFUNCTION()
	bool GetIsInInteractiveMap();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	TArray<ABFPCharacter*> FoundActors;
	///** Lock Enemy and turn Camera on It */
	UFUNCTION()
	void LockEnemy();

	void OnClickLeft();

   UFUNCTION(BluePrintCallable, Reliable, Server, WithValidation)
   void OnPressedP();


	void OnClickRight();
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void MoveActionInteractiveMap();

	UFUNCTION(BlueprintCallable)
	void ShowActionWheelInteractiveMap();

	UFUNCTION(BlueprintCallable)
	void StopMoveInteractiveMap();

    UFUNCTION()
    void SetStrategieInteractiveMap(ManeuverType maneuverType);

	UFUNCTION(BlueprintCallable)
	void SetStrategieAgressiveInteractiveMap();
	UFUNCTION(BlueprintCallable)
	void SetStrategieDefensiveInteractiveMap();
	UFUNCTION(BlueprintCallable)
	void SetStrategieAssaultInteractiveMap();
	UFUNCTION(BlueprintCallable)
	void SetStrategieSurvieInteractiveMap();

	void ExitGameMenu();


	UPROPERTY(EditAnywhere, Replicated)
	ECharacterName Name;

   UPROPERTY(EditAnywhere)
      TSubclassOf<ASoundActorPlayable> SoundActor;
   UPROPERTY(EditAnywhere)
      TSubclassOf<AHttpBackEndActor> HttpActor;

   UFUNCTION(BlueprintCallable)
      ECharacterName getName() { return Name; };

   UFUNCTION(BlueprintCallable)
   void Spell1();

protected:

	// Called when the game starts
	virtual void BeginPlay() override;
    virtual void PostInitProperties() override;
	/**
	* Called via input to turn at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void TurnAtRate(float Rate);

	/**
	* Called via input to turn look up/down at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void LookUpAtRate(float Rate);

	/*Function which lock Axis Y when a target is locked */
	void TurnAxisY(float Rate);

	UFUNCTION(BlueprintCallable)
	void ApparitionInteractiveMap();

	void DisparitionInteractiveMap();

	void MoveActionFromInteractiveMap();


	bool MoveClickIsEnable = false;

	bool InteractiveMap = false;

	bool MoveButtonClicked = false;
	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	/** Lock Camera on the target */
	void LockCamera(ABFPCharacter* NearestTarget);

    /* Permet de lancer un sort ! :) */
    // Utilisation d'un template pour pouvoir specifier l'indice du sort ! :)
    template<int32 Index>
    void TryCastSpellWithIndice() { // Je dois mettre l'implementation ici car c'est un template
		//on s'assure qu'on est bien dans le jeu et non dans le Lobby
		if (IsValid(GetController<ABFP_PlayerController>())) {
			if (!IsDead() && !bGhosted) {
				if (spellComponents.Num() - 1 >= Index)
					TryCastSpell(Index);
			}
		}
    }


public:
	virtual void RegisterToGameManager() override;

	virtual void UnRegisterToGameManager() override;



protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};