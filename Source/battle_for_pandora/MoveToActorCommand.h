// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Command.h"
#include "Math/Vector.h"
#include "MoveToActorCommand.generated.h"

UENUM(BlueprintType)
enum class OffsetType : uint8
{
    ABSOLU,     // La positition est fixe par rapport � l'acteur. // L'angle est fix� d�s le depart.
    RELATIF,     // La position est relative � l'acteur par rapport � l'angle avec le lieutenant // L'angle est recalcul� par rapport au lieutenant � chaque fois.
    RELATIF_TO_POINT     // La position est relative � un point specifie // L'angle est recalcul� par rapport � l'angle entre le lieutenant et le point specifie a chaque fois.
};

/**
*
*/
UCLASS()
class BATTLE_FOR_PANDORA_API UMoveToActorCommand : public UCommand
{
    GENERATED_BODY()

    bool arrived = false;
    bool pathCalculated = false;

    UPROPERTY(EditAnywhere, Category = "Command")
    AActor* actorTarget;

    UPROPERTY(EditAnywhere, Category = "Command")
    FVector offset = {};

    UPROPERTY(EditAnywhere, Category = "Command")
    OffsetType offsetType = OffsetType::RELATIF;

    UPROPERTY(EditAnywhere, Category = "Command")
    FVector pointForAngle; // Only used in RELATIF_TO_POINT mode

    UPROPERTY(EditAnywhere, Category = "Command")
    float refreshTime = 0.5f;
    float lastTimeRefresh;

public:
    UMoveToActorCommand();

    UFUNCTION(BlueprintCallable, Category = "Command")
    void SetActorTarget(AActor* target);

    UFUNCTION(BlueprintCallable, Category = "Command")
    AActor* GetActorTarget();

    UFUNCTION(BlueprintCallable, Category = "Command")
    void SetOffset(FVector offset_);

    UFUNCTION(BlueprintCallable, Category = "Command")
    FVector GetOffset();

    UFUNCTION(BlueprintCallable, Category = "Command")
    void SetPointForAngle(FVector point);

    UFUNCTION(BlueprintCallable, Category = "Command")
    FVector GetPointForAngle();

    UFUNCTION(BlueprintCallable, Category = "Command")
    void SetRefreshTime(float refreshTime_);

    UFUNCTION(BlueprintCallable, Category = "Command")
    float GetRefreshTime();

    UFUNCTION(BlueprintCallable, Category = "Command")
    void SetOffsetType(OffsetType type);

    UFUNCTION(BlueprintCallable, Category = "Command")
    OffsetType GetOffsetType();

    FVector GetDestination();

    class UCommandReturn* execute() override;
};
