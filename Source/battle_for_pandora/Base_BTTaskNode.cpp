// Fill out your copyright notice in the Description page of Project Settings.

#include "Base_BTTaskNode.h"
#include "SquadSbireController.h"
#include "Logger.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Maneuver.h"
#include "BFPCharacterPlayable.h"
#include "SquadDescriptor.h"


UBaseBTTaskNode::UBaseBTTaskNode(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    NodeName = "AttaquerSelected";
}

EBTNodeResult::Type UBaseBTTaskNode::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8*
    NodeMemory)
{
    Super::ExecuteTask(OwnerComp, NodeMemory);

    // On recupere la manoeuvre !
    AManeuver* maneuvre = Cast<AManeuver>(OwnerComp.GetOwner());
    maneuvre->ClearMessages();
    maneuvre->nbCompleted = 0;

    // Si on a pas de lieutenant, on fail tout de suite !
    if (maneuvre->GetDescriptor()->GetLieutenants().Num() <= 0) {
        return EBTNodeResult::Failed;
    }

    return ExecuteTaskImpl(OwnerComp, NodeMemory, maneuvre);
}

EBTNodeResult::Type UBaseBTTaskNode::ExecuteTaskImpl(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, AManeuver* maneuvre) {
    DEBUG_SCREEN(-1, 5.0f, FColor::Red, "Il FAUT implementer ExecuteTaskImpl !!!");
    return EBTNodeResult::Failed; // On est pas cens� passer ici !
}

FString UBaseBTTaskNode::GetStaticDescription() const
{
    return TEXT("Ceci est une tache de maneuvre.");
}

void UBaseBTTaskNode::OnMessage(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, FName Message, int32 RequestID, bool bSuccess) {
   //MY_LOG_NETWORK(TEXT("UBaseBTTaskNode OnMessage"));
    // On distingue les messages
    AManeuver* maneuvre = Cast<AManeuver>(OwnerComp.GetOwner());
    switch (maneuvre->GetLastMessage()->GetType()) {
    case MessageType::COMPLETED:
        if (bUseCompletedMessages) {
            maneuvre->nbCompleted++;
            // Si on a re�u assez de messages alors on quitte la tache ! :3
            if (maneuvre->nbCompleted >= maneuvre->GetDescriptor()->GetNbMembers() * 0.75f) {
                FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
                //MY_LOG_IA(TEXT("%s SUCCEEDED because of COMPLETED"), *maneuvre->GetDescriptor()->GetName());
            }
        }
        break;
    case MessageType::ADD_TARGET_POINT:
        if (bShouldFailOnAddTargetPoint) {
            FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
            //MY_LOG_IA(TEXT("%s FAILED because of ADD_TARGET_POINT"), *maneuvre->GetDescriptor()->GetName());
        }
       break;
    default:
        break;
    }
}
