// Fill out your copyright notice in the Description page of Project Settings.

#include "CheckTargetActorNearbyBTTaskNode.h"
#include "SquadSbireController.h"

UCheckTargetActorNearbyBTTaskNode::UCheckTargetActorNearbyBTTaskNode() {
   // Le nom que prendra le noeud dans le BT
   NodeName = "CheckTargetActorNearby";
}

/* Sera appel�e au d�marrage de la t�che et devra retourner Succeeded, Failed ou InProgress */
EBTNodeResult::Type UCheckTargetActorNearbyBTTaskNode::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8*
   NodeMemory)
{
   // Obtenir un pointeur sur notre AIEnemyController
   ASquadSbireController* sbireController = Cast<ASquadSbireController>(OwnerComp.GetOwner());

   if (sbireController->CheckTargetActorNearby())
   {
   return EBTNodeResult::Succeeded;
   }
   else {
      return EBTNodeResult::Failed;
   }
}

FString UCheckTargetActorNearbyBTTaskNode::GetStaticDescription() const {
   return TEXT("Recherche un joueur a proximite");
}

