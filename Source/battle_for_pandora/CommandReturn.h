// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Navigation/PathFollowingComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "CommandReturn.generated.h"

/**
 * 
 */
UCLASS()
class BATTLE_FOR_PANDORA_API UCommandReturn : public UObject
{
   GENERATED_BODY()

public:
	UCommandReturn();
	~UCommandReturn();

   UFUNCTION(BlueprintCallable, Category = "CommandReturn")
   void setReturn(TEnumAsByte<EPathFollowingRequestResult::Type> type);

   UFUNCTION(BlueprintCallable, Category = "CommandReturn")
   TEnumAsByte<EPathFollowingRequestResult::Type> getReturn();

   UFUNCTION(BlueprintCallable, Category = "CommandReturn")
   TEnumAsByte<EPathFollowingRequestResult::Type> getReturnSimpleAttack();

   UFUNCTION(BlueprintCallable, Category = "CommandReturn")
   void setReturnSimpleAttack(TEnumAsByte<EPathFollowingRequestResult::Type> type);

   UFUNCTION(BlueprintCallable, Category = "CommandReturn")
   bool GetAttackLaunched();

   UFUNCTION(BlueprintCallable, Category = "CommandReturn")
   void SetAttackLaunched(bool type);

   UFUNCTION(BlueprintCallable, Category = "CommandReturn")
   void SetEndCommand(bool _noCommand);

   UFUNCTION(BlueprintCallable, Category = "CommandReturn")
   bool HasEndCommand();

protected:
   UPROPERTY(EditAnywhere, Category = "CommandReturn")
   TEnumAsByte<EPathFollowingRequestResult::Type> movementReturn;

   UPROPERTY(EditAnywhere, Category = "CommandReturn")
   TEnumAsByte<EPathFollowingRequestResult::Type> attackReturn;

   UPROPERTY(EditAnywhere, Category = "CommandReturn")
   bool attackLaunched;

   UPROPERTY(EditAnywhere, Category = "CommandReturn")
   bool endCommand;
};
