// Fill out your copyright notice in the Description page of Project Settings.

#include "HasSquadBTTaskNode.h"
#include "SquadSbireController.h"
#include "Logger.h"

UHasSquadBTTaskNode::UHasSquadBTTaskNode(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer){
   NodeName = "HasSquad";
}

/* Sera appel�e au d�marrage de la t�che et devra retourner Succeeded, Failed ou InProgress */
EBTNodeResult::Type UHasSquadBTTaskNode::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8*
   NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);
   // Obtenir un pointeur sur notre AIEnemyController
   ASquadSbireController* AIControllerSbire = Cast<ASquadSbireController>(OwnerComp.GetOwner());

   // Nous pr�parons le r�sultat de la t�che. Elle doit retourner InProgress
   //EBTNodeResult::Type NodeResult = EBTNodeResult::Failed;

   //MY_LOG_IA(TEXT("UHasSquadBTTaskNode ExecuteTask AIControllerSbire ? %u"), IsValid(AIControllerSbire));

   if (IsValid(AIControllerSbire)) {
      // Appel de la fonction AttackEnemy du contr�leur
      if (AIControllerSbire->HasSquad()) {
         return EBTNodeResult::Succeeded;
      }
      else {
         return EBTNodeResult::Failed;
      }
   }

   return EBTNodeResult::Failed;
}

/** Permet de d�finir une description pour la t�che. C'est ce texte qui
appara�tra dans le noeud que nous ajouterons au Behavior Tree */
FString UHasSquadBTTaskNode::GetStaticDescription() const
{
   return TEXT("V�rifie si le sbire appartient � une escouade");
}