// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Message.generated.h"

UENUM(BlueprintType)
enum class MessageType : uint8
{
    COMPLETED,      // La tache a �t� compl�t�e.
    FAILED,         // La tache ne peut pas �tre effectu�e.
    DIED,           // Le pnj en train d'effectuer la tache est mort.
    JOIN,           // Un sbire vient de rejoindre la squad
    KILLED,          // La cible de l'attaque a �t� tu�e.
    IN_VISION_RANGE,          // Il y a un ennemi en vue.
    IN_AGRESSION_RANGE,          // Il y a un ennemi � port�e d'agression.
    IN_RUSH_RANGE,          // Il y a un ennemi � port�e de rush.
    OUT_OF_VISION_RANGE,          // Il n'y a pas d'ennemis en vue.
    OUT_OF_AGRESSION_RANGE,          // Il n'y a pas un ennemi � port�e d'agression.
    OUT_OF_RUSH_RANGE,          // Il n'y a pas un ennemi � port�e de rush.
    RECALL,                      // Pour que la squad revienne defendre son PDC !!! :)
    STOP_RECALL,                      // Pour que la squad arrete de defendre son PDC :)
    STOP,                        // Détruit le parcours courant de la Squad
    ADD_TARGET_POINT             // Permet de notifier la Squad que l'on vient de lui rajouter un targetPoint !
};

/**
 * 
 */
UCLASS()
class BATTLE_FOR_PANDORA_API UMessage : public UObject
{
    GENERATED_BODY()

    UPROPERTY(VisibleAnywhere, Category = "Description", meta = (AllowPrivateAccess = "true"))
    MessageType type;

    UPROPERTY(VisibleAnywhere, Category = "Description", meta = (AllowPrivateAccess = "true"))
    class ASquadMemberController* character;

public:
	UMessage();
	~UMessage();
    void SetParams(MessageType type_, ASquadMemberController* character_);
    MessageType GetType() const;
    ASquadMemberController* GetSquadMemberController() const;
};
