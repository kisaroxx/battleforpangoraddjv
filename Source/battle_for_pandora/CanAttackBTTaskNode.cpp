// Fill out your copyright notice in the Description page of Project Settings.

#include "CanAttackBTTaskNode.h"
#include "SquadSbireController.h"

UCanAttackBTTaskNode::UCanAttackBTTaskNode() {
   // Le nom que prendra le noeud dans le BT
   NodeName = "CanAttack";
}

/* Sera appelee au demarrage de la tache et devra retourner Succeeded, Failed ou InProgress */
EBTNodeResult::Type UCanAttackBTTaskNode::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8*
   NodeMemory)
{
   // Obtenir un pointeur sur notre AIEnemyController
   ASquadSbireController* sbireController = Cast<ASquadSbireController>(OwnerComp.GetOwner());

   if (sbireController->CanAttack())
   {
      return EBTNodeResult::Succeeded;
   }
   else {
      return EBTNodeResult::Failed;
   }
}

FString UCanAttackBTTaskNode::GetStaticDescription() const {
   return TEXT("Savoir si le sbire attaque ou fuit");
}