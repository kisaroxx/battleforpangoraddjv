// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Spell.h"
#include "Engine/TriggerCapsule.h"
#include "Engine/StaticMeshActor.h"
#include "AeresStunSpell.generated.h"

UCLASS()
class BATTLE_FOR_PANDORA_API AAeresStunSpell : public ASpell
{
    GENERATED_BODY()

    /* Le rayon de l'aoe. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float rangeSpell = 500.0f;

    /* Les degats de l'aoe. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float degats = 50.0f;

    /* La duree du stun (et donc du spell). */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float dureeStun = 5.0f;

protected:
   AAeresStunSpell(const FObjectInitializer & ObjectInitializer);
   // Called when the game starts or when spawned
   virtual void BeginPlay() override;
};