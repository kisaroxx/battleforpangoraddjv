// Fill out your copyright notice in the Description page of Project Settings.

#include "SpellComponent.h"
#include "Engine/World.h"
#include "BFPCharacter.h"
#include "BFPCharacterPlayable.h"
#include "Logger.h"
#include "Engine/Canvas.h"
#include "Engine/Texture2D.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "Animation/WidgetAnimation.h"
#include "Components/WidgetComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"
#include "WidgetBlueprintLibrary.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "Components/Image.h"
#include "Components/Border.h"
#include "Components/CanvasPanel.h"
#include "Components/CircularThrobber.h"
#include "UserWidget.h"
#include "Spell.h"
#include "Util.h"
#include "Engine/UserInterfaceSettings.h"
#include "Engine/RendererSettings.h"


USpellComponent::USpellComponent(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
	PrimaryComponentTick.bCanEverTick = true;
    SetIsReplicated(true); //autorise la replication des elements
}

void USpellComponent::BeginPlay() {
	Super::BeginPlay();
	 spellWidget = CreateWidget<UUserWidget>(GetWorld(), spellWidgetClass);
    if(IsValid(spellWidget)) {
      PositionnerWidget();
    } else {
        DEBUG_SCREEN(-1, 5.f, FColor::Red, TEXT("Le spellWidget est invalide dans le BeginPlay !"));
    }
    if (GetWorld()->IsServer()) {
        currentTime = UGameplayStatics::GetRealTimeSeconds(GetWorld());
    }
    lastTimeCastSpell = currentTime - dureeCooldown;
}

void USpellComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    //MY_LOG_UI(TEXT("CA REPLIQUE USpellComponent !!!"));
    DOREPLIFETIME(USpellComponent, spellState);
    DOREPLIFETIME(USpellComponent, lastTimeCastSpell);
    DOREPLIFETIME(USpellComponent, currentTime);
}

void USpellComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction) {
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
    if (GetWorld()->IsServer()) {
        currentTime = UGameplayStatics::GetRealTimeSeconds(GetWorld());
    }
}

void USpellComponent::SetIndiceInHUD(int indice) {
    indiceBouton = indice;
    PositionnerWidget();
}

bool USpellComponent::IsActivable() {
    // On fait ça ici pour eviter d'avoir a ticker :)
    if (GetRemainingCooldown() <= 0.0f) {
        spellState = SpellState::ACTIVATABLE;
    } else {
        spellState = SpellState::IN_COOLDOWN;
    }

    ABFPCharacter* owner = Cast<ABFPCharacter>(GetOwner());
    return spellState == SpellState::ACTIVATABLE && owner->CanCastSpell();
}

void USpellComponent::TryActivate() {
    if (IsActivable()) {
        ABFPCharacter* owner = Cast<ABFPCharacter>(GetOwner());
        if (IsValid(owner)) {
            FVector position = position = owner->GetActorLocation()
                + owner->GetTransform().GetRotation().GetForwardVector() * offsetSpawning.X
                + FVector::UpVector * offsetSpawning.Z;
            FRotator rotation = owner->GetActorRotation();
            FActorSpawnParameters spawnParams;
            spawnParams.Owner = owner;
            spawnParams.bNoFail = true;
            spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
            
            ASpell* spell = GetWorld()->SpawnActor<ASpell>(spellClass, position, rotation, spawnParams);
            if (IsValid(spell)) {
                ApplyImpulseIfMeteor(spell);
                OnActivateSpell(spell);
            } else {
                DEBUG_SCREEN(-1, 5.f, FColor::Red, TEXT("Le spell n'a pas pu etre spawne !!"));
            }
        } else {
            DEBUG_SCREEN(-1, 5.f, FColor::Red, TEXT("Impossible de recuperer l'owner d'un spell !!!"));
        }
    }
}

void USpellComponent::DrawWidget() {
    if (IsValid(spellWidget)) {
		 //spellWidget->AddToViewport(0);
		  spellWidget->SetVisibility(ESlateVisibility::Visible);
        //UImage* image = dynamic_cast<UImage*>(spellWidget->GetWidgetFromName(FName("Image")));
        UBorder* greyFilter = dynamic_cast<UBorder*>(spellWidget->GetWidgetFromName(FName("GreyFilter")));
        UWidget* circular = dynamic_cast<UWidget*>(spellWidget->GetWidgetFromName(FName("Circular")));
        UTextBlock* timer = dynamic_cast<UTextBlock*>(spellWidget->GetWidgetFromName(FName("Timer")));
        if (greyFilter == nullptr || circular == nullptr || timer == nullptr) {
            DEBUG_SCREEN(-1, 5.f, FColor::Red, TEXT("Un spell widget a de mauvais noms d'elements !"));
            return;
        }
        bool isActivable = IsActivable();
        if (isActivable) {
            greyFilter->SetVisibility(ESlateVisibility::Hidden);
        } else {
            greyFilter->SetVisibility(ESlateVisibility::Visible);
        }
        if (spellState == SpellState::IN_COOLDOWN) {
            circular->SetVisibility(ESlateVisibility::Visible);
        } else {
            circular->SetVisibility(ESlateVisibility::Hidden);
        }
        if (isActivable) {
            timer->SetVisibility(ESlateVisibility::Hidden);
        } else {
            if (spellState != SpellState::ACTIVATABLE) {
                timer->SetVisibility(ESlateVisibility::Visible);
                FString str = GetFStringCooldown();
                FText t = FText::AsCultureInvariant(str);
                timer->SetText(t);
            } else {
                timer->SetVisibility(ESlateVisibility::Hidden);
            }
        }
    } else {
        DEBUG_SCREEN(-1, 5.f, FColor::Red, TEXT("Le spellWidget est n'est pas valide !"));
    }
}

void USpellComponent::HideWidget() {
	if (IsValid(spellWidget)) {
		//DEBUG_SCREEN(-1, 5.f, FColor::Blue, TEXT("YO LE SPELL WIDGET DOIT SEFFACER"));
		//
		//UImage* image = dynamic_cast<UImage*>(spellWidget->GetWidgetFromName(FName("Background/Image")));
		//UBorder* greyFilter = dynamic_cast<UBorder*>(spellWidget->GetWidgetFromName(FName("GreyFilter")));
		//UWidget* circular = dynamic_cast<UWidget*>(spellWidget->GetWidgetFromName(FName("Circular")));
		//UTextBlock* timer = dynamic_cast<UTextBlock*>(spellWidget->GetWidgetFromName(FName("Timer")));
		//if (IsValid(image)) {
		//	DEBUG_SCREEN(-1, 5.f, FColor::Blue, TEXT("IMAGE DOIT SEFFACER"));
		//	image->SetVisibility(ESlateVisibility::Hidden);
		//}
		//if (IsValid(greyFilter)) {
		//	DEBUG_SCREEN(-1, 5.f, FColor::Blue, TEXT("greyFilter DOIT SEFFACER"));
		//	greyFilter->SetVisibility(ESlateVisibility::Hidden);
		//}
		//if (IsValid(circular)) {
		//	DEBUG_SCREEN(-1, 5.f, FColor::Blue, TEXT("circular DOIT SEFFACER"));
		//	circular->SetVisibility(ESlateVisibility::Hidden);
		//}
		//if (IsValid(timer)) {
		//	DEBUG_SCREEN(-1, 5.f, FColor::Blue, TEXT("timer DOIT SEFFACER"));
		//	timer->SetVisibility(ESlateVisibility::Hidden);
		//}
		spellWidget->SetVisibility(ESlateVisibility::Hidden);
		//spellWidget->RemoveFromViewport();
	}
}


float USpellComponent::GetDureeCooldown() const {
    return dureeCooldown;
}

void USpellComponent::SetDureeCooldown(float duree) {
    dureeCooldown = duree;
}

float USpellComponent::GetRemainingCooldown() {
    if (GetWorld()->IsServer()) {
        currentTime = UGameplayStatics::GetRealTimeSeconds(GetWorld());
    }
    return lastTimeCastSpell + GetDureeCooldown() - currentTime;
}

SpellState USpellComponent::GetSpellState() const {
    return spellState;
}

void USpellComponent::PositionnerWidget() {
    //MY_LOG_UI(TEXT("POSITIONNER WIDGET !!!"));
	
    if (IsValid(spellWidget) && !GetWorld()->IsServer()) {
		
        TWeakObjectPtr<APlayerController> MyController = GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld());
        if (MyController.IsValid()) {
			
            ABFPCharacterPlayable* characterOwner = Cast<ABFPCharacterPlayable>(MyController->GetPawn());
            if (IsValid(characterOwner)) {
                ECharacterName nameOwner = characterOwner->Name;
                ABFPCharacterPlayable* characterWidget = Cast<ABFPCharacterPlayable>(GetOwner());
					
                if (IsValid(characterWidget)) {
                    ECharacterName nameWidget = characterWidget->Name;
						  
                    if (nameWidget == nameOwner) {
                        spellWidget->AddToViewport(0);
                        DEBUG_SCREEN(-1, 5.f, FColor::Red, TEXT("Sort Ajoute"));
                        spellWidget->SetVisibility(ESlateVisibility::Hidden);
                        return; // important ^^'
                    }
                }
            }
        }
        // Si jamais ça n'a pas marché, quelque soit la raison, on ne l'affiche pas !
        spellWidget->SetVisibility(ESlateVisibility::Hidden);
    }
}

FString USpellComponent::GetFStringCooldown() {
    FString str;
    float remainingTime = GetRemainingCooldown();
    std::pair<int, int> timerPair = Util::GetSecAndMilli(remainingTime);
    if (remainingTime <= 1.0f) {
        str.AppendInt(timerPair.first);
        str.Append(".");
        str.AppendInt(timerPair.second / 10); // je veux juste des centièmes de secondes :)
    } else if (remainingTime <= 3.0f) {
        str.AppendInt(timerPair.first);
        str.Append(".");
        str.AppendInt(timerPair.second / 100); // je veux juste des dixièmes de secondes :)
    } else {
        str.AppendInt(timerPair.first);
    }
    return str;
}

void USpellComponent::OnActivateSpell(ASpell* spell) {
    activatedSpells.Push(spell);
    if (GetWorld()->IsServer()) {
        currentTime = UGameplayStatics::GetRealTimeSeconds(GetWorld());
    }
    lastTimeCastSpell = currentTime;
    spellState = SpellState::IN_COOLDOWN;
}

void USpellComponent::ApplyImpulseIfMeteor(ASpell * spell) {
    ABFPCharacter* owner = Cast<ABFPCharacter>(GetOwner());
    FVector dep_0 = owner->GetTransform().GetRotation().GetForwardVector();
    FVector dep = FVector(2000.0f * dep_0.X, 2000.0f * dep_0.Y, 1000.0f * dep_0.Z);
    auto meteor = spell->GetComponentsByTag(UStaticMeshComponent::StaticClass(), "Meteor2");
    if (meteor.Num() != 0) {
       Cast<UStaticMeshComponent>(meteor[0])->AddImpulse(dep + FVector(0, 0, -1000), NAME_None, true);
    }
}
