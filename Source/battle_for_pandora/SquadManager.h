// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Message.h"
#include "Maneuver.h"
#include "SquadManager.generated.h"

/**
*
*/
UCLASS()
class BATTLE_FOR_PANDORA_API ASquadManager : public AActor
{
    GENERATED_BODY()

    /* Le descripteur de la Squad. */
    UPROPERTY(VisibleAnywhere, Category = "Squa")
    class ASquadDescriptor* desc;

    ///* Les differentes manoeuvres possibles !!! */
    //UPROPERTY(EditAnywhere, Category = "Manoeuvres")
    //TSubClassOf<AManeuver> maneuverAgressive;

public:
    ASquadManager();
    ~ASquadManager();

    void SetSquadDescriptor(ASquadDescriptor* squadDescriptor);

    void ReceiveMessage(UMessage * message);

    virtual void Tick(float DeltaTime) override;

    UFUNCTION()
    void SetManeuver(ManeuverType type);
};
