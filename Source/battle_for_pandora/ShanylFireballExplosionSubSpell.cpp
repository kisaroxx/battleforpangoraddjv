// Fill out your copyright notice in the Description page of Project Settings.

#include "ShanylFireballExplosionSubSpell.h"
#include "BFPCharacter.h"
#include "Public/TimerManager.h"
#include "Components/SphereComponent.h"
#include "BFP_GameState.h"

AShanylFireballExplosionSubSpell::AShanylFireballExplosionSubSpell(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    PrimaryActorTick.bCanEverTick = false;
}


void AShanylFireballExplosionSubSpell::BeginPlay() {
   Super::BeginPlay();

   ABFP_GameState* gs = GetWorld()->GetGameState<ABFP_GameState>();
   if (IsValid(gs)) {
       TArray<ABFPCharacter*> mechants = gs->GetAllMechant();
       mechants = gs->GetAllInDistanceFrom(mechants, GetTransform(), porteeExplosion);

       if (GetWorld()->IsServer()) {
           MY_LOG_GAME(TEXT("Nb de personnes touchees avec l'explosion de Shanyl = %d"), mechants.Num());
           for (int i = 0; i < mechants.Num(); i++) {
               // On fait les degats
               FPoussee poussee{ forceRepulsion, dureeRepulsion };
               poussee.SetDirection(mechants[i]->GetActorLocation() - GetActorLocation());
               mechants[i]->TakeDamage(degats, dureeRepulsion, poussee);
           }
       }
   }

   DestroyIn(dureeSpell);
}
