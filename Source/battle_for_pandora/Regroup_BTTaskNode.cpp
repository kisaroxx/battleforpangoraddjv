// Fill out your copyright notice in the Description page of Project Settings.

#include "Regroup_BTTaskNode.h"
#include "SquadSbireController.h"
#include "Logger.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Maneuver.h"
#include "ManeuverDefensive.h"
#include "BFPCharacterPlayable.h"
#include "SquadDescriptor.h"

URegroup::URegroup(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    NodeName = "Regroup";
    bUseCompletedMessages = true;
    bShouldFailOnAddTargetPoint = true;
}

FString URegroup::GetStaticDescription() const
{
    return TEXT("Permet a la squad de se reformer lorsqu'elle n'a rien a faire.");
}

EBTNodeResult::Type URegroup::ExecuteTaskImpl(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, AManeuver* maneuvre) {
    if (maneuvre->GetDescriptor()->GetLieutenants().Num() <= 0) { // Si on a pas de lieutenant, on fail tout de suite !
        return EBTNodeResult::Failed;
    }
    MY_LOG_IA(TEXT("%s MODE = Regroup"), *maneuvre->GetDescriptor()->GetName());
    FTransform posLieutenant = maneuvre->GetDescriptor()->GetFirstLieutenant()->GetPawn()->GetTransform();
    FVector direction = posLieutenant.GetLocation() + posLieutenant.GetRotation().GetForwardVector() * 10.0f;
    posLieutenant.SetLocation(direction);
    maneuvre->OrienterTo(posLieutenant);
    return EBTNodeResult::InProgress;
}

void URegroup::OnMessage(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, FName Message, int32 RequestID, bool bSuccess) {
    Super::OnMessage(OwnerComp, NodeMemory, Message, RequestID, bSuccess);

    // On distingue les messages
    AManeuver* maneuvre = Cast<AManeuver>(OwnerComp.GetOwner());
    UMessage* message = maneuvre->GetLastMessage();
    TArray<MessageType> messagesToIgnore{ MessageType::COMPLETED, MessageType::OUT_OF_VISION_RANGE, MessageType::OUT_OF_AGRESSION_RANGE, MessageType::OUT_OF_RUSH_RANGE };
    if (!messagesToIgnore.Contains(message->GetType())) {
        FinishLatentTask(OwnerComp, EBTNodeResult::Failed); // Tous les messages qui ne sont pas des completed ou des outs sont des raisons de relancer le BT !
    }
}
