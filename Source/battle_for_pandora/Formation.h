// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "CoreMinimal.h"
#include "Math/Vector.h"
#include "Components/ActorComponent.h"
#include "Formation.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BATTLE_FOR_PANDORA_API UFormation : public UActorComponent
{
    GENERATED_BODY()

protected:

    UPROPERTY(VisibleAnywhere, Category = "Positionnement")
        TArray<FTransform> transformsRelativesSbires;

    UPROPERTY(VisibleAnywhere, Category = "Positionnement")
        TArray<FTransform> transformsRelativesLieutenants;


public:
    // Sets default values for this component's properties
    UFormation();

protected:
    // Called when the game starts
    virtual void BeginPlay() override;

public:
    // Called every frame
    virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

    /* Permet de recuperer les positions que doivent avoir les lieutenants. */
    TArray<FTransform> GetRelativesPositionsLieutenants(int nbSbires, int nbLieutenants, float angle = 0.0f);

    /* Permet de recuperer les positions que doivent avoir les sbires. */
    TArray<FTransform> GetRelativesPositionsSbires(int nbSbires, int nbLieutenants, float angle = 0.0f);

protected:

    /* Calcul les positions relatives de tous les membres de la Squad par rapport � son centre. */
    UFUNCTION()
        virtual void ComputeAllPositions(int nbSbires, int nbLieutenants);

    TArray<FTransform> RotateArroundZero(TArray<FTransform> transforms, float angle);
};
