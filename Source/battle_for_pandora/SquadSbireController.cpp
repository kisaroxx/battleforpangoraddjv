// Fill out your copyright notice in the Description page of Project Settings.

#include "SquadSbireController.h"

#include "UObject/ConstructorHelpers.h"
#include "runtime/AIModule/Classes/BehaviorTree/BlackboardComponent.h"
#include "Runtime/AIModule/Classes/BrainComponent.h"
#include "BFPCharacterSbire.h"
#include "BFPCharacterPlayable.h"
#include "HealthComponent.h"
#include "BFP_GameState.h"
#include "UnrealNetwork.h"
#include "BehaviorTree/BehaviorTreeManager.h"
#include "SquadDescriptor.h"
#include "SquadManager.h"
#include "BFPCharacterLieutenant.h"
#include "Util.h"

ASquadSbireController::ASquadSbireController(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
	static ConstructorHelpers::FObjectFinder<UBehaviorTree> SquadSbireBT(TEXT("/Game/IA/IASbire/SquadSbireBT"));
	if (!ensure(IsValid(SquadSbireBT.Object))) return;
	sbireBT = SquadSbireBT.Object;
}


void ASquadSbireController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASquadSbireController, sbireBT);
}

void ASquadSbireController::PostInitializeComponents() {
	Super::PostInitializeComponents();

	// On set la manoeuvre
	if (IsValid(sbireBTClass.Get())) {
		//sbireBT = NewObject<UBehaviorTree>(this, sbireBTClass); // Don't touch this ! <3
	}
	else {
		MY_LOG_NETWORK(TEXT("ASquadSbireController PostInitializeComponents sbireBTClass non valide"));
	}
}

void ASquadSbireController::BeginPlay() {
	Super::BeginPlay();
   //GetSquad()->RegisterSbireController(this);
}


void ASquadSbireController::RunBT() {
	MY_LOG_NETWORK(TEXT("ASquadSbireController RunBT sbireBT ? %u"), IsValid(sbireBT));
	bool succes = RunBehaviorTree(sbireBT);
	MY_LOG_NETWORK(TEXT("ASquadSbireController RunBT result ? %u"), succes);
	UBehaviorTreeManager* BTManager = UBehaviorTreeManager::GetCurrent(GetWorld());
	if (BTManager)
	{
		BTManager->DumpUsageStats();
	}
}

void ASquadSbireController::CheckLieutenantNearby() {
   APawn *pawn = GetPawn();
   ABFPCharacterSbire* sbireActor = Cast<ABFPCharacterSbire>(pawn);
   ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
   TArray<ABFPCharacterLieutenant*> nearbyLieutenant = gameState->GetAllNearestFrom<ABFPCharacterLieutenant>(gameState->GetAllLieutenants(), sbireActor->GetActorTransform(), 1);
   UBlackboardComponent* BlackboardComponent = BrainComponent->GetBlackboardComponent();
   BlackboardComponent->SetValueAsObject("TargetLieutenant", NULL);
   if (nearbyLieutenant.Num() > 0) {
      if (FVector::Dist(nearbyLieutenant[0]->GetActorLocation(), sbireActor->GetActorLocation()) <= visionRange) {
         BlackboardComponent->SetValueAsObject("TargetLieutenant", nearbyLieutenant[0]);
      }
   }
}

void ASquadSbireController::JoinSquad() {
   UBlackboardComponent* BlackboardComponent = BrainComponent->GetBlackboardComponent();
   ABFPCharacterLieutenant* lieutenant = Cast<ABFPCharacterLieutenant>(BlackboardComponent->GetValueAsObject("TargetLieutenant"));
   //if (lieutenant && !HasSquad()) {
      ASquadLieutenantController* lieutenantController = Cast<ASquadLieutenantController>(lieutenant->GetController());
      ABFPCharacterSbire* sbireActor = Cast<ABFPCharacterSbire>(GetPawn());
      sbireActor->SetSquad(lieutenantController->GetSquad());
      sbireActor->RegisterToSquad();
  /* }
   else {
      DEBUG_SCREEN(-1, 5.f, FColor::Red, this->GetName().Append(" Join Squad with lieutenant null !"));
   }*/
}

bool ASquadSbireController::CheckTargetActorNearby() {
   APawn *pawn = GetPawn();
   ABFPCharacterSbire* sbireActor = Cast<ABFPCharacterSbire>(pawn);
   ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
   // TODO : On regarde toujours le plus proche, prendre en consideration l'acteur target courante !!!
   //TArray<ABFPCharacterPlayable*> joueursAllies = gameManager->GetAllInDistanceFrom<ABFPCharacterPlayable>(gameManager->GetAllJoueursAllies(), sbireActor->GetActorTransform(), visionRange);
   TArray<ABFPCharacterPlayable*> joueursAllies = gameState->GetAllNearestFrom<ABFPCharacterPlayable>(gameState->GetAllJoueursAllies(), sbireActor->GetActorTransform(), 1);
   UBlackboardComponent* BlackboardComponent = BrainComponent->GetBlackboardComponent();
   BlackboardComponent->SetValueAsObject("TargetActor", NULL);
   if (joueursAllies.Num() > 0) {
      if (FVector::Dist(joueursAllies[0]->GetActorLocation(), sbireActor->GetActorLocation())  <= visionRange) {
         BlackboardComponent->SetValueAsObject("TargetActor", joueursAllies[0]);
         return true;
      }
   }
   return false;
}

float ASquadSbireController::CanAttack() {
   APawn *pawn = GetPawn();
   ABFPCharacterSbire* sbireActor = Cast<ABFPCharacterSbire>(pawn);
   ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
   TArray<ABFPCharacterSbire*> nearSbires = gameState->GetAllInDistanceFrom<ABFPCharacterSbire>(gameState->GetAllSbires(), sbireActor->GetActorTransform(), visionRange);
   float nearSbiresHealth = 0.f;
   for (ABFPCharacterSbire* sbire : nearSbires) {
      nearSbiresHealth += sbire->GetHealthSystem()->GetHealth();
   }

   TArray<ABFPCharacterPlayable*> nearJoueurAllies = gameState->GetAllInDistanceFrom<ABFPCharacterPlayable>(gameState->GetAllJoueursAllies(), sbireActor->GetActorTransform(), visionRange);
   float joueursAlliesHealth = 0.f;
   for (ABFPCharacterPlayable* joueur : nearJoueurAllies) {
      joueursAlliesHealth += joueur->GetHealthSystem()->GetHealth();
   }

   if (nearSbiresHealth >= joueursAlliesHealth) {
      return true;
   }
   return false;
}


EPathFollowingRequestResult::Type ASquadSbireController::AttackEnemy() {

   APawn *pawn = GetPawn();
   ABFPCharacterSbire* sbireActor = Cast<ABFPCharacterSbire>(pawn);
   // Obtenir un pointeur sur le blackboard
   UBlackboardComponent* BlackboardComponent = BrainComponent->GetBlackboardComponent();
   // Obtenir un pointeur sur le personnage reference par la cle TargetActorToFollow
   AActor* HeroCharacterActor = Cast<AActor>(
      BlackboardComponent->GetValueAsObject("TargetActor"));

   if (FVector::Dist(sbireActor->GetActorLocation(), HeroCharacterActor->GetActorLocation()) < 200) {
      sbireActor->SimpleAttack();
      return EPathFollowingRequestResult::Type::AlreadyAtGoal;
   }
   else {
      EPathFollowingRequestResult::Type resultatFromMove = MoveToActor(HeroCharacterActor);
      return resultatFromMove;
   }
}

EPathFollowingRequestResult::Type ASquadSbireController::RunAway() {
   APawn *pawn = GetPawn();
   ABFPCharacterSbire* sbireActor = Cast<ABFPCharacterSbire>(pawn);
   // Obtenir un pointeur sur le blackboard
   UBlackboardComponent* BlackboardComponent = BrainComponent->GetBlackboardComponent();
   // Obtenir un pointeur sur le personnage reference par la cle TargetActorToFollow
   AActor* HeroCharacterActor = Cast<AActor>(
      BlackboardComponent->GetValueAsObject("TargetActor"));
   if (!HeroCharacterActor || sbireActor->IsDead()) {
      StopMovement();
      return EPathFollowingRequestResult::Failed;
   }
   // Recuperer actor Sbire
   APawn *Pawn = GetPawn();
   ABFPCharacter* SbireActor = Cast<ABFPCharacter>(Pawn);
   // Vecteur direction de fuite
   FVector fleeDirection = (SbireActor->GetActorLocation() - HeroCharacterActor->GetActorLocation());
   ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
   // Projeté dans le plan (X,Y) et normalisé
   fleeDirection.Z = 0;
   fleeDirection.Normalize();
   // Point de fuite dans le plan (X,Y)
   runAwayGoal = SbireActor->GetActorLocation() + fleeDirection * fleeDistance;
   // On redétermine sa valeur en Z selon le landscape pour être dans le NavMesh
   runAwayGoal.Z = gameState->landscapeHeights.getHeight(runAwayGoal.X, runAwayGoal.Y);
   return MoveToSpiraled(FAIMoveRequest(runAwayGoal), &runAwayGoal);
}

EPathFollowingRequestResult::Type ASquadSbireController::Wander() {
   ABFP_GameState* gameState = GetWorld()->GetGameState<ABFP_GameState>();
   float x = Util::Random(float(-gameState->landscapeHeights.getDemiSizeLandscape()), float(gameState->landscapeHeights.getDemiSizeLandscape()));
   float y = Util::Random(float(-gameState->landscapeHeights.getDemiSizeLandscape()), float(gameState->landscapeHeights.getDemiSizeLandscape()));
   FVector goal = { x,y,gameState->landscapeHeights.getHeight(x, y)};
   FAIMoveRequest request = FAIMoveRequest(goal);
   return MoveToSpiraled(request, &wanderGoal);
}