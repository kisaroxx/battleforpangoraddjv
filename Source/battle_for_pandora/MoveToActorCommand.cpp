// Fill out your copyright notice in the Description page of Project Settings.

#include "MoveToActorCommand.h"
#include "SquadMemberController.h"
#include "SquadManager.h"
#include "SquadDescriptor.h"
#include "CommandReturn.h"
#include "Logger.h"
#include "Util.h"
#include "Message.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Util.h"

UMoveToActorCommand::UMoveToActorCommand() {
    lastTimeRefresh = UGameplayStatics::GetRealTimeSeconds(GetOuter()->GetWorld());
    SetRefreshTime(refreshTime);
}

void UMoveToActorCommand::SetActorTarget(AActor* target) {
    actorTarget = target;
}

AActor* UMoveToActorCommand::GetActorTarget() {
    return actorTarget;
}

void UMoveToActorCommand::SetOffset(FVector offset_) {
    offset = offset_;
}

FVector UMoveToActorCommand::GetOffset() {
    return offset;
}

void UMoveToActorCommand::SetPointForAngle(FVector point) {
    pointForAngle = point;
}

FVector UMoveToActorCommand::GetPointForAngle()
{
    return pointForAngle;
}

void UMoveToActorCommand::SetRefreshTime(float refreshTime_) {
    refreshTime = refreshTime_;
    refreshTime += (refreshTime / 100.0f) * ((rand() % 21) - 10); // On fait en sorte que tous le monde ne refresh pas en m�me temps :3 C'est uniforme, donc c'est parfait : th�orie des grands nombres, niark :)
}

float UMoveToActorCommand::GetRefreshTime() {
    return refreshTime;
}

void UMoveToActorCommand::SetOffsetType(OffsetType type) {
    offsetType = type;
}

UCommandReturn* UMoveToActorCommand::execute() {
    UCommandReturn* comRet = NewObject<UCommandReturn>(this, UCommandReturn::StaticClass());
    comRet->setReturn(EPathFollowingRequestResult::RequestSuccessful);


    if (UGameplayStatics::GetRealTimeSeconds(GetOuter()->GetWorld()) - lastTimeRefresh > refreshTime) {
        lastTimeRefresh = UGameplayStatics::GetRealTimeSeconds(GetOuter()->GetWorld());
        pathCalculated = false;
    }

    if (pathCalculated && character->GetPathFollowingComponent()->GetStatus() != EPathFollowingStatus::Type::Moving) {
        pathCalculated = false;
    }

    if (!pathCalculated) {
        FPathFollowingRequestResult result = character->MoveToSpiraled(FAIMoveRequest(GetDestination()));
        if (result.Code == EPathFollowingRequestResult::Failed) {
            //DEBUG_SCREEN(-1, 5.0f, FColor::Red, "Attention une commande moveTo a echou�e !!!");
            UMessage* message = NewObject<UMessage>();
            message->SetParams(MessageType::FAILED, character);
            character->GetSquad()->GetSquadManager()->ReceiveMessage(message);
        }
        else {
            pathCalculated = true;
        }
        comRet->setReturn(result.Code);
    }

    //character->GetPathFollowingComponent()->GetPath()->GetPathPoints().Last().Location = actorTarget->GetActorLocation() + offset;

    // Si on est arriv�, on envoie un message
    if (!arrived) {
        float dist = FVector::Dist(Util::ProjectInXYPlane(character->GetPawn()->GetActorLocation()), Util::ProjectInXYPlane(GetDestination()));
        if (dist < 250.0f) {
            UMessage* message = NewObject<UMessage>();
            message->SetParams(MessageType::COMPLETED, character);
            character->GetSquad()->GetSquadManager()->ReceiveMessage(message);
            arrived = true;
        }
    }

    return comRet;
}

OffsetType UMoveToActorCommand::GetOffsetType() {
    return offsetType;
}

FVector UMoveToActorCommand::GetDestination() {
    if (offsetType == OffsetType::RELATIF) {
        FVector lieutenantPosition = character->GetSquad()->GetFirstLieutenant()->GetPawn()->GetActorLocation();
        float angle = Util::AngleBetweenVectorsInXYPlane(FVector::ForwardVector, lieutenantPosition - actorTarget->GetActorLocation());
        FVector newOffset = offset.RotateAngleAxis(angle, FVector::UpVector);
        return actorTarget->GetActorLocation() + newOffset;
    }
    else if (offsetType == OffsetType::RELATIF_TO_POINT) {
        FVector lieutenantPosition = character->GetSquad()->GetFirstLieutenant()->GetPawn()->GetActorLocation();
        float angle = Util::AngleBetweenVectorsInXYPlane(FVector::ForwardVector, lieutenantPosition - pointForAngle);
        FVector newOffset = offset.RotateAngleAxis(angle, FVector::UpVector);
        return actorTarget->GetActorLocation() + newOffset;
    }
    else { // ABSOLU
        return actorTarget->GetActorLocation() + offset;
    }
}
