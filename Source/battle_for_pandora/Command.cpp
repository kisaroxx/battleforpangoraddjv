// Fill out your copyright notice in the Description page of Project Settings.

#include "Command.h"
#include "SquadMemberController.h"
#include "Util.h"
#include "BFP_GameState.h"
#include "DrawDebugHelpers.h"

UCommand::UCommand()
{
}

UCommand::~UCommand()
{
}

UCommandReturn* UCommand::execute() {
    // Personne ne doit instancier de commandes simples
    return nullptr;
}

void UCommand::setCharacter(ASquadMemberController * chara)
{
    character = chara;
}
