// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "battle_for_pandora.h"
#include "Engine/Engine.h"
#include "DrawDebugHelpers.h"

// REMPLACE UE_LOG(LogTemp, Log, TEXT("Health apres = %f"), Health);
//Remplace GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, TEXT("Start game"));
//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("GameMode ALobbyGM start game %s"), *playerController->GetFName().ToString()));



#define UE_LOG_FATAL(CategoryName, Format, ...)		UE_LOG(CategoryName, Fatal, Format, ##__VA_ARGS__)
#define UE_LOG_ERROR(CategoryName, Format, ...)		UE_LOG(CategoryName, Error, Format, ##__VA_ARGS__)
#define UE_LOG_WARNING(CategoryName, Format, ...)	UE_LOG(CategoryName, Warning, Format, ##__VA_ARGS__)
#define UE_LOG_DISPLAY(CategoryName, Format, ...)	UE_LOG(CategoryName, Display, Format, ##__VA_ARGS__)
#define UE_LOG_LOG(CategoryName, Format, ...)		UE_LOG(CategoryName, Log, Format, ##__VA_ARGS__)
#define UE_LOG_VERBOSE(CategoryName, Format, ...)	UE_LOG(CategoryName, Verbose, Format, ##__VA_ARGS__)
#define UE_LOG_VERY_VERBOSE(CategoryName, Format, ...) UE_LOG(CategoryName, VeryVerbose, Format, ##__VA_ARGS__)

#define MY_LOG(Format, ...)			UE_LOG_LOG(MyLogMyGame, Format, ##__VA_ARGS__)
#define MY_LOG_ERROR(Format, ...)	UE_LOG_ERROR(MyLogMyGame, Format, ##__VA_ARGS__)
#define MY_LOG_WARNING(Format, ...)	UE_LOG_WARNING(MyLogMyGame, Format, ##__VA_ARGS__)

#define MY_LOG_IA(Format, ...)			UE_LOG_LOG(MyLogIA, Format, ##__VA_ARGS__)
#define MY_LOG_IA_ERROR(Format, ...)	UE_LOG_ERROR(MyLogIA, Format, ##__VA_ARGS__)
#define MY_LOG_IA_WARNING(Format, ...)	UE_LOG_WARNING(MyLogIA, Format, ##__VA_ARGS__)

#define MY_LOG_UI(Format, ...)			UE_LOG_LOG(MyLogUI, Format, ##__VA_ARGS__)
#define MY_LOG_UI_ERROR(Format, ...)	UE_LOG_ERROR(MyLogUI, Format, ##__VA_ARGS__)
#define MY_LOG_UI_WARNING(Format, ...)	UE_LOG_WARNING(MyLogUI, Format, ##__VA_ARGS__)

#define MY_LOG_NETWORK(Format, ...)			UE_LOG_LOG(MyLogNetwork, Format, ##__VA_ARGS__)
#define MY_LOG_NETWORK_ERROR(Format, ...)	UE_LOG_ERROR(MyLogNetwork, Format, ##__VA_ARGS__)
#define MY_LOG_NETWORK_WARNING(Format, ...)	UE_LOG_WARNING(MyLogNetwork, Format, ##__VA_ARGS__)

#define MY_LOG_RENDER(Format, ...)			UE_LOG_LOG(MyLogRender, Format, ##__VA_ARGS__)
#define MY_LOG_RENDER_ERROR(Format, ...)	UE_LOG_ERROR(MyLogRender, Format, ##__VA_ARGS__)
#define MY_LOG_RENDER_WARNING(Format, ...)	UE_LOG_WARNING(MyLogRender, Format, ##__VA_ARGS__)

#define MY_LOG_GAME(Format, ...)			UE_LOG_LOG(MyLogGame, Format, ##__VA_ARGS__)
#define MY_LOG_GAME_ERROR(Format, ...)		UE_LOG_ERROR(MyLogGame, Format, ##__VA_ARGS__)
#define MY_LOG_GAME_WARNING(Format, ...)	UE_LOG_WARNING(MyLogGame, Format, ##__VA_ARGS__)

#define MY_LOG_SON(Format, ...)			UE_LOG_LOG(MyLogSon, Format, ##__VA_ARGS__)
#define MY_LOG_SON_ERROR(Format, ...)	UE_LOG_ERROR(MyLogSon, Format, ##__VA_ARGS__)
#define MY_LOG_SON_WARNING(Format, ...)	UE_LOG_WARNING(MyLogSon, Format, ##__VA_ARGS__)

#ifndef UE_BUILD_DEVELOPMENT
    #define DEBUG_SCREEN(key, timeToDisplay, color, text) \
        if(GEngine) \
        { \
            GEngine->AddOnScreenDebugMessage(key, timeToDisplay, color, text); \
        }
    #define DRAW_DEBUG_LINE(world, start, end, ...) DrawDebugLine(world, start, end, ##__VA_ARGS__)
#else
    #define DEBUG_SCREEN(key, timeToDisplay, color, text) 0
    #define DRAW_DEBUG_LINE(world, start, end, ...) 0
#endif
