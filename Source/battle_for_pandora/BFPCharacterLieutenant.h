// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BFPCharacterNonPlayable.h"
#include "BFPCharacterLieutenant.generated.h"

/**
 *
 */
UCLASS()
class BATTLE_FOR_PANDORA_API ABFPCharacterLieutenant : public ABFPCharacterNonPlayable
{
    GENERATED_BODY()

    UPROPERTY(Replicated, Transient, VisibleAnywhere, Category = "Checkpoint")
    class ACheckpointTriggerSphere* checkpointAssociated = nullptr;

public :
   ABFPCharacterLieutenant(const FObjectInitializer & ObjectInitializer);

	void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;

   // Called when the game starts
   virtual void BeginPlay() override;

	virtual void Die() override;

	virtual void RegisterToSquad() override;

	virtual void UnRegisterToSquad() override;

protected:
	virtual void UnRegisterToGameManager() override;
};
