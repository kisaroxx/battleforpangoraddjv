// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <vector>

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/TriggerCapsule.h"
#include "GameFramework/MovementComponent.h"
#include "Poussee.h"
#include "Attaque.generated.h"

UCLASS()
class BATTLE_FOR_PANDORA_API AAttaque : public ATriggerCapsule
{
    GENERATED_BODY()

    /* Les donn�es n�cessaires pour suivre le poing du perso. */
    class USkeletalMeshComponent* mesh;
    int boneIndex;
    FVector offset;
    FRotator offsetRotation;
    float dammages;
    float dureeStun;
    float dureeActivation;
    FPoussee poussee;

    /* Le lanceur de l'attaque. */
    class AActor* owner;
    /* L'attaque component responsable de cette attaque. */
    class UAttaqueComponent* attaqueComponent;

    // La liste des personnes touches par l'attaque. 
    std::vector<class AActor*> touchedList{};

public:	
	// Sets default values for this actor's properties
	AAttaque();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

    /* Permet de supprimer une attaque.
     * Par exemple si on se fait annul� en pleine attaque. */
    UFUNCTION()
    void DestroyAttaque();

    void InitPointAttache(class USkeletalMeshComponent* mesh, int boneIndex);

    UFUNCTION()
    void OnActorOverlap(class AActor* OverlappedActor, class AActor* OtherActor);

    void SetOwner(class AActor* owner_, class UAttaqueComponent* attaqueComponent);

    void SetParams(float dammages_, FVector offset_, FRotator offsetRotation_, float dureeStun_, float dureeActivation_, const FPoussee& poussee);
};
