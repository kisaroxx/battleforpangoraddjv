// Fill out your copyright notice in the Description page of Project Settings.

#include "SoundActorPlayable.h"

// Sets default values
ASoundActorPlayable::ASoundActorPlayable()
{
   // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
   PrimaryActorTick.bCanEverTick = false;
   CurrentSoundState = ESoundState::SS_Exploration;

}

void ASoundActorPlayable::SoundStateFlag_Broadcast(FSoundStateDelegate &toFlag, ESoundState EnumValue)
{
   if (CurrentSoundState != EnumValue) {
      setCurrentSoundState(EnumValue);
      toFlag.Broadcast();      
   }
}

void ASoundActorPlayable::setCurrentSoundState(ESoundState EnumValue)
{
   CurrentSoundState = EnumValue;
}

// Called when the game starts or when spawned
void ASoundActorPlayable::BeginPlay()
{
   Super::BeginPlay();

}

// Called every frame
void ASoundActorPlayable::Tick(float DeltaTime)
{
   Super::Tick(DeltaTime);

}

