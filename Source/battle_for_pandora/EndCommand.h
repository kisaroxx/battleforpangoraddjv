// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Command.h"
#include "EndCommand.generated.h"

/**
 * 
 */
UCLASS()
class BATTLE_FOR_PANDORA_API UEndCommand : public UCommand
{
	GENERATED_BODY()

   class UCommandReturn* execute() override;
	
};
