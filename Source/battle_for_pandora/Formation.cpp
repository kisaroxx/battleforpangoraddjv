// Fill out your copyright notice in the Description page of Project Settings.

#include "Formation.h"

// Sets default values for this component's properties
UFormation::UFormation()
{
    // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
    // off to improve performance if you don't need them.
    PrimaryComponentTick.bCanEverTick = false;
}


// Called when the game starts
void UFormation::BeginPlay() {
    Super::BeginPlay();

    // ...

}


// Called every frame
void UFormation::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

    // ...
}

TArray<FTransform> UFormation::GetRelativesPositionsLieutenants(int nbSbires, int nbLieutenants, float angle) {
    if (nbLieutenants != transformsRelativesLieutenants.Num() || nbSbires != transformsRelativesSbires.Num()) {
        ComputeAllPositions(nbSbires, nbLieutenants); // Dynamic Linking ! <3
    }
    return RotateArroundZero(transformsRelativesLieutenants, angle);
}

TArray<FTransform> UFormation::GetRelativesPositionsSbires(int nbSbires, int nbLieutenants, float angle) {
    if (nbLieutenants != transformsRelativesLieutenants.Num() || nbSbires != transformsRelativesSbires.Num()) {
        ComputeAllPositions(nbSbires, nbLieutenants); // Dynamic Linking ! <3
    }
    return RotateArroundZero(transformsRelativesSbires, angle);
}

void UFormation::ComputeAllPositions(int nbSbires, int nbLieutenants) {
    transformsRelativesSbires.Empty();
    transformsRelativesLieutenants.Empty();

    for (int i = 0; i < nbSbires; ++i) {
        FVector pos{ 0.0f, 0.0f, 0.0f };
        FQuat quat = FQuat::Identity;
        FVector scale{ 1.0f, 1.0f, 1.0f };
        transformsRelativesSbires.Add(FTransform{ quat, pos, scale });
    }
    for (int i = 0; i < nbLieutenants; ++i) {
        FVector pos{ 0.0f, 0.0f, 0.0f };
        FQuat quat = FQuat::Identity;
        FVector scale{ 1.0f, 1.0f, 1.0f };
        transformsRelativesLieutenants.Add(FTransform{ quat, pos, scale });
    }
}

TArray<FTransform> UFormation::RotateArroundZero(TArray<FTransform> transforms, float angle) {
    for (int i = 0; i < transforms.Num(); i++) {
        // On veut tourner autour de (0, 0, 1) de angle degree
        FVector pos = transforms[i].GetLocation();
        pos = pos.RotateAngleAxis(angle, FVector::UpVector);
        transforms[i].SetLocation(pos);
    }
    return transforms;
}

