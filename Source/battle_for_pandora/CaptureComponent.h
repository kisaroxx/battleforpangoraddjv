// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core.h" //Pour la replication on utilise Core.h
#include "GameFramework/Character.h"
#include "Components/ActorComponent.h"
#include "Net/UnrealNetwork.h"//Pour la replication
#include "CheckpointTriggerSphere.h"
#include "CaptureComponent.generated.h"

DECLARE_EVENT(UCaptureComponent, FChangeValueEvent);
DECLARE_EVENT(UCaptureComponent, FShowEvent);
DECLARE_EVENT(UCaptureComponent, FChangeAffiliationEvent);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BATTLE_FOR_PANDORA_API UCaptureComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCaptureComponent(const FObjectInitializer & ObjectInitializer);

	UPROPERTY(ReplicatedUsing = OnRep_Show, Transient, EditAnywhere,  Category = "Capture")
	uint8 bShow:1;
	UPROPERTY(ReplicatedUsing = OnRep_Affiliation, Transient, EditAnywhere,  Category = "Capture")
	Affiliation checkpointAffiliation;
	UPROPERTY(ReplicatedUsing = OnRep_ValueChange, Transient, EditAnywhere,  Category = "Capture")
	float captureValue;

	void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;

	FChangeValueEvent ChangeValueEvent;
	FShowEvent ShowEvent;
	FChangeAffiliationEvent ChangeAffiliationEvent;

	UFUNCTION()
	void OnRep_Show();
	UFUNCTION()
	void OnRep_Affiliation();
	UFUNCTION()
	void OnRep_ValueChange();
};
