// Fill out your copyright notice in the Description page of Project Settings.

#include "ManeuverDefensive.h"
#include "Logger.h"
#include "MoveCommand.h"
#include "SquadDescriptor.h"
#include "SquadMemberController.h"
#include "SquadLieutenantController.h"
#include "SquadSbireController.h"
#include "GameFramework/Controller.h"
#include "GameFramework/Actor.h"
#include "BFPCharacter.h"
#include "BFPCharacterPlayable.h"
#include "Formation.h"
#include "BFP_GameState.h"

void AManeuverDefensive::BeginPlay() {
    Super::BeginPlay();
}

void AManeuverDefensive::SetSquadDescriptor(ASquadDescriptor * descriptor) {
    Super::SetSquadDescriptor(descriptor);
    FVector position = GetDescriptor()->GetFirstLieutenant()->GetPawn()->GetActorLocation();
    lastTargetPoint = GetWorld()->SpawnActor<AManeuverTargetPoint>(position, FRotator(0, 0, 0));
    if (targetPoints.Num() == 0)
        targetPoints.Add(lastTargetPoint);
}

void AManeuverDefensive::RemoveFirstTargetPoint() {
    lastTargetPoint->SetActorLocation(targetPoints[0]->GetActorLocation());
    if (targetPoints.Num() > 0)
        targetPoints.RemoveAt(0);
    if (targetPoints.Num() == 0)
        targetPoints.Add(lastTargetPoint);
}

FVector AManeuverDefensive::GetLastTargetPoint() const {
    return lastTargetPoint->GetActorLocation();
}

float AManeuverDefensive::GetDistanceMaxFromLastTargetPoint() const
{
    return distanceMaxFromLastTargetPoint;
}

