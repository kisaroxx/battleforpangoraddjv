// Fill out your copyright notice in the Description page of Project Settings.

#include "KiaraBackstabSpell.h"
#include "Public/TimerManager.h"
#include "Engine/World.h"
#include "BFPCharacter.h"

AKiaraBackstabSpell::AKiaraBackstabSpell(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    PrimaryActorTick.bCanEverTick = true;
}

void AKiaraBackstabSpell::BeginPlay() {
   Super::BeginPlay();
   ABFPCharacter* kiara = Cast<ABFPCharacter>(GetOwner());
   if (IsValid(kiara)) {
       float currentMultiplicateur = kiara->GetDegatMultiplicateur();
       if (currentMultiplicateur < pow(degatMultiplicateur, nbStacksMax)) { // S'il n'y a pas deja trop de stacks
           int currentStack = 1;
           for (; currentMultiplicateur >= degatMultiplicateur; currentMultiplicateur /= degatMultiplicateur)
               currentStack++;
           if(GetWorld()->IsServer())
               kiara->ModifyDegatMultiplicateur(degatMultiplicateur);
           SetActorScale3D(FVector::OneVector * currentStack * facteurDAgrandissement);
       } else {
           AutoDestroy(); // sinon il ne se passe rien haha
       }
   }
}

void AKiaraBackstabSpell::Tick(float DeltaTime) {
   Super::Tick(DeltaTime);
   ABFPCharacter* kiara = Cast<ABFPCharacter>(GetOwner());
   if (IsValid(kiara)) {
       // On se positionne sur kiara
       SetActorLocation(kiara->GetActorLocation());

        // Si on remarque que kiara a declenche un coup, on enleve le multiplicateur et on s'auto-detruit !
       AttackState attaqueState = kiara->GetAttackState();
       CombatState combatState = kiara->GetCombatState();
       // On lui laisse quand meme une chance, faudrait pas qu'elle se fasse cancel sur l'arming quand meme haha
       // Par contre elle se fait cancel si elle se fait toucher ! :)
       if (attaqueState == AttackState::ACTIVATING || combatState == CombatState::HIT) {
           if (GetWorld()->IsServer()) {
               kiara->ModifyDegatMultiplicateur(1.0f / degatMultiplicateur); // On enleve les degats
           }
           AutoDestroy();
       }
   }
}
