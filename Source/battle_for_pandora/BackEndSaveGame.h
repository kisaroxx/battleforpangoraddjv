// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "Kismet/GameplayStatics.h"
#include "BackEndSaveGame.generated.h"

/**
 *
 */
UCLASS()
class BATTLE_FOR_PANDORA_API UBackEndSaveGame : public USaveGame
{
   GENERATED_BODY()

public:
   UPROPERTY(BlueprintReadWrite, Category = Basic)
      FString SaveSlotName;

   UPROPERTY(VisibleAnywhere, Category = Basic)
      uint32 UserIndex;

   UPROPERTY(BlueprintReadWrite, Category = Basic)
      FString PlayerIndex;

   UPROPERTY(BlueprintReadWrite, Category = Basic)
      FString PlayerGuestToken;

   UPROPERTY(BlueprintReadWrite, Category = Basic)
      FString PlayerSessionToken;

   UPROPERTY(BlueprintReadWrite, Category = Basic)
      FString PlayerName;

   UPROPERTY(VisibleAnywhere, Category = Basic)
      uint32 PlayerWins;

   UPROPERTY(VisibleAnywhere, Category = Basic)
      uint32 PlayerLoss;

   UBackEndSaveGame();

};
