// Fill out your copyright notice in the Description page of Project Settings.

#include "ManeuverAgressive.h"
#include "Logger.h"
#include "MoveCommand.h"
#include "SquadDescriptor.h"
#include "SquadMemberController.h"
#include "SquadLieutenantController.h"
#include "SquadSbireController.h"
#include "GameFramework/Controller.h"
#include "GameFramework/Actor.h"
#include "BFPCharacter.h"
#include "BFPCharacterPlayable.h"
#include "Formation.h"
#include "BFP_GameState.h"

void AManeuverAgressive::Execute() {

    // Detecter les joueurs en vue
    DetecterJoueurs();

    // Si aucun, aller au point d�finis
    if (joueursVus.Num() <= 0) {
        MoveToNextPoint();

    // Si il y en a au moins un
    } else {
        // trouver le plus proche
        FTransform lieutT = desc->GetLieutenants()[0]->GetPawn()->GetTransform();
        ABFPCharacterPlayable* plusProche = GetWorld()->GetGameState<ABFP_GameState>()->GetAllNearestFrom(joueursVus, lieutT, 1)[0];
        if (plusProche != cibleLaPlusProche) {
            shouldCircle = true;
            cibleLaPlusProche = plusProche;
        }

        // L'encercler puis l'attaquer si il est assez proche
        float dist = FVector::Dist(cibleLaPlusProche->GetActorLocation(), desc->GetLieutenants()[0]->GetPawn()->GetActorLocation());
        if (dist <= distanceMaxToCircle) {
            EncerclerPuisAttaquer(cibleLaPlusProche);
        } else {
            MoveTo(cibleLaPlusProche->GetTransform());
        }
    }
}

void AManeuverAgressive::EncerclerPuisAttaquer(class ABFPCharacterPlayable* joueur) {
    float dist = FVector::Dist(cibleLaPlusProche->GetActorLocation(), desc->GetLieutenants()[0]->GetPawn()->GetActorLocation());
    if (dist > distanceMaxToAttack
    || (shouldCircle && !DejaEncercle(joueur, pourcentageEncerclementAvantAttaque, distanceToPosEncerclementBeforeAttaque))) {
        shouldCircle = true;
        EncerclerJoueur(joueur);
    } else {
        shouldCircle = false;
        Attaquer(joueur);
    }
}
