// Fill out your copyright notice in the Description page of Project Settings.

#include "CommandReturn.h"
#include "Navigation/PathFollowingComponent.h"

UCommandReturn::UCommandReturn()
{
}

UCommandReturn::~UCommandReturn()
{
}

void UCommandReturn::setReturn(TEnumAsByte<EPathFollowingRequestResult::Type> type)
{
   movementReturn = type;
}

TEnumAsByte<EPathFollowingRequestResult::Type> UCommandReturn::getReturn()
{
   return movementReturn;
}

TEnumAsByte<EPathFollowingRequestResult::Type> UCommandReturn::getReturnSimpleAttack() {
   return attackReturn;
}

void UCommandReturn::setReturnSimpleAttack(TEnumAsByte<EPathFollowingRequestResult::Type> type) {
   attackReturn = type;
}

void UCommandReturn::SetAttackLaunched(bool nb) {
   attackLaunched = nb;
}

bool UCommandReturn::GetAttackLaunched() {
   return attackLaunched;
}

void UCommandReturn::SetEndCommand(bool _endcommand) {
   endCommand = _endcommand;
}

bool UCommandReturn::HasEndCommand() {
   return endCommand;
}