// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Spell.h"
#include "Engine/TriggerCapsule.h"
#include "Engine/StaticMeshActor.h"
#include "ShanylFireballExplosionSubSpell.generated.h"

UCLASS()
class BATTLE_FOR_PANDORA_API AShanylFireballExplosionSubSpell : public ASpell
{
    GENERATED_BODY()

    /* Les degats de l'explosion. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float degats = 100.0f;

    /* La duree du spell. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float dureeSpell = 0.2f;

    /* La force avec laquelle on est poussee. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float forceRepulsion = 1200.0f;

    /* La duree pendant laquelle on est poussee. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float dureeRepulsion = 0.3f;

    /* Portee de l'explosion. */
    UPROPERTY(EditAnywhere, Category = "Spell")
    float porteeExplosion = 250.0f;

protected:
   AShanylFireballExplosionSubSpell(const FObjectInitializer & ObjectInitializer);

   // Called when the game starts or when spawned
   virtual void BeginPlay() override;
};