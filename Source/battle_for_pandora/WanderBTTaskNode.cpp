// Fill out your copyright notice in the Description page of Project Settings.

#include "WanderBTTaskNode.h"
#include "SquadSbireController.h"

UWanderBTTaskNode::UWanderBTTaskNode()
{
   // Le nom que prendra le noeud dans le BT
   NodeName = "Wander";
   // Cette t�che appelle TickTask
   bNotifyTick = true;
}

/* Sera appel�e au d�marrage de la t�che et devra retourner Succeeded, Failed ou InProgress */
EBTNodeResult::Type UWanderBTTaskNode::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8*
   NodeMemory)
{

   // Obtenir un pointeur sur notre AIEnemyController
   ASquadSbireController* sbireController = Cast<ASquadSbireController>(OwnerComp.GetOwner());

   EPathFollowingRequestResult::Type MoveToActorResult = sbireController->Wander();

   if (MoveToActorResult == EPathFollowingRequestResult::Failed)
   {
      return EBTNodeResult::Failed;
   }
   else if (MoveToActorResult == EPathFollowingRequestResult::AlreadyAtGoal) {
      return EBTNodeResult::Succeeded;
   }
   return EBTNodeResult::InProgress;

}

/* Sera appel�e constamment tant que la t�che n'est pas finie (tant que ExecuteTask retourne
InProgress) */
void UWanderBTTaskNode::TickTask(class UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) {
   ASquadSbireController* sbireController = Cast<ASquadSbireController>(OwnerComp.GetOwner());
   //GetPathFollowingComponent()->GetStatus() : Savoir l'etat du mvmt en cours, peut empecher de refaire un move ?
   EPathFollowingRequestResult::Type MoveToActorResult = sbireController->MoveTo(FAIMoveRequest(sbireController->wanderGoal));

   if (!sbireController->CheckTargetActorNearby() && !sbireController->HasSquad()) {

      if (MoveToActorResult == EPathFollowingRequestResult::AlreadyAtGoal)
      {
         FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
      }
      else if (MoveToActorResult == EPathFollowingRequestResult::Failed) {
         FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
      }
      FinishLatentTask(OwnerComp, EBTNodeResult::InProgress);
   }
   else {
      FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
   }
}

/** Retourne une chaine de description pour la t�che. Ce texte appara�tre dans le BT */
FString UWanderBTTaskNode::GetStaticDescription() const
{
   return TEXT("Fait errer le sbire dans le monde");
}