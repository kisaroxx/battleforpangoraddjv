// Fill out your copyright notice in the Description page of Project Settings.

#include "EndCommand.h"

UCommandReturn* UEndCommand::execute() {
   UCommandReturn* comRet = NewObject<UCommandReturn>(this, UCommandReturn::StaticClass());
   comRet->SetEndCommand(true);
   return comRet;
}