// Fill out your copyright notice in the Description page of Project Settings.

#include "Attaque.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/MovementComponent.h"
#include "HealthComponent.h"
#include "BFPCharacter.h"
#include "BFPCharacterPlayable.h"
#include "Engine/World.h"
#include "AttaqueComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "BFPCharacterNonPlayable.h"
#include "Message.h"
#include "SquadDescriptor.h"
#include "SquadManager.h"
#include "Logger.h"
#include "Engine.h"
#include "Util.h"


using namespace std;
using std::vector;

// Sets default values
AAttaque::AAttaque()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SetActorHiddenInGame(false);
    SetReplicates(false);
	//FScriptDelegate delegateOnOverlapBegin;
	//delegateOnOverlapBegin.BindUFunction(this, FName("OnCapsuleCompOverlapBegin"));

	OnActorBeginOverlap.AddDynamic(this, &AAttaque::OnActorOverlap);
}

void AAttaque::OnActorOverlap(class AActor* OverlappedActor, class AActor* OtherActor)
{
    if (!GetWorld()->IsServer()) {
        return;
    }
	//DEBUG_SCREEN(-1, 5.f, FColor::Yellow, TEXT("OnActorOverlap Overlap Detected"));
    if (!IsValid(owner)) {
        DEBUG_SCREEN(-1, 5.f, FColor::Red, TEXT("Le owner d'une attaque est null !!!"));
        // Si on est mort entre temps, alors on ne fait pas de degats !!! :)
        return;
    }
	if (Cast<ABFPCharacter>(owner)->getCharacterAffiliation() == Affiliation::NEUTRAL) {
		DEBUG_SCREEN(-1, 5.f, FColor::Yellow, TEXT("OnActorOverlap attaque Neutral impact"));
		return;
	}
	if (OtherActor != owner) {
		ABFPCharacter* otherChar = Cast<ABFPCharacter>(OtherActor);
		MY_LOG_NETWORK(TEXT("AAttaque OnActorOverlap begin OtherActor != owner"));
		MY_LOG_NETWORK(TEXT("AAttaque OnActorOverlap begin otherChar valid : %u"), IsValid(otherChar));
		if (otherChar) {
			if (find(touchedList.begin(), touchedList.end(), OtherActor) == touchedList.end()) {
				ABFPCharacter* ownerCharacter = Cast<ABFPCharacter>(owner);
				MY_LOG_NETWORK(TEXT("AAttaque OnActorOverlap begin ownerCharacter valid : %u"), IsValid(ownerCharacter));
				// Si on est de la même faction, on ne se touche pas
				if (ownerCharacter && otherChar->getCharacterAffiliation() != ownerCharacter->getCharacterAffiliation()) {
					if (IsValid(Cast<ABFPCharacterPlayable>(OtherActor)) && !Cast<ABFPCharacterPlayable>(OtherActor)->bGhosted) {
						FVector directionPoussee = Util::ProjectInXYPlane(otherChar->GetActorLocation() - GetActorLocation()) + FVector::UpVector * 30.0f;
						poussee.SetDirection(directionPoussee);
						MY_LOG_NETWORK(TEXT("AAttaque OnActorOverlap PlayerCharacter %s"), *otherChar->GetName());
						MY_LOG_NETWORK(TEXT("AAttaque OnActorOverlap healthsystem PlayerCharacter valid test"));
						MY_LOG_NETWORK(TEXT("AAttaque OnActorOverlap healthsystem PlayerCharacter valid %u"), IsValid(otherChar->GetHealthSystem()));
						otherChar->TakeDamage(static_cast<float>(dammages), dureeStun, poussee);
						// Si la cible est morte et qu'elle a été tué par un squadmember, alors on envoie un message
						if (otherChar->IsDead() && IsValid(Cast<ABFPCharacterNonPlayable>(ownerCharacter))) {
							UMessage* message = NewObject<UMessage>();
							message->SetParams(MessageType::KILLED, ownerCharacter->GetController<ASquadMemberController>());
							ASquadDescriptor* squad = ownerCharacter->GetController<ASquadMemberController>()->GetSquad();
							if (IsValid(squad)) {
								squad->GetSquadManager()->ReceiveMessage(message);
							}
						}
						GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("Touche !"));
						// On ajoute l'actor a la liste des personnes touches pour le pas le toucher � nouveau !
						touchedList.push_back(OtherActor);
					}
					else if(!IsValid(Cast<ABFPCharacterPlayable>(OtherActor))){
						FVector directionPoussee = Util::ProjectInXYPlane(otherChar->GetActorLocation() - GetActorLocation()) + FVector::UpVector * 30.0f;
						poussee.SetDirection(directionPoussee);
						MY_LOG_NETWORK(TEXT("AAttaque OnActorOverlap healthsystem OtherCharacter valid test"));
						MY_LOG_NETWORK(TEXT("AAttaque OnActorOverlap healthsystem OtherCharacter valid %u"), IsValid(otherChar->GetHealthSystem()));
						otherChar->TakeDamage(static_cast<float>(dammages), dureeStun, poussee);
						//// Si la cible est morte et qu'elle a été tué par un squadmember, alors on envoie un message
						//if (otherChar->IsDead() && Cast<ABFPCharacterNonPlayable>(ownerCharacter) != nullptr) {
						//	UMessage* message = NewObject<UMessage>();
						//	message->SetParams(MessageType::KILLED, ownerCharacter->GetController<ASquadMemberController>());
						//	ASquadDescriptor* squad = ownerCharacter->GetController<ASquadMemberController>()->GetSquad();
						//	if (squad != nullptr) {
						//		squad->GetSquadManager()->ReceiveMessage(message);
						//	}
						//}
						GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("Touche !"));
						// On ajoute l'actor a la liste des personnes touches pour le pas le toucher � nouveau !
						touchedList.push_back(OtherActor);						
					}
					else
					{
						DEBUG_SCREEN(-1, 5.f, FColor::Yellow, TEXT("AAttaque OnActorOverlap can NOT be damaged"));
					}
				}
			}
			else {
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Black, TEXT("Deja touche !"));
			}
		}
	}
}

void AAttaque::SetOwner(AActor * owner_, UAttaqueComponent* attaqueComponent_) {
	owner = owner_;
	attaqueComponent = attaqueComponent_;
}

// Called when the game starts or when spawned
void AAttaque::BeginPlay() {
	Super::BeginPlay();
}

// Called every frame
void AAttaque::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	// La position de l'attaque va suivre le pointDAttache
	FTransform transform = mesh->GetBoneTransform(boneIndex);

	// On décale l'arme de son offset, une fois pour chaque offset
	FVector forward = transform.GetRotation().GetForwardVector();
	FVector right = transform.GetRotation().GetRightVector();
	FVector up = transform.GetRotation().GetUpVector();
	forward.Normalize();
	right.Normalize();
	up.Normalize();
	FVector newLocation = transform.GetLocation() + offset.X * forward + offset.Y * right + offset.Z * up;

	// Puis on la fait tourner selon ses orientations
	FRotator newRotation = transform.GetRotation().Rotator();
	newRotation.Add(offsetRotation.Pitch, offsetRotation.Yaw, offsetRotation.Roll);
	SetActorLocationAndRotation(newLocation, newRotation.Quaternion());
}

void AAttaque::DestroyAttaque() {
	attaqueComponent->UntrackAttack(this);
	Destroy();
}

void AAttaque::InitPointAttache(USkeletalMeshComponent* mesh_, int boneIndex_) {
	mesh = mesh_;
	boneIndex = boneIndex_;
}

void AAttaque::SetParams(float dammages_, FVector offset_, FRotator offsetRotation_, float dureeStun_, float dureeActivation_, const FPoussee& poussee_) {
	dammages = dammages_;
	offset = offset_;
	offsetRotation = offsetRotation_;
	dureeStun = dureeStun_;
	dureeActivation = dureeActivation_;
	poussee = poussee_;

	// On d�truit l'objet � la fin de son temps d'activation !
	// C'est pas bo de le faire ici mais j'ai besoin que le param soit set ! :'(
	if (dureeActivation <= 0.0f) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("dureeActivation � 0 !!!! C'est pas bon !"));
	}
	FTimerHandle fuzeTimerHandle;
	GetWorld()->GetTimerManager().SetTimer(fuzeTimerHandle, this, &AAttaque::DestroyAttaque, dureeActivation, false);
}
