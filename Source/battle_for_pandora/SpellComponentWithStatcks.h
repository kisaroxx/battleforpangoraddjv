// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SpellComponent.h"
#include "Components/ActorComponent.h"
#include "SpellComponentWithStatcks.generated.h"


UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BATTLE_FOR_PANDORA_API USpellComponentWithStatkcs : public USpellComponent
{
    GENERATED_BODY()

    /* Le nombre de statcks maximum. */
    UPROPERTY(EditAnywhere, Category = "Spell|Stacks")
    int maxNbStacks = 3;

    /* Le nombre de statcks maximum. */
    UPROPERTY(Replicated, Transient, VisibleAnywhere, Category = "Spell|Stacks")
    int currentNbStacks = maxNbStacks;

public:	
	USpellComponentWithStatkcs(const FObjectInitializer & ObjectInitializer);
    virtual void BeginPlay() override;
    virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
    virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;

    /* Pour savoir si l'on peut activer le Spell. */
    /* La methode n'est pas const car elle met � jour l'�tat spellState � chaque appel. */
    // Don't use UFUNCTION() here as it will use the same parameters as the function it overrides ! :)
    virtual bool IsActivable() override;

    /* Cette fonction est appell�e quand le sort est r��llement d�clanch� ! :)*/
    virtual void OnActivateSpell(ASpell* spell) override;

    //* Affiche le widget sur le HUD. */
    virtual void DrawWidget() override;
};
