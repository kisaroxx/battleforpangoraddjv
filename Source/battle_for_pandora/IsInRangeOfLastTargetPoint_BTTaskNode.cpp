// Fill out your copyright notice in the Description page of Project Settings.

#include "IsInRangeOfLastTargetPoint_BTTaskNode.h"
#include "SquadSbireController.h"
#include "Logger.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Maneuver.h"
#include "ManeuverDefensive.h"
#include "BFPCharacterPlayable.h"
#include "SquadDescriptor.h"

UIsInRangeOfLastTargetPoint::UIsInRangeOfLastTargetPoint(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    NodeName = "IsInRangeOfLastTargetPoint";
}

EBTNodeResult::Type UIsInRangeOfLastTargetPoint::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8*
    NodeMemory)
{
    Super::ExecuteTask(OwnerComp, NodeMemory);

    // On recupere la manoeuvre !
    AManeuverDefensive* maneuvre = Cast<AManeuverDefensive>(OwnerComp.GetOwner());
    FVector posTargetPoint = maneuvre->GetLastTargetPoint();
    FVector posLieutenant = maneuvre->GetDescriptor()->GetFirstLieutenant()->GetPawn()->GetActorLocation();
    float d = FVector::DistSquared(posTargetPoint, posLieutenant);
    float distanceToPoint = maneuvre->GetDistanceMaxFromLastTargetPoint();

    if (d <= distanceToPoint * distanceToPoint) {
        return EBTNodeResult::Succeeded;
    } else {
        return EBTNodeResult::Failed;
    }
}

FString UIsInRangeOfLastTargetPoint::GetStaticDescription() const
{
    return TEXT("Permet de savoir si l'on est assez proche du PDC a proteger.");
}