// Fill out your copyright notice in the Description page of Project Settings.

#include "BFP_GameState.h"
#include "BFPCharacter.h"
#include "BFPCharacterPlayable.h"
#include "BFPCharacterSbire.h"
#include "BFPCharacterLieutenant.h"
#include "BFPHUD.h"
#include "Logger.h"
#include "BFP_PlayerController.h"
#include "Util.h"
#include <string>

ABFP_GameState::ABFP_GameState(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
	this->SetReplicates(true);
}


void ABFP_GameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

   DOREPLIFETIME(ABFP_GameState, HerosCount);
   DOREPLIFETIME(ABFP_GameState, FiendCount);
   DOREPLIFETIME(ABFP_GameState, ControlPointsCount);
	//DOREPLIFETIME(ABFP_GameState, ControlPointsToVictory);
	DOREPLIFETIME(ABFP_GameState, sbires);
	DOREPLIFETIME(ABFP_GameState, lieutenants);
	DOREPLIFETIME(ABFP_GameState, joueursAllies);
	DOREPLIFETIME(ABFP_GameState, davros);
	DOREPLIFETIME(ABFP_GameState, squads);
   DOREPLIFETIME(ABFP_GameState, checkpoints);
}

void ABFP_GameState::BeginPlay()
{
	DEBUG_SCREEN(-1, 5.f, FColor::Yellow, this->GetName().Append(" Begin init Game state !"));
	Super::BeginPlay();

	// Creation de la LookUpTable
   FString RelativePath = FPaths::ProjectContentDir();
   landscapeHeights.ReadLandscapeHeights(std::string(TCHAR_TO_UTF8(*RelativePath)) + "/LandscapeHeights", this->GetWorld());
}


void ABFP_GameState::ChangePlayerCount(Affiliation aff)
{
	MY_LOG_NETWORK(TEXT("ABFP_GameState ChangePlayerCount"));
	MY_LOG_NETWORK(TEXT("ABFP_GameState ChangePlayerCount Fiend count : %u"), FiendCount);
	MY_LOG_NETWORK(TEXT("ABFP_GameState ChangePlayerCount Heros count : %u"), HerosCount);
	MY_LOG_NETWORK(TEXT("ABFP_GameState ChangePlayerCount Player count : %u"), nbJoueurs);

   DEBUG_SCREEN(-1, 5.f, FColor::Yellow, this->GetName().Append(" Player Died Event Found !"));
   if (aff == Affiliation::GOOD) {
      this->HerosCount--;
	  MY_LOG_NETWORK(TEXT("ABFP_GameState ChangePlayerCount heros"));
   }
   else if (aff == Affiliation::EVIL)
   {
      this->FiendCount--;
	  MY_LOG_NETWORK(TEXT("ABFP_GameState ChangePlayerCount fiend"));
   }
   CheckIfEndOfGame();
}

void ABFP_GameState::CheckIfEndOfGame()
{
	MY_LOG_NETWORK(TEXT("ABFP_GameState CheckIfEndOfGame"));
   if (FiendCount == 0 || ControlPointsCount == ControlPointsToVictory) {
      DEBUG_SCREEN(-1, 5.f, FColor::Yellow, this->GetName().Append(" Fin du Jeu !"));
	  SetEndOfGame(Affiliation::GOOD);
   }
   else if (HerosCount == 0) {
	   SetEndOfGame(Affiliation::EVIL);
   }
}

void ABFP_GameState::AddCheckpointListener(ACheckpointTriggerSphere* checkpoint)
{
	checkpoint->CapturedFlag.AddDynamic(this, &ABFP_GameState::CheckpointStateChange);
}

void ABFP_GameState::AddPlayer(ABFPCharacter * character)
{
	MY_LOG_NETWORK(TEXT("ABFP_GameState AddPlayer"));
	MY_LOG_NETWORK(TEXT("ABFP_GameState AddPlayer characterName : %s"), *character->GetName());
	character->DiedFlag.AddDynamic(this, &ABFP_GameState::ChangePlayerCount);
}

void ABFP_GameState::CheckpointStateChange(Affiliation affOld, Affiliation affNew)
{
	DEBUG_SCREEN(-1, 5.f, FColor::Yellow, this->GetName().Append(" Point Captured "));
	if (affOld == Affiliation::NEUTRAL) {
		if (affNew == Affiliation::GOOD) {
			ControlPointsCount++;
         DEBUG_SCREEN(-1, 5.f, FColor::Green, "ControlPointsCount++");
         MY_LOG_NETWORK(TEXT("ControlPointsCount++ : %u"), ControlPointsCount);
		}
	}
	else if (affOld == Affiliation::GOOD)
	{
		ControlPointsCount--;
      DEBUG_SCREEN(-1, 5.f, FColor::Red, "ControlPointsCount--");
      MY_LOG_NETWORK(TEXT("ControlPointsCount-- : %u"), ControlPointsCount);
	}
	CheckIfEndOfGame();
   DEBUG_SCREEN(-1, 5.f, FColor::Green, FString::FromInt(ControlPointsCount));
}

void ABFP_GameState::OnRep_UpdateHUD() {
   //MY_LOG_NETWORK(TEXT("ABFP_GameState OnRep_UpdateHUD bAllPlayersSpawned %u"), bAllPlayersSpawned);
      ABFP_PlayerController* playerController = Cast<ABFP_PlayerController>(GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld()));
      playerController->UpdateHUDFromGameStateModification();
}

void ABFP_GameState::OnRep_UpdateHUDSquads() {
   MY_LOG_NETWORK(TEXT("Replication squad"));
   ABFP_PlayerController* playerController = Cast<ABFP_PlayerController>(GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld()));
   playerController->UpdateHUDFromGameStateModification();

}

void ABFP_GameState::OnRep_UpdateHUDCheckpoints() {
   ABFP_PlayerController* playerController = Cast<ABFP_PlayerController>(GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld()));
   MY_LOG_NETWORK(TEXT("ABFP_GameState OnRep_UpdateHUDCheckpoints playerController : %u"), IsValid(playerController));
   if (IsValid(playerController)) {
      playerController->UpdateHUDCheckpointsFromGameStateModification();
   }
}


void ABFP_GameState::SetEndOfGame(Affiliation aff)
{

	this->etatForces = aff;
	for (ABFP_PlayerController* playerController : this->AllPlayerControllers) {
		if (IsValid(Cast<ABFPCharacter>(playerController->GetPawn()))) {
			MY_LOG_NETWORK(TEXT("SetEndOfGame playerContoller valid %s"), *Cast<ABFPCharacter>(playerController->GetPawn())->GetName());
		}		
		MY_LOG_NETWORK(TEXT("SetEndOfGame %u"), (Cast<ABFPCharacter>(playerController->GetPawn())->getCharacterAffiliation() == Affiliation::EVIL));
		playerController->SetEndGameStatus(Cast<ABFPCharacter>(playerController->GetPawn())->getCharacterAffiliation() == aff);
      if (!Cast<ABFPCharacterPlayable>(playerController->GetPawn())->IsDead() && !(Cast<ABFPCharacter>(playerController->GetPawn())->getCharacterAffiliation() == aff)) {
         Cast<ABFPCharacterPlayable>(playerController->GetPawn())->DiedFunction();
      }
	}
}

void ABFP_GameState::Register(ABFPCharacterSbire *sbire) {
	sbires.Add(sbire);
}

void ABFP_GameState::Register(ABFPCharacterLieutenant *lieutenant) {
	lieutenants.Add(lieutenant);
}

void ABFP_GameState::Register(ABFPCharacterPlayable *joueur) {
    // C'est pas opti, mais �a fera l'affaire pour distinguer Davros !
	this->AddPlayer(joueur);
    if (joueur->getCharacterAffiliation() == Affiliation::EVIL) {
        davros = joueur;
		FiendCount++;
    }
    else {
        joueursAllies.Add(joueur);
		HerosCount++;
    }
}

void ABFP_GameState::ReRegister(ABFPCharacterPlayable *joueur) {
	// C'est pas opti, mais �a fera l'affaire pour distinguer Davros !
	if (joueur->getCharacterAffiliation() == Affiliation::EVIL) {
		davros = joueur;
		FiendCount++;
	}
	else {
		joueursAllies.Add(joueur);
		HerosCount++;
	}
}

void ABFP_GameState::Register(ASquadDescriptor *desc) {
	squads.Add(desc);
	AGameplayGM* gameMode = GetWorld()->GetAuthGameMode<AGameplayGM>();
	MY_LOG_NETWORK(TEXT("ABFP_GameState Register ASquadDescriptor"));
}

void ABFP_GameState::Register(ACheckpointTriggerSphere *CP) {
   checkpoints.Add(CP);
   MY_LOG_NETWORK(TEXT("ABFP_GameState Register Checkpoint"));
   AGameplayGM* gameMode = GetWorld()->GetAuthGameMode<AGameplayGM>();
   if (checkpoints.Num() == MAX_CONTROL_POINTS_COUNT) {
       InitAffiliationsCheckpoints();
   }
}

void ABFP_GameState::UnRegister(ABFPCharacter *) {
	DEBUG_SCREEN(-1, 5.f, FColor::Red, FString("Erreur : Tentative de UnRegister un ABFPCharacter non sp�cialis� !"));
}
void ABFP_GameState::UnRegister(ABFPCharacterSbire *sbire) {
	sbires.Remove(sbire);
}

void ABFP_GameState::UnRegister(ABFPCharacterLieutenant *lieutenant) {
	lieutenants.Remove(lieutenant);
}

void ABFP_GameState::UnRegister(ABFPCharacterPlayable *joueur) {
	// C'est pas opti, mais ca fera l'affaire pour distinguer Davros !
	if (joueur->getCharacterAffiliation() == Affiliation::EVIL) {
		davros = nullptr;
	}
	else {
		joueursAllies.Remove(joueur);
	}
}

void ABFP_GameState::UnRegister(ASquadDescriptor *desc) {
	squads.Remove(desc);
}


TArray<ABFPCharacterSbire*> ABFP_GameState::GetAllSbires() const
{
	return sbires;
}

TArray<ABFPCharacterLieutenant*> ABFP_GameState::GetAllLieutenants() const
{
	return lieutenants;
}

TArray<ABFPCharacterPlayable*> ABFP_GameState::GetAllJoueursAllies() const
{
	return joueursAllies;
}

TArray<ASquadDescriptor*> ABFP_GameState::GetAllSquads() const
{
	return squads;
}

ABFPCharacterPlayable * ABFP_GameState::GetDavros() const
{
	return davros;
}

TArray<ABFPCharacter*> ABFP_GameState::GetAllMechant()  const
{
	TArray<ABFPCharacter*> mechants;
	mechants.Append(lieutenants);
	mechants.Append(sbires);
	mechants.Push(davros);
	return mechants;
}

TArray<ACheckpointTriggerSphere*> ABFP_GameState::GetAllCPs() const {
   return checkpoints;
}

int ABFP_GameState::GetNbSbires() const
{
	return sbires.Num();
}

int ABFP_GameState::GetNbLieutenants() const
{
	return lieutenants.Num();
}

int ABFP_GameState::GetNbJoueursAllies() const
{
	return joueursAllies.Num();
}

void ABFP_GameState::InitAffiliationsCheckpoints() {
    int nbPdcEvil = MAX_CONTROL_POINTS_COUNT * 0.75f;
    TArray<ACheckpointTriggerSphere*> pdcDisponibles = checkpoints;
    for (int i = 0; i < nbPdcEvil; i++) {
        int ind = Util::Random(0, pdcDisponibles.Num() - 1);
        pdcDisponibles[ind]->captureCheckpoint(Affiliation::EVIL);
        pdcDisponibles.RemoveAt(ind);
    }
}
