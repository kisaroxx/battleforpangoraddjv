// Fill out your copyright notice in the Description page of Project Settings.

#include "NoCommand.h"

UCommandReturn* UNoCommand::execute() {
   return NewObject<UCommandReturn>(this, UCommandReturn::StaticClass());
}