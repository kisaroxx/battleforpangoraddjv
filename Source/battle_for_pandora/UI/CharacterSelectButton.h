// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/MenuWidget.h"
#include "Engine/Texture2D.h"
#include "CharacterSelectButton.generated.h"

/**
 * 
 */
UCLASS()
class BATTLE_FOR_PANDORA_API UCharacterSelectButton : public UMenuWidget
{
	GENERATED_BODY()

public:

	UCharacterSelectButton(const FObjectInitializer & ObjectInitializer);

	UPROPERTY(EditAnywhere, Category = "Variables")
	int16 id_;
	UPROPERTY(EditAnywhere, Category = "Variables")
	FText nomCharacter_;
	UPROPERTY(EditAnywhere, Category = "Variables")
	UTexture2D* image;

	UFUNCTION()
	virtual bool Initialize();

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UButton> ButtonCharacter;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UTextBlock> NomCharacterTextBlock;

	UFUNCTION()
	void OnClickedCallback();

	DECLARE_EVENT_OneParam(UCharacterSelectButton, FClickedEvent, int16);
	FClickedEvent OnClickedEvent;
};
