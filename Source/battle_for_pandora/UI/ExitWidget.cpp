// Fill out your copyright notice in the Description page of Project Settings.

#include "ExitWidget.h"

#include "Components/Button.h"
#include "Components/TextBlock.h"

#include "Kismet/GameplayStatics.h"
#include "Network/GameInfoInstance.h"

#include "BFP_PlayerController.h"

#include "Logger.h"

UExitWidget::UExitWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {
	bIsFocusable = true;
}
bool UExitWidget::Initialize() {
	bool Success = Super::Initialize();
	if (!Success) return false;

	if (!ensure(AcceptButton.IsValid())) return false;
	AcceptButton->OnClicked.AddDynamic(this, &UExitWidget::MainMenu);

	if (!ensure(CancelButton.IsValid())) return false;
	CancelButton->OnClicked.AddDynamic(this, &UExitWidget::Cancel);

	if (!ensure(MessageError.IsValid())) return false;
	if (message.IsEmptyOrWhitespace()) {
		MessageError->SetText(FText::FromString("Erreur connexion"));
	}
	else {
		MessageError->SetText(message);
	}
	
	if (GetWorld()) {
		gameInstanceRef = GetGameInstance<UGameInfoInstance>();
	}

	return true;
}

void UExitWidget::MainMenu() {
	this->Clean();
	gameInstanceRef->DestroySession(GetOwningPlayer());
}

void UExitWidget::Cancel() {
	this->Clean();
}