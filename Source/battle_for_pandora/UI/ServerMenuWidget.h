// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/MenuWidget.h"
#include "ServerMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class BATTLE_FOR_PANDORA_API UServerMenuWidget : public UMenuWidget
{
	GENERATED_BODY()
	
public:
	UServerMenuWidget(const FObjectInitializer & ObjectInitializer);

private:

	UFUNCTION()
	virtual bool Initialize();

	bool sessionFound_ = false;
	bool searchFinished_ = false;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UButton> ValidButton;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UButton> RefreshButton;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UButton> BackButton;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UWidgetSwitcher> DisplaySwitcher;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UCircularThrobber> Loading;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UTextBlock> TextMessage;
	
	UPROPERTY()
	TWeakObjectPtr<class UGameInfoInstance> gameInstanceRef;

	UFUNCTION()
	void MainMenu();

	UFUNCTION()
	void JoinServer();

	UFUNCTION()
	void RefreshServer();

	UFUNCTION()
	void SessionFound(bool sessionFound);
};
