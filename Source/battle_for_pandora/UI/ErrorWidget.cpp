// Fill out your copyright notice in the Description page of Project Settings.

#include "ErrorWidget.h"

#include "Components/Button.h"
#include "Components/TextBlock.h"

#include "Kismet/GameplayStatics.h"
#include "Network/GameInfoInstance.h"

UErrorWidget::UErrorWidget(const FObjectInitializer& ObjectInitializer) {
}
bool UErrorWidget::Initialize() {
	bool Success = Super::Initialize();
	if (!Success) return false;

	if (!ensure(AcceptButton.IsValid())) return false;
	AcceptButton->OnClicked.AddDynamic(this, &UErrorWidget::MainMenu);

	if (!ensure(MessageError.IsValid())) return false;
	if (message.IsEmptyOrWhitespace()) {
		MessageError->SetText(FText::FromString("Erreur connexion"));
	}
	else {
		MessageError->SetText(message);
	}
	
	if (GetWorld()) {
		gameInstanceRef = GetGameInstance<UGameInfoInstance>();
	}

	return true;
}

void UErrorWidget::QuitGame() {
	this->Clean();
	gameInstanceRef->quitGame(GetOwningPlayer());
}

void UErrorWidget::MainMenu() {
	this->Clean();
	gameInstanceRef->mainMenu(GetOwningPlayer());
}

void UErrorWidget::DestroySession() {
	this->Clean();
	gameInstanceRef->DestroySession(GetOwningPlayer());
}
