// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/MenuWidget.h"
#include "OptionMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class BATTLE_FOR_PANDORA_API UOptionMenuWidget : public UMenuWidget
{
	GENERATED_BODY()
	
};
