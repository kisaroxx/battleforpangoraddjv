// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/MenuWidget.h"
//#include "ScriptDelegates.h"
#include "HostMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class BATTLE_FOR_PANDORA_API UHostMenuWidget : public UMenuWidget
{
	GENERATED_BODY()
	
public:
	UHostMenuWidget(const FObjectInitializer & ObjectInitializer);
private:
	UFUNCTION()
	virtual bool Initialize();

	bool enableAccept_ = true;
	bool sessionCreated_ = false;
	static const int NB_MIN_PLAYER = 1;
	static const int NB_MAX_PLAYER = 5;
	int nbPlayer_;

	



	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UButton> AcceptButton;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UButton> BackButton;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UButton> IncreasePlayersButton;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UButton> DecreasePlayersButton;


	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UTextBlock> NbPlayerMessage;

	UPROPERTY()
	TWeakObjectPtr<class UGameInfoInstance> gameInstanceRef;

	UFUNCTION()
	void MainMenu();

	UFUNCTION()
	void LaunchServer();

	UFUNCTION()
	void IncreaseNbPlayer();
	
	UFUNCTION()
	void DecreaseNbPlayer();

	UFUNCTION()
	void NbPlayerModify(int newNbPlayer);



	UFUNCTION()
	void SessionCreated(bool sessionCreated);
};
