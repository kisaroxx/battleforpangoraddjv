// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/MenuWidget.h"
#include "Network/GameInfoInstance.h"
#include "MainMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class BATTLE_FOR_PANDORA_API UMainMenuWidget : public UMenuWidget
{
	GENERATED_BODY()
	
public:
	UMainMenuWidget(const FObjectInitializer & ObjectInitializer);

private:
	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UButton> HostButton;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UButton> FindButton;

	//UPROPERTY(meta = (BindWidget))
	//TWeakObjectPtr<class UButton> OptionButton;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UButton> QuitButton;

	UPROPERTY()
	TWeakObjectPtr<class UGameInfoInstance> gameInstanceRef;

	UFUNCTION()
	virtual bool Initialize();

	UFUNCTION()
	void HostMenu();

	UFUNCTION()
	void ServerMenu();

	UFUNCTION()
	void OptionMenu();

	UFUNCTION()
	void QuitGame();
};
