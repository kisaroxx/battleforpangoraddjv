// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/MenuWidget.h"
#include "PlayerButtonWidget.generated.h"

/**
 * 
 */
UCLASS()
class BATTLE_FOR_PANDORA_API UPlayerButtonWidget : public UMenuWidget
{
	GENERATED_BODY()
	
};
