// Fill out your copyright notice in the Description page of Project Settings.

#include "CharacterSelectButtonPoolWidget.h"

#include "UI/CharacterSelectButton.h"
#include "Components/UniformGridPanel.h"

UCharacterSelectButtonPoolWidget::UCharacterSelectButtonPoolWidget(const FObjectInitializer& ObjectInitializer) {

}

bool UCharacterSelectButtonPoolWidget::Initialize() {
	bool Success = Super::Initialize();
	if (!Success) return false;

	if (!ensure(CharactersPanel.IsValid())) return false;
	
	if (!searchCharacterButton(CharactersPanel)) return false;

	return true;
}

void UCharacterSelectButtonPoolWidget::CharacterSelected(int16 id) {
	OnClickedEvent.Broadcast(id);
}

bool UCharacterSelectButtonPoolWidget::searchCharacterButton(const TWeakObjectPtr<UPanelWidget> parent) {
	bool contientCharacterButton = false;
	int nbChildren = parent->GetChildrenCount();
	for (int i = 0; i < nbChildren; ++i) {
		TWeakObjectPtr<UWidget> child = parent->GetChildAt(i);
		if (child.IsValid()) {
			if (child->IsA(UCharacterSelectButton::StaticClass())) {
				contientCharacterButton = true;
				TWeakObjectPtr<UCharacterSelectButton> childCasted = Cast<UCharacterSelectButton>(child);
				childCasted->OnClickedEvent.AddUObject(this, &UCharacterSelectButtonPoolWidget::CharacterSelected);
				characterSelectButtons.Add(childCasted);
			}
			else if (child->IsA(UPanelWidget::StaticClass()) || IsValid(Cast<UPanelWidget>(child))) {
				TWeakObjectPtr<UPanelWidget> childCasted = Cast<UPanelWidget>(child);
				if (contientCharacterButton) {
					searchCharacterButton(childCasted);
				}
				else {
					contientCharacterButton = searchCharacterButton(childCasted);
				}
			}
		}
	}
	return contientCharacterButton;
}