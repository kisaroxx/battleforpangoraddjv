// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/MenuWidget.h"
#include "UI/CharacterSelectWidget.h"
#include "Network/GameInfoInstance.h"
#include "LobbyMenuWidget.generated.h"

/**
 *
 */
UCLASS()
class BATTLE_FOR_PANDORA_API ULobbyMenuWidget : public UMenuWidget
{
	GENERATED_BODY()
public:
	ULobbyMenuWidget(const FObjectInitializer & ObjectInitializer);

	int nbMaxPlayer;
	int currentNbPlayer;
	bool readyStatus;

	UPROPERTY(meta = (BindWidget))
		TWeakObjectPtr<class UButton> StartButton;

	UPROPERTY(meta = (BindWidget))
		TWeakObjectPtr<class UTextBlock> StartButtonText;

	UPROPERTY(meta = (BindWidget))
		TWeakObjectPtr<class UButton> ChooseCharacterButton;

	UPROPERTY(meta = (BindWidget))
		TWeakObjectPtr<class UButton> QuitButton;

	UPROPERTY(meta = (BindWidget))
		TWeakObjectPtr<class UTextBlock> CompteurJoueur;

	UPROPERTY(meta = (BindWidget))
		TWeakObjectPtr<class UVerticalBox> PlayerWindow;

	UPROPERTY()
		TWeakObjectPtr<class UGameInfoInstance> gameInstanceRef;

	UPROPERTY()
		TWeakObjectPtr<UCharacterSelectWidget> CharacterSelectWD;

	UFUNCTION()
		virtual bool Initialize();

	UFUNCTION()
		void StartGame();

	UFUNCTION()
		void ChooseCharacter();

	UFUNCTION()
		void MainMenu();

	UFUNCTION()
		void NbPlayerModify(int newNbPlayer);

	UFUNCTION()
		void CharacterSelectCallBack(int16 idCharacter);

	UFUNCTION()
		bool EnableStart();

	UFUNCTION()
		void ClearPlayerList();

	UFUNCTION()
		void UpdatePlayerWindow(UConnectedPlayerWidget* newPlayerWidget);

	UFUNCTION()
		void SetStatut(bool statutIn);

	UFUNCTION()
		FText SetStatutText();
};
