// Fill out your copyright notice in the Description page of Project Settings.

#include "ConnectedPlayerWidget.h"

#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"
#include "Engine/Texture2D.h"

UConnectedPlayerWidget::UConnectedPlayerWidget(const FObjectInitializer& ObjectInitializer) {

}

bool UConnectedPlayerWidget::Initialize() {
	bool Success = Super::Initialize();
	if (!Success) return false;

	if (!ensure(NomCharacterTextBlock.IsValid())) return false;
	NomCharacterTextBlock->TextDelegate.BindUFunction(this, "SetNomCharacter");
	if (!ensure(ImageCharacter.IsValid())) return false;
	ImageCharacter->BrushDelegate.BindUFunction(this, "SetImageCharacter");
	if (!ensure(StatutPlayerTextBlock.IsValid())) return false;
	StatutPlayerTextBlock->TextDelegate.BindUFunction(this, "SetStatut");

	return true;
}

FText UConnectedPlayerWidget::SetStatut() {
	if (statutPlayer) {
		return FText::FromString("Ready");
	}
	else {
		return  FText::FromString("Not Ready");
	}
}

FSlateBrush UConnectedPlayerWidget::SetImageCharacter() {
	ImageCharacter->Brush.SetResourceObject(image);
	return ImageCharacter->Brush;
}

FText UConnectedPlayerWidget::SetNomCharacter() {
	return nomCharacter;
}

