// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "RedFlashWidget.generated.h"

/**
 * 
 */
UCLASS()
class BATTLE_FOR_PANDORA_API URedFlashWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintNativeEvent)
	void Activate();
};
