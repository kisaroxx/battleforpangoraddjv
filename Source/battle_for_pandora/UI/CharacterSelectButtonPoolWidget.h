// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/MenuWidget.h"
#include "UI/CharacterSelectButton.h"
#include "CharacterSelectButtonPoolWidget.generated.h"

/**
 * 
 */
UCLASS()
class BATTLE_FOR_PANDORA_API UCharacterSelectButtonPoolWidget : public UMenuWidget 
{
	GENERATED_BODY()
public:
	UCharacterSelectButtonPoolWidget(const FObjectInitializer & ObjectInitializer);

	TArray<TWeakObjectPtr<UCharacterSelectButton>> characterSelectButtons;

	DECLARE_EVENT_OneParam(UCharacterSelectButtonPoolWidget, FClickedEvent, int16);
	FClickedEvent OnClickedEvent;

	UFUNCTION()
	virtual bool Initialize();

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UUniformGridPanel> CharactersPanel;

	UFUNCTION()
	void CharacterSelected(int16 id);
private:
	bool searchCharacterButton(const TWeakObjectPtr<UPanelWidget> parent);
};
