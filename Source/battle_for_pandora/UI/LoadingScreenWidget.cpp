// Fill out your copyright notice in the Description page of Project Settings.

#include "LoadingScreenWidget.h"

#include "Kismet/GameplayStatics.h"
#include "Network/GameInfoInstance.h"

ULoadingScreenWidget::ULoadingScreenWidget(const FObjectInitializer & ObjectInitializer)
{
}

bool ULoadingScreenWidget::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	gameInstanceRef = GetGameInstance<UGameInfoInstance>();
	if (!ensure(gameInstanceRef.IsValid())) return false;
	gameInstanceRef->sessionCreatedEvent.AddUObject(this, &ULoadingScreenWidget::EndWaiting);

	return true;
}

void ULoadingScreenWidget::EndWaiting(bool waitingEnd) {
	this->Clean();
}
