// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/MenuWidget.h"
#include "ConnectedPlayerWidget.generated.h"

/**
 *
 */
UCLASS()
class BATTLE_FOR_PANDORA_API UConnectedPlayerWidget : public UMenuWidget
{
	GENERATED_BODY()

public:

	UConnectedPlayerWidget(const FObjectInitializer & ObjectInitializer);

	UFUNCTION()
	virtual bool Initialize();

	UPROPERTY(EditAnywhere, Category = "Variables")
	FText nomCharacter;
	UPROPERTY(EditAnywhere, Category = "Variables")
	UTexture2D* image;
	UPROPERTY(EditAnywhere, Category = "Variables")
	bool statutPlayer;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UTextBlock> NomCharacterTextBlock;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UImage> ImageCharacter;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UTextBlock> StatutPlayerTextBlock;

	UFUNCTION()
	FText SetStatut();

	UFUNCTION()
	FSlateBrush SetImageCharacter();
	
	UFUNCTION()
	FText SetNomCharacter();
};
