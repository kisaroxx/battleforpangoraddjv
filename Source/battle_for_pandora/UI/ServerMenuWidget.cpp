// Fill out your copyright notice in the Description page of Project Settings.

#include "ServerMenuWidget.h"

#include "Components/Button.h"
#include "Components/WidgetSwitcher.h"
#include "Components/CircularThrobber.h"
#include "Components/TextBlock.h"

#include "Kismet/GameplayStatics.h"
#include "Network/GameInfoInstance.h"

UServerMenuWidget::UServerMenuWidget(const FObjectInitializer & ObjectInitializer)
{

}

bool UServerMenuWidget::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	if (!ensure(ValidButton.IsValid())) return false;
	ValidButton->OnClicked.AddDynamic(this, &UServerMenuWidget::JoinServer);

	if (!ensure(BackButton.IsValid())) return false;
	BackButton->OnClicked.AddDynamic(this, &UServerMenuWidget::MainMenu);

	if (!ensure(RefreshButton.IsValid())) return false;
	RefreshButton->OnClicked.AddDynamic(this, &UServerMenuWidget::RefreshServer);

	gameInstanceRef = GetGameInstance<UGameInfoInstance>();

	if (!ensure(gameInstanceRef.IsValid())) return false;
	gameInstanceRef->sessionFoundEvent.AddUObject(this, &UServerMenuWidget::SessionFound);
	RefreshServer();

	return true;
}

void UServerMenuWidget::MainMenu() {
	this->Clean();
	gameInstanceRef->showMainMenuWidget(GetOwningPlayer());
}

void UServerMenuWidget::JoinServer() {
	if (sessionFound_) {
		this->Clean();
		gameInstanceRef->JoinServer(GetOwningPlayer(), 0);
	}
}

void UServerMenuWidget::RefreshServer() {
	sessionFound_ = false;
	searchFinished_ = false;
	ValidButton->SetIsEnabled(sessionFound_);
	RefreshButton->SetIsEnabled(searchFinished_);
	BackButton->SetIsEnabled(searchFinished_);

	TextMessage->SetText(FText::FromString("Searching"));
	DisplaySwitcher->SetActiveWidgetIndex(0);

	gameInstanceRef->FindSessions(GetOwningPlayer());
}

void UServerMenuWidget::SessionFound(bool sessionFound) {
	sessionFound_ = sessionFound;
	searchFinished_ = true;
	ValidButton->SetIsEnabled(sessionFound_);
	RefreshButton->SetIsEnabled(searchFinished_);
	BackButton->SetIsEnabled(searchFinished_);

	FString message = (sessionFound_) ? FString("Session Found") : FString("Session not Found");
	TextMessage->SetText(FText::FromString(message));
	
	DisplaySwitcher->SetActiveWidgetIndex(1);
}

