// Fill out your copyright notice in the Description page of Project Settings.

#include "HostMenuWidget.h"

#include "Components/Button.h"
#include "Components/EditableTextBox.h"
#include "Components/TextBlock.h"

#include "ScriptDelegates.h"
#include "Kismet/GameplayStatics.h"
#include "Network/GameInfoInstance.h"
#include "Math/UnrealMathUtility.h"
#include "Logger.h"

#include "BFP_PlayerState.h"
#include "Network/MainMenu/MainMenuPC.h"

UHostMenuWidget::UHostMenuWidget(const FObjectInitializer & ObjectInitializer)
{

}

bool UHostMenuWidget::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	if (!ensure(AcceptButton.IsValid())) return false;
	AcceptButton->OnClicked.AddDynamic(this, &UHostMenuWidget::LaunchServer);
	AcceptButton->SetIsEnabled(enableAccept_);

	if (!ensure(BackButton.IsValid())) return false;
	BackButton->OnClicked.AddDynamic(this, &UHostMenuWidget::MainMenu);

	if (!ensure(IncreasePlayersButton.IsValid())) return false;
	IncreasePlayersButton->OnClicked.AddDynamic(this, &UHostMenuWidget::IncreaseNbPlayer);

	if (!ensure(DecreasePlayersButton.IsValid())) return false;
	DecreasePlayersButton->OnClicked.AddDynamic(this, &UHostMenuWidget::DecreaseNbPlayer);

	gameInstanceRef = GetGameInstance<UGameInfoInstance>();

	if (!ensure(gameInstanceRef.IsValid())) return false;
	gameInstanceRef->sessionFoundEvent.AddUObject(this, &UHostMenuWidget::SessionCreated);

	if (!ensure(NbPlayerMessage.IsValid())) return false;
	NbPlayerModify(NB_MIN_PLAYER);

	return true;
}

void UHostMenuWidget::MainMenu() {
	this->Clean();
	gameInstanceRef->showMainMenuWidget(GetOwningPlayer());
}

void UHostMenuWidget::LaunchServer() {
	this->Clean();
	// le +1 correspond au serveur qui est non joueur
	gameInstanceRef->LaunchLobby(GetOwningPlayer(), nbPlayer_ + 1, FText());
}

void UHostMenuWidget::IncreaseNbPlayer() {
	NbPlayerModify(nbPlayer_ + 1);
}

void UHostMenuWidget::DecreaseNbPlayer() {
	NbPlayerModify(nbPlayer_ - 1);
}

void UHostMenuWidget::NbPlayerModify(int newNbPlayer) {
	nbPlayer_ = FMath::Min(newNbPlayer, NB_MAX_PLAYER);
	nbPlayer_ = FMath::Max(nbPlayer_, NB_MIN_PLAYER);
	NbPlayerMessage->SetText(FText::FromString(FString::FromInt(nbPlayer_)));
}



void UHostMenuWidget::SessionCreated(bool sessionCreated) {
	sessionCreated_ = sessionCreated;
}

