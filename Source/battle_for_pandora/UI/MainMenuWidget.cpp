// Fill out your copyright notice in the Description page of Project Settings.

#include "MainMenuWidget.h"

#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "Network/GameInfoInstance.h"

UMainMenuWidget::UMainMenuWidget(const FObjectInitializer & ObjectInitializer)
{
}

bool UMainMenuWidget::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	if (!ensure(HostButton.IsValid())) return false;
	HostButton->OnClicked.AddDynamic(this, &UMainMenuWidget::HostMenu);

	if (!ensure(FindButton.IsValid())) return false;
	FindButton->OnClicked.AddDynamic(this, &UMainMenuWidget::ServerMenu);

	//if (!ensure(OptionButton.IsValid())) return false;
	//OptionButton->OnClicked.AddDynamic(this, &UMainMenuWidget::OptionMenu);

	if (!ensure(QuitButton.IsValid())) return false;
	QuitButton->OnClicked.AddDynamic(this, &UMainMenuWidget::QuitGame);

	gameInstanceRef = GetGameInstance<UGameInfoInstance>();

	return true;
}

void UMainMenuWidget::HostMenu() {
	this->Clean();
	gameInstanceRef->showHostMenuWidget(GetOwningPlayer());
}

void UMainMenuWidget::ServerMenu() {
	this->Clean();
	gameInstanceRef->showServerMenuWidget(GetOwningPlayer());
}

void UMainMenuWidget::OptionMenu() {
	this->Clean();
	gameInstanceRef->showOptionMenuWidget(GetOwningPlayer());
}

void UMainMenuWidget::QuitGame() {
	this->Clean();
	gameInstanceRef->quitGame(GetOwningPlayer());
}
