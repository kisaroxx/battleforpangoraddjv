// Fill out your copyright notice in the Description page of Project Settings.

#include "MenuWidget.h"
#include "GameFramework/PlayerController.h"
#include "Logger.h"

void UMenuWidget::Setup(bool bShowMouseCursor, const EInputModeWidget& inputModeIn, const bool bChangeVisibility)
{

	TWeakObjectPtr<APlayerController> PlayerController = GetOwningPlayer();
	//if (ensure(PlayerController.IsValid())) {
   if (PlayerController.IsValid()) {
		switch (inputModeIn) {
		case EInputModeWidget::GameAndUI: PlayerController->SetInputMode(GetInputModeForUI<FInputModeGameAndUI>(this)); break;
		case EInputModeWidget::UI_Only: PlayerController->SetInputMode(GetInputModeForUI<FInputModeUIOnly>(this)); break;
		case EInputModeWidget::Game_Only: PlayerController->SetInputMode(FInputModeGameOnly{}); break;
		default:break;
		}

		PlayerController->bShowMouseCursor = bShowMouseCursor;
	}

	if (initialized) {
		if (bChangeVisibility) { 
			Show(); 
		}
	}
	else {
		this->AddToViewport(20);
		initialized = true;
	}
}

void UMenuWidget::Clean(bool destroy) 
{
	TWeakObjectPtr<APlayerController> PlayerController = GetOwningPlayer();
	if (ensure(PlayerController.IsValid())) {

		PlayerController->SetInputMode(FInputModeGameOnly{});

		PlayerController->bShowMouseCursor = false;
	}

	if (destroy) {
		this->RemoveFromViewport();
		initialized = false;
	}
	else {
		Hide();
	}
}

void UMenuWidget::Hide() {
	this->SetVisibility(ESlateVisibility::Hidden);
}

void UMenuWidget::Show() {
	this->SetVisibility(ESlateVisibility::Visible);
}