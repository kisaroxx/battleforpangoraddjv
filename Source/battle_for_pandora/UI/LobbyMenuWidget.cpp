// Fill out your copyright notice in the Description page of Project Settings.

#include "LobbyMenuWidget.h"

#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "Components/VerticalBox.h"

#include "Kismet/GameplayStatics.h"
#include "Logger.h"
#include "GameFramework/Pawn.h"

#include "Network/GameInfoInstance.h"

#include "Network/Lobby/LobbyPC.h"
#include "Network/Lobby/LobbyGM.h"

#include "UI/CharacterSelectWidget.h"
#include "UI/ConnectedPlayerWidget.h"


ULobbyMenuWidget::ULobbyMenuWidget(const FObjectInitializer & ObjectInitializer)
{
}

bool ULobbyMenuWidget::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	if (!ensure(StartButton.IsValid())) return false;
	StartButton->OnClicked.AddDynamic(this, &ULobbyMenuWidget::StartGame);
	StartButton->SetIsEnabled(false);
	StartButton->bIsEnabledDelegate.BindDynamic(this, &ULobbyMenuWidget::EnableStart);

	if (!ensure(ChooseCharacterButton.IsValid())) return false;
	ChooseCharacterButton->OnClicked.AddDynamic(this, &ULobbyMenuWidget::ChooseCharacter);
	ChooseCharacterButton->SetIsEnabled(GetWorld() && !GetWorld()->IsServer());

	if (!ensure(QuitButton.IsValid())) return false;
	QuitButton->OnClicked.AddDynamic(this, &ULobbyMenuWidget::MainMenu);

	if (!ensure(CompteurJoueur.IsValid())) return false;
	nbMaxPlayer = 0;
	NbPlayerModify(0);
	readyStatus = false;

	if (!ensure(StartButtonText.IsValid())) return false;
	StartButtonText->TextDelegate.BindUFunction(this, "SetStatutText");

	if (!ensure(PlayerWindow.IsValid())) return false;
	
	gameInstanceRef = GetGameInstance<UGameInfoInstance>();
	if (!ensure(gameInstanceRef.IsValid())) return false;
	CharacterSelectWD = Cast<UCharacterSelectWidget>(gameInstanceRef->createWidget(GetOwningPlayer(), gameInstanceRef->CharacterSelectWD_Class));
	CharacterSelectWD->OnCharacterSelectedEvent.AddUObject(this, &ULobbyMenuWidget::CharacterSelectCallBack);

	return true;
}

void ULobbyMenuWidget::StartGame() {
	TWeakObjectPtr<UWorld> world = GetWorld();
	if (!ensure(world.IsValid())) return;
	if (world->IsServer()) {
		this->Clean();
		TWeakObjectPtr<ALobbyGM> gameMode = world->GetAuthGameMode<ALobbyGM>();
		if (gameMode.IsValid()) {
			gameMode->StartGame();
		}
	}
	else {
		readyStatus = !readyStatus;
		if (!readyStatus){
			CharacterSelectWD->UnselectButton->OnClicked.Broadcast(); //on simule un clique
		}
		else { 
			SetStatut(readyStatus); 
		}
	}
}

void ULobbyMenuWidget::ChooseCharacter() {
	if (!CharacterSelectWD.IsValid()) {
		
	}
	UMenuWidget* widgetCast = Cast<UMenuWidget>(CharacterSelectWD);
	gameInstanceRef->showWidget(widgetCast, gameInstanceRef->CharacterSelectWD_Class, GetOwningPlayer(), true);
}

void ULobbyMenuWidget::MainMenu() {
	this->Clean();
	TWeakObjectPtr<APlayerController> playerController = GetOwningPlayer();
	if (!ensure(playerController.IsValid())) return;
	TWeakObjectPtr<ALobbyPC> playerControllerCast = Cast<ALobbyPC>(playerController);
	if (!ensure(playerControllerCast.IsValid())) return;
	playerControllerCast->DestroySession();
}

bool ULobbyMenuWidget::EnableStart() {
	bool enableStart = false;
	TWeakObjectPtr<UWorld> world = GetWorld();
	TWeakObjectPtr<ALobbyGM> gameMode = world->GetAuthGameMode<ALobbyGM>();
	if (world->IsServer()) {
		if (gameMode.IsValid()) { 
			enableStart = gameMode->canWeStart; 
		}
	}
	else {
		enableStart = CharacterSelectWD.IsValid() && CharacterSelectWD->characterSelectedID > 0;
	}
	return enableStart;
}

void ULobbyMenuWidget::NbPlayerModify(int newNbPlayer) {
	currentNbPlayer = FMath::Min(newNbPlayer, nbMaxPlayer);
	currentNbPlayer = FMath::Max(currentNbPlayer, 0);
	CompteurJoueur->SetText(FText::FormatOrdered(FTextFormat::FromString("{0} of {1}"), currentNbPlayer, nbMaxPlayer));
}

void ULobbyMenuWidget::CharacterSelectCallBack(int16 idCharacter) {
	if (GetWorld()->IsServer()) {
		
		readyStatus = true;
	}
	if (idCharacter == 0) {
		readyStatus = false;
	}
	SetStatut(readyStatus);
	// on force le start boutton a se rafraichir
	StartButton->bIsEnabledDelegate.Execute();
}

void ULobbyMenuWidget::ClearPlayerList() {
	PlayerWindow->ClearChildren();
}

void ULobbyMenuWidget::UpdatePlayerWindow(UConnectedPlayerWidget* newPlayerWidget) {
	PlayerWindow->AddChildToVerticalBox(newPlayerWidget);
}

void ULobbyMenuWidget::SetStatut(bool statutIn) {
	readyStatus = statutIn;
	ALobbyPC* playerController = GetOwningPlayer<ALobbyPC>();
	if (IsValid(playerController)) {
		playerController->PlayerReadyStatus(readyStatus);
	}
}

FText ULobbyMenuWidget::SetStatutText() {
	if (GetWorld()->IsServer()) {
		return FText::FromString("Start");
	}
	else {
		if (!readyStatus) {
			return FText::FromString("Ready");
		}
		else {
			return FText::FromString("Not Ready");
		}
	}
}