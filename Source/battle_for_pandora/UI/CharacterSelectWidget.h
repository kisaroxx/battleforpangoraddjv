// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/MenuWidget.h"
#include "UI/CharacterSelectButtonPoolWidget.h"
#include "Engine/Texture2D.h"
#include "CharacterSelectWidget.generated.h"

/**
 * 
 */
UCLASS()
class BATTLE_FOR_PANDORA_API UCharacterSelectWidget : public UMenuWidget
{
	GENERATED_BODY()
public:
	UCharacterSelectWidget(const FObjectInitializer & ObjectInitializer);

	UPROPERTY(EditAnywhere, Category = "Variables")
	int16 characterSelectedID;
	UPROPERTY(EditAnywhere, Category = "Variables")
	TArray<UTexture2D*> characterImages;
	UPROPERTY(EditAnywhere, Category = "Variables")
	TArray<UTexture2D*> IconInGameImages;
	UPROPERTY(EditAnywhere, Category = "Variables")
	TArray<UTexture2D*> IconOnMapImages;
	UPROPERTY(EditAnywhere, Category = "Variables")
	TArray<FText> characterNames;

	DECLARE_EVENT_OneParam(UCharacterSelectWidget, FCharacterSelectedEvent, int16);
	FCharacterSelectedEvent OnCharacterSelectedEvent;

	UFUNCTION()
	virtual bool Initialize();

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<UCharacterSelectButtonPoolWidget> CharacterPanel;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UButton> UnselectButton;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UButton> BackButton;

	UPROPERTY(meta = (BindWidget))
	TWeakObjectPtr<class UTextBlock> Titre;

	UFUNCTION()
	void CharacterSelected(int16 id);

	UFUNCTION()
	void UnselectCharacter();

	UFUNCTION()
	void HideWidget();
};
