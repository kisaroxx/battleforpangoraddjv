// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MenuWidget.generated.h"

UENUM()
enum class EInputModeWidget : uint8
{
	GameAndUI,
	UI_Only,
	Game_Only
};

/**
 * 
 */
UCLASS()
class BATTLE_FOR_PANDORA_API UMenuWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	UFUNCTION()
	virtual void Setup(const bool bShowMouseCursor = true, const EInputModeWidget& inputMode = EInputModeWidget::UI_Only, const bool bChangeVisibility = true);
	UFUNCTION()
	virtual void Clean(bool destroy = true);
	UFUNCTION()
	virtual void Hide();
	UFUNCTION()
	virtual void Show();

	bool initialized = false;

	template<class T>
	T GetInputModeForUI(const UMenuWidget* widget) {
		T inputMode{}; 
		inputMode.SetWidgetToFocus(widget->GetCachedWidget());
		return inputMode;
	}
};
