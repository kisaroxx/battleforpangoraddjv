// Fill out your copyright notice in the Description page of Project Settings.

#include "FormationCercle.h"


void UFormationCercle::ComputeAllPositions(int nbSbires, int nbLieutenants) {
    transformsRelativesSbires.Empty();
    transformsRelativesLieutenants.Empty();

    // Les lieutenants sont au centre
    // Je ne gere pas le multi-lieutenant pour le moment
    for (int i = 0; i < nbLieutenants; ++i) {
        FVector pos{ 0.0f, 0.0f, 0.0f };
        FQuat quat = FQuat::Identity;
        FVector scale{ 1.0f, 1.0f, 1.0f };
        FTransform t{ quat, pos, scale };
        transformsRelativesLieutenants.Add(t);
    }

    // On remplit les anneaux de sbires 1 � 1
    int nbRings = GetNbRings(nbSbires);
    int nbSbiresDernierAnneau = nbSbires;
    for (int i = 0; i < nbRings; ++i) {
        // Et le dernier anneau est diff�rent car on r�partit ceux qui restent
        int tailleAnneau = (i != nbRings - 1) ? taillePremierAnneau * (i + 1) : nbSbiresDernierAnneau;
        nbSbiresDernierAnneau -= tailleAnneau;
        float angle = angleEncerclement / (tailleAnneau - 1);
        for (int j = 0; j < tailleAnneau; ++j) {
            float angleFinal = j * angle + (360.0f - angleEncerclement) / 2.0f;
            FVector pos{ largeurAnneau * (i + 1), 0.0f, 0.0f };
            pos = pos.RotateAngleAxis(angleFinal, FVector{ 0.0f, 0.0f, 1.0f });
            FQuat quat = FQuat::FindBetweenVectors(FVector{ 1.0f, 0.0f, 0.0f }, pos);
            FVector scale{ 1.0f, 1.0f, 1.0f };
            FTransform t{ quat, pos, scale };
            transformsRelativesSbires.Add(t);
        }
    }
}

int UFormationCercle::GetNbRings(int nbSbires) {
    if (nbSbires == 0) return 0;

    int tailleCurrentRing = taillePremierAnneau;
    int nbAnneaux = 1;

    while (nbSbires > tailleCurrentRing) {
        nbSbires -= tailleCurrentRing;
        tailleCurrentRing += taillePremierAnneau;
        nbAnneaux++;
    }

    return nbAnneaux;
}

void UFormationCercle::SetParams(int taillePremierAnneau_, float largeurAnneau_, float angleEncerclement_) {
    taillePremierAnneau = taillePremierAnneau_;
    largeurAnneau = largeurAnneau_;
    angleEncerclement = angleEncerclement_;
    transformsRelativesLieutenants.Empty();
    transformsRelativesSbires.Empty();
}

