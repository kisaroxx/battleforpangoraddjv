// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "battle_for_pandoraCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "battle_for_pandoraGameMode.h"
#include "Attaque.h"
#include "AttaqueComponent.h"

#include <string.h>
#include <stdio.h>
//
////////////////////////////////////////////////////////////////////////////
//// Abattle_for_pandoraCharacter
//
//Abattle_for_pandoraCharacter::Abattle_for_pandoraCharacter()
//{
//	// Set size for collision capsule
//	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
//
//	// set our turn rates for input
//	BaseTurnRate = 45.f;
//	BaseLookUpRate = 45.f;
//
//	// Don't rotate when the controller rotates. Let that just affect the camera.
//	bUseControllerRotationPitch = false;
//	bUseControllerRotationYaw = false;
//	bUseControllerRotationRoll = false;
//
//	// Configure character movement
//	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
//	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
//	GetCharacterMovement()->JumpZVelocity = 600.f;
//	GetCharacterMovement()->AirControl = 0.2f;
//
//	// Create a camera boom (pulls in towards the player if there is a collision)
//	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
//	CameraBoom->SetupAttachment(RootComponent);
//	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
//	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller
//
//	// Create a follow camera
//	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
//	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
//	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
//
//	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
//	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
//}
//
////////////////////////////////////////////////////////////////////////////
//// Input
//
//void Abattle_for_pandoraCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
//{
//	// Set up gameplay key bindings
//	check(PlayerInputComponent);
//	PlayerInputComponent->BindAction("Attack", IE_Pressed, this, &Abattle_for_pandoraCharacter::SimpleAttackStart);
//
//   PlayerInputComponent->BindAction("Run", IE_Pressed, this, &Abattle_for_pandoraCharacter::Run);
//   PlayerInputComponent->BindAction("Run", IE_Released, this, &Abattle_for_pandoraCharacter::StopRunning);
//
//	PlayerInputComponent->BindAxis("MoveForward", this, &Abattle_for_pandoraCharacter::MoveForward);
//	PlayerInputComponent->BindAxis("MoveRight", this, &Abattle_for_pandoraCharacter::MoveRight);
//
//	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
//	// "turn" handles devices that provide an absolute delta, such as a mouse.
//	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
//	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
//	PlayerInputComponent->BindAxis("TurnRate", this, &Abattle_for_pandoraCharacter::TurnAtRate);
//	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
//	PlayerInputComponent->BindAxis("LookUpRate", this, &Abattle_for_pandoraCharacter::LookUpAtRate);
//
//	// handle touch devices
//	PlayerInputComponent->BindTouch(IE_Pressed, this, &Abattle_for_pandoraCharacter::TouchStarted);
//	PlayerInputComponent->BindTouch(IE_Released, this, &Abattle_for_pandoraCharacter::TouchStopped);
//
//	// VR headset functionality
//	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &Abattle_for_pandoraCharacter::OnResetVR);
//
//	PlayerInputComponent->BindAction("Attaque", IE_Pressed, this, &Abattle_for_pandoraCharacter::OnStartAttack);
//	PlayerInputComponent->BindAction("Attaque", IE_Released, this, &Abattle_for_pandoraCharacter::OnEndAttack);
//
//   PlayerInputComponent->BindAction("HeavyAttaqueTest", IE_Pressed, this, &Abattle_for_pandoraCharacter::OnStartHeavyAttackTest);
//   PlayerInputComponent->BindAction("HeavyAttaqueTest", IE_Released, this, &Abattle_for_pandoraCharacter::OnEndHeavyAttackTest);
//}
//
//
//void Abattle_for_pandoraCharacter::OnResetVR()
//{
//	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
//}
//
//void Abattle_for_pandoraCharacter::OnStartAttack()
//{
//   GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Attaque debut"));
//   Attacking = true;
//}
//
//void Abattle_for_pandoraCharacter::OnEndAttack()
//{
//   GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Attaque fin"));
//   Attacking = false;
//   HeavyAttacking = false;
//}
//
//void Abattle_for_pandoraCharacter::OnStartHeavyAttackTest()
//{
//   GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Attaque Lourde debut"));
//   HeavyAttacking = true;
//}
//
//void Abattle_for_pandoraCharacter::OnEndHeavyAttackTest()
//{
//   GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Attaque Lourde fin"));
//   Attacking = false;
//   HeavyAttacking = false;
//}
//
//void Abattle_for_pandoraCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
//{
//		Jump();
//}
//
//void Abattle_for_pandoraCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
//{
//		StopJumping();
//}
//
//void Abattle_for_pandoraCharacter::TurnAtRate(float Rate)
//{
//	// calculate delta for this frame from the rate information
//	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
//}
//
//void Abattle_for_pandoraCharacter::LookUpAtRate(float Rate)
//{
//	// calculate delta for this frame from the rate information
//	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
//}
//
//void Abattle_for_pandoraCharacter::MoveForward(float Value)
//{
//	if ((Controller != NULL) && (Value != 0.0f))
//	{
//		// find out which way is forward
//		const FRotator Rotation = Controller->GetControlRotation();
//		const FRotator YawRotation(0, Rotation.Yaw, 0);
//
//		// get forward vector
//		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
//		AddMovementInput(Direction, Value);
//	}
//}
//
//void Abattle_for_pandoraCharacter::MoveRight(float Value)
//{
//	if ( (Controller != NULL) && (Value != 0.0f) )
//	{
//		// find out which way is right
//		const FRotator Rotation = Controller->GetControlRotation();
//		const FRotator YawRotation(0, Rotation.Yaw, 0);
//	
//		// get right vector 
//		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
//		// add movement in that direction
//		AddMovementInput(Direction, Value);
//	}
//}
//
//void Abattle_for_pandoraCharacter::Run()
//{
//   this->GetCharacterMovement()->MaxWalkSpeed = this->RunSpeed;
//}
//
//void Abattle_for_pandoraCharacter::StopRunning()
//{
//   this->GetCharacterMovement()->MaxWalkSpeed = this->WalkSpeed;
//}
//
//void Abattle_for_pandoraCharacter::SimpleAttackStart() {
//    // Si le personnage n'est pas dans l'�tat idle, alors il ne peut pas attaquer !
//    if (combatState != CombatState::IDLE)
//        return;
//
//    //GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, this->GetName().Append(" attaque !"));
//
//    // On change l'�tat du personnage
//    combatState = CombatState::ATTACK;
//    attackState = AttackState::ARMING;
//
//    // On d�clanche l'attaque apr�s un certain d�lais !
//    FTimerHandle fuzeTimerHandle;
//    GetWorld()->GetTimerManager().SetTimer(
//        fuzeTimerHandle,
//        this,
//        &Abattle_for_pandoraCharacter::SimpleAttackActivate,
//        attaqueSimpleComponent.GetDefaultObject()->GetDureeArmement(),
//        false);
//
//}
//
//void Abattle_for_pandoraCharacter::SimpleAttackActivate() {
//    //GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, this->GetName().Append(" attaque ACTIVATE !!!"));
//
//    // On change l'�tat du personage
//    attackState = AttackState::ACTIVATING;
//
//    // On va lancer une attaque !
//    // Ne fonctionne plus car on utilise maintenant BFPCharacter
//    //attaqueSimpleComponent.GetDefaultObject()->ActivateAttack(this, GetWorld());
//
//    // On arr�te l'attaque apr�s un certain d�lais
//    FTimerHandle fuzeTimerHandle;
//    GetWorld()->GetTimerManager().SetTimer(
//        fuzeTimerHandle,
//        this,
//        &Abattle_for_pandoraCharacter::SimpleAttackEnd,
//        attaqueSimpleComponent.GetDefaultObject()->GetDureeActivation(),
//        false);
//}
//
//void Abattle_for_pandoraCharacter::SimpleAttackEnd() {
//    //GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, this->GetName().Append(" fin de l'attaque !"));
//
//    // On change l'�tat du personage
//    attackState = AttackState::DISARMING;
//
//    // On arr�te l'attaque apr�s un certain d�lais
//    FTimerHandle fuzeTimerHandle;
//    GetWorld()->GetTimerManager().SetTimer(
//        fuzeTimerHandle,
//        this,
//        &Abattle_for_pandoraCharacter::SetCombatStateIdle,
//        attaqueSimpleComponent.GetDefaultObject()->GetDureeDesarmement(),
//        false);
//}
//
//void Abattle_for_pandoraCharacter::SetCombatStateIdle() {
//    //GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Black, this->GetName().Append(" set state !"));
//    combatState = CombatState::IDLE;
//}
//