// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Core.h"
#include "Net/UnrealNetwork.h"
#include "GameFramework/Character.h"
#include "Poussee.h"
#include "Logger.h"
#include "Public/TimerManager.h"
#include "BFPCharacter.generated.h"


// Les differents modes de combats possibles.
// Note : Le mouvement est ind�pendant de l'etat de combat du personnage !

class UHealthComponent;
class UWidgetComponent;
class UProgressBar;

class UUserWidget;
// Les diff�rents modes de combats possibles.
// Note : Le mouvement est ind�pendant de l'�tat de combat du personnage !

UENUM(BlueprintType)
enum class CombatState : uint8
{
	IDLE,       // Immobile, ou en train de se d�placer. 
	ATTACK,     // En train d'attaquer
	HIT,        // En train de recevoir un coup
	DODGING,    // Roulade
	BLOCKING,   // En train de parer
	DEAD        // Est mort
};

// Les differentes parties d'une attaque possible.
UENUM(BlueprintType)
enum class AttackState : uint8
{
	ARMING,     // La phase d'attente avant le declanchement d'une attaque
	ACTIVATING, // La phase ou l'attaque fait des degats
	DISARMING   // La phase d'attente apr�s la fin de l'activation de l'attaque
};

// Les differentes parties d'une attaque possible.
UENUM(BlueprintType)
enum class AttackType : uint8
{
	SIMPLE_ATTACK,
	HEAVY_ATTACK
};

// Les differents types de mouvements possibles
UENUM(BlueprintType)
enum class MovementState : uint8 {
	WALKING,    // Quand on marche
	RUNNING     // Quand on ... cours !
};

// Les differents types d'inputs possibles.
UENUM(BlueprintType)
enum class InputTypes : uint8 {
	NO_INPUT,               // Le joueur ne desire rien faire
	SIMPLE_ATTACK_INPUT,    // Le joueur desire lancer une attaque simple
	HEAVY_ATTACK_INPUT,     // Le joueur desire lancer une attaque lourde
	BLOCK_START_INPUT,      // Le joueur desire blocker
	BLOCK_END_INPUT,        // Le joueur desire arrêter de blocker
	DODGE_INPUT             // Le joueur desire esquiver
};


//Les camps possible
UENUM(BlueprintType)
enum class Affiliation : uint8
{
	EVIL,
	GOOD,
	NEUTRAL
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FIDied, Affiliation, CAffiliation);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FUpdatePlayrate, float, newPlayrate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FAttackDelegate);

UCLASS(config = Game)
class ABFPCharacter : public ACharacter
{
	GENERATED_BODY()

protected:
	/********************************************************************
	HEALTH
	********************************************************************/

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = "true"))
	class UHealthComponent *HealthSystem;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "UI", meta = (AllowPrivateAccess = "true"))
	UWidgetComponent *HealthBarTopHead;

	UPROPERTY(EditAnywhere, Category = "UI", meta = (AllowPrivateAccess = "true"))
	UWidgetComponent *CrossHairLock;

	UPROPERTY(EditAnywhere, Category = "UI", meta = (AllowPrivateAccess = "true"))
	class UParticleSystemComponent* EffectHealCheckpoint;
	/********************************************************************
	MOVEMENT
	********************************************************************/

	/* L'etat de mouvement du personnage. */
	UPROPERTY(VisibleAnywhere, Category = "Combat")
	MovementState movementState = MovementState::WALKING; // On commence en marchant !

		/** La vitesse maximale de course dans le 3Pchar*/
	UPROPERTY(Category = "Character Movement: Walking", EditAnywhere)
	float RunSpeed = 1200;

	/** La vitesse maximale de marche dans le 3Pchar*/
	UPROPERTY(Category = "Character Movement: Walking", EditAnywhere)
	float WalkSpeed = 600;


protected:
	/** * Le camp du character */
	UPROPERTY(Category = "Ennemi type", EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	Affiliation CharacterAffiliation = Affiliation::EVIL;

private:
	/** La vitesse maximale de marche dans le 3Pchar*/
	UPROPERTY(Category = "Character Movement: Walking", EditAnywhere)
	float accelerationDeBase = 2048.0f;

	/********************************************************************
	INPUT BUFFER
	********************************************************************/

	/* Le dernierInput enregistre par le joueur. */
	UPROPERTY(VisibleAnywhere, Category = InputBuffer)
	InputTypes lastInput = InputTypes::NO_INPUT;

	/* Le moment où le dernier input a ete enregistre. */
	UPROPERTY(VisibleAnywhere, Category = InputBuffer)
	float tempsLastInput;

	/* Le temps pendant lequel on peut mettre des commandes en buffer ! */
	UPROPERTY(EditAnywhere, Category = InputBuffer)
	float dureeInputBuffer = 0.5f;

public:
	ABFPCharacter(const FObjectInitializer & ObjectInitializer);

	virtual void PostInitializeComponents() override;

	UFUNCTION(BluePrintCallable)
	bool IsAttacking();

	UFUNCTION(BluePrintCallable)
	bool IsHeavyAttacking();

	// Fait mourir un personnage.
	UFUNCTION()
	virtual void Die();

	UFUNCTION()
	void DestroyCharacter();

	//Permet de savoir si le personnage est mort (permet de debloquer le lock)
	bool IsDead();

   virtual bool IsGhost();

   UFUNCTION(BluePrintCallable)
   bool IsStun();

	//Met a jour la barre de vie au dessus des personnages
	UFUNCTION()
	void UpdateHealthBar(float HealtPercentage);
	UFUNCTION()
	void UpdateHealth();
	UFUNCTION()
	void HealingAction();

	// Permet de faire prendre des dammages à un personnage.
	// DamageAmount est le nombre de degats pris.
	// dureeHit est la duree pendant laquelle le personnage jouera son animation de prise de degats ET la duree pendant laquelle il ne pourra rien faire.
	UFUNCTION()
	virtual float TakeDamage(float DamageAmount, float dureeHit, FPoussee poussee = FPoussee{}, bool shouldDeduceArmor = true, bool canBeDodge = true);

   UFUNCTION(BlueprintCallable)
   virtual float TakeDamageFromSpell(float DamageAmount);

   UFUNCTION(BlueprintCallable)
   virtual float TakeDamageFromSpellWithPousse(float DamageAmount, FPoussee poussee);

   UFUNCTION(BlueprintCallable)
   float HealFromSpell(float DamageAmount);

    bool CanCastSpell() const;


	UPROPERTY(BlueprintAssignable, Category = "EventOnRun")
	FIDied DiedFlag;

    UPROPERTY(BlueprintAssignable, Category = "EventOnRun")
    FUpdatePlayrate UpdatePlayrateFlag;

	virtual void OnClickLeft();
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	virtual void PostInitProperties() override;

  
   //virtual void PostInitializeComponents() override;

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

public:
	/** Returns HealthSystem subobject **/
	FORCEINLINE UHealthComponent* GetHealthSystem() const { return HealthSystem; }

	/** Returns Widget subobject **/
	FORCEINLINE UWidgetComponent* GetHealthBarTopHead() const { return HealthBarTopHead; }


	/** Returns Widget subobject **/
	FORCEINLINE UWidgetComponent* GetCrossHairLock() const { return CrossHairLock; }

	void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;


	/** Fait courrir le joueur */
	UFUNCTION(BlueprintCallable, Category = Character)
	virtual void Run();
	UFUNCTION(BlueprintCallable, Category = Character, Reliable, Server, WithValidation)
	virtual void RunServer();

	/* Fait retourner le joueur en mode marche */
	UFUNCTION(BlueprintCallable, Category = Character, Reliable, Server, WithValidation)
	virtual void StopRunning();


	/* Accesseur pour le camp du personnage */
	Affiliation getCharacterAffiliation()
	{
		return CharacterAffiliation;
	}
	void setCharacterAffiliation(Affiliation aff) {
		CharacterAffiliation = aff;
	}


	UFUNCTION(BluePrintCallable)
	void RotateWidget(UWidgetComponent *widget);

	// Called every frame
	virtual void Tick(float DeltaTime) override;


	/******************************************************************
	COMBAT SYSTEM
	******************************************************************/
protected:
	/* L'etat de combat actuel du personage. */
	UPROPERTY(Replicated, Transient, VisibleAnywhere, Category = Combat)
	CombatState combatState = CombatState::IDLE; // On commence immobile.

	/* L'etat de l'attaque actuelle du personnage.
	* Ceci n'a de sens que si le personnage est en train d'attaquer ! */
	UPROPERTY(ReplicatedUsing = OnRep_AttackState, Transient, VisibleAnywhere, Category = Combat)
	AttackState attackState;

	/* Le type de l'attaque actuelle du personnage.
	* Ceci n'a de sens que si le personnage est en train d'attaquer ! */
	UPROPERTY(Replicated, Transient, VisibleAnywhere, Category = Combat)
	AttackType attackType = AttackType::SIMPLE_ATTACK;

    /* Le multiplicateur de degats qui permet de modifier les degats de toutes les attaques d'un personnage. */
    UPROPERTY(Replicated, Transient, VisibleAnywhere, Category = Combat)
    float degatMultiplicateur = 1.0f;

	/* Le nombre maximum d'attaques differentes que peut enchainter ce personnage. */
	UPROPERTY(EditAnywhere, Category = Combat)
	int nbMaximumAttaquesCombo = 2;

	/* Le numero de l'attaque courante dans le combo ! */
	UPROPERTY(Replicated, Transient, VisibleAnywhere, Category = Combat)
	int numeroAttackInCombo = 0;

	UPROPERTY(Transient, VisibleAnywhere, Category = Combat)
	float playrateAttack;

	bool isAttacking{};

	bool saveAttack{};

	/* La hauteur du HUD au dessus de la tete ! */
	UPROPERTY(EditAnywhere, Category = UI)
	int offsetHUDInWorld = 20;

	UPROPERTY()
	UProgressBar *ProgressBar;
	UPROPERTY()
	UUserWidget *Widget;

	UPROPERTY(BlueprintAssignable, Category = Combat)
	FAttackDelegate SimpleAttackFlag;
	UPROPERTY(BlueprintAssignable, Category = Combat)
	FAttackDelegate HeavyAttackFlag;

public:
	UFUNCTION()
		void OnRep_AttackState();

	/* L'attaque component contenant l'attaque de base. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat|Attaque")
	TArray<TSubclassOf<class UAttaqueComponent>> attaquesSimplesComponentClass; // Ceci est le MODEL de nos attaquesComponents

   UPROPERTY()
	TArray<class UAttaqueComponent*> attaquesSimplesComponent; // Nos véritables attaques

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat|Attaque")
	TSubclassOf<class UAttaqueComponent> attaqueLourdeComponentClass; // Ceci est le MODEL de nos attaquesComponents

   UPROPERTY()
   FTimerHandle fuzeAttackTimerHandle;

   UPROPERTY()
   FTimerHandle fuzeAnimationTimerHandle;

   UPROPERTY()
	class UAttaqueComponent * attaqueLourdeComponent; // Nos véritables attaques

	/* Les classes des spells components du personnage. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat|Spell")
	TArray<TSubclassOf<class USpellComponent>> spellComponentsClass; // Ceci est le MODEL de nos spellComponents
    /* Les spellsComponent du personnage. */
    UPROPERTY(Replicated, Transient, VisibleAnywhere, BlueprintReadWrite, Category = "Combat|Spell")
	TArray<class USpellComponent*> spellComponents; // Nos véritables spells

	 /* Les informations pour faire durer la roulade sur un temps complet. */
	float tempsDebutRoulade;
	float tempsFinRoulade = 0;
	FVector directionRoulade;
	UPROPERTY(EditAnywhere, Category = "Combat|Roulade")
	float dureeRoulade = 0.5f;
	UPROPERTY(EditAnywhere, Category = "Combat|Roulade")
	float cooldownRoulade = 0.3f;
	UPROPERTY(EditAnywhere, Category = "Combat|Roulade")
	float vitesseRoulade = 1200.0f;
	UPROPERTY(EditAnywhere, Category = "Combat|Roulade")
	float accelerationRoulade = 9999999.0f;

	/* Les informations de Mort */
	UPROPERTY(EditAnywhere, Category = "Animation")
	float dureeDeath = 0.5f;

	/* La duree de l'animation de hit. */
	UPROPERTY(EditAnywhere, Category = "Animation")
	float dureeAnimationHit = 0.5f;

	/* Permet de savoir s'il faut jouer l'animation de Hit (au debut) ou de Stun (qui loop) lorsque l'on se fait touche pendant une duree inderteminee. */
	UPROPERTY(Replicated, VisibleAnywhere, Category = "Animation")
	bool bStunState = false;

	/* Les poussees auquel est actuellement sujet le personnage. */
	TArray<FPoussee> poussees; // Ahhhh un beau vector c'est incroyable ! <3

   /* Le coefficiant de parade.
	* Lorsque l'on prends un coup, on multiplie par ce coefficiant qui est entre 0 et 1.
	* 0.5f représente une petite armure (ex: sbires) et 1.0f représente une bonne parade (ex: players) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "0.0", ClampMax = "1.0"), Category = "Combat|Parade")
	float coefParade = 0.95f;

public:

	UFUNCTION(BluePrintCallable)
	CombatState GetCombatState() const;

	UFUNCTION(BluePrintCallable)
	AttackState GetAttackState();

   UFUNCTION(BluePrintCallable)
   AttackType GetAttackType();

	UFUNCTION(BluePrintCallable)
	MovementState GetMovementState();

    UFUNCTION(BluePrintCallable)
    float GetDegatMultiplicateur();

    /* Multiplie le multiplicateur actuel par une nouvelle valeur strictement positive. */
	UFUNCTION(BluePrintCallable)
	void ModifyDegatMultiplicateur(float multiplicateur);

	UFUNCTION(BluePrintCallable)
	int GetNumeroAttackInCombo();

	UFUNCTION(BluePrintCallable)
	int GetNbMaxAtttackInCombo();

	UFUNCTION(BluePrintCallable)
	void AddAttackInCombo();

   UFUNCTION(BluePrintCallable)
   int PreviousAttackInCombo();

	UFUNCTION(BluePrintCallable)
	bool IsSimpleAttackStartPossible();

	UFUNCTION(BluePrintCallable, Reliable, Server, WithValidation)
	void SimpleAttack();
	//   /* Declanche la phase d'armement d'une attaque. */

	UFUNCTION(BluePrintCallable, Reliable, Server, WithValidation)
	void HeavyAttackStart();

	/* Permet de changer l'etat de combat du joueur pour qu'il passe en IDLE. */
	UFUNCTION()
	void SetCombatStateIdle();

	/* Fait parer le personnage. */
	UFUNCTION(BluePrintCallable, Reliable, Server, WithValidation)
	void BlockingStart();

	/* Fait arreter de parer le personnage. */
	UFUNCTION(BluePrintCallable, Reliable, Server, WithValidation)
	void BlockingStop();

	/* Fait faire une roulade au personnage. */
	UFUNCTION(BluePrintCallable, Reliable, Server, WithValidation)
	void DodgingStart();

    /* Permet d'essayer de lancer un sort ! */
    UFUNCTION(BluePrintCallable, Reliable, Server, WithValidation)
    void TryCastSpell(int index);

    TArray<USpellComponent*> GetSpellComponents() const;

private:

	/* Declenche la phase d'activation d'une attaque. */

	UFUNCTION(BluePrintCallable)
	void SimpleAttackStart();

	UFUNCTION()
		void SimpleAttackActivate();

	UFUNCTION(BluePrintCallable)
		void HeavyAttackActivate();

	UFUNCTION(BluePrintCallable)
		void SimpleAttackCombo();

	/* Declanche la phase de desarmement d'une attaque. */
	UFUNCTION(BluePrintCallable)
		void SimpleAttackEnd();

	UFUNCTION(BluePrintCallable)
		void HeavyAttackEnd();

	// Methodes gerant l'animation cote client

	UFUNCTION(BluePrintCallable, Reliable, NetMulticast, WithValidation)
	void StartAttackAnim(const AttackType typeAttack);

   UFUNCTION(BluePrintCallable)
	   void SimpleAttackStartAnim();

   UFUNCTION(BluePrintCallable)
	   void HeavyAttackStartAnim();

   UFUNCTION()
	   void SimpleAttackActivateAnim();

   UFUNCTION(BluePrintCallable)
	   void HeavyAttackActivateAnim();

   UFUNCTION(BluePrintCallable)
	   void SimpleAttackEndAnim();

   UFUNCTION(BluePrintCallable)
	   void HeavyAttackEndAnim();

	/* Permet de mettre le personnage dans le bon etat lorsqu'il a pris un coup.
	* dureeHit represente la duree de son inaction. */
	UFUNCTION()
	void ReceiveAttackHit(float dureeHit, FPoussee poussee);

	/* Met un INPUT en buffer !*/
	void putInInputBuffer(InputTypes input);

	/* Utilise l'INPUT dans le buffer ! */
	bool tryUseBufferInput();

	/* Permet d'appliquer la roulade ! */
	UFUNCTION()
	void ApplyDodging();

	/* Methode pour terminer la roulade ! */
	UFUNCTION()
	void FinishDodging();

	/* Permet de supprimer toutes les attaques en cours. */
	void CancelAllAttacks();

	/* Met à jour une variable de l'animation. */
	UFUNCTION()
	void SetBStunState(bool newState);

	/* Applique les poussees en cours lors d'un Tick. */
	void ApplyPoussees(float deltaTime);

    /* Permet de mettre à jour la vitesse d'animation pour qu'elle dure le temps voulu. :) */

    /* Permet de mettre à jour la vitesse d'animation pour qu'elle dure le temps voulu. :) */
    /* Et c'est vraiment super cool de ouuuuuf <3 <3 <3 */
    void UpdatePlayrateAnimation(UAttaqueComponent* attaqueComponent, AttackState attackState);

public:
	virtual void RegisterToGameManager();

	virtual void RegisterToSquad();

	virtual void UnRegisterToGameManager();

	virtual void UnRegisterToSquad();

    void AddPoussee(FPoussee poussee);
};