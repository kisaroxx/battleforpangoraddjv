// Fill out your copyright notice in the Description page of Project Settings.

#include "CheckpointTriggerSphere.h"
#include "BFP_GameState.h"
#include "BFP_PlayerController.h"
#include "Message.h"
#include "SquadDescriptor.h"
#include "SquadManager.h"
#include "HealthComponent.h"
#include "Logger.h"

#define print(text) DEBUG_SCREEN(-1, 1.5, FColor::Green,text);
#define printFString(text, fstring) DEBUG_SCREEN(-1, 5.f, FColor::Green, FString::Printf(TEXT(text), fstring));

TArray<FColor> ACheckpointTriggerSphere::COULEURS_BANK = TArray<FColor>{
    FColor(0, 255, 0),
    FColor(0, 0, 255),
    FColor(255, 255, 0),
    FColor(255, 0, 255),
    FColor(0, 255, 255),
    FColor(255, 255, 255),
    FColor(127, 127, 127),
    FColor(255, 0, 0)
};
int ACheckpointTriggerSphere::indiceCouleur = 0;

ACheckpointTriggerSphere::ACheckpointTriggerSphere(const FObjectInitializer & ObjectInitializer)
   : Super(ObjectInitializer) {
   //Register Events
   OnActorBeginOverlap.AddDynamic(this, &ACheckpointTriggerSphere::OnOverlapBegin);
   OnActorEndOverlap.AddDynamic(this, &ACheckpointTriggerSphere::OnOverlapEnd);

   NetCullDistanceSquared *= 2000; // Osef cette ligne est override par le blueprint !

   CurrentCheckpointState = ECheckpointState::CS_HELD;
   OldCheckpointState = ECheckpointState::CS_HELD;
   CurrentCheckpointAffiliation = Affiliation::NEUTRAL;
   charOnCheckpoint.Add(Affiliation::GOOD, 0);
   charOnCheckpoint.Add(Affiliation::NEUTRAL, 0);
   charOnCheckpoint.Add(Affiliation::EVIL, 0);
   //Add a Mesh to the component (cirle to notify the owner of the checkpoint)
   StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("My Super Mesh"));

   // On commence avec aucune squad associe, c'est le game state qui s'occupera de nous faire nous associer une squad !
   squadAssociee = nullptr;

   PrimaryActorTick.bCanEverTick = false;

   bAlwaysRelevant = true;
   bReplicates = true;
}

void ACheckpointTriggerSphere::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
   Super::GetLifetimeReplicatedProps(OutLifetimeProps);

   DOREPLIFETIME(ACheckpointTriggerSphere, CurrentCheckpointAffiliation);
   DOREPLIFETIME(ACheckpointTriggerSphere, CurrentCheckpointState);
   DOREPLIFETIME(ACheckpointTriggerSphere, couleurSquadCheckpoint);
   DOREPLIFETIME(ACheckpointTriggerSphere, capturingTeam);
}

void ACheckpointTriggerSphere::PostInitializeComponents() {
   Super::PostInitializeComponents();
   affiliationMaterial.Add(Affiliation::GOOD, MaterialGood);
   affiliationMaterial.Add(Affiliation::NEUTRAL, MaterialNeutral);
   affiliationMaterial.Add(Affiliation::EVIL, MaterialEvil);
}

void ACheckpointTriggerSphere::BeginPlay()
{
   Super::BeginPlay();
   captureRate *= captureSpeed >= 1.0f ? 1 : captureSpeed;
   StaticMeshComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));

   if (GetWorld()->IsServer()) {
      GetWorld()->GetGameState<ABFP_GameState>()->AddCheckpointListener(this);
      GetWorld()->GetGameState<ABFP_GameState>()->Register(this);

      setCheckpointAffiliation(CurrentCheckpointAffiliation);

      FColor color = ACheckpointTriggerSphere::COULEURS_BANK[ACheckpointTriggerSphere::indiceCouleur];
      couleurSquadCheckpoint = color;
      MY_LOG_NETWORK(TEXT("Couleur = %d %d %d"), couleurSquadCheckpoint.R, couleurSquadCheckpoint.G, couleurSquadCheckpoint.B);
      ACheckpointTriggerSphere::indiceCouleur = (ACheckpointTriggerSphere::indiceCouleur + 1) % ACheckpointTriggerSphere::COULEURS_BANK.Num();
   }
}


void ACheckpointTriggerSphere::OnOverlapBegin(class AActor* OverlappedActor, class AActor* OtherActor)
{
   if (GetWorld()->IsServer()) {
      // check if Actors do not equal nullptr and that
      TWeakObjectPtr<ABFPCharacter> intermediateActor = Cast<ABFPCharacter>(OtherActor);
      if (intermediateActor.IsValid() && !(intermediateActor->getCharacterAffiliation() == Affiliation::NEUTRAL)) {
         Affiliation lAffiliation = intermediateActor->getCharacterAffiliation();
         if (OtherActor && (OtherActor != this)) {
            charOnCheckpoint[lAffiliation]++;
            ListeActorOnCheckpoint.Push(OtherActor);
            ABFPCharacterPlayable* playable = Cast<ABFPCharacterPlayable>(OtherActor);

            if (isThereTwoFactionsOnTheCheckpoint()) {
               InitiateContest();
               //playerController->ChangeColorProgressBarRPC(FLinearColor::Black);
            }
            else {
               if (IsValid(playable)) {
                  ABFP_PlayerController* playerController = Cast<ABFP_PlayerController>(playable->GetController());
                  if (IsValid(playerController)) {
                     playerController->CaptureSystem->bShow = true;
                     playerController->CaptureSystem->checkpointAffiliation = getCheckpointAffiliation();
                  }
                  if (isOwnedByFaction(lAffiliation)) {
                     StartHealing(playable);
                  }
                  else if (isFactionCapturing(lAffiliation)) {
                     KeepCapturing();
                  }
                  else {
                     InitiateCapture(lAffiliation);
                  }
               }

            }
            ////else {
            //	// checkpoint controlle par le joueur
            //	if (IsValid(playable)) {
            //		
            //	}
            //}

         }
      }
   }
}


void ACheckpointTriggerSphere::OnOverlapEnd(class AActor* OverlappedActor, class AActor* OtherActor)
{
   if (GetWorld()->IsServer()) {
      TWeakObjectPtr<ABFPCharacter> intermediateActor = Cast<ABFPCharacter>(OtherActor);
      if (intermediateActor.IsValid() && !(intermediateActor->getCharacterAffiliation() == Affiliation::NEUTRAL)) {
         Affiliation lAffiliation = intermediateActor->getCharacterAffiliation();
         if (OtherActor && (OtherActor != this)) {
            charOnCheckpoint[lAffiliation]--;
            ABFPCharacterPlayable* playable = Cast<ABFPCharacterPlayable>(OtherActor);
            if (IsValid(playable)) {
               ABFP_PlayerController* playerController = Cast<ABFP_PlayerController>(playable->GetController());
               if (IsValid(playerController)) {
                  playerController->CaptureSystem->bShow = false;
               }
            }
            //on arrete de soigner
            if (IsValid(playable)) {
               StopHealing(playable);
            }
            ListeActorOnCheckpoint.Remove(OtherActor);
            if (isThereTwoFactionsOnTheCheckpoint()) {
               KeepContesting();
               return;
            }
            else if (isThereSomeoneOnTheCheckpoint()) {
               lAffiliation = isThereEvilOnTheCheckpoint() ? Affiliation::EVIL : Affiliation::GOOD;
               if (isFactionCapturing(lAffiliation)) {
                  KeepCapturing(); // Do nothing ?
                  return;
               }
               else if (wasFactionCapturing(lAffiliation)) {
                  KeepCapturing();
                  ReleaseContest();
                  return;
               }
               else if (!isOwnedByFaction(lAffiliation)) {
                  InitiateCapture(lAffiliation);
                  return;
               }
            }
            GetWorldTimerManager().ClearTimer(CountdownTimerHandle);
            CountdownHasBeenInterupted();
            return;
         }
      }
   }
}

// Called every frame
void ACheckpointTriggerSphere::Tick(float DeltaTime)
{
   Super::Tick(DeltaTime);

}

void ACheckpointTriggerSphere::UpdateTimerDisplay()
{
   printFString("Percentage : %f", capturePercentage);
}

void ACheckpointTriggerSphere::AdvanceTimer()
{

   capturePercentage += (captureRate * FMath::Pow(captureBonus, charOnCheckpoint[capturingTeam] < 7 ?
      charOnCheckpoint[capturingTeam] : 7));
   //Mise a jour de la ProgressBar
   if (CurrentCheckpointState != ECheckpointState::CS_CONTESTED) {
      for (int i = 0; i != ListeActorOnCheckpoint.Num(); i++) {
         ABFPCharacterPlayable* playable = Cast<ABFPCharacterPlayable>(ListeActorOnCheckpoint[i]);
         if (IsValid(playable)) {
            ABFP_PlayerController* playerController = Cast<ABFP_PlayerController>(playable->GetController());
            if (IsValid(playerController)) {
               playerController->CaptureSystem->captureValue = capturePercentage;
               playerController->CaptureSystem->checkpointAffiliation = getCheckpointAffiliation();
            }
         }
      }

   }
   UpdateTimerDisplay();
   MY_LOG_NETWORK(TEXT("Capture Percentage PDC : %f"), capturePercentage);

   if (capturePercentage >= 100.0f)
   {

      CountdownHasFinished();

   }

}

void ACheckpointTriggerSphere::CountdownHasFinished()
{
   setCheckpointState(ECheckpointState::CS_HELD);
   if (CurrentCheckpointAffiliation != Affiliation::NEUTRAL) {
      setCheckpointAffiliation(Affiliation::NEUTRAL);
      CapturedFlag.Broadcast(OldCheckpointAffiliation, capturingTeam);
      capturePercentage -= 100.0f;
      KeepCapturing(capturingTeam);
      return;
   }
   captureCheckpoint(capturingTeam);
   GetWorldTimerManager().ClearTimer(CountdownTimerHandle);
   ResetCapture();
}

// If the Countdown is interrupted (= We leave the trigger zone during a capturing phase)
// We go back to the older state
void ACheckpointTriggerSphere::CountdownHasBeenInterupted()
{
   ResetCapture();
   GetWorldTimerManager().ClearTimer(CountdownTimerHandle);
   setCheckpointAffiliation(CurrentCheckpointAffiliation);
}


// Initiate the Timer if needed
void ACheckpointTriggerSphere::InitiateTimer() {
   //Start the time manager 
   GetWorldTimerManager().SetTimer(CountdownTimerHandle, this, &ACheckpointTriggerSphere::AdvanceTimer, captureSpeed, true);
}

void ACheckpointTriggerSphere::setCheckpointAffiliation(Affiliation EnumValue)
{
   if (EnumValue != CurrentCheckpointAffiliation)
      OldCheckpointAffiliation = CurrentCheckpointAffiliation;
   CurrentCheckpointAffiliation = EnumValue;
   setCheckpointState(ECheckpointState::CS_HELD);
   setMeshMaterial(EnumValue);
}

void ACheckpointTriggerSphere::captureCheckpoint(Affiliation EnumValue)
{
   setCheckpointAffiliation(EnumValue);
   CapturedFlag.Broadcast(OldCheckpointAffiliation, EnumValue);

   for (int i = 0; i != ListeActorOnCheckpoint.Num(); i++) {
      ABFPCharacterPlayable* playable = Cast<ABFPCharacterPlayable>(ListeActorOnCheckpoint[i]);
      if (IsValid(playable) && isOwnedByFaction(playable->getCharacterAffiliation())) {
         StartHealing(playable);
      }
   }

   //// Faire spawner une squad si aucune n'est deja linke !
   if (CurrentCheckpointAffiliation == Affiliation::EVIL) {
      if (squadAssociee == nullptr) {
         SpawnerAssociatedSquad();
      }
      else {
         StopRecallAssociatedSquad();
      }
   }

   // Rappeller la Squad !
   if (CurrentCheckpointAffiliation == Affiliation::GOOD) {
      RecallAssociatedSquad();
   }
}

// AFFILIATION
void ACheckpointTriggerSphere::InitiateCapture(Affiliation EnumValue)
{
   if (CurrentCheckpointAffiliation == EnumValue) {
      setCheckpointAffiliation(EnumValue);
      ResetCapture();
      return;
   }
   ResetCapture();
   capturingTeam = EnumValue;
   setCheckpointState(ECheckpointState::CS_CAPTURING);
   InitiateTimer();
}

void ACheckpointTriggerSphere::setCheckpointState(ECheckpointState EnumValue)
{
   if (CurrentCheckpointState == EnumValue) return;
   OldCheckpointState = CurrentCheckpointState;
   CurrentCheckpointState = EnumValue;
   setMeshMaterial(EnumValue);
}


void ACheckpointTriggerSphere::setMeshMaterial(UMaterial * Material)
{
   StaticMeshComponent->SetMaterial(0, Material);
}

void ACheckpointTriggerSphere::setMeshMaterial(Affiliation EnumValue)
{
   setMeshMaterial(affiliationMaterial[EnumValue]);
   this->StateChange.Broadcast(CurrentCheckpointAffiliation, CurrentCheckpointState, capturingTeam);
}

void ACheckpointTriggerSphere::setMeshMaterial(ECheckpointState EnumValue)
{
   if (EnumValue == ECheckpointState::CS_CONTESTED) {
      setMeshMaterial(MaterialContest);
   }
   else {
      setMeshMaterial(CurrentCheckpointAffiliation);
   }
   this->StateChange.Broadcast(CurrentCheckpointAffiliation, CurrentCheckpointState, capturingTeam);
}

void ACheckpointTriggerSphere::ResetCapture()
{
   capturingTeam = Affiliation::NEUTRAL;
   capturePercentage = 0.0f;
}

void ACheckpointTriggerSphere::InitiateContest()
{
   setCheckpointState(ECheckpointState::CS_CONTESTED);
   GetWorldTimerManager().PauseTimer(CountdownTimerHandle);
   isTimerPaused = true;
}

void ACheckpointTriggerSphere::ReleaseContest()
{
   if (isTimerPaused) {
      GetWorldTimerManager().UnPauseTimer(CountdownTimerHandle);
      isTimerPaused = false;
   }
}


void ACheckpointTriggerSphere::KeepCapturing()
{
   setCheckpointState(ECheckpointState::CS_CAPTURING);
}

void ACheckpointTriggerSphere::KeepCapturing(Affiliation EnumValue)
{
   capturingTeam = EnumValue;
   KeepCapturing();
}
void ACheckpointTriggerSphere::KeepContesting()
{
}

bool ACheckpointTriggerSphere::isThereSomeoneOnTheCheckpoint()
{
   return (isThereEvilOnTheCheckpoint() || isThereGoodOnTheCheckpoint());
}

bool ACheckpointTriggerSphere::isThereTwoFactionsOnTheCheckpoint()
{
   return (isThereEvilOnTheCheckpoint() && isThereGoodOnTheCheckpoint());
}

bool ACheckpointTriggerSphere::isThereEvilOnTheCheckpoint()
{
   return charOnCheckpoint[Affiliation::EVIL];
}

bool ACheckpointTriggerSphere::isThereGoodOnTheCheckpoint()
{
   return charOnCheckpoint[Affiliation::GOOD];
}

bool ACheckpointTriggerSphere::isFactionOnCheckpoint(Affiliation EnumValue)
{
   return charOnCheckpoint[EnumValue];
}

bool ACheckpointTriggerSphere::wasFactionCapturing(Affiliation EnumValue)
{
   return (isFactionOnCheckpoint(EnumValue) && OldCheckpointState == ECheckpointState::CS_CAPTURING && capturingTeam == EnumValue);
}

bool ACheckpointTriggerSphere::isFactionCapturing(Affiliation EnumValue)
{
   return (isFactionOnCheckpoint(EnumValue) && CurrentCheckpointState == ECheckpointState::CS_CAPTURING && capturingTeam == EnumValue);
}

bool ACheckpointTriggerSphere::isOwnedByFaction(Affiliation EnumValue)
{
   return (CurrentCheckpointAffiliation == EnumValue);
}

float ACheckpointTriggerSphere::getCapturePercentage()
{
   return capturePercentage;
}

float ACheckpointTriggerSphere::getCaptureRate()
{
   return captureRate;
}

float ACheckpointTriggerSphere::getCaptureBonus()
{
   return captureBonus;
}

Affiliation ACheckpointTriggerSphere::getCheckpointAffiliation()
{
   return CurrentCheckpointAffiliation;
}

void ACheckpointTriggerSphere::OnRep_Affiliation() {
   MY_LOG_NETWORK(TEXT("ACheckpointTriggerSphere OnRep_Affiliation"));
   setMeshMaterial(CurrentCheckpointAffiliation);
   switch (CurrentCheckpointAffiliation)
   {
   case Affiliation::EVIL:
      SoundFlagEvil.Broadcast();
      break;
   case Affiliation::GOOD:
      SoundFlagGood.Broadcast();
      break;
   default:
      break;
   }
   
}

void ACheckpointTriggerSphere::OnRep_State() {
   MY_LOG_NETWORK(TEXT("ACheckpointTriggerSphere OnRep_State"));
   setMeshMaterial(CurrentCheckpointState);
}

void ACheckpointTriggerSphere::UnlinkFromSquad() {
   squadAssociee = nullptr;
}

void ACheckpointTriggerSphere::SpawnerAssociatedSquad() {
   if (squadAssociee == nullptr) {
      // Creer une nouvelle squad !
      FActorSpawnParameters params;
      params.bNoFail = true;
      params.Owner = this;
      params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
      ASquadDescriptor* squad = GetWorld()->SpawnActor<ASquadDescriptor>(squadClass.Get(), GetActorLocation(), GetActorRotation(), params);
      if (IsValid(squad)) {
         squad->SetCheckpointAssociated(this);
         squadAssociee = squad;
      }
      else {
         DEBUG_SCREEN(-1, 5.0f, FColor::Red, "Attention, un PDC n'a pas reussi � faire spawner une Squad !");
      }
   }
}

void ACheckpointTriggerSphere::RecallAssociatedSquad() {
   // Envoyer un message à la squad si il y en a une !
   if (IsValid(squadAssociee)) {
      UMessage * message = NewObject<UMessage>();
      message->SetParams(MessageType::RECALL, nullptr);
      squadAssociee->GetSquadManager()->ReceiveMessage(message);
   }
}

void ACheckpointTriggerSphere::StopRecallAssociatedSquad() {
   // Envoyer un message à la squad si il y en a une !
   if (IsValid(squadAssociee)) {
      UMessage * message = NewObject<UMessage>();
      message->SetParams(MessageType::STOP_RECALL, nullptr);
      squadAssociee->GetSquadManager()->ReceiveMessage(message);
   }
}

FColor ACheckpointTriggerSphere::GetColor() const {
   return couleurSquadCheckpoint;
}

ASquadDescriptor * ACheckpointTriggerSphere::GetSquadAssociee() const
{
   return squadAssociee;
}

void ACheckpointTriggerSphere::StartHealing(ABFPCharacterPlayable* playable) {
   if (handles.Find(playable) == nullptr && (playable->GetHealthSystem()->GetHealth() < playable->GetHealthSystem()->GetFullHealth())) {
      FTimerHandle handle;
      playable->GetHealthSystem()->StartHealing(handle, 1, true, 0.5f, true);
      handles.Add(playable, handle);
   }
}

void ACheckpointTriggerSphere::StopHealing(ABFPCharacterPlayable * playable) {
   if (handles.Find(playable) != nullptr) {
      playable->GetHealthSystem()->StopHealing(handles[playable]);
      handles.Remove(playable);
   }
}
