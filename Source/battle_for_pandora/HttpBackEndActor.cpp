// Fill out your copyright notice in the Description page of Project Settings.

#include "HttpBackEndActor.h"
#include "Runtime/Json/Public/Dom/JsonObject.h"
#include "Runtime/Json/Public/Serialization/JsonReader.h"
#include "Runtime/Json/Public/Serialization/JsonSerializer.h"
#include "Logger.h"

// Sets default values
AHttpBackEndActor::AHttpBackEndActor(const class FObjectInitializer& ObjectInitializer)
   : Super(ObjectInitializer)
{
   // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
   Http = &FHttpModule::Get();

}

// Called when the game starts or when spawned
void AHttpBackEndActor::BeginPlay()
{
   Super::BeginPlay();
   LoadSaveFile();

}

// Called every frame
void AHttpBackEndActor::Tick(float DeltaTime)
{
   Super::Tick(DeltaTime);

}

void AHttpBackEndActor::LoadSaveFile()
{
   SaveGameInstance = Cast<UBackEndSaveGame>(UGameplayStatics::CreateSaveGameObject(UBackEndSaveGame::StaticClass()));
   //try {
   UBackEndSaveGame* LoadGameInstance = Cast<UBackEndSaveGame>(UGameplayStatics::CreateSaveGameObject(UBackEndSaveGame::StaticClass()));
   LoadGameInstance = Cast<UBackEndSaveGame>(UGameplayStatics::LoadGameFromSlot(LoadGameInstance->SaveSlotName, LoadGameInstance->UserIndex));
   if (!LoadGameInstance) {
      InviteAccountCreateRequest();
   }
   else {
      SaveGameInstance = LoadGameInstance;
      if (SaveGameInstance->PlayerIndex.IsEmpty() || SaveGameInstance->PlayerGuestToken.IsEmpty()) {
         InviteAccountCreateRequest();
      }
      else {
         if (!SaveGameInstance->PlayerSessionToken.IsEmpty()) {
            SessionRefreshRequest();
         }
      }
   }
   //}
   //catch (...) {

   //}

}

void AHttpBackEndActor::InviteAccountCreateRequest()
{
   requestCount++;
   TSharedRef<IHttpRequest> Request = Http->CreateRequest();
   Request->OnProcessRequestComplete().BindUObject(this, &AHttpBackEndActor::OnInviteAccountCreateResponseReceived);
   //This is the url on which to process the request
   Request->SetURL(adress + "api/users");
   Request->SetVerb("POST");
   Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
   Request->SetHeader("Content-Type", TEXT("application/json"));
   Request->ProcessRequest();
}

void AHttpBackEndActor::OnInviteAccountCreateResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
   if (Response.IsValid()) {
      MY_LOG_GAME(TEXT("Invite Account Create Succeed"));
      //Create a pointer to hold the json serialized data
      TSharedPtr<FJsonObject> JsonObject;
      //if (Response == nullptr) return;
      //Create a reader pointer to read the json data
      TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());

      //Deserialize the json data given Reader and the actual object to deserialize
      if (FJsonSerializer::Deserialize(Reader, JsonObject))
      {
         //Get the value of the json object by field name
         FString receivedToken = JsonObject->GetStringField("guestToken");
         FString receivedId = JsonObject->GetStringField("id");
         SaveGameInstance->PlayerGuestToken = receivedToken;
         SaveGameInstance->PlayerIndex = receivedId;
         UGameplayStatics::SaveGameToSlot(SaveGameInstance, SaveGameInstance->SaveSlotName, SaveGameInstance->UserIndex);
      }
      requestCount = 0;
      SessionCreateRequest();
   }
   else {
      // Retry ?
      MY_LOG_GAME(TEXT("Invite Account Create Failed"));
      if (requestCount < 3)
         InviteAccountCreateRequest();
   }
}

void AHttpBackEndActor::SessionCreateRequest()
{
   TSharedRef<IHttpRequest> Request = Http->CreateRequest();
   Request->OnProcessRequestComplete().BindUObject(this, &AHttpBackEndActor::OnSessionCreateResponseReceived);
   //This is the url on which to process the request
   Request->SetURL(adress + "api/users/" + SaveGameInstance->PlayerIndex + "/sessions/create");
   Request->SetVerb("POST");
   Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
   Request->SetHeader("Content-Type", TEXT("application/x-www-form-urlencoded"));
   Request->SetContentAsString("guestToken=" + FGenericPlatformHttp::UrlEncode(SaveGameInstance->PlayerGuestToken));
   Request->ProcessRequest();
}

void AHttpBackEndActor::OnSessionCreateResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
   if (Response.IsValid()) {
      MY_LOG_GAME(TEXT("Session Created"));
      FString receivedSessionToken = Response->GetContentAsString();
      if (receivedSessionToken.IsEmpty()) return;
      SaveGameInstance->PlayerSessionToken = receivedSessionToken;
      UGameplayStatics::SaveGameToSlot(SaveGameInstance, SaveGameInstance->SaveSlotName, SaveGameInstance->UserIndex);
      requestCount = 0;
   }
   else {
      MY_LOG_GAME(TEXT("Session Create Failed"));
      if (requestCount < 3)
         SessionCreateRequest();
   }
}

void AHttpBackEndActor::SessionRefreshRequest()
{
   requestCount++;
   TSharedRef<IHttpRequest> Request = Http->CreateRequest();
   Request->OnProcessRequestComplete().BindUObject(this, &AHttpBackEndActor::OnSessionRefreshResponseReceived);
   //This is the url on which to process the request
   Request->SetURL(adress + "api/users/" + SaveGameInstance->PlayerIndex + "/sessions/refresh");
   Request->SetVerb("POST");
   Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
   Request->SetHeader("Content-Type", TEXT("application/x-www-form-urlencoded"));
   Request->SetHeader(TEXT("Authorization"), "Bearer " + SaveGameInstance->PlayerSessionToken);
   Request->ProcessRequest();
}

void AHttpBackEndActor::OnSessionRefreshResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
   if (Response.IsValid()) {
      MY_LOG_GAME(TEXT("Session Refresh Succeed"));
      FString receivedSessionToken = Response->GetContentAsString();
      if (receivedSessionToken.IsEmpty()) return;
      SaveGameInstance->PlayerSessionToken = receivedSessionToken;
      UGameplayStatics::SaveGameToSlot(SaveGameInstance, SaveGameInstance->SaveSlotName, SaveGameInstance->UserIndex);
      bLoggedIn = true;
      requestCount = 0;
   }
   else {
      MY_LOG_GAME(TEXT("Session Refresh failed"));
      if(requestCount < 3)
         SessionRefreshRequest();
      bLoggedIn = false;
   }
}

void AHttpBackEndActor::ProfileRequest()
{
   TSharedRef<IHttpRequest> Request = Http->CreateRequest();
   Request->OnProcessRequestComplete().BindUObject(this, &AHttpBackEndActor::OnProfileRequestResponseReceived);
   //This is the url on which to process the request
   Request->SetURL(adress + "api/users/" + SaveGameInstance->PlayerIndex);
   Request->SetVerb("GET");
   Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
   Request->SetHeader("Content-Type", TEXT("application/x-www-form-urlencoded"));
   Request->SetHeader(TEXT("Authorization"), "Bearer " + SaveGameInstance->PlayerSessionToken);
   Request->ProcessRequest();
}

void AHttpBackEndActor::OnProfileRequestResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
   if (Response.IsValid()) {
      MY_LOG_GAME(TEXT("Profile Request Succeed"));
      //Create a pointer to hold the json serialized data
      TSharedPtr<FJsonObject> JsonObject;
      //if (Response == nullptr) return;
      //Create a reader pointer to read the json data
      TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());

      //Deserialize the json data given Reader and the actual object to deserialize
      if (FJsonSerializer::Deserialize(Reader, JsonObject))
      {
         //Get the value of the json object by field name
         FString receivedId = JsonObject->GetStringField("id");
         FString receivedName = JsonObject->GetStringField("name");
         uint32 receivedWins = JsonObject->GetIntegerField("wins");
         uint32 receivedLoss = JsonObject->GetIntegerField("loss");
         SaveGameInstance->PlayerIndex = receivedId;
         SaveGameInstance->PlayerName = receivedName;
         SaveGameInstance->PlayerWins = receivedWins;
         SaveGameInstance->PlayerLoss = receivedLoss;
         UGameplayStatics::SaveGameToSlot(SaveGameInstance, SaveGameInstance->SaveSlotName, SaveGameInstance->UserIndex);
         bLoggedIn = true;
      }
   }
   else {
      //GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Red, "Erreur dans la recuperation du profil!");
      MY_LOG_GAME(TEXT("Profile Request Failed"));
      bLoggedIn = false;
   }
}

void AHttpBackEndActor::NicknameEditRequest(const FString playerName)
{
   TSharedRef<IHttpRequest> Request = Http->CreateRequest();
   Request->OnProcessRequestComplete().BindUObject(this, &AHttpBackEndActor::OnNicknameEditResponseReceived);
   //This is the url on which to process the request
   Request->SetURL(adress + "api/users/" + SaveGameInstance->PlayerIndex + "/name?value=" + playerName);
   Request->SetVerb("PUT");
   Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
   Request->SetHeader("Content-Type", TEXT("application/x-www-form-urlencoded"));
   Request->SetHeader(TEXT("Authorization"), "Bearer " + SaveGameInstance->PlayerSessionToken);
   Request->ProcessRequest();
}

void AHttpBackEndActor::OnNicknameEditResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
   if (Response.IsValid()) {
      // Ici il faudra broadcaster le nouveau pseudo et mettre  jour la sauvegarde locale !
      ProfileRequest();
   }
   else {
      // Ici il faudra afficher un message d'erreur et remettre l'ancien pseudo
   }
}

void AHttpBackEndActor::UpdateWinsRequest()
{
   TSharedRef<IHttpRequest> Request = Http->CreateRequest();
   Request->OnProcessRequestComplete().BindUObject(this, &AHttpBackEndActor::OnUpdateWinsRequestResponseReceived);
   //This is the url on which to process the request
   Request->SetURL(adress + "api/users/" + SaveGameInstance->PlayerIndex + "/wins");
   Request->SetVerb("PUT");
   Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
   Request->SetHeader("Content-Type", TEXT("application/x-www-form-urlencoded"));
   Request->SetHeader(TEXT("Authorization"), "Bearer " + SaveGameInstance->PlayerSessionToken);
   Request->ProcessRequest();
}

void AHttpBackEndActor::OnUpdateWinsRequestResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
   if (Response.IsValid()) {
      // Cool
      MY_LOG_GAME(TEXT("Wins Updated Succeed"));
   }
   else {
      // Pas cool
      MY_LOG_GAME(TEXT("Wins Updated Succeed"));
   }
}

void AHttpBackEndActor::UpdateLossRequest()
{
   TSharedRef<IHttpRequest> Request = Http->CreateRequest();
   Request->OnProcessRequestComplete().BindUObject(this, &AHttpBackEndActor::OnUpdateLossRequestResponseReceived);
   //This is the url on which to process the request
   Request->SetURL(adress + "api/users/" + SaveGameInstance->PlayerIndex + "/loss");
   Request->SetVerb("PUT");
   Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
   Request->SetHeader("Content-Type", TEXT("application/x-www-form-urlencoded"));
   Request->SetHeader(TEXT("Authorization"), "Bearer " + SaveGameInstance->PlayerSessionToken);
   Request->ProcessRequest();
}

void AHttpBackEndActor::OnUpdateLossRequestResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
   if (Response.IsValid()) {
      MY_LOG_GAME(TEXT("Loss Updated Succeed"));
      // Cool
   }
   else {
      // Pas cool
      MY_LOG_GAME(TEXT("Loss Updated Failed"));
   }
}
