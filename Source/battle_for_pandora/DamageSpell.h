//// Fill out your copyright notice in the Description page of Project Settings.
//
//#pragma once
//
//#include "CoreMinimal.h"
//#include "Spell.h"
//#include "DamageSpell.generated.h"
//
///**
// * 
// */
//UCLASS()
//class BATTLE_FOR_PANDORA_API ADamageSpell : public ASpell
//{
//	GENERATED_BODY()
//
//public :
//   ADamageSpell();
//
//   UPROPERTY(EditAnywhere)
//   float damage = 5000;
//
//   void OnActorOverlap(class AActor* OverlappedActor, class AActor* OtherActor);
//
//protected:
//   // Called when the game starts or when spawned
//   virtual void BeginPlay() override;
//
//public:
//   // Called every frame
//   virtual void Tick(float DeltaTime) override;
//	
//};
