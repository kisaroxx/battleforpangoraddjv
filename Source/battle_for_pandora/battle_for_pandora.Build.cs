// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class battle_for_pandora : ModuleRules
{
	public battle_for_pandora(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicLibraryPaths.AddRange(new string[] {
             "battle_for_pandora"
        });

        PublicDependencyModuleNames.AddRange(
            new string[] {
                "Core",
                "CoreUObject",
                "Engine",
                "InputCore",
                "HeadMountedDisplay",
                "OnlineSubsystem",
                "OnlineSubsystemUtils",
                "Sockets",
                "UMG",
                "Slate",
                "SlateCore",
                "AIModule",
                "GameplayTasks",
                "Http",
                "Json",
                "JsonUtilities"
            }
        );
		
		PublicDependencyModuleNames.AddRange(new string[] { "Landscape", "Foliage" });

        DynamicallyLoadedModuleNames.Add("OnlineSubsystemNull");
    }
}
