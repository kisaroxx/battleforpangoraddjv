// Fill out your copyright notice in the Description page of Project Settings.

#include "MoveCommand.h"
#include "SquadMemberController.h"
#include "SquadManager.h"
#include "SquadDescriptor.h"
#include "CommandReturn.h"
#include "Logger.h"
#include "Util.h"
#include "Message.h"

UMoveCommand::UMoveCommand() {
    lastTimeRefresh = UGameplayStatics::GetRealTimeSeconds(GetOuter()->GetWorld());
    SetRefreshTime(refreshTime);
}

void UMoveCommand::SetTargetPoint(FVector _targetPoint)
{
    targetPoint = _targetPoint;
}

FVector UMoveCommand::GetTargetPoint()
{
    return targetPoint;
}

void UMoveCommand::SetTryUseLieutenantNavMesh(bool use) {
    bTryUseLieutenantNavMesh = use;
}

bool UMoveCommand::GetTryUseLieutenantNavMesh() const
{
    return bTryUseLieutenantNavMesh;
}

void UMoveCommand::SetNotArrived() {
    arrived = false;
}

UCommandReturn* UMoveCommand::execute() {
    UCommandReturn* comRet = NewObject<UCommandReturn>(this, UCommandReturn::StaticClass());
    comRet->setReturn(EPathFollowingRequestResult::RequestSuccessful);

    if (UGameplayStatics::GetRealTimeSeconds(GetOuter()->GetWorld()) - lastTimeRefresh > refreshTime) {
        lastTimeRefresh = UGameplayStatics::GetRealTimeSeconds(GetOuter()->GetWorld());
        pathCalculated = false;
    }

    if (pathCalculated && character->GetPathFollowingComponent()->GetStatus() != EPathFollowingStatus::Type::Moving) {
        pathCalculated = false;
    }

    if (!pathCalculated) {
        RadiusType radius = bTryUseLieutenantNavMesh ? (character->IsLieutenant() ? RadiusType::LARGE : RadiusType::SMALL) : RadiusType::SMALL;
        FPathFollowingRequestResult result = character->MoveToSpiraled(FAIMoveRequest(targetPoint), &targetPoint, radius);
        //Path->GetPathPoints()[LastSegmentEndIndex].Location = FVector();
        if (result.Code == EPathFollowingRequestResult::Failed) {
            //DEBUG_SCREEN(-1, 5.0f, FColor::Red, "Attention une commande moveTo a echou�e !!!");
            UMessage* message = NewObject<UMessage>();
            message->SetParams(MessageType::FAILED, character);
            character->GetSquad()->GetSquadManager()->ReceiveMessage(message);
        } else {
            pathCalculated = true;
        }
        comRet->setReturn(result.Code);
    }


    // Si on est arriv�, on envoie un message
    if (!arrived) {
        float dist = FVector::Dist(Util::ProjectInXYPlane(character->GetPawn()->GetActorLocation()), Util::ProjectInXYPlane(targetPoint));
        if (dist < 250.0f) {
            UMessage* message = NewObject<UMessage>();
            message->SetParams(MessageType::COMPLETED, character);
            character->GetSquad()->GetSquadManager()->ReceiveMessage(message);
            arrived = true;
        }
    }

    return comRet;
}

void UMoveCommand::SetRefreshTime(float refreshTime_) {
    refreshTime = refreshTime_;
    refreshTime += (refreshTime / 100.0f) * ((rand() % 21) - 10); // On fait en sorte que tous le monde ne refresh pas en m�me temps :3 C'est uniforme, donc c'est parfait : th�orie des grands nombres, niark :)
}
