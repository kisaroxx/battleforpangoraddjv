// Fill out your copyright notice in the Description page of Project Settings.

#include "Spell.h"
#include "Public/TimerManager.h"
#include "Engine/World.h"
#include "Logger.h"

ASpell::ASpell(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
   bAlwaysRelevant = true;
   bReplicates = true;
   bReplicateMovement = true;
   SetReplicates(true);
}

void ASpell::BeginPlay()
{
   Super::BeginPlay();
}

void ASpell::Tick(float DeltaTime)
{
   Super::Tick(DeltaTime);
}

void ASpell::DestroyIn(float seconds) {
    // On detruit cet acteur apr�s un certain temps ! :)
    FTimerHandle fuzeTimerHandle;
    FTimerDelegate timerDelegate;
    timerDelegate.BindUFunction(this, FName("AutoDestroy"));
    GetWorld()->GetTimerManager().SetTimer(
        fuzeTimerHandle,
        timerDelegate,
        seconds,
        false);
}

void ASpell::AutoDestroy() {
	if (IsValid(this)) {
		Destroy();
	}
}
