#include "SquadManager.h"
#include "SquadDescriptor.h"
#include "BFP_GameState.h"
#include "ExecuteBTTaskNode.h"



ASquadManager::ASquadManager() {
    PrimaryActorTick.bCanEverTick = true;
    PrimaryActorTick.bStartWithTickEnabled = true;
}


ASquadManager::~ASquadManager()
{
}
// Called every frame
void ASquadManager::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    if (desc->GetManeuver() == nullptr) {
        DEBUG_SCREEN(-1, 5.0f, FColor::Cyan, "Pas de maneuvre courante dans la squad petit galopin !");
    }
    else {
       //if (GetWorld()->GetGameState<ABFP_GameState>()->bAllPlayersSpawned) {
          //desc->GetManeuver()->Execute();
       //}
    }

    //MY_LOG_IA(TEXT("UExecuteBTTaskNode NB_APPEL_EXECUTE : %u"), UExecuteBTTaskNode::nbAppelExecute);
}

void ASquadManager::SetManeuver(ManeuverType type) {
    MY_LOG_IA(TEXT("ASquadManager::SetManeuver, ManeuverType = %d"), static_cast<int>(type));
    TSubclassOf<AManeuver> nouvelleManeuvre = desc->GetManeuverClass(type);
    MY_LOG_IA(TEXT("ASquadManager::SetManeuver, nouvelleManeuvre = %u"), IsValid(nouvelleManeuvre.Get()));
    if (IsValid(nouvelleManeuvre) && IsValid(nouvelleManeuvre.Get())) {
        MY_LOG_IA(TEXT("ASquadManager::SetManeuver, on est rentre !"));
        FActorSpawnParameters params{};
        params.Owner = this;
        AManeuver* newManeuver = GetWorld()->SpawnActor<AManeuver>(nouvelleManeuvre.Get(), params);
        newManeuver->SetSquadDescriptor(desc);
        // Arreter l'ancien behavior tree
        TArray<AManeuverTargetPoint*> targetPoints = desc->GetManeuver()->GetTargetPoints();
        desc->GetManeuver()->GetBrainComponent()->StopLogic(FString("Changement de Maneuvre !")); // Verifier que �a marche !!!
        desc->GetManeuver()->UnPossess(); // Je pense que ca ca marche mieux ! :)
        // Et lancer le nouveau ! ==> ca se fait tout seul !
        desc->SetManeuver(newManeuver, type);
        // Transferer les targets points !!!
        desc->GetManeuver()->SetTargetPoints(targetPoints);
        MY_LOG_IA(TEXT("ASquadManager::SetManeuver, on est sorti !"));
    }
    else {
        MY_LOG_IA(TEXT("Attention une manoeuvre est nulle !!!"));
    }
}

void ASquadManager::SetSquadDescriptor(ASquadDescriptor* squadDescriptor) {
    desc = squadDescriptor;
}

void ASquadManager::ReceiveMessage(UMessage* message) {
    // On peut faire ne sorte ici de filtrer les messages que l'on veut transmettre ou pas !
    switch (message->GetType()) {
    case MessageType::RECALL:
        SetManeuver(ManeuverType::RECALL);
        break;
    case MessageType::STOP_RECALL:
        SetManeuver(ManeuverType::AGRESSIVE);
        break;
    case MessageType::ADD_TARGET_POINT:
        MY_LOG_IA(TEXT("%s RECOIT ADD_TARGET_POINT (tailleListe = %d)"), *desc->GetName(), desc->GetManeuver()->GetTargetPoints().Num());
        break;
    default:
        break;
    }
    desc->GetManeuver()->ReceiveMessage(message);
}
